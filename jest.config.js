/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
    preset: "ts-jest",
    testEnvironment: "jsdom",

    moduleNameMapper: {
        "@/(.*)$": `${__dirname}/src/$1`
    }
}
