## 关于

DNFCalcext 是为 [DNF](http://dnf.qq.com) 装备搭配及数据计算提供便利的网页端工具

本项目参考了[DNFCalclating(DNF 纸飞机计算器)](https://gitee.com/i_melon/DNFCalculating)

## 开发

安装[nodejs](https://nodejs.org/en/)

安装[Git](https://git-scm.com/download)

安装[pnpm](https://pnpm.io/installation)

    npm install -g pnpm

中国境内用户可以设置淘宝镜像加快依赖安装速度

    pnpm config set registry https://registry.npmmirror.org

下载项目到本地

    git clone https://gitee.com/apateat/dnf-calcext

安装依赖

    pnpm install

开发模式启动

    vite dev

打包发布

    vite build
