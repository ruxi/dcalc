/**
 * @format
 * @Author: Kritsu
 * @Date: 2021/11/17 16:07:22
 * @Last Modified by:   Kritsu
 * @Last Modified time: 2021/11/17 18:43:27
 */

module.exports = {
    printWidth: 200,
    tabWidth: 4,
    useTabs: false,
    semi: false, // 未尾逗号
    vueIndentScriptAndStyle: true,
    singleQuote: false, // 单引号
    quoteProps: "as-needed",
    bracketSpacing: true,
    trailingComma: "none", // 未尾分号
    jsxBracketSameLine: false,
    jsxSingleQuote: false,
    arrowParens: "avoid",
    insertPragma: false,
    requirePragma: false,
    proseWrap: "never",
    htmlWhitespaceSensitivity: "strict",
    endOfLine: "lf"
}
