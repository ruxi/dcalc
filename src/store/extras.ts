import { Equip } from "./../core/equips/equip"
import { Character } from "@/core/character"
import { createSkillColumn, Skill, SkillColumn, SkillContext, SkillResult } from "@/core/skill"
import { useCharacterStore } from "./character"
import character from "../core/alter/enchantress/index"

export function hasCharacter(name: string): boolean {
    const { character_list } = useCharacterStore()
    return !!character_list[name]
}

export function isCareStatus(status: string) {
    const { character } = useCharacterStore()
    if (character) {
        return character.care_status[status] ?? 0
    }
    return 0
}

export function getCharacterParams(key: string): any | undefined {
    const { character } = useCharacterStore()
    if (character) {
        return character[key]
    }
    return
}

export function isBuffer() {
    const { character } = useCharacterStore()
    return !!character?.is_buffer
}

export function isDamageBuffer() {
    const { character, damage_buffer } = useCharacterStore()
    return !!character?.is_buffer && damage_buffer
}

export function getSkillColumn(name: string): SkillColumn | undefined {
    const { character } = useCharacterStore()
    if (character) {
        const { skills }: Character = character
        for (let column of skills) {
            const { skill, enabled } = column
            if (skill.name == name && enabled) {
                return column
            }
        }
    }
}

export function getEquip(part: string): Equip | undefined {
    const { equip_columns } = useCharacterStore()
    return equip_columns[part]?.item
}

export function hasEquip(...names: string[]) {
    const { using_equips } = useCharacterStore()
    for (let name of names) {
        if (!using_equips.some(e => e.name == name)) {
            return false
        }
    }
    return names.length > 0
}

export function hasSiroco(name: string) {
    const { siroco_columns } = useCharacterStore()
    return siroco_columns.some(e => e?.name == name)
}
export function hasOzma(name: string, index: number) {
    const { ozma_columns } = useCharacterStore()
    return ozma_columns[index]?.name == name
}
