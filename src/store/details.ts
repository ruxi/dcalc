import { defineStore } from "pinia"

export const useDetailsStore = defineStore("details", {
    state() {
        return {
            part: "pants"
        }
    },

    actions: {
        setPart(part: string) {
            this.part = part
        }
    }
})
