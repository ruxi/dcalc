import { createPinia } from "pinia"

export * from "./character"
export * from "./extras"
export * from "./config"
export * from "./calculate"

export const pinia = createPinia()
