import { avatar_parts } from "./../core/avatar"
import { useDetailsStore } from "@/store/details"
import { equip_parts, findEquipsByPart } from "@/core/equips"
import { defineStore } from "pinia"
import { useCharacterStore } from "./character"
import { sirocos } from "@/core/sirocos"
import { ozmas } from "@/core/ozmas"
import { ModifierConfigs, modifier_parts } from "@/core/modifiers"
import user_config from "@/assets/data/defaults.json"

interface UserConfig {
    equips: ItemConfig[]
    avatars: ItemConfig[]
    sirocos: string[]
    ozmas: string[]
    modifiers: ModifierConfigs
}

export interface ItemConfig {
    name: string | undefined | null
    part: string
    data: Record<string, number | string>
}

export const useConfigStore = defineStore("config", {
    getters: {
        avatars() {
            const { avatar_columns } = useCharacterStore()
            const configs: ItemConfig[] = []
            for (let part in avatar_columns) {
                const column = avatar_columns[part]

                configs.push({
                    part,
                    name: column.item?.name,
                    data: Object.fromEntries(column.data.entries())
                })
            }
            return configs
        },
        equips() {
            const { equip_columns } = useCharacterStore()
            const configs: ItemConfig[] = []
            for (let part in equip_columns) {
                const column = equip_columns[part]

                configs.push({
                    part,
                    name: column.item?.name,
                    data: Object.fromEntries(column.data.entries())
                })
            }

            return configs
        },
        sirocos() {
            const { siroco_columns } = useCharacterStore()
            return siroco_columns.map(e => e?.name ?? "")
        },
        ozmas() {
            const { ozma_columns } = useCharacterStore()
            return ozma_columns.map(e => e?.name ?? "")
        },

        modifers(): ModifierConfigs {
            const { equip_columns } = useCharacterStore()
            const rs: ModifierConfigs = {}
            for (let i = 0; i < modifier_parts.length; i++) {
                const p = modifier_parts[i]
                const { data } = equip_columns[p]
                rs[p] = (data.get("modifier") as [string, string, number]) ?? ["none", "none", 0]
            }
            return rs
        },
        userConfig(): UserConfig {
            return {
                avatars: this.avatars,
                equips: this.equips,
                sirocos: this.sirocos,
                ozmas: this.ozmas,
                modifiers: this.modifers
            }
        }
    },
    actions: {
        load(alter?: string) {
            if (alter) {
                let config = user_config as unknown as UserConfig
                const localData = localStorage.getItem(`user-config-${alter}`)
                if (localData) {
                    config = Object.assign({}, config, JSON.parse(localData)) as UserConfig
                }
                const { avatars = [], equips = [], sirocos = [], ozmas = [], modifiers = {} } = config
                loadAvatars(avatars)
                loadEquips(equips)
                loadSiroco(sirocos)
                loadOzmas(ozmas)
                loadModifiers(modifiers)
            }
        },
        save(alter?: string) {
            if (alter) {
                localStorage.setItem(`user-config-${alter}`, JSON.stringify(this.userConfig))
            }
        }
    }
})

function loadEquips(configs: ItemConfig[]) {
    const characterStore = useCharacterStore()

    for (let part of equip_parts) {
        const partEquips = findEquipsByPart(part)
        const config = configs.find(e => e.part == part)
        let equip = partEquips[0]
        if (partEquips.length > 0 && config) {
            equip = partEquips.find(e => config.name == e.name) ?? partEquips[0]
            let data = config.data ?? {}
            for (let key in data) {
                const value = data[key]
                characterStore.setColumnData(part, key, value)
            }
        }
        characterStore.setEquip(part, equip)
    }
}

function loadAvatars(configs: ItemConfig[]) {
    const characterStore = useCharacterStore()

    for (let part of avatar_parts) {
        const partEquips = characterStore.avatar_list.filter(e => e.part == part)
        const config = configs.find(e => e.part == part)
        let avatar = partEquips[0]
        if (partEquips.length > 0 && config) {
            avatar = partEquips.find(e => config.name == e.name) ?? partEquips[0]
            let data = config.data ?? {}
            for (let key in data) {
                const value = data[key]
                characterStore.avatar_columns[part]?.data.set(key, value)
            }
        }
        characterStore.setAvatar(part, avatar)
    }
}

function loadSiroco(siroco_names: string[]) {
    const characterStore = useCharacterStore()
    const using_sirocos = sirocos.filter(e => siroco_names.includes(e.name))
    using_sirocos.forEach((siroco, index) => characterStore.setSiroco(index, siroco))
}

function loadOzmas(ozma_names: string[]) {
    const characterStore = useCharacterStore()
    const using_ozmas = ozma_names.map(name => ozmas.find(ozma => ozma.name == name))
    using_ozmas.forEach((ozma, index) => characterStore.setOzma(index, ozma))
}

function loadModifiers(modifiers: ModifierConfigs) {
    const characterStore = useCharacterStore()
    for (let part in modifiers) {
        characterStore.setColumnData(part, "modifier", modifiers[part])
    }
}
