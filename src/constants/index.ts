/**
 * @Author: Kritsu
 * @Date:   2021/11/16 17:44:08
 * @Last Modified by:   Kritsu
 * @Last Modified time: 2021/11/17 18:49:15
 */

export const CurrentAlterSymbol = Symbol("[App]CurrentAlter")

export const CurrentPartSymbol = Symbol("[App]CurrentPart")

// 角色等级上限
export const CHARACTER_LEVEL_MAX = 100

// 冒险团等级上限
export const ADVENTURER_LEVEL_MAX = 40
