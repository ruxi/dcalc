/**
 * @Author: Kritsu
 * @Date:   2021/11/09 12:35:36
 * @Last Modified by:   Kritsu
 * @Last Modified time: 2021/11/17 18:37:30
 */
import { createRouter, createWebHashHistory } from "vue-router"
import { isMobile } from "./utils/mobile"

const router = createRouter({
    history: createWebHashHistory("/"),
    routes: [
        {
            name: "index",
            path: "/",
            redirect: "/pc"
        },
        {
            name: "pc",
            path: "/pc",
            component: () => import("@/pages/pc/home")
        },
        {
            name: "pc-character",
            path: "/pc/character/:alter",
            component: () => import("@/pages/pc/character")
        },
        {
            name: "mobile",
            path: "/mobile",
            component: () => import("@/pages/pc/home")
        },
        {
            name: "mobile-character",
            path: "/mobile/character/:alter",
            component: () => import("@/pages/pc/character")
        }
    ]
})

router.beforeEach((t, f, next) => {
    const isPC = !isMobile()
    if (!isPC && t.path.startsWith("/pc")) {
        return next({ path: t.path.replace("/pc", "/mobile") })
    } else if (isPC && t.path.startsWith("mobile")) {
        return next({ path: t.path.replace("/mobile", "/pc") })
    }
    return next()
})

export default router
