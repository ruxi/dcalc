/**
 * @Author: Kritsu
 * @Date:   2021/11/09 10:40:34
 * @Last Modified by:   Kritsu
 * @Last Modified time: 2021/11/17 18:37:34
 */
/// <reference types="vite/client" />

declare module "*.vue" {
    import { DefineComponent } from "vue"
    // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
    const component: DefineComponent<{}, {}, any>
    export default component
}
