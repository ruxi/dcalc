export interface DungeonBuff {
    name: string
    ceoff: number
    base: number
}
// 系统奶
export const dungeon_buffs = [
    {
        name: "siroco",
        ceoff: 1.35,
        base: 7664
    },
    {
        name: "ozma",
        ceoff: 2.31,
        base: 4581
    }
] as DungeonBuff[]

export function use_dungeon_buff(name: string, power: number) {
    const buff = dungeon_buffs.find(e => e.name == name)
    if (buff) {
        const { ceoff, base } = buff
        return power.minus(950).multiply(ceoff).plus(base)
    }
    return power
}
