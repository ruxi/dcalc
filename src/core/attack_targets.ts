import { DefenseStatus } from "./status"

export interface AttackTarget extends DefenseStatus {
    name: string
}

export default {
    name: "green-sandbags(120)",
    magdefense: 443243,
    ice_resistance: 0,
    fire_resistance: 0,
    light_resistance: 0,
    dark_resistance: 0
} as AttackTarget
