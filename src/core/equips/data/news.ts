import { Equip, GrowthEquip } from "./../equip"
export default [
    {
        name: "new_epic_coat_01",
        level: 105,
        type: "cloth",
        part: "coat",
        growth_properties: [
            {
                effect(context, [start, end]) {
                    context.once("attack", self => {})
                }
            }
        ]
    } as GrowthEquip
] as Equip[]
