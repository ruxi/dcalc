/**
 * @Author: Kritsu
 * @Date:   2021/11/13 19:11:15
 * @Last Modified by:   Kritsu
 * @Last Modified time: 2021/11/17 18:35:47
 */
import armors from "./armor"
import accessories from "./accessory"
import specials from "./special"
import weapons from "./weapon"
import title from "./title"

export default [...armors, ...accessories, ...specials, ...weapons, ...title]
