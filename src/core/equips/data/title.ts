import { Equip } from "@/core/equips"

export default [
    {
        name: "title_2021_labour_supremacy",
        icon: "/icon/title/451.png",
        part: "title",
        rarity: "legendary",
        status: {
            quadra: 80
        },
        effect(context) {
            context.changeSkill({
                start: 1,
                end: 35,
                value: 1
            })
        }
    },
    {
        name: "title_2021_spring_supremacy",
        icon: "/icon/title/446.png",
        part: "title",
        status: {
            quadra: 100,
            triatk: 65
        },
        effect(context) {
            context.changeStatus({
                crirate: 15,
                elemental_boost: 22,
                speed: 4
            })
            context.changeDamage({
                triatk_per: 0.12,
                bonus_damage: 0.1
            })
        }
    },
    {
        name: "title_2022_spring_supremacy",
        icon: "/icon/title/484.png",
        part: "title",
        fame: 1076,
        status: {
            quadra: 120,
            triatk: 70
        },
        effect(context) {
            context.changeStatus({
                crirate: 15,
                elemental_boost: 25,
                speed: 4
            })
            context.changeDamage({
                triatk_per: 0.05,
                critical_damage: 0.05,
                final_damage: 0.05,
                intstr_per: 0.05,
                bonus_damage: 0.05
            })
        }
    }
] as Equip[]
