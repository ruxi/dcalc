/**
 * @Author: Kritsu
 * @Date:   2021/11/13 18:55:43
 * @Last Modified by:   Kritsu
 * @Last Modified time: 2021/11/17 18:35:54
 */
import { Equip } from "../equip"

/**
 * 上古尘封术式/幸运三角
 */
export default [
    {
        name: "accessory_epic_bracelet_01", //莱多：变幻的规律
        suit_name: "accessory_suit_01", //上古尘封术式
        type: "accessory",
        part: "bracelet",
        effect(context) {
            context.changeBuff({
                buff_intstr_per: 0.08,
                passive_vitspi_alter: 230,
                passive_int_alter: 250
            })

            context.changeDamage({
                bonus_damage: 0.12,
                skill_attack: 0.2
            })

            //todo 移速
            context.changeStatus({
                elemental_boost: 25
            })
        }
    },
    {
        name: "accessory_epic_necklace_01", //肯那兹：精神燎原之火
        suit_name: "accessory_suit_01", //上古尘封术式
        type: "accessory",
        part: "necklace",
        effect(context) {
            context.changeBuff({
                buff_phyatk_per: 0.06,
                passive_vitspi_alter: 277,
                passive_int_alter: 151
            })

            context.changeDamage({
                triatk_per: 0.2,
                bonus_damage: 0.12
            })

            //todo 移速
            context.changeStatus({ elemental_boost: 25 })
        }
    },
    {
        name: "accessory_epic_ring_01", //盖柏：完美的均衡
        suit_name: "accessory_suit_01", //上古尘封术式
        type: "accessory",
        part: "ring",
        modifier: ["intstr_per", 0.2],
        effect(context) {
            context.changeBuff({
                buff_intstr_per: 0.08,
                passive_vitspi_alter: 266,
                passive_int_alter: 86
            })

            context.changeDamage({
                bonus_damage: 0.12,
                intstr_per: 0.2 //todo 变换属性
            })

            //todo 移速
            context.changeStatus({ elemental_boost: 25 })
        }
    },

    {
        name: "accessory_epic_bracelet_02", //白象之庇护
        suit_name: "accessory_suit_02", //幸运三角
        type: "accessory",
        part: "bracelet",
        effect(context) {
            context.changeBuff({
                buff_intstr_per: 0.08,
                passive_vitspi_alter: 220,
                passive_int_alter: 250
            })

            context.changeDamage({
                additional_damage: 0.07,
                critical_damage: 0.07,
                bonus_damage: 0.07,
                intstr_per: 0.07,
                skill_attack: 0.07
            })

            //todo 移速
            context.changeStatus({})
        }
    },
    {
        name: "accessory_epic_necklace_02", //四叶草之初心
        suit_name: "accessory_suit_02", //幸运三角
        type: "accessory",
        part: "necklace",
        effect(context) {
            context.changeBuff({
                buff_phyatk_per: 0.06,
                passive_vitspi_alter: 264,
                passive_int_alter: 151
            })

            context.changeDamage({
                additional_damage: 0.07,
                critical_damage: 0.07,
                triatk_per: 0.07,
                intstr_per: 0.07,
                final_damage: 0.07
            })
        }
    },
    {
        name: "accessory_epic_ring_02", //红兔之祝福
        suit_name: "accessory_suit_02", //幸运三角
        type: "accessory",
        part: "ring",
        modifier: ["critical_damage", 0.07],
        effect(context) {
            context.changeBuff({
                buff_intstr_per: 0.08,
                passive_vitspi_alter: 266,
                passive_int_alter: 86
            })

            context.changeDamage({
                additional_damage: 0.07,
                triatk_per: 0.07,
                intstr_per: 0.07,
                final_damage: 0.07
            })
        }
    }
] as Equip[]
