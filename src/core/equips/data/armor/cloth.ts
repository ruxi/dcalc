import { MythicEquip } from "./../../equip"
import { values } from "lodash"
import { Equip } from "../../equip"

const mythics = [
    {
        name: "cloth_mythic_coat_01",
        suit_name: "cloth_suit_01",
        part: "coat",
        rarity: "mythic",

        mythic_properties: [
            {
                deal_data: [0.01, 0.01, 0.11],
                buff_data: [0.01, 0.01, 0.11],
                effect(context, [deal_value, buff_value]) {
                    context.changeDamage({
                        bonus_damage: deal_value
                    })
                    context.changeBuff({
                        buff_intstr_per: buff_value
                    })
                }
            },
            {
                deal_data: [5, 5, 45],
                buff_data: [10, 10, 90],
                effect(context, [deal_value, buff_value]) {
                    context.changeSkill({
                        start: 1,
                        end: deal_value,
                        value: 1
                    })
                    context.changeBuff({
                        awake_intstr_add: buff_value
                    })
                }
            },
            {
                deal_data: [1, 3, 12],
                buff_data: [20, 20, 200],
                effect(context, [deal_value, buff_value]) {
                    context.changeDamage({
                        intstr_per: deal_value
                    })
                    context.changeBuff({
                        passive_alter: buff_value
                    })
                }
            }
        ],
        effect(context) {
            context.changeDamage({
                final_damage: 0.14,
                triatk_per: 0.1,
                intstr_per: 0.05
            })
            context.changeSkill({
                start: 1,
                end: 45,
                value: 1
            })
            context.changeBuff({
                passive_alter: 80,
                passive_vitspi_alter: 100,
                buff_phyatk_per: 0.25
            })
        }
    }
] as MythicEquip[]

export default [
    {
        name: "cloth_epic_coat_01", //魔法师[???]的长袍
        suit_name: "cloth_suit_01", //遗忘魔法师的馈赠
        type: "cloth",
        part: "coat",
        rarity: "epic",
        effect(context) {
            context.changeBuff({
                buff_phyatk_per: 0.25,
                passive_vitspi_alter: 180,
                passive_int_alter: 80
            })

            context.changeDamage({
                intstr_per: 0.05,
                triatk_per: 0.1,
                final_damage: 0.12
            })
            context.changeSkill({
                start: 1,
                end: 45,
                value: 1
            })
        },
        dungeon_effect(context) {}
    },
    {
        name: "cloth_epic_pants_01", //魔法师[???]的护腿
        suit_name: "cloth_suit_01", //遗忘魔法师的馈赠
        part: "pants",
        type: "cloth",
        modifier: ["additional_damage", 0.12],
        effect(context) {
            context.changeBuff({
                buff_magatk_per: 0.25, // buff独立攻击力+25%
                passive_int_alter: 150,
                passive_vitspi_alter: 250
            })

            context.changeDamage({
                intstr_per: 0.05, // 力量、智力+5%
                triatk_per: 0.1 // 物理、魔法、独立攻击力+10%
            })
            context.changeSkill({
                start: 1,
                end: 45,
                value: 1
            })
        },
        dungeon_effect(context) {}
    },
    {
        name: "cloth_epic_shoulder_01", //魔法师[???]的披风
        suit_name: "cloth_suit_01", //遗忘魔法师的馈赠
        part: "shoulder",
        type: "cloth",
        effect(context) {
            context.changeBuff({
                buff_intstr_per: 0.04, // buff 力量、智力+4%
                passive_int_alter: 200,
                passive_vitspi_alter: 300
            })

            context.changeDamage({
                intstr_per: 0.05, // 力量、智力+5%
                triatk_per: 0.1, // 物理、魔法、独立攻击力+10%,
                skill_attack: 0.13 //技能攻击力+10%
            })
            //todo 暴击率
            context.changeStatus({})
        },
        dungeon_effect(context) {}
    },
    {
        name: "cloth_epic_belt_01", //魔法师[???]的腰带
        suit_name: "cloth_suit_01", //遗忘魔法师的馈赠
        part: "belt",
        type: "cloth",
        effect(context) {
            context.changeBuff({
                buff_indatk_per: 0.25, // buff 独立攻击力+25%
                passive_int_alter: 150,
                passive_vitspi_alter: 250
            })

            context.changeDamage({
                intstr_per: 0.05, // 力量、智力+5%
                triatk_per: 0.1, // 物理、魔法、独立攻击力+10%,
                additional_damage: 0.14 //技能攻击力+10%
            })
            //todo 暴击率
            context.changeStatus({})
        },
        dungeon_effect(context) {}
    },
    {
        name: "cloth_epic_shoes_01", //魔法师[???]的长靴
        suit_name: "cloth_suit_01", //遗忘魔法师的馈赠
        part: "shoes",
        type: "cloth",
        effect(context) {
            context.changeBuff({
                buff_intstr_per: 0.04, // buff 力量、智力+4%
                passive_int_alter: 200,
                passive_vitspi_alter: 300
            })

            context.changeDamage({
                intstr_per: 0.05, // 力量、智力+5%
                triatk_per: 0.1, // 物理、魔法、独立攻击力+10%,
                critical_damage: 0.16 //暴击伤害+16%
            })
            //todo 三速
            context.changeStatus({})
        },
        dungeon_effect(context) {}
    },
    {
        name: "cloth_epic_coat_02", //优雅旋律华尔兹
        suit_name: "cloth_suit_02", //天堂舞姬
        part: "coat",
        type: "cloth",
        effect(context) {
            context.changeBuff({
                buff_phyatk_per: 0.25,
                passive_int_alter: 100,
                passive_vitspi_alter: 200
            })

            context.changeDamage({
                additional_damage: 0.17,
                critical_damage: 0.14 //暴击伤害+14%
            })
            context.changeSkill({
                start: 1,
                end: 45,
                type: "cd_raduction",
                value: 0.15
            })
        }
    },
    {
        name: "cloth_epic_pants_02", //魅惑律动伦巴
        suit_name: "cloth_suit_02", //天堂舞姬
        part: "pants",
        type: "cloth",
        modifier: ["intstr_per", 0.14],
        effect(context) {
            context.changeBuff({
                buff_magatk_per: 0.25,
                passive_int_alter: 100,
                passive_vitspi_alter: 200
            })

            context.changeDamage({
                final_damage: 0.17
            })
            context.changeSkill({
                start: 35,
                end: 45,
                type: "cd_raduction",
                value: 0.15
            })
        }
    },
    {
        name: "cloth_epic_shoulder_02", //性感洒脱探戈
        suit_name: "cloth_suit_02", //天堂舞姬
        part: "shoulder",
        type: "cloth",
        effect(context) {
            context.changeBuff({
                buff_intstr_per: 0.04,
                passive_int_alter: 160,
                passive_vitspi_alter: 260
            })

            context.changeDamage({
                bonus_damage: 0.17,
                skill_attack: 0.14
            })
            context.changeSkill({
                start: 75,
                end: 85,
                type: "cd_raduction",
                value: 0.15
            })
        }
    },
    {
        name: "cloth_epic_shoes_02", //激烈欢动踢踏
        suit_name: "cloth_suit_02", //天堂舞姬
        part: "shoes",
        type: "cloth",
        effect(context) {
            context.changeBuff({
                buff_intstr_per: 0.04,
                passive_int_alter: 80,
                passive_vitspi_alter: 180
            })

            context.changeDamage({
                triatk_per: 0.17,
                bonus_damage: 0.14
            })
            context.changeSkill({
                start: 1,
                end: 30,
                type: "cd_raduction",
                value: 0.15
            })
        }
    },
    {
        name: "cloth_epic_belt_02", //热情舞动桑巴
        suit_name: "cloth_suit_02", //天堂舞姬
        part: "belt",
        type: "cloth",
        effect(context) {
            context.changeBuff({
                buff_indatk_per: 0.25,
                passive_int_alter: 200,
                passive_vitspi_alter: 100
            })

            context.changeDamage({
                intstr_per: 0.17,
                final_damage: 0.14
            })
            context.changeSkill({
                start: 60,
                end: 70,
                type: "cd_raduction",
                value: 0.15
            })
        }
    },
    ...mythics
] as Equip[]
