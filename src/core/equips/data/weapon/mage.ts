/**
 * @Author: Kritsu
 * @Date:   2021/11/13 18:55:43
 * @Last Modified by:   Kritsu
 * @Last Modified time: 2021/11/17 18:35:54
 */
import { Equip } from "@/core/equips/equip"

/**
 * 上古尘封术式/幸运三角
 */
export default [
    {
        name: "weapon_epic_broom_01", //世界树之精灵
        type: "broom",
        part: "weapon",
        modifier: ["awake", 0.14],
        status: {
            intelligence: 81,
            phyatk: 1229,
            magatk: 1352,
            indatk: 770
        },
        effect(context) {
            context.changeBuff({
                buff_intstr_per: 0.08,
                passive_int_alter: 152,
                buff_level_add: 5,
                buff_triatk_per: 0.12,
                awake_level_add: 1,
                awake_intstr_add: 26
            })

            context.changeDamage({
                final_damage: 0.26,
                skill_attack: 0.26,
                bonus_elemental_damage: 0.15
            })

            //todo 移速
            context.changeStatus({})
        },
        dungeon_effect(context) {}
    }
] as Equip[]
