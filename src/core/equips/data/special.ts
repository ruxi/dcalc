import { CalcData } from "@/core/calc"
/**
 * @Author: Kritsu
 * @Date:   2021/11/13 19:31:58
 * @Last Modified by:   Kritsu
 * @Last Modified time: 2021/11/17 18:03:26
 */
import { hasEquip } from "@/store"
import { Equip } from "../equip"

export default [
    {
        name: "special_epic_subequip_01", //军神的遗书
        suit_name: "special_suit_01", //军神的隐秘遗产
        type: "special",
        part: "subequip",
        modifier: ["additional_damage", 0.25],
        effect(context) {
            context.changeBuff({
                buff_magatk_per: 0.06,
                passive_vitspi_alter: 208,
                passive_int_alter: 124,
                awake_level_add: 1
            })

            context.changeDamage({})
        }
    },
    {
        name: "special_epic_magicstones_01", //军神的庇护宝石
        suit_name: "special_suit_01", //军神的隐秘遗产
        type: "special",
        part: "magicstones",
        effect(context) {
            context.changeBuff({
                buff_intstr_per: 0.08,
                buff_level_add: 1,
                passive_vitspi_alter: 177,
                passive_int_alter: 80,
                awake_level_add: 1
            })

            context.changeDamage({
                critical_damage: 0.21,
                skill_attack: 0.17
            })
        }
    },
    {
        name: "special_epic_earrings_01", //军神的古怪耳环
        suit_name: "special_suit_01", //军神的隐秘遗产
        type: "special",
        part: "earrings",
        effect(context) {
            context.changeBuff({
                buff_indatk_per: 0.06,
                passive_vitspi_alter: 176,
                passive_int_alter: 88
            })

            //穿戴军神的遗书时
            context.once(
                "use_equip",
                self => {
                    console.log("use_equip:secial_epic_subequip_01")
                    self.changeDamage({
                        intstr_per: 0.1
                    })
                },
                ["special_epic_subequip_01"]
            )

            //todo
            context.changeDamage({
                triatk_per: 0.15,
                final_damage: 0.17
            })
        }
    }
] as Equip[]
