import { Suit } from "@/core/item"

const armors = [
    {
        name: "cloth_suit_01", //遗忘的魔法师馈赠
        needCount: 2,
        effect(data) {
            data.changeBuff({
                buff_level_add: 2,
                awake_level_add: 1,
                awake_intstr_add: 135,
                passive_alter: 100
            })
            data.changeDamage({
                final_damage: 0.14,
                skill_attack: 0.14
            })
        }
    },
    {
        name: "cloth_suit_01", //遗忘的魔法师馈赠
        needCount: 3,
        effect(data) {
            data.changeBuff({
                buff_intstr_per: 0.24,
                awake_intstr_add: 153,
                passive_alter: 50
            })
            data.changeDamage({
                additional_damage: 0.22,
                critical_damage: 0.1
            })
        }
    },
    {
        name: "cloth_suit_01", //遗忘的魔法师馈赠
        needCount: 5,

        effect(data) {
            data.changeBuff({
                buff_intstr_per: 0.2,
                awake_intstr_per: 0.08,
                passive_alter: 85
            })
            data.changeDamage({
                skill_attack: 0.19
            })
            data.changeStatus({
                crirate: 0.05
            })
            data.changeSkill({
                start: 1,
                end: 100,
                value: 2,
                condition: {
                    ignores: [95]
                }
            })
        }
    },
    {
        name: "cloth_suit_02",
        needCount: 2,
        effect(data) {
            data.changeDamage({
                critical_damage: 0.17,
                final_damage: 0.1
            })

            data.changeBuff({
                passive_alter: 105,
                buff_level_add: 1,
                buff_intstr_per: 0.12,
                awake_intstr_add: 135
            })
        }
    },
    {
        name: "cloth_suit_02",
        needCount: 3,
        effect(data) {
            data.changeDamage({
                triatk_per: 0.11,
                skill_attack: 0.15
            })

            data.changeStatus({
                attack_speed: 0.1,
                movement_speed: 0.1,
                casting_speed: 0.15
            })

            data.changeBuff({
                passive_alter: 145,
                buff_level_add: 1,
                buff_intstr_per: 0.08,
                awake_intstr_add: 192,
                awake_intstr_per: 0.05
            })
        }
    },
    {
        name: "cloth_suit_02",
        needCount: 5,
        effect(data) {
            data.changeDamage({
                skill_attack: 0.4
            })

            data.changeStatus({
                crirate: 0.1
            })

            data.changeBuff({
                passive_alter: 100,
                buff_level_add: 1,
                buff_intstr_per: 0.35,
                awake_intstr_per: 0.05
            })

            if (data.is_buffer) {
                data.changeSkill({
                    start: 30,
                    end: 50,
                    value: 2
                })
            }
        }
    }
] as Suit[]

armors.forEach(e => (e.parts = ["coat", "shoulder", "pants", "belt", "shoes"]))

export default armors
