import { CalcListener } from "@/core/calc"
import { hasEquip } from "@/store"
import { Suit } from "@/core/item"

const specials = [
    {
        name: "special_suit_01", //军神的隐秘遗产
        needCount: 2,
        effect(context) {
            context.changeBuff({
                buff_level_add: 1,
                buff_intstr_per: 0.04,
                awake_level_add: 1,
                awake_intstr_add: 35,
                passive_alter: 205
            })
            context.changeDamage({
                triatk_per: 0.1,
                final_damage: 0.08
            })

            const fn: CalcListener = (self, arg) => {
                self.changeDamage({
                    critical_damage: 0.05
                })
            }

            context.once("use_equip", fn, ["special_epic_subequip_01", "special_epic_earrings_01"])

            context.once("use_equip", fn, ["special_epic_subequip_01", "special_mythic_earrings_01"])
        }
    },
    {
        name: "special_suit_01", //军神的隐秘遗产
        needCount: 3,
        effect(context) {
            context.changeBuff({
                buff_level_add: 1,
                awake_level_add: 2
            })
            context.changeDamage({
                skill_attack: 0.1,
                intstr_per: 0.1
            })
        }
    }
] as Suit[]

specials.forEach(e => (e.parts = ["subequip", "magicstones", "earrings"]))

export default specials
