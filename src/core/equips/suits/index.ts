import { Suit } from "./../../item"
import accessory from "./accessory"
import armors from "./armor"
import special from "./special"

export const suits = [...armors, ...special, ...accessory].map(e => {
    e.fame = e.fame ?? 265
    return e
})
