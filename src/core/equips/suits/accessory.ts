import { Suit } from "@/core/item"

const accessories = [
    {
        name: "accessory_suit_01", //上古尘封术式,
        needCount: 2,
        effect(context) {
            context.changeBuff({
                buff_intstr_per: 0.02,
                passive_alter: 60,
                awake_intstr_add: 45
            })

            context.changeDamage({
                intstr_per: 0.1,
                triatk_per: 0.14
            })
        }
    },
    {
        name: "accessory_suit_01", //上古尘封术式,
        needCount: 3,
        effect(context) {
            context.changeBuff({
                buff_level_add: 1,
                buff_intstr_per: 0.02,
                awake_level_add: 2,
                awake_intstr_per: 0.08
            })

            context.changeDamage({
                critical_damage: 0.2
            })
        }
    },
    {
        name: "accessory_suit_02",
        needCount: 2,
        effect(context) {
            context.changeStatus({
                elemental_boost: 77,
                triatk: 77,
                crirate: 0.07
            })
            context.changeBuff({
                passive_alter: 140,
                buff_intstr_per: 0.04,
                awake_intstr_add: 45
            })
        }
    },
    {
        name: "accessory_suit_02",
        needCount: 3,
        effect(context) {
            context.changeStatus({
                elemental_boost: 77,
                triatk: 77,
                crirate: 0.07
            })
            context.changeBuff({
                buff_level_add: 1,
                awake_level_add: 2,
                awake_intstr_add: 8
            })

            //随机
            context.changeDamage({
                skill_attack: 0.31
            })
        }
    }
] as Suit[]

accessories.forEach(e => (e.parts = ["bracelet", "necklace", "ring"]))

export default accessories
