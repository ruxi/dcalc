import { CalcData } from "./../calc"
/**
 * @Author: Kritsu
 * @Date:   2021/11/09 15:48:14
 * @Last Modified by:   Kritsu
 * @Last Modified time: 2021/11/17 18:48:05
 */
import { StatusOptions } from "@/core/status"
import { DealerEntry } from "../entries/dealer"
import { Item, ItemColumn, ItemRarity } from "../item"

/**
 * 防具种类 布甲|皮甲|轻甲|重甲|板甲
 */
export type ArmorType = "cloth" | "leather" | "light" | "heavy" | "plate"

/**
 * 防具部位
 */
export type ArmorPart = "coat" | "shoulder" | "belt" | "pants" | "shoes"

/**
 * 首饰种类
 */
export type AccessoryPart = "bracelet" | "necklace" | "ring" | "title"

/**
 * 特殊装备种类
 */
export type SpecialPart = "subequip" | "magicstones" | "earrings"

export type WeaponType = "broom"

export type EquipType = ArmorType | "accessory" | "special" | WeaponType

export const armor_parts = ["coat", "shoulder", "pants", "belt", "shoes"]

export const equip_parts = [...armor_parts, "bracelet", "necklace", "ring", "subequip", "earrings", "magicstones", "weapon", "title"]

export type EquipPart = typeof equip_parts[number]

export interface EquipProperties {
    status?: StatusOptions

    level?: number

    part: EquipPart | string

    rarity?: ItemRarity

    /**
     * 种类
     */
    type: EquipType
}

export type EquipOptionData = [number, number, number]

export interface EquipOption {
    buff_data: EquipOptionData

    deal_data: EquipOptionData
}

export interface Equip extends Item, EquipProperties {
    suit_name?: string

    modifier?: [keyof DealerEntry | "awake", number]
}

export interface MythicEquip extends Equip {
    mythic_properties: MythicProperty[]
}

export interface MythicProperty extends EquipOption {
    effect(context: CalcData, values: [number, number]): void
}

export interface GrowthEquip extends Equip {
    growth_properties: MythicProperty[]
}

export interface EquipColumn extends ItemColumn<EquipPart, Equip> {}

export interface EquipColumns {
    [key: EquipPart | string]: EquipColumn
}
