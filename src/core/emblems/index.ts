import { ItemRarity, Items } from "./../item"
import { Emblem, SocketColor } from "../item"
import { EmitsOptions } from "vue"
import { useCharacterStore } from "@/store"

export default [
    //红色四维
    ...createEmblems(
        "red",
        ["strength", "intelligence", "vitality", "spirit"],
        [
            {
                rarity: "legendary",
                value: 35
            },
            {
                rarity: "unique",
                value: 25
            },
            {
                rarity: "rare",
                value: 17
            }
        ]
    ),
    //红绿力智
    ...createDualEmblems(
        ["red", "green"],
        [
            ["strength", "phycri_rate"],
            ["intelligence", "magcri_rate"]
        ],
        [
            {
                rarity: "legendary",
                values: [20, 2.2]
            },
            {
                rarity: "unique",
                values: [15, 1.5]
            },
            {
                rarity: "rare",
                values: [10, 1.1]
            }
        ]
    ),
    //蓝色三攻
    ...createEmblems(
        "blue",
        ["phyatk", "magatk", "indatk"],
        [
            {
                rarity: "legendary",
                value: 20
            },
            {
                rarity: "unique",
                value: 15
            },
            {
                rarity: "rare",
                value: 10
            }
        ]
    ),
    ...createEmblems(
        "yellow",
        ["strength", "intelligence"],
        [
            {
                rarity: "unique",
                value: 15
            },
            {
                rarity: "rare",
                value: 10
            }
        ]
    ),
    // 白金徽章 动态生成
    () => {
        const { character } = useCharacterStore()
        if (character) {
            const { skills } = character
            return skills
                .map(e => e.skill)
                .filter(e => e.need_level <= 45)
                .map(e => {
                    return {
                        name: `[{$skills.${e.name}}]{skill} Lv+1`,
                        colors: ["platinum"],
                        rarity: "legendary",
                        fame: 232,
                        effect(data) {
                            data.record()
                            data.changeStatus({
                                quadra: 8
                            })
                            data.changeSkill({
                                condition: {
                                    names: [e.name]
                                },
                                value: 1
                            })
                            data.record()
                        }
                    } as Emblem
                })
        }
        return []
    }
] as Items<Emblem>

interface EmblemOption {
    rarity: ItemRarity
    value: number
}

function getEmblemFame(rarity: ItemRarity) {
    switch (rarity) {
        case "legendary":
            return 46
        case "unique":
            return 36
        case "rare":
            return 30
    }
    return 0
}

function createEmblems(color: SocketColor, keys: string[], options: EmblemOption[]) {
    const emblems: Emblem[] = []
    for (let key of keys) {
        for (let { rarity, value } of options) {
            let fame = getEmblemFame(rarity)

            const emblem: Emblem = {
                name: `emblem_${rarity}_${color}_${key}`,
                rarity,
                fame,
                colors: [color],
                part: "",
                effect(data) {
                    data.changeStatus({
                        [key]: value
                    })
                }
            }
            emblems.push(emblem)
        }
    }
    return emblems
}

interface DualEmblemOption {
    rarity: ItemRarity
    values: number[]
}

function createDualEmblems(colors: SocketColor[], keys: string[][], options: DualEmblemOption[]) {
    const emblems: Emblem[] = []
    for (let key of keys) {
        for (let { rarity, values } of options) {
            let fame = getEmblemFame(rarity)

            const emblem: Emblem = {
                name: `emblem_${rarity}_dual_${key.join("_")}`,
                rarity,
                fame,
                colors,
                part: "",
                effect(data) {
                    const status: Record<string, number> = {}
                    for (let i = 0; i < key.length; i++) {
                        status[key[i]] = values[i]
                    }
                    data.changeStatus(status)
                }
            }
            emblems.push(emblem)
        }
    }
    return emblems
}
