/**
 * @Author: Kritsu
 * @Date:   2021/11/09 17:24:42
 * @Last Modified by:   Kritsu
 * @Last Modified time: 2021/11/17 18:47:56
 */
import { CharacterOptions } from "../character"
import enchantress from "./enchantress"

export interface CharacterList {
    [key: string]: CharacterOptions
}

export const character_list = {
    enchantress
} as CharacterList
