/**
 * @Author: Kritsu
 * @Date:   2021/11/09 16:57:21
 * @Last Modified by:   Kritsu
 * @Last Modified time: 2021/11/17 18:48:01
 */
import { CharacterOptions } from "@/core/character"
import skills from "./skills"

const character = {
    job: "mage_female",
    alter: "enchantress",
    armor_type: "plate",
    dealer_type: "magind",
    adapt_status: "intelligence",
    care_status: {
        intelligence: 4,
        indatk: 0.5,
        magcri_rate: 0.5,
        ice_boost: 0.1,
        fire_boost: 0.1,
        light_boost: 0.1,
        dark_boost: 0.1,
        elemental_boost: 0.1
    },
    is_buffer: true,
    skills
} as CharacterOptions

export default character
