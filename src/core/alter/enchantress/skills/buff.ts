import { StatusOptions } from "@/core/status"
import { Skill, SkillType } from "@/core/skill"
import madd from "./madd"
import { mapRecord } from "@/utils"
const show_favoritism = false

const EMPTY_VALUES = [0, 0, 0, 0, 0]

const buff_intstr_data = [
    0, 131, 140, 149, 158, 167, 175, 184, 193, 202, 211, 220, 229, 238, 247, 256, 264, 273, 282, 291, 300, 309, 318, 327, 336, 345, 353, 362, 371, 380, 389, 398, 407, 416, 425, 434, 442, 451, 460,
    469, 478
]

const buff_atk_data = [0, 34, 35, 37, 38, 39, 41, 42, 43, 45, 46, 47, 49, 50, 51, 53, 54, 55, 57, 58, 60, 61, 62, 64, 65, 66, 68, 69, 70, 72, 73, 74, 76, 77, 78, 80, 81, 82, 84, 85, 87]

const ForbiddenCurse: Skill = {
    name: "forbidden_curse", //禁忌诅咒
    level_spacing: 3,
    level_master: 10,
    level_max: 40,
    need_level: 30,
    point: 30,
    cooldown: 10,
    type: SkillType.BUFF,
    effect({ adapt_number = 0, level, useSkill }, data) {
        const { buff_entry } = data
        const { buff_level_add, buff_indatk_per, buff_magatk_per, buff_phyatk_per, buff_attack_add, buff_intstr_add, buff_int_per, buff_str_per } = buff_entry
        level = level.plus(buff_level_add)

        const attack = buff_atk_data[level].plus(buff_attack_add)
        const intstr = buff_intstr_data[level].plus(buff_intstr_add)

        let mp = adapt_number.divide(665).plus(1)
        const favoritism = useSkill(Favoritism.name)

        if (!show_favoritism && favoritism) {
            mp = mp.multiply(1.15)
        }

        const indatk = attack.multiply(buff_indatk_per.plus(1)).multiply(mp).round(2)
        const phyatk = attack.multiply(buff_phyatk_per.plus(1)).multiply(mp).round(2)
        const magatk = attack.multiply(buff_magatk_per.plus(1)).multiply(mp).round(2)
        const strength = intstr.multiply(buff_str_per.plus(1)).multiply(mp).round(2)
        const intelligence = intstr.multiply(buff_int_per.plus(1)).multiply(mp).round(2)

        //增加转职技能攻击力
        data.changeSkill({
            start: 15,
            end: -1,
            type: "attack_add",
            value: 0.7
        })

        return {
            level,
            adapt_number,
            value: {
                intelligence,
                strength,
                phyatk,
                magatk,
                indatk
            }
        }
    }
}

const madd_skill_names = madd.map(e => `mage_female.enchantress.${e.name}`)

const Favoritism: Skill = {
    name: "favoritism", //小魔女的偏爱
    level_spacing: 3,
    level_master: 20,
    level_max: 40,
    need_level: 20,
    point: 10,
    cooldown: 10,
    type: SkillType.BUFF,
    effect({ useSkill }, data) {
        let buffer_status: StatusOptions = {}

        const mp = 0.15

        if (show_favoritism) {
            const buff = useSkill(ForbiddenCurse.name)
            if (buff) {
                buffer_status = buff().value ?? {}

                buffer_status = mapRecord(buffer_status, value => {
                    if (value) {
                        return value.multiply(mp).round(3)
                    }
                })
            }
        }
        //["疯狂乱抓", "疯熊火箭拳", "疯熊守护", "疯疯熊坠击", "变大吧！疯疯熊", "哇咔咔！", "咆哮吧！疯疯熊"]
        data.changeSkill({
            condition: {
                names: madd_skill_names
            },
            type: "attack_add",
            value: 0.1
        })

        return {
            value: buffer_status,
            data: [mp]
        }
    }
}

const DESTINYPUPPET_ATTACK_DATA = [
    0, 13968, 16195, 18422, 20649, 22876, 25103, 27329, 29556, 31784, 34011, 36238, 38465, 40691, 42918, 45145, 47372, 49599, 51825, 54052, 56280, 58507, 60734, 62961, 65187, 67414, 69641, 71868,
    74095, 76321, 78548, 80776, 83003, 85230, 87457, 89683, 91910, 94137, 96364, 98591, 100817, 103044, 105272, 107499, 109726, 111953, 114179, 116406, 118633, 120860, 123087, 125313, 127540, 129768,
    131995, 134222, 136449, 138675, 140902, 143129, 145356, 147583, 149809, 152036, 154264, 156491, 158718, 160945, 163171, 165398, 167625
]

//死命召唤
const DestinyPuppet: Skill = {
    name: "destiny_puppet",
    level_spacing: 3,
    level_master: 50,
    level_max: 60,
    need_level: 35,
    cooldown: 7,
    point: 40,
    type: SkillType.BUFF,
    effect({ useSkill, level }) {
        const buff = useSkill(ForbiddenCurse.name)

        const mp = 0.25

        let buffer_status: StatusOptions = {}

        if (buff) {
            buffer_status = buff().value ?? {}

            const favoritism = useSkill(Favoritism.name)
            if (favoritism) {
                const temps = favoritism().value ?? {}
                buffer_status = mapRecord(buffer_status, (value, key) => {
                    if (value) {
                        const temp = temps[key]
                        if (temp) {
                            value = value.plus(temp)
                        }
                        return value
                    }
                })
            }
        }

        buffer_status = mapRecord(buffer_status, value => {
            if (value) {
                return value.multiply(mp).round(3)
            }
        })

        return {
            value: buffer_status,
            data: [mp],
            frames: [
                {
                    attack: DESTINYPUPPET_ATTACK_DATA[level],
                    hits: 1
                }
            ]
        }
    }
}

const HOTAFFECTION_ATTACK_DATA = [
    0, 1193, 1383, 1573, 1763, 1953, 2145, 2335, 2525, 2715, 2905, 3095, 3285, 3476, 3666, 3856, 4046, 4236, 4426, 4618, 4808, 4998, 5188, 5378, 5568, 5758, 5948, 6139, 6329, 6519, 6709, 6900, 7090,
    7281, 7471, 7661, 7851, 8041, 8231, 8421, 8611
]

//火热的爱意
const HotAffection: Skill = {
    name: "hot_affection",
    need_level: 25,
    level_spacing: 3,
    level_master: 30,
    level_max: 40,
    cooldown: 0,
    point: 15,
    type: SkillType.BUFF,
    effect({ level }, data) {
        data.changeStatus({
            speed: level == 0 ? 0 : level.multiply(0.5).plus(13)
        })
        return {
            frames: [
                {
                    attack: HOTAFFECTION_ATTACK_DATA[level],
                    hits: 3
                }
            ]
        }
    }
}

export default [ForbiddenCurse, HotAffection, Favoritism, DestinyPuppet]
