import { Skill, SkillType } from "@/core/skill"

//别过来
const KEYAWAY_ATTACK_DATA = [
    0, 2978, 3279, 3582, 3884, 4186, 4488, 4791, 5092, 5395, 5697, 5999, 6301, 6604, 6906, 7208, 7510, 7813, 8114, 8417, 8719, 9021, 9323, 9626, 9927, 10230, 10532, 10834, 11136, 11439, 11740, 12043,
    12345, 12647, 12949, 13252, 13553, 13856, 14158, 14459, 14762, 15065, 15366, 15668, 15971, 16272, 16575, 16877, 17179, 17481, 17784, 18085, 18388, 18690, 18992, 19294, 19597, 19898, 20201, 20503,
    20805, 21107, 21410, 21711, 22014, 22316, 22618, 22920, 23223, 23524, 23827
]
const KeepAway: Skill = {
    name: "keep_away",
    level_spacing: 2,
    level_master: 50,
    level_max: 60,
    need_level: 5,
    cooldown: 6,
    point: 15,
    type: SkillType.ACTIVE,
    effect({ level }) {
        return {
            frames: [
                {
                    attack: KEYAWAY_ATTACK_DATA[level],
                    hits: 3
                }
            ]
        }
    }
}

const HEX_ATTACK_DATA_0 = [
    0, 1570, 1729, 1889, 2048, 2206, 2366, 2526, 2685, 2844, 3003, 3164, 3323, 3481, 3641, 3800, 3959, 4119, 4279, 4438, 4596, 4755, 4915, 5075, 5234, 5394, 5553, 5711, 5871, 6031, 6190, 6349, 6508,
    6668, 6828, 6986, 7145, 7305, 7464, 7624, 7784
]

const HEX_ATTACK_DATA_1 = [
    0, 6280, 6918, 7555, 8191, 8830, 9468, 10105, 10741, 11379, 12016, 12654, 13290, 13928, 14565, 15201, 15839, 16478, 17115, 17751, 18389, 19026, 19663, 20300, 20938, 21575, 22211, 22849, 23486,
    24123, 24761, 25399, 26036, 26673, 27310, 27948, 28585, 29221, 29859, 30496, 31133
]

const Hex: Skill = {
    name: "hex",
    level_spacing: 2,
    level_master: 30,
    level_max: 40,
    need_level: 60,
    cooldown: 45,
    point: 15,
    type: SkillType.ACTIVE,
    effect({ level }) {
        return {
            frames: [
                {
                    attack: HEX_ATTACK_DATA_0[level],
                    hits: 4
                },
                {
                    attack: HEX_ATTACK_DATA_1[level],
                    hits: 1
                }
            ]
        }
    }
}

export default [KeepAway, Hex]
