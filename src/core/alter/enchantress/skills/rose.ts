import { Skill, SkillType } from "@/core/skill"

const ROSEVINES_ATTACK_DATA_0 = [
    0, 650, 715, 781, 848, 913, 980, 1046, 1111, 1177, 1243, 1310, 1375, 1441, 1508, 1573, 1640, 1706, 1770, 1837, 1903, 1970, 2035, 2101, 2168, 2233, 2300, 2366, 2430, 2497, 2563, 2629, 2695, 2761,
    2827, 2893, 2960, 3024, 3090, 3157, 3222, 3289, 3355, 3420, 3487, 3553, 3620, 3684, 3750, 3817, 3882, 3949, 4015, 4080, 4147, 4213, 4279, 4344, 4410, 4476, 4542
]

const ROSEVINES_ATTACK_DATA_1 = [
    0, 85, 93, 102, 110, 119, 128, 136, 145, 153, 162, 170, 179, 188, 196, 205, 213, 222, 230, 238, 247, 255, 264, 272, 281, 289, 298, 307, 315, 324, 332, 341, 349, 358, 367, 375, 384, 393, 402, 411,
    419, 428, 436, 445, 453, 462, 471, 479, 488, 496, 505, 513, 522, 531, 539, 548, 556, 565, 574, 582, 591
]

/**
 * 玫瑰藤蔓
 */
export const RoseVines: Skill = {
    name: "rose_vines",
    level_spacing: 2,
    level_master: 50,
    level_max: 60,
    need_level: 10,
    cooldown: 6,
    type: SkillType.ACTIVE,
    point: 15,
    effect({ level }) {
        return {
            frames: [
                {
                    attack: ROSEVINES_ATTACK_DATA_0[level],
                    hits: 4
                },
                {
                    attack: ROSEVINES_ATTACK_DATA_1[level],
                    hits: 4
                }
            ]
        }
    }
}

const HARVEST_ATTACK_DATA = [
    0, 2414, 2798, 3182, 3567, 3951, 4337, 4722, 5107, 5491, 5876, 6261, 6645, 7030, 7415, 7801, 8185, 8570, 8955, 9339, 9724, 10109, 10494, 10878, 11264, 11649, 12034, 12418, 12803, 13188, 13572,
    13957, 14342, 14728, 15112, 15497, 15882, 16266, 16651, 17036, 17421, 17805, 18191, 18576, 18960, 19345, 19730, 20115, 20499, 20883, 21268, 21652, 22038, 22423, 22808, 23192, 23577, 23962, 24346,
    24731, 25116
]

/**
 * 蔷薇藤鞭
 */
const Harvest: Skill = {
    name: "harvest",
    need_level: 20,
    level_spacing: 2,
    level_master: 50,
    level_max: 60,
    cooldown: 10,
    type: SkillType.ACTIVE,
    effect({ level }) {
        return {
            frames: [
                {
                    attack: HARVEST_ATTACK_DATA[level],
                    hits: 2
                }
            ]
        }
    }
}

const BRAMBLEPRISON_ATTACK_DATA_0 = [
    0, 843, 928, 1015, 1100, 1186, 1271, 1357, 1442, 1527, 1614, 1699, 1785, 1870, 1955, 2042, 2127, 2213, 2298, 2384, 2469, 2555, 2641, 2726, 2812, 2897, 2984, 3069, 3154, 3240, 3325, 3411, 3497,
    3583, 3668, 3753, 3839, 3924, 4011, 4096, 4182, 4267, 4352, 4439, 4524, 4610, 4695, 4781, 4866, 4951, 5038, 5123, 5209, 5294, 5379, 5466, 5551, 5637, 5722, 5808, 5893
]

const BRAMBLEPRISON_ATTACK_DATA_1 = [
    0, 6328, 6969, 7611, 8254, 8896, 9538, 10179, 10821, 11464, 12106, 12748, 13389, 14031, 14673, 15316, 15958, 16600, 17241, 17883, 18526, 19168, 19810, 20451, 21093, 21735, 22378, 23020, 23661,
    24303, 24945, 25588, 26230, 26871, 27513, 28155, 28797, 29440, 30082, 30723, 31365, 32007, 32650, 33292, 33933, 34575, 35217, 35860, 36502, 37143, 37785, 38427, 39069, 39712, 40354, 40995, 41637,
    42279, 42922, 43564, 44205
]

const BRAMBLEPRISON_ATTACK_DATA_2 = [
    0, 114, 126, 138, 149, 161, 173, 184, 196, 208, 219, 231, 243, 254, 266, 278, 289, 301, 313, 324, 337, 348, 359, 372, 383, 394, 407, 418, 429, 442, 453, 464, 477, 488, 499, 512, 523, 534, 547,
    558, 569, 582, 593, 604, 617, 628, 639, 652, 663, 674, 687, 698, 709, 722, 733, 744, 757, 768, 779, 792, 803
]

/**
 * 蔷薇囚狱
 */
const BramblePrison: Skill = {
    name: "bramble_prison",
    need_level: 40,
    level_spacing: 2,
    level_master: 50,
    level_max: 60,
    cooldown: 20,
    type: SkillType.ACTIVE,
    effect({ level }) {
        return {
            frames: [
                {
                    attack: BRAMBLEPRISON_ATTACK_DATA_0[level],
                    hits: 6
                },
                {
                    attack: BRAMBLEPRISON_ATTACK_DATA_1[level],
                    hits: 1
                },
                {
                    attack: BRAMBLEPRISON_ATTACK_DATA_2[level],
                    hits: 7
                }
            ]
        }
    }
}
const BLACKFORESTBRIER_ATTACK_DATA = [
    0, 1493, 1645, 1796, 1948, 2100, 2251, 2403, 2554, 2706, 2857, 3009, 3160, 3312, 3464, 3615, 3767, 3918, 4070, 4221, 4373, 4524, 4676, 4828, 4979, 5131, 5282, 5434, 5585, 5737, 5888, 6040, 6192,
    6343, 6495, 6646, 6798, 6949, 7101, 7252, 7404, 7556, 7707, 7859, 8010, 8162, 8313, 8465, 8616, 8768, 8920, 9071, 9223, 9374, 9526, 9677, 9829, 9980, 10132, 10284, 10435, 10587, 10738, 10890,
    11041, 11193, 11344, 11496, 11648, 11799, 11951
]

/**
 * 林中小屋
 */
const BlackForestBrier: Skill = {
    name: "black_forest_brier",
    need_level: 45,
    level_spacing: 2,
    level_master: 1,
    level_max: 11,
    cooldown: 20,
    type: SkillType.ACTIVE,
    effect({ level }) {
        return {
            frames: [
                {
                    attack: BLACKFORESTBRIER_ATTACK_DATA[level],
                    hits: 4
                }
            ]
        }
    }
}

const GARDENOFPAIN_ATTACK_DATA_0 = [
    0, 3140, 3459, 3778, 4096, 4414, 4733, 5053, 5371, 5690, 6009, 6326, 6645, 6964, 7283, 7601, 7920, 8238, 8558, 8876, 9195, 9514, 9833, 10150, 10469, 10788, 11106, 11425, 11744, 12061, 12381,
    12700, 13019, 13338, 13656
]

const GARDENOFPAIN_ATTACK_DATA_1 = [
    0, 698, 769, 839, 909, 980, 1051, 1123, 1194, 1265, 1334, 1405, 1476, 1548, 1618, 1689, 1760, 1830, 1901, 1971, 2043, 2114, 2185, 2255, 2325, 2396, 2468, 2539, 2610, 2680, 2750, 2821, 2893, 2964,
    3034
]

const GARDENOFPAIN_ATTACK_DATA_2 = [
    0, 28268, 31136, 34004, 36873, 39739, 42606, 45475, 48343, 51210, 54078, 56946, 59814, 62683, 65549, 68418, 71285, 74154, 77020, 79889, 82756, 85625, 88493, 91360, 94228, 97096, 99964, 102833,
    105699, 108566, 111435, 114303, 117170, 120038, 122906
]

/**
 *  苦痛庭院
 */
const GardenOfPain: Skill = {
    name: "garden_of_pain",
    need_level: 75,
    level_spacing: 2,
    level_master: 30,
    level_max: 40,
    cooldown: 40,
    type: SkillType.ACTIVE,
    effect({ level }) {
        return {
            frames: [
                {
                    attack: GARDENOFPAIN_ATTACK_DATA_0[level],
                    hits: 9
                },
                {
                    attack: GARDENOFPAIN_ATTACK_DATA_1[level],
                    hits: 10
                },
                {
                    attack: GARDENOFPAIN_ATTACK_DATA_2[level],
                    hits: 1
                }
            ]
        }
    }
}

//藤蔓系
export default [RoseVines, Harvest, BramblePrison, BlackForestBrier, GardenOfPain]
