import { Skill, SkillType } from "@/core/skill"

const MADDSLASH_ATTACK_DATA = [
    966, 1065, 1161, 1260, 1359, 1457, 1554, 1652, 1751, 1850, 1946, 2045, 2144, 2241, 2340, 2436, 2535, 2634, 2732, 2829, 2927, 3026, 3124, 3221, 3320, 3418, 3516, 3615, 3711, 3810, 3909, 4006, 4104,
    4202, 4301, 4399, 4497, 4595, 4693, 4791, 4890, 4986, 5085, 5184, 5281, 5379, 5477, 5575, 5674, 5772, 5869, 5968, 6066, 6165, 6261, 6360, 6459, 6556, 6655, 6751, 6850, 6949, 7047, 7144, 7243, 7341
]

//疯狂乱抓
const MaddSlash: Skill = {
    name: "madd_slash",
    need_level: 15,
    level_spacing: 2,
    level_master: 50,
    level_max: 60,
    type: SkillType.ACTIVE,
    cooldown: 4,
    point: 20,
    effect({ level }) {
        return {
            frames: [
                {
                    attack: MADDSLASH_ATTACK_DATA[level],
                    hits: 2
                }
            ]
        }
    }
}

const MADDSTRAIGHT_ATTACK_DATA = [
    3223, 3551, 3879, 4204, 4532, 4859, 5187, 5512, 5840, 6168, 6495, 6821, 7148, 7476, 7804, 8130, 8457, 8784, 9112, 9438, 9765, 10093, 10420, 10747, 11073, 11401, 11729, 12055, 12382, 12709, 13037,
    13363, 13690, 14018, 14345, 14672, 14998, 15326, 15654, 15980, 16307, 16634, 16962, 17289, 17616, 17943, 18270, 18597, 18925, 19251, 19579, 19905, 20233, 20560, 20887, 21214, 21541, 21868, 22195,
    22522, 22850, 23176, 23504, 23830, 24158, 24485, 24812, 25139, 25466, 25793
]
//疯熊火箭拳
const MaddStraight: Skill = {
    name: "madd_straight",
    need_level: 20,
    level_spacing: 2,
    level_master: 50,
    level_max: 60,
    cooldown: 7,
    point: 20,
    type: SkillType.ACTIVE,
    effect({ level }) {
        return {
            frames: [
                {
                    attack: MADDSTRAIGHT_ATTACK_DATA[level],
                    hits: 1
                }
            ]
        }
    }
}

const MADDGUARD_ATTACK_DATA = [
    0, 5362, 5907, 6451, 6996, 7539, 8083, 8628, 9172, 9715, 10259, 10804, 11349, 11892, 12436, 12980, 13526, 14069, 14613, 15157, 15700, 16245, 16790, 17334, 17877, 18421, 18966, 19511, 20054, 20598,
    21142, 21687, 22230, 22774, 23319, 23863, 24407, 24951, 25495, 26040, 26584, 27128, 27672, 28216, 28759, 29305, 29849, 30393, 30936, 31480, 32026, 32570, 33113, 33657, 34201, 34747, 35289, 35834,
    36378, 36921, 37466
]

const MaddGuard: Skill = {
    name: "madd_guard",
    need_level: 30,
    level_spacing: 2,
    level_master: 50,
    level_max: 60,
    cooldown: 10,
    point: 20,
    type: SkillType.ACTIVE,
    effect({ level }) {
        return {
            frames: [
                {
                    attack: MADDGUARD_ATTACK_DATA[level],
                    hits: 5
                }
            ]
        }
    }
}

const JUMPINGBEARPRESS_ATTACK_DATA = [
    0, 8399, 9250, 10103, 10955, 11807, 12658, 13511, 14363, 15214, 16068, 16919, 17773, 18624, 19475, 20329, 21180, 22032, 22884, 23736, 24588, 25440, 26293, 27144, 27998, 28849, 29700, 30553, 31405,
    32258, 33109, 33961, 34814, 35665, 36518, 37370, 38222, 39074, 39926, 40778, 41630, 42483, 43334, 44186, 45039, 45890, 46744, 47595, 48446, 49300, 50151, 51003, 51856, 52708, 53559, 54412, 55264,
    56115, 56969, 57820, 58671, 59525, 60376, 61228, 62081, 62933, 63785, 64637, 65489, 66341, 67194
]

const JumpingBearPress: Skill = {
    name: "jumping_bear_press",
    need_level: 35,
    level_spacing: 2,
    level_master: 50,
    level_max: 60,
    cooldown: 15,
    point: 40,
    type: SkillType.ACTIVE,
    effect({ level }) {
        return {
            frames: [
                {
                    attack: JUMPINGBEARPRESS_ATTACK_DATA[level],
                    hits: 5
                }
            ]
        }
    }
}

const BIGMADD_ATTACK_DATA_0 = [
    0, 2615, 2880, 3146, 3412, 3677, 3943, 4207, 4473, 4738, 5004, 5269, 5535, 5799, 6065, 6330, 6596, 6861, 7127, 7391, 7657, 7923, 8188, 8454, 8719, 8984, 9249, 9515, 9780, 10046, 10311, 10576,
    10841, 11107, 11373, 11638, 11904, 12168, 12434, 12699, 12965, 13230, 13496, 13760, 14026, 14291, 14557, 14823, 15088, 15354, 15618, 15884, 16149, 16415, 16680, 16946, 17210, 17476, 17741, 18007,
    18272, 18538, 18802, 19068, 19334, 19599, 19865, 20130, 20395, 20660, 20926
]

const BIGMADD_ATTACK_DATA_1 = [
    0, 10463, 11524, 12585, 13647, 14708, 15771, 16832, 17893, 18955, 20016, 21077, 22139, 23200, 24262, 25324, 26385, 27446, 28508, 29569, 30630, 31693, 32754, 33815, 34877, 35938, 36999, 38061,
    39123, 40185, 41246, 42307, 43369, 44430, 45491, 46554, 47615, 48676, 49738, 50799, 51860, 52922, 53984, 55045, 56107, 57168, 58229, 59291, 60352, 61415, 62476, 63537, 64599, 65660, 66721, 67783,
    68844, 69906, 70968, 72029, 73090, 74152, 75213, 76274, 77337, 78398, 79459, 80521, 81582, 82643, 83705
]

const BigMadd: Skill = {
    name: "big_madd",
    need_level: 45,
    level_spacing: 2,
    level_master: 50,
    level_max: 60,
    cooldown: 45,
    point: 60,
    type: SkillType.ACTIVE,
    effect({ level }) {
        return {
            frames: [
                {
                    attack: BIGMADD_ATTACK_DATA_0[level],
                    hits: 6
                },
                {
                    attack: BIGMADD_ATTACK_DATA_1[level],
                    hits: 1
                }
            ]
        }
    }
}

const TERRIBLEROAR_ATTACK_DATA = [
    0, 3465, 3817, 4168, 4519, 4871, 5223, 5574, 5926, 6278, 6629, 6981, 7333, 7684, 8035, 8387, 8738, 9090, 9443, 9794, 10146, 10497, 10847, 11199, 11550, 11903, 12255, 12606, 12958, 13309, 13661,
    14011, 14364, 14716, 15067, 15419, 15770, 16122, 16473, 16826, 17177
]

const TerribleRoar: Skill = {
    name: "terrible_roar",
    need_level: 70,
    level_spacing: 2,
    level_master: 30,
    level_max: 40,
    cooldown: 40,
    point: 60,
    type: SkillType.ACTIVE,
    effect({ level }) {
        return {
            frames: [
                {
                    attack: TERRIBLEROAR_ATTACK_DATA[level],
                    hits: 10
                }
            ]
        }
    }
}

const CURSEOFTERRORBEAR_ATTACK_DATA = [
    0, 2618, 2883, 3149, 3414, 3680, 3945, 4211, 4476, 4743, 5008, 5274, 5539, 5805, 6070, 6336, 6601, 6868, 7133, 7399, 7664, 7930, 8195, 8461, 8726, 8991, 9258, 9523, 9789, 10054, 10320, 10585,
    10851, 11116, 11383, 11648, 11914, 12179, 12445, 12710, 12976, 13241, 13508, 13773, 14039, 14304, 14570, 14835, 15101, 15366, 15631, 15898, 16163, 16429, 16694, 16960, 17225, 17491, 17756, 18023,
    18288, 18554, 18819, 19085, 19350, 19616, 19881, 20148, 20413, 20679, 20944
]

const CurseOfTerrorBear: Skill = {
    name: "curse_of_terror_bear",
    need_level: 80,
    level_spacing: 2,
    level_master: 30,
    level_max: 40,
    cooldown: 45,
    point: 60,
    type: SkillType.ACTIVE,
    effect({ level }) {
        return {
            frames: [
                {
                    attack: CURSEOFTERRORBEAR_ATTACK_DATA[level],
                    hits: 24
                }
            ]
        }
    }
}

export default [MaddSlash, MaddStraight, MaddGuard, JumpingBearPress, BigMadd, TerribleRoar, CurseOfTerrorBear]
