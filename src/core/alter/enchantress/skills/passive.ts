import { Skill, SkillType } from "@/core/skill"

const PUPPETEER_DATA = [
    0, 69, 73, 77, 81, 85, 90, 95, 100, 106, 112, 118, 124, 130, 137, 144, 152, 160, 168, 176, 184, 193, 202, 212, 221, 231, 241, 252, 262, 273, 284, 296, 308, 320, 332, 344, 358, 371, 384, 398, 412,
    426, 440, 456, 470, 486, 502, 518, 534, 550, 567, 581, 597, 613, 629, 645, 660, 676, 692, 708, 724
]

//人偶操纵者
const Puppeteer: Skill = {
    name: "puppeteer",
    level_spacing: 3,
    level_master: 50,
    level_max: 60,
    need_level: 15,
    point: 15,
    type: SkillType.PASSIVE,
    effect({ level }, data) {
        const { buff_entry } = data
        const { passive_int_alter, passive_alter_level_add } = buff_entry

        level = level.plus(passive_alter_level_add)

        const intelligence = PUPPETEER_DATA[level].plus(passive_int_alter)
        data.changeStatus({
            intelligence
        })

        return {
            level
        }
    }
}

//邪恶的好奇心
const WickedCuriosity: Skill = {
    name: "wicked_curiosity",
    need_level: 20,
    level_spacing: 3,
    level_master: 10,
    level_max: 60,
    type: SkillType.PASSIVE,
    effect({ level }, data) {
        if (level > 1) {
            data.changeSkill({
                start: 15,
                end: -1,
                type: "attack_add",
                value: (0.1).plus((0.015).multiply(level))
            })
        }
    }
}

const PETITEDIABLO_DATA = [
    0, 14, 37, 59, 82, 104, 127, 149, 172, 194, 217, 239, 262, 284, 307, 329, 352, 374, 397, 419, 442, 464, 487, 509, 532, 554, 577, 599, 622, 644, 667, 689, 712, 734, 757, 779, 802, 824, 847, 869,
    892
]

const PetiteDiablo: Skill = {
    name: "petite_diablo", //少女的爱
    level_spacing: 3,
    level_master: 30,
    level_max: 40,
    need_level: 48,
    type: SkillType.PASSIVE,
    effect({ level }, data) {
        const { buff_entry } = data
        const { passive_int_awake } = buff_entry
        const value = PETITEDIABLO_DATA[level].plus(passive_int_awake)

        data.changeDungeonStatus({
            intelligence: value
        })

        return {
            value: {
                intstr: value
            }
        }
    }
}

const OMINOUSLY_SMILING_DATA = [
    0, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290, 300, 310, 320, 330, 340, 350, 360, 370, 380, 390, 400, 410, 420, 430, 440, 450, 460, 470, 480, 490, 500, 510, 520, 530,
    540, 550
]

const OminouslySmiling: Skill = {
    name: "ominously_smiling", //不详的微笑
    level_spacing: 3,
    level_master: 30,
    level_max: 40,
    need_level: 95,
    type: SkillType.PASSIVE,
    effect({ level }, data) {
        const value = OMINOUSLY_SMILING_DATA[level]
        data.changeStatus({
            intelligence: value
        })
    }
}

const DARKROSE_DATA = [
    0, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290, 300, 310, 320, 330, 340, 350, 360, 370, 380, 390, 400, 410, 420, 430, 440, 450, 460, 470, 480, 490, 500, 510, 520,
    530, 540
]
/**
 * 冥月绽放
 */
const DarkRose: Skill = {
    name: "dark_rose",
    level_spacing: 3,
    level_master: 30,
    level_max: 40,
    need_level: 75,
    type: SkillType.PASSIVE,
    effect({ level }, data) {
        const intelligence = DARKROSE_DATA[level]
        data.changeStatus({
            intelligence
        })
    }
}

export default [Puppeteer, WickedCuriosity, PetiteDiablo, DarkRose, OminouslySmiling]
