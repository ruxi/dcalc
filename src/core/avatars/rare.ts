import { createAvatars, OptionMap } from "./common"

const rare_options = {
    intelligence: 55,
    strength: 55,
    spirit: 55,
    vitality: 55,
    casting_speed: 14,
    attack_speed: 6,
    movement_speed: 6
} as OptionMap

export default createAvatars(rare_options, "rare")
