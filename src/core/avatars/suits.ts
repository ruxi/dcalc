import { Suit } from "../item"

export default [
    {
        name: "unique_avatar",
        compatible: "rare_avatar",
        needCount: 3,
        effect(data) {
            data.changeStatus({
                quadra: 50,
                speed: 3
            })
        }
    },
    {
        name: "unique_avatar",
        compatible: "rare_avatar",
        needCount: 8,
        effect(data) {
            data.changeStatus({
                quadra: 50,
                speed: 3,
                elemental_boost: 10
            })
        }
    },
    {
        name: "rare_avatar",
        needCount: 3,
        effect(data) {
            data.changeStatus({
                quadra: 40,
                speed: 2
            })
        }
    },
    {
        name: "rare_avatar",
        needCount: 8,
        effect(data) {
            data.changeStatus({
                quadra: 40,
                speed: 2,
                elemental_boost: 6
            })
        }
    },
    {
        name: "holiday_avatar",
        needCount: 8,
        effect(data) {
            data.changeStatus({
                quadra: 25,
                speed: 2
            })
        }
    },
    {
        name: "uncommon_avatar",
        needCount: 3,
        effect(data) {
            data.changeStatus({
                quadra: 10
            })
        }
    },
    {
        name: "uncommon_avatar",
        needCount: 8,
        effect(data) {
            data.changeStatus({
                quadra: 10,
                speed: 1
            })
        }
    }
] as Suit[]
