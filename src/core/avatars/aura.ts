import { isBuffer } from "@/store"
import { Avatar, AvatarBase } from "../avatar"
import { DealerKey } from "../entries/dealer"

export default [
    {
        name: "aura_2021_autumn_buff",
        fame: 464,
        part: "aura",
        icon: "/icon/avatar/aura/2021_autumn_buff.png",
        has_socket: true,

        options() {
            const is_buffer = isBuffer()
            if (is_buffer) {
                return [
                    data =>
                        data.changeBuff({
                            buff_intstr_per: 0.03
                        })
                ]
            }

            const keys: DealerKey[] = ["additional_damage", "critical_damage", "triatk_per"]
            return keys.map(k => data => {
                data.changeDamage({
                    [k]: 0.05
                })
            })
        },
        effect(data) {
            data.changeSkill({
                start: 1,
                end: 80,
                value: 1
            })
        }
    }
] as AvatarBase[]
