import { AvatarBase } from "./../avatar"
import { mapWithKeys } from "@/utils"
import { Avatar } from "../avatar"
import { coatOptions, OptionMap } from "./common"

const holiday_options = {
    intelligence: 45,
    strength: 45,
    spirit: 45,
    vitality: 45,
    casting_speed: 12,
    attack_speed: 5,
    movement_speed: 5
} as OptionMap

const holiday = [
    {
        part: "hair",
        options: mapWithKeys(holiday_options, ["intelligence", "spirit", "casting_speed"])
    },
    {
        part: "cap",
        options: mapWithKeys(holiday_options, ["intelligence", "spirit", "casting_speed"])
    },
    {
        part: "face",
        options: mapWithKeys(holiday_options, ["attack_speed"])
    },
    {
        part: "neck",
        options: mapWithKeys(holiday_options, ["attack_speed"])
    },
    {
        part: "coat",
        effect(data) {
            data.changeStatus({
                elemental_boost: 6
            })
        },
        options: coatOptions
    },
    {
        part: "belt",
        options: mapWithKeys(holiday_options, ["strength", "vitality"])
    },
    {
        part: "pants",
        options: []
    },
    {
        part: "shoes",
        options: mapWithKeys(holiday_options, ["strength", "vitality", "movement_speed"])
    }
] as AvatarBase[]

export default holiday.map(e => {
    return {
        ...e,
        name: `holiday_avatar_${e.part}`,
        icon: `/icon/avatar/holiday/${e.part}.png`,
        title: `{holiday_avatar}{${e.part}}`,
        suit_name: "holiday_avatar",
        rarity: "uncommon"
    }
}) as Avatar[]
