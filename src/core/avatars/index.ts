import aura from "./aura"
import weapon from "./weapon"

import holiday from "./holiday"
import rare from "./rare"
import unique from "./unique"

export const avatars = [...holiday, ...rare, ...unique, ...aura, ...weapon]

export { default as suits } from "./suits"
