import { AvatarOption } from "@/core/avatar"
import { ItemRarity } from "../item"
import { useCharacterStore } from "@/store"
import { mapWithKeys } from "@/utils"
import { AvatarBase } from "../avatar"

// 天空套
export interface OptionMap {
    intelligence: number
    strength: number
    spirit: number
    vitality: number
    casting_speed: number
    attack_speed: number
    movement_speed: number
}

const fameMap: Partial<Record<ItemRarity, number>> = {
    rare: 76,
    unique: 116
}

//上衣选项
export function coatOptions(): AvatarOption[] {
    const { character } = useCharacterStore()
    if (character) {
        const { skills } = character
        return skills
            .map(e => e.skill)
            .filter(e => e.need_level >= 15 && e.need_level <= 85)
            .map(e => {
                return data => {
                    data.changeSkill({
                        condition: {
                            names: [e.name]
                        },
                        value: 1
                    })
                }
            })
    }
    return []
}

export function createAvatars(options: OptionMap, rarity: ItemRarity) {
    const suit = [
        {
            part: "hair",
            options: mapWithKeys(options, ["intelligence", "spirit", "casting_speed"])
        },
        {
            part: "cap",
            options: mapWithKeys(options, ["intelligence", "spirit", "casting_speed"])
        },
        {
            part: "face",
            options: mapWithKeys(options, ["attack_speed"])
        },
        {
            part: "neck",
            options: mapWithKeys(options, ["attack_speed"])
        },
        {
            part: "coat",
            options: coatOptions
        },
        {
            part: "belt",
            options: mapWithKeys(options, ["strength", "vitality"])
        },
        {
            part: "pants",
            options: []
        },
        {
            part: "shoes",
            options: mapWithKeys(options, ["strength", "vitality", "movement_speed"])
        }
    ] as AvatarBase[]

    return suit.map(e => {
        return {
            ...e,
            fame: fameMap[rarity],
            name: `${rarity}_avatar_${e.part}`,
            title: `{${rarity}_avatar}{${e.part}}`,
            icon: `/icon/avatar/${rarity}/${e.part}.png`,
            suit_name: `${rarity}_avatar`,
            rarity
        }
    }) as AvatarBase[]
}
