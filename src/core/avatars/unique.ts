import { createAvatars, OptionMap } from "./common"

const unique_options = {
    intelligence: 65,
    strength: 65,
    spirit: 65,
    vitality: 65,
    casting_speed: 14,
    attack_speed: 7,
    movement_speed: 7
} as OptionMap

export default createAvatars(unique_options, "unique")
