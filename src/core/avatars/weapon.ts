import { SkillType } from "../skill"
import { StatusKey } from "../status"
import { useCharacterStore } from "@/store"
import { AvatarBase } from "./../avatar"
export default [
    ...[40, 45, 60, 70, 80].map(e => {
        return {
            fame: 166,
            part: "weapon",
            name: `rare_avatar_clone_weapon(Lv${e})`,
            title: `{rare_avatar_clone_weapon}(Lv${e}{skill})`,
            icon: `/icon/avatar/weapon/rare_clone.png`,
            rarity: "rare",
            effect(data) {
                const { skills } = useCharacterStore()

                const names = skills
                    .filter(s => s.need_level == e)
                    .map(e => e.name)
                    .slice(0, 1)

                data.changeSkill({
                    condition: {
                        names,
                        type: SkillType.ACTIVE,
                        jobs: false
                    },
                    value: 1
                })
            },
            options() {
                const keys: StatusKey[] = ["intelligence", "strength", "vitality", "spirit"]
                return keys.map(key => {
                    return {
                        [key]: 55
                    }
                })
            }
        } as AvatarBase
    })
] as AvatarBase[]
