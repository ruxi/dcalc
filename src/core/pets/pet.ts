import { Item, ItemColumn, ItemEffect } from "../item"
import createEnchatingSlot from "../slots/enchanting"

interface PetSkill {
    time: number
    effect: ItemEffect
}

export type PetColor = "red" | "blue" | "green"

export interface Artifact extends Item {
    color: PetColor
}

export interface Pet extends Item {
    skill: PetSkill
}

export interface PetColumn extends ItemColumn<"pet", Pet> {
    red?: Artifact
    blue?: Artifact
    green?: Artifact
}

export function createPetColumn(): PetColumn {
    const slot = createEnchatingSlot("pet")
    return {
        part: "pet",
        slots: slot ? [slot] : [],
        data: new Map<string, any>()
    }
}
