import { Pet } from "./pet"

export default [
    {
        name: "pet_2021_spring_normal",
        icon: "/icon/pet/2021_spring_normal.png",
        effect(data) {
            data.changeStatus({
                quadra: 150,
                elemental_boost: 25,
                crirate: 10
            })
            data.changeDamage({
                bonus_damage: 0.1,
                triatk_per: 0.12
            })
            data.changeSkill({
                start: 1,
                end: 80,
                value: 1
            })
            data.changeSkill({
                start: 1,
                end: -1,
                type: "cd_raduction",
                value: 0.05
            })
        }
    }
] as Pet[]
