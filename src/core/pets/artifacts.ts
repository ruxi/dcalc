import { Artifact } from "./pet"

export default [
    {
        color: "red",
        name: "artifact_red_01",
        icon: "/icon/artifact/red/01.png",
        effect(data) {
            data.changeStatus({
                quadra: 25
            })
            data.changeDamage({
                bonus_damage: 0.04
            })
        }
    },
    {
        color: "red",
        name: "artifact_red_02",
        icon: "/icon/artifact/red/02.png",
        effect(data) {
            data.changeStatus({
                quadra: 33
            })
        }
    },
    {
        color: "red",
        name: "artifact_red_03",
        icon: "/icon/artifact/red/03.png",
        effect(data) {
            data.changeDamage({
                additional_damage: 0.07
            })
        }
    },
    {
        color: "red",
        name: "artifact_red_04",
        icon: "/icon/artifact/red/03.png",
        effect(data) {
            data.changeDamage({
                intstr_per: 0.07
            })
        }
    },
    {
        color: "red",
        name: "artifact_red_05",
        icon: "/icon/artifact/red/03.png",
        effect(data) {
            data.changeDamage({
                bonus_damage: 0.08
            })
        }
    }
] as Artifact[]
