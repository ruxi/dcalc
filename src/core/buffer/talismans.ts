import { Item } from "../item"

export default [
    {
        name: "tailismans_buff_08a",
        effect(data) {
            data.changeBuff({
                buff_intstr_per: 0.08
            })
        }
    },
    {
        name: "tailismans_buff_08b",
        effect(data) {
            data.changeBuff({
                buff_intstr_per: 0.08
            })
        }
    },

    {
        name: "tailismans_buff_06a",
        effect(data) {
            data.changeBuff({
                buff_intstr_per: 0.06
            })
        }
    },

    {
        name: "tailismans_buff_06b",
        effect(data) {
            data.changeBuff({
                buff_intstr_per: 0.06
            })
        }
    },

    {
        name: "tailismans_buff_04a",
        effect(data) {
            data.changeBuff({
                buff_intstr_per: 0.04
            })
        }
    },

    {
        name: "tailismans_buff_04b",
        effect(data) {
            data.changeBuff({
                buff_intstr_per: 0.04
            })
        }
    },

    {
        name: "tailismans_buff_04c",
        effect(data) {
            data.changeBuff({
                buff_intstr_per: 0.04
            })
        }
    }
] as Item[]
