import { Character } from "./character"
import { isCareStatus } from "@/store"
import { CalcData } from "./calc"

export const rarities: string[] = ["uncommon", "rare", "unique", "legendary", "epic", "mythic"]

export type ItemRarity = "uncommon" | "rare" | "unique" | "legendary" | "epic" | "mythic"

export interface Item {
    name: string

    title?: string | (() => string)

    icon?: string

    part: string

    suit_name?: string

    fame?: number

    rarity?: ItemRarity

    effect?: ItemEffect

    dungeon_effect?: ItemEffect
}

export type ItemEffect = (data: CalcData, ...args: any[]) => void

export interface ItemColumn<P extends string = string, I extends Item = Item> {
    item?: I
    part: P
    slots: ItemSlot[]
    data: Map<string, any>
    [key: string]: any
}

/**
 * 插槽
 */
export interface ItemSlot {
    readonly name: string
    effect(column: ItemColumn, data: CalcData): void
}

export type SlotRate = {
    [rarity in ItemRarity]: number
}

export interface Suit extends Item {
    needCount: number
    compatible?: string
    parts: string[]
}
export interface Enchant extends Item {
    fame: number
    tags?: string[]
    parts: string[]
}

export type CreateItems<T> = () => T[]

export type Items<T> = (T | CreateItems<T>)[] | CreateItems<T>

export interface Emblem extends Item {
    fame: number
    colors: SocketColor[]
}

export type SocketColor = "yellow" | "green" | "red" | "blue" | "platinum"

export const SocketMaps: { [color in SocketColor]: string[] } = {
    yellow: ["shoulder", "necklace"],
    green: ["coat", "pants"],
    red: ["belt", "ring"],
    blue: ["bracelet", "shoes"],
    platinum: ["subequip", "magicstones"]
}

export function toItems<T>(items: T[] | CreateItems<T>) {
    if (typeof items == "function") {
        items = items()
    }
    return items
}

export function createItems<T>(items: Items<T>) {
    const list: T[] = []
    items = toItems(items)
    for (let item of items) {
        if (typeof item == "function") {
            item = item as CreateItems<T>
            list.push(...item())
        } else {
            list.push(item)
        }
    }
    return list
}

export function compareItem(item0: Item, item1: Item) {
    return rateItem(item1) - rateItem(item0)
}

export function rateItem(item: Item) {
    const data = new CalcData()
    item.effect?.(data)
    const { status, skill_entries } = data
    let rate = 0
    for (let key in status) {
        const i = isCareStatus(key)
        rate += i * status[key]
    }
    for (let skill of skill_entries) {
        if (skill.type == "none") {
            continue
        }
        rate += 200
    }
    return rate
}
