import { CalcData } from "./calc"
import { Character } from "./character"
import { Item, ItemColumn, ItemEffect, Items } from "./item"
import { StatusOptions } from "./status"

export const avatar_parts = ["weapon", "aura", "hair", "cap", "face", "neck", "coat", "skin", "belt", "pants", "shoes"] as const

export type AvatarPart = typeof avatar_parts[number]

export type AvatarColumns = {
    [part: AvatarPart | string]: ItemColumn
}

export type AvatarOption = StatusOptions | ItemEffect

export interface AvatarBase extends Partial<Item> {
    options: Items<AvatarOption>
    has_socket?: boolean
}

export interface Avatar extends Item {
    options: AvatarOption[]
    has_socket?: boolean
}

export { avatars, suits } from "./avatars"
