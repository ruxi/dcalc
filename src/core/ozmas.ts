import { Item } from "./item"

export interface Ozma extends Item {}

export const ozmas = [
    {
        name: "astaroth",
        fame: 116,
        effect(data) {
            data.changeBuff({
                buff_triatk_per: 0.01,
                awake_intstr_add: 22,
                passive_alter: 55
            })
        }
    },
    {
        name: "berias",
        fame: 116,
        effect(data) {
            data.changeBuff({
                passive_alter: 138
            })
        }
    },
    {
        name: "redmayne",
        fame: 116,
        effect(data) {
            data.changeBuff({
                buff_intstr_per: 0.02,
                awake_intstr_add: 25,
                passive_alter: 46
            })
        }
    },
    {
        name: "rosenbach",
        fame: 116,
        effect(data) {
            data.changeBuff({
                buff_intstr_per: 0.01,
                buff_triatk_per: 0.01,
                awake_intstr_add: 37
            })
        }
    },
    {
        name: "timat",
        fame: 116,
        effect(data) {
            data.changeBuff({
                buff_intstr_per: 0.03,
                awake_level_per: 0.01,
                awake_intstr_add: 21
            })
        }
    }
] as Ozma[]
