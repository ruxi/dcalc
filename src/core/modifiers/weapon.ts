import { SkillType } from "@/core/skill"
import { Modifier } from "./modifier"

const weapon_modifiers = [
    {
        name: "intstr_per",
        buff_data: [20, 120, 180],
        key: ["intstr_per", "passive_alter"],
        effect(data) {}
    },
    {
        name: "triatk_per",
        key: ["triatk_per", "buff_intstr_per"],
        buff_data: [0.01, 0.01, 0.04],

        effect(data) {
            data.changeBuff({
                awake_level_add: 1
            })
        }
    },

    {
        name: "critical_damage",
        key: ["critical_damage", "buff_intstr_per"],
        buff_data: [0.01, 0.02, 0.05],
        effect(data) {
            data.changeBuff({
                awake_intstr_per: 0.03
            })
        }
    },

    {
        name: "additional_damage",
        key: ["additional_damage", "awake_intstr_add"],
        buff_data: [10, 30, 60],

        effect(data) {
            data.changeBuff({
                buff_intstr_per: 0.03
            })
        }
    },

    {
        name: "final_damage",
        key: ["final_damage", "awake_intstr_per"],
        buff_data: [0.01, 0.02, 0.05],
        effect(data) {
            data.changeBuff({
                buff_triatk_per: 0.02
            })
        }
    },
    {
        name: "bonus_damage",
        key: ["bonus_damage", "buff_triatk_per"],
        buff_data: [0.01, 0.01, 0.04],
        effect(data) {
            data.changeBuff({
                awake_intstr_add: 30
            })
        }
    }
] as Modifier[]

export default weapon_modifiers
