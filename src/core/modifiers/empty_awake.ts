import { SkillType } from "../skill"
import { Modifier } from "./modifier"
export default {
    name: "awake",
    key: ["awake", "awake"],
    effect(data) {
        data.changeSkill({
            condition: {
                levels: [50, 75, 100],
                type: SkillType.AWAKE
            },
            value: 2
        })
    }
} as Modifier
