import { Modifier } from "./modifier"

const modifiers = [
    {
        name: "intstr_per",
        key: ["intstr_per", "passive_alter"],
        buff_data: [20, 100, 160],
        effect(data) {}
    },
    {
        name: "triatk_per",
        key: ["triatk_per", "awake_intstr_add"],

        buff_data: [10, 10, 30],
        effect(data, level) {
            if (level) {
                data.changeBuff({
                    buff_level_add: 1
                })
            }
        }
    },

    {
        name: "critical_damage",
        key: ["critical_damage", "buff_intstr_per"],
        buff_data: [0.01, 0.02, 0.05],
        effect(data, level) {
            if (level) {
                data.changeBuff({
                    awake_intstr_per: 0.02
                })
            }
        }
    },

    {
        name: "additional_damage",
        key: ["additional_damage", "awake_intstr_add"],
        buff_data: [10, 10, 40],

        effect(data, level) {
            if (level) {
                data.changeBuff({
                    buff_intstr_per: 0.04
                })
            }
        }
    },

    {
        name: "final_damage",
        key: ["final_damage", "awake_intstr_per"],
        buff_data: [0.01, 0.01, 0.04],
        effect(data, level) {
            if (level) {
                data.changeBuff({
                    buff_triatk_per: 0.02
                })
            }
        }
    },
    {
        name: "bonus_damage",
        key: ["bonus_damage", "buff_triatk_per"],
        buff_data: [0.01, 0.01, 0.04],
        effect(data, level) {
            if (level) {
                data.changeBuff({
                    awake_intstr_add: 25
                })
            }
        }
    }
] as Modifier[]

export default modifiers
