import { isBuffer } from "@/store"
import { CalcData } from "../calc"
import { EquipOptionData, getOptionValue } from "../equips"
import awake from "./awake"
import empty_awake from "./empty_awake"
import { Modifier } from "./modifier"
import others from "./others"
import weapon from "./weapon"

//触发动态属性,不包括固定属性
export function effectModifier(modifier: Modifier, data: CalcData, value: number, level = 5, is_weapon = false) {
    let { buff_data, key, name } = modifier

    const [deal_key, buff_key] = key

    if (deal_key != "awake") {
        const deal_unit = is_weapon ? 0.04 : 0.02

        value = name.startsWith("awake_") ? 0 : value
        const deal_data: EquipOptionData = [deal_unit, value, value.plus(deal_unit.multiply(4))]

        let deal_value = level > 0 ? getOptionValue(deal_data, level) : value
        data.changeDamage({
            [deal_key]: deal_value
        })
        if (buff_data) {
            const buff_value = level > 0 ? getOptionValue(buff_data, level - 1) : 0 //buff要低1级
            data.changeBuff({
                [buff_key]: buff_value
            })
        }
    }

    modifier.effect(data, level)
}

export function getModifiers(part: string, type: string) {
    let list: Modifier[] = []

    if (part == "weapon") {
        list.push(...weapon)
        if (type == "auto" || type == "awake") {
            if (isBuffer()) {
                list.push(awake[0])
            } else {
                list.push(...awake)
            }
        } else {
            list.push(empty_awake)
        }
    } else {
        list.push(...others)
    }

    return list
}
