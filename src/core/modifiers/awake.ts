import { Modifier, modifier_keys } from "."
import { SkillType } from "../skill"

function createAwakeModifier(keys: string[]) {
    const awakes: Modifier[] = []
    for (let key of keys) {
        awakes.push({
            name: `awake_modifier_${key}`,
            key: [key, "awake"],
            effect(data) {
                data.changeSkill({
                    condition: {
                        levels: [50, 75, 100],
                        type: SkillType.AWAKE
                    },
                    value: 2
                })
            }
        })
    }
    return awakes
}

export default createAwakeModifier(modifier_keys)
