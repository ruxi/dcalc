import { CalcData } from "../calc"

export interface Modifier {
    name: string

    key: [string, string]

    buff_data?: [number, number, number]

    effect: (data: CalcData, level?: number) => void
}

export const modifier_parts = ["pants", "ring", "subequip", "weapon"]

export const modifier_keys = ["intstr_per", "triatk_per", "critical_damage", "additional_damage", "final_damage", "bonus_damage"]

export type ModifierConfigs = {
    [key: string]: [string, string, number]
}
