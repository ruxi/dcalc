/**
 * @Author: Kritsu
 * @Date:   2021/11/12 15:47:26
 * @Last Modified by:   Kritsu
 * @Last Modified time: 2021/11/17 18:54:00
 */

import { EquipProperties } from "./equips"
import armor_status from "@/assets/data/armor_status.json"

export interface DefenseStatus {
    // 物理防御力
    phydefense: number
    // 魔法防御力
    magdefense: number

    //火属性抗性
    fire_resistance: number

    //冰属性抗性
    ice_resistance: number

    //光属性抗性
    light_resistance: number

    //暗属性抗性
    dark_resistance: number
}

export interface StatusBase extends Record<string, number>, DefenseStatus {
    strength: number

    //智力
    intelligence: number
    //物理攻击力
    phyatk: number

    //魔法攻击力
    magatk: number

    //独立攻击力
    indatk: number
    //体力
    vitality: number

    //精神
    spirit: number

    //物理暴击率
    phycri_rate: number

    //魔法暴击率
    magcri_rate: number

    //火属性强化
    fire_boost: number

    //冰属性强化
    ice_boost: number

    //光属性强化
    light_boost: number

    //暗属性强化
    dark_boost: number

    //攻击速度
    attack_speed: number
    //施放速度
    casting_speed: number
    //移动速度
    movement_speed: number
}

export interface Status extends StatusBase, Record<string, number> {}

export type StatusKey = keyof StatusBase

export type QuadraStatus = "strength" | "intelligence" | "vitality" | "spirit"

export interface StatusExtra {
    triatk: number
    intstr: number
    vitspi: number
    quadra: number
    defense: number
    elemental_boost: number
    elemental_resistance: number
    speed: number
    crirate: number
}

export type StatusOptions = Partial<Status & StatusExtra>

export function createCharacterStatus(options: StatusOptions = {}): Status {
    let {
        strength = 0,
        intelligence = 0,
        vitality = 0,
        spirit = 0,
        phyatk = 0,
        magatk = 0,
        indatk = 0,
        phycri_rate = 0,
        magcri_rate = 0,
        crirate = 0,
        ice_boost = 0,
        fire_boost = 0,
        light_boost = 0,
        dark_boost = 0,
        attack_speed = 0,
        casting_speed = 0,
        movement_speed = 0,
        defense = 0,
        phydefense = 0,
        magdefense = 0,

        fire_resistance = 0,
        ice_resistance = 0,
        light_resistance = 0,
        dark_resistance = 0,
        intstr = 0,
        vitspi = 0,
        quadra = 0,
        triatk = 0,
        speed = 0,
        elemental_boost = 0,
        elemental_resistance = 0
    } = options

    if (intstr > 0) {
        strength = intstr.plus(strength)
        intelligence = intstr.plus(intelligence)
    }
    if (triatk > 0) {
        phyatk = triatk.plus(phyatk)
        magatk = triatk.plus(magatk)
        indatk = triatk.plus(indatk)
    }
    if (vitspi > 0) {
        vitality = vitspi.plus(vitality)
        spirit = vitspi.plus(spirit)
    }

    if (quadra > 0) {
        strength = quadra.plus(strength)
        intelligence = quadra.plus(intelligence)
        vitality = quadra.plus(vitality)
        spirit = quadra.plus(spirit)
    }

    if (elemental_boost > 0) {
        ice_boost = elemental_boost.plus(ice_boost)
        fire_boost = elemental_boost.plus(fire_boost)
        light_boost = elemental_boost.plus(light_boost)
        dark_boost = elemental_boost.plus(dark_boost)
    }

    if (elemental_resistance > 0) {
        ice_resistance = elemental_resistance.plus(ice_resistance)
        fire_resistance = elemental_resistance.plus(fire_resistance)
        light_resistance = elemental_resistance.plus(light_resistance)
        dark_resistance = elemental_resistance.plus(dark_resistance)
    }

    if (crirate > 0) {
        phycri_rate = crirate.plus(phycri_rate)
        magcri_rate = crirate.plus(magcri_rate)
    }

    if (speed > 0) {
        attack_speed = speed.plus(attack_speed)
        casting_speed = speed.plus(casting_speed)
        movement_speed = speed.plus(movement_speed)
    }

    if (defense > 0) {
        phydefense = defense.plus(phydefense)
        magdefense = defense.plus(magdefense)
    }

    return {
        phyatk,
        magatk,
        indatk,
        strength,
        intelligence,
        vitality,
        spirit,
        attack_speed,
        casting_speed,
        movement_speed,
        phycri_rate,
        magcri_rate,
        fire_boost,
        ice_boost,
        light_boost,
        dark_boost,
        ice_resistance,
        fire_resistance,
        light_resistance,
        dark_resistance,
        phydefense,
        magdefense
    }
}

export function mergeStatusOptions(old_status: Status, options: StatusOptions = {}) {
    const new_status = createCharacterStatus(options)
    return mergeStatus(old_status, new_status)
}

export function mergeStatus(old_status: Status, new_status: Status) {
    const keys = new Set<string>(Object.keys(old_status).concat(Object.keys(new_status)))
    for (let key of keys) {
        let oldValue = old_status[key] ?? 0
        let newValue = new_status[key] ?? 0
        old_status[key] = oldValue.plus(newValue)
    }
    return old_status
}

export function createStatusByEquip({ level, rarity, type, part, status }: EquipProperties) {
    const key = `${level}-${rarity}-${type}-${part}`
    //@ts-ignore
    const value: number[] = armor_status[key]
    let options = {}
    if (!!value && value.length > 3) {
        options = {
            strength: value[0],
            intelligence: value[1],
            vitality: value[2],
            spirit: value[3]
        }
    }
    return mergeStatusOptions(createCharacterStatus(status), options)
}
