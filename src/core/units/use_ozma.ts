import { Ozma } from "./../ozmas"
import { useCharacterStore } from "@/store"
import { CalcData, CalcFunction } from "../calc"

const use_ozma: CalcFunction = function () {
    const data = new CalcData()

    const { ozma_columns } = useCharacterStore()
    for (let ozma of ozma_columns) {
        data.force(ozma as Ozma)
    }
    return [data]
}

export default use_ozma
