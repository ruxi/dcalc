/**
 * @Author: Kritsu
 * @Date:   2021/11/16 12:17:23
 * @Last Modified by:   Kritsu
 * @Last Modified time: 2021/11/17 18:47:54
 *  * 计算装备属性
 */
import { CalcData, CalcFunction } from "@/core/calc"
import { getSuitByItems } from "@/core/equips"
import { useCharacterStore } from "@/store"
import { Avatar, AvatarOption, suits } from "@/core/avatar"

const use_avatar: CalcFunction = function () {
    const context = new CalcData()

    const items = []

    const { character, avatar_columns } = useCharacterStore()

    for (let key of Object.keys(avatar_columns)) {
        const column = avatar_columns[key]
        const { item, slots = [], data } = column
        if (!item) {
            continue
        }
        items.push(item)
        const avatar = item as Avatar
        let options = avatar.options

        if (character && options?.length) {
            const index = (data.get("option_index") as number) ?? 0
            let first: AvatarOption = options[index]
            if (typeof first == "function") {
                first(context)
            } else {
                context.changeStatus(first)
            }
        }

        context.force(item)

        for (let slot of slots) {
            slot.effect(column, context)
        }
    }

    //计算套装效果
    let apdata_suits = getSuitByItems(items, suits)

    for (let suit of apdata_suits) {
        context.force(suit)
    }

    return [context]
}
export default use_avatar
