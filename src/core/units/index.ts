import use_avatar from "@/core/units/use_avatar"
import use_equip from "@/core/units/use_equip"
import use_siroco from "@/core/units/use_siroco"
import use_ozma from "@/core/units/use_ozma"
import use_pet from "./use_pet"
import use_base from "./use_base"
import use_talismans from "./use_talismans"
import use_modifier from "./use_modifier"

export default [use_base, use_avatar, use_equip, use_talismans, use_modifier, use_siroco, use_ozma, use_pet]
