import { useCharacterStore } from "@/store"
import { CalcData, CalcFunction } from "../calc"

const use_pet: CalcFunction = function () {
    const data = new CalcData()

    const { pet_column } = useCharacterStore()

    const { item, red, green, blue, slots } = pet_column
    if (item) {
        data.force(item)
        if (red) {
            data.force(red)
        }
        if (green) {
            data.force(green)
        }
        if (blue) {
            data.force(blue)
        }
        for (let slot of slots) {
            slot.effect?.(pet_column, data)
        }
    }
    return [data]
}

export default use_pet
