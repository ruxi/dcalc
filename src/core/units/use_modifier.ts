import { getModifiers, effectModifier, modifier_parts } from "@/core/modifiers"
import { isBuffer, useCharacterStore } from "@/store"
import { cartesian } from "@/utils"
import { CalcData, CalcFunction } from "../calc"

const use_modifier: CalcFunction = function () {
    const { equip_columns } = useCharacterStore()
    const is_buffer = isBuffer()
    let data_matrix: CalcData[][] = []

    for (let p of modifier_parts) {
        const { item, data } = equip_columns[p]
        const arr: CalcData[] = []

        if (!item?.modifier) {
            continue
        }

        let value = 0
        let [type, key, level] = (data.get("modifier") as [string, string, number]) ?? ["none", "none", 0]
        value = item.modifier[1] ?? 0
        if (type == "auto") {
            level = 4
        } else if (type == "none" || key == "none") {
            if (is_buffer) {
                //如果是辅助职业 遴选为空直接跳过
                continue
            }
            key = item.modifier[0]
            level = 0
        }
        const is_weapon = p == "weapon"

        const list = getModifiers(p, type)

        for (let modifier of list) {
            const calc = new CalcData()

            if (modifier) {
                effectModifier(modifier, calc, value, level, is_weapon)

                if (item.name == "weapon_epic_broom_01") {
                    calc.changeBuff({
                        awake_level_add: 2
                    })
                }
            }
            if (type == "auto") {
                calc.modifiers.push([p, modifier.name, level])
            }
            arr.push(calc)
        }

        data_matrix.push(arr)
    }

    data_matrix = cartesian(...data_matrix)

    const rs = data_matrix.map(values => {
        const calc = new CalcData()
        for (let data of values) {
            calc.apply(data)
        }
        return calc
    })

    return rs
}

export default use_modifier
