import { useCharacterStore } from "@/store"
import { CalcData, CalcFunction } from "../calc"
import { ADVENTURER_LEVEL_MAX } from "../../constants/index"

export const adventurer_buff = [
    0, 0, 10, 30, 50, 70, 90, 110, 125, 140, 155, 170, 185, 185, 185, 185, 200, 200, 215, 215, 230, 230, 230, 230, 230, 245, 245, 245, 245, 245, 260, 260, 260, 260, 260, 275, 275, 275, 275, 275, 290
]

const use_base: CalcFunction = function () {
    const data = new CalcData()
    let { adventurer_level, guild_buff, wedding_ring } = useCharacterStore().base_setting
    adventurer_level = Math.max(1, Math.min(ADVENTURER_LEVEL_MAX, adventurer_level))

    data.changeStatus({
        //冒险团等级
        quadra: adventurer_buff[adventurer_level]
    })
    if (guild_buff) {
        //加入公会
        data.changeStatus({
            quadra: 120
        })
    }
    if (wedding_ring) {
        // 结婚戒指
        data.changeStatus({
            quadra: 15
        })
    }
    return [data]
}

export default use_base
