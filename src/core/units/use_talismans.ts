import { useCharacterStore } from "@/store"
import { CalcData, CalcFunction } from "../calc"

const talismans = [0.08, 0.08, 0.06, 0.06, 0.06, 0.04, 0.04]

const use_talismans: CalcFunction = function () {
    const data = new CalcData()
    let { talismans_columns } = useCharacterStore()

    talismans_columns = talismans_columns.slice(0, 3)

    for (let i of talismans_columns) {
        data.changeBuff({
            buff_intstr_per: talismans[i]
        })
        data.changeFame(46 * 3 + 185)
    }
    return [data]
}

export default use_talismans
