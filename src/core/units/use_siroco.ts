import { Siroco } from "./../sirocos/siroco"
import { useCharacterStore } from "@/store"
import { CalcData, CalcFunction } from "../calc"

const use_siroco: CalcFunction = function () {
    const data = new CalcData()

    const { siroco_columns } = useCharacterStore()
    for (let siroco of siroco_columns) {
        data.force(siroco as Siroco)
    }
    return [data]
}

export default use_siroco
