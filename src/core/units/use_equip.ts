import { getOptionValue } from "@/core/equips"
/**
 * @Author: Kritsu
 * @Date:   2021/11/16 12:17:23
 * @Last Modified by:   Kritsu
 * @Last Modified time: 2021/11/17 18:47:54
 *  * 计算装备属性
 */
import { CalcData, CalcFunction } from "@/core/calc"
import { armor_parts, EquipColumns, getSuitByItems, MythicEquip } from "@/core/equips"
import { suits } from "@/core/equips/suits"
import { createStatusByEquip } from "../status"

const use_equip: CalcFunction = function (optionals: EquipColumns[]) {
    const rs: CalcData[] = []

    for (let columns of optionals) {
        const data = new CalcData()

        const items = []

        for (let key of Object.keys(columns)) {
            const column = columns[key]
            const { item, slots = [] } = column

            if (!item) {
                continue
            }
            items.push(item)
            data.force(item)
            if (item.rarity == "mythic") {
                let { mythic_properties } = item as MythicEquip
                for (let { deal_data, buff_data, effect } of mythic_properties) {
                    let deal_value = getOptionValue(deal_data, 4)
                    let buff_value = getOptionValue(buff_data, 4)
                    effect(data, [deal_value, buff_value])
                }
            }
            let type = item.type
            if (armor_parts.includes(type)) {
                type = column.data.get("armor_type") ?? type
            }
            const status = createStatusByEquip({ ...item, type })
            data.changeStatus(status) //通用属性
            for (let slot of slots) {
                slot.effect(column, data)
            }
            data.listen("use_equip", item.name)
        }

        //计算套装效果
        let apdata_suits = getSuitByItems(items, suits)
        for (let suit of apdata_suits) {
            data.force(suit)
        }
        rs.push(data)
    }

    return rs
}
export default use_equip
