import { CalcData } from "@/core/calc"
import { EquipColumn } from "@/core/equips"
import { isBuffer } from "@/store"
import { ItemRarity, ItemSlot } from "../item"

const wepaon_refine_coeff = [0, 2, 3, 4, 6, 8, 13, 18, 25, 32, 41]
const weapon_refine_weigths: RefineRarityWeigths = {
    uncommon: {
        rate: 8,
        growth: 0.8
    },
    rare: {
        rate: 10,
        growth: 1
    },
    unique: {
        rate: 12,
        growth: 1.25
    },

    legendary: {
        rate: 13,
        growth: 1.35
    },
    epic: {
        rate: 14,
        growth: 1.45
    },
    mythic: {
        rate: 15,
        growth: 1.55
    }
}

const weapon_type_coeff = {
    broom: [1.1, 1.11]
}
type RefineRarityWeigths = {
    [rarity in ItemRarity]: RefineRarityWeigth
}

interface RefineRarityWeigth {
    rate: number
    growth: number
}

/**
 *  锻造
 */

class Refine implements ItemSlot {
    name: string = "refine"

    constructor() {}

    effect(column: EquipColumn, context: CalcData) {
        const { item, data } = column
        let refine_level = (data.get("refine_level") as number) ?? 0
        refine_level = Math.max(Math.min(refine_level, 8), 0)
        if (item) {
            let { level = 100, rarity = "epic" } = item
            const { rate, growth } = weapon_refine_weigths[rarity]
            const indatk = level.plus(rate).divide(8).multiply(growth).multiply(wepaon_refine_coeff[refine_level]).round()

            context.changeFame(480, `upgrade[${item.part}]`)

            context.record()
            context.changeStatus({
                indatk
            })

            if (isBuffer()) {
                const quadra = level.plus(rate).divide(80).multiply(growth).multiply(wepaon_refine_coeff[refine_level]).round()
                context.changeStatus({ quadra })
            }
        }
    }
}

export default function createRefineSlot(part: string): ItemSlot | undefined {
    if (part == "weapon") {
        return new Refine()
    }
    return undefined
}
