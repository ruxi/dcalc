import { useCharacterStore } from "@/store"
import { CalcData } from "@/core/calc"
import { ItemColumn, ItemSlot } from "../item"

function getEnchant(part: string, name: string) {
    const { enchant_list } = useCharacterStore()
    for (let enchant of enchant_list) {
        if (enchant.parts.includes(part)) {
            if (name && enchant.name == name) {
                return enchant
            }
        }
    }
}

class Enchanting implements ItemSlot {
    name: string = "enchanting"

    effect(column: ItemColumn, context: CalcData) {
        const { item, data, part } = column
        if (item) {
            const enchant_name = data.get("enchanting") as string
            const enchant = getEnchant(part, enchant_name)
            context.force(enchant)
        }
    }
}

export default function createEnchatingSlot(part: string): ItemSlot | undefined {
    return new Enchanting()
}
