import { CalcData } from "@/core/calc"
import { ArmorPart, armor_parts, EquipColumn, EquipPart } from "@/core/equips"
import { QuadraStatus } from "@/core/status"
import { isBuffer } from "@/store"
import { ItemSlot, SlotRate } from "@/core/item"

const armor_coeff: ArmorCoeff = { coat: 0.3, shoulder: 0.2, pants: 0.25, belt: 0.1, shoes: 0.15 }
const rarity_rate: SlotRate = { uncommon: 4, rare: 5, unique: 8, legendary: 14, epic: 17, mythic: 18 }

type ArmorCoeff = {
    [type in ArmorPart | string]: number
}

/**
 *  精通
 */

class ArmorMastery implements ItemSlot {
    name: string = "armor_mastery"

    effect(column: EquipColumn, context: CalcData) {
        const { item, data } = column
        if (item) {
            const upgrade_level = (data.get("upgrade_level") as number) ?? 0
            const adapt_status = data.get("adapt_status") as QuadraStatus
            const is_buffer = isBuffer()

            let { level = 100, rarity = "epic", part } = item
            const base = is_buffer ? 4 : 20
            const times = is_buffer ? 1 : 2.5

            let value = level.plus(rarity_rate[rarity]).plus(upgrade_level.exactlyDivide(3)).multiply(times).plus(base).multiply(armor_coeff[part])
            if (adapt_status != "vitality") {
                value *= 2
            }
            context.changeStatus({
                [adapt_status]: value
            })
        }
    }
}

export default function createArmorMasterySlot(part: string): ItemSlot | undefined {
    if (armor_parts.includes(part as EquipPart)) {
        return new ArmorMastery()
    }
    return undefined
}
