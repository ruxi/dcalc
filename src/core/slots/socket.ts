import { useCharacterStore } from "@/store"
import { CalcData } from "@/core/calc"
import { getSocketColor } from "@/core/equips"
import { ItemColumn, ItemSlot, SocketColor } from "../item"

function getSocket(color: SocketColor, name?: string) {
    const { emblem_list } = useCharacterStore()
    if (name && color) {
        for (let emblem of emblem_list) {
            const { colors = [] } = emblem
            if (colors.includes(color)) {
                if (emblem.name == name) {
                    return emblem
                }
            }
        }
    }
}

class Socket implements ItemSlot {
    name: string = "socket"

    effect(column: ItemColumn, context: CalcData) {
        const { item, data, part } = column
        if (item) {
            const color = getSocketColor(part)
            if (color) {
                let socket_left = (data.get("socket_left") as string) ?? "all"
                const socket_0 = getSocket(color, socket_left)
                context.record()
                context.force(socket_0)
                if (color != "platinum") {
                    let socket_right = (data.get("socket_right") as string) ?? "all"
                    const socket_1 = getSocket(color, socket_right)
                    context.force(socket_1)
                }
                context.record()
            }
        }
    }
}

const ignore_parts = ["weapon", "earrings", "title"]

export default function createSocket(part: string): ItemSlot | undefined {
    if (!ignore_parts.includes(part)) {
        return new Socket()
    }
}
