import { CalcData } from "@/core/calc"
import { EquipColumn } from "@/core/equips"
import { QuadraStatus } from "@/core/status"
import { ItemRarity, ItemSlot } from "../item"

const amplify_coeff = [0, 1, 2, 3, 4, 6, 8, 10, 22, 28, 35, 42, 50, 67, 108, 150, 192, 267, 342, 417, 500, 583, 667, 750, 833, 917, 1000, 1083, 1167, 1250, 1333, 1417]
const amplify_rarity_weigths: AmplifyRarityWeigths = {
    uncommon: {
        rate: 45,
        growth: 1,
        base: 10
    },
    rare: {
        rate: 45,
        growth: 1,
        base: 10
    },
    unique: {
        rate: 45,
        growth: 1.3,
        base: 11
    },

    legendary: {
        rate: 46,
        growth: 1.4,
        base: 11
    },
    epic: {
        rate: 46,
        growth: 1.5,
        base: 12
    },
    mythic: {
        rate: 46,
        growth: 1.6,
        base: 13
    }
}

const epic_fames = [0, 3, 6, 8, 11, 14, 16, 19, 32, 36, 50, 99, 158, 171, 185, 198, 211, 224, 237, 250, 263, 276, 289, 302, 315, 328, 341, 354, 369, 382, 395, 408]

const mythic_fames = [0, 4, 7, 10, 14, 17, 20, 24, 40, 45, 63, 123, 198, 214, 231, 247, 263, 280, 296, 313, 329, 346, 362, 378, 395, 411, 428, 444, 461, 477, 493, 510]

const weapon_fames = [0, 9, 17, 25, 33, 41, 50, 58, 99, 111, 156, 307, 492, 533, 574, 615, 656, 697, 738, 779, 820, 861, 902, 943, 984, 1025, 1066, 1107, 1148, 1189, 1230, 1271]

type AmplifyRarityWeigths = {
    [rarity in ItemRarity]: RarityWeigth
}

interface RarityWeigth {
    rate: number
    growth: number
    base: number
}

class Amplify implements ItemSlot {
    name: string = "amplify"

    effect(column: EquipColumn, context: CalcData) {
        const { item, data } = column
        if (item) {
            const has_cursed = !!data.get("has_cursed")
            if (!has_cursed) {
                return
            }

            const upgrade_level = (data.get("upgrade_level") as number) ?? 0

            const adapt_status = data.get("adapt_status") as QuadraStatus
            let { level = 100, rarity = "epic" } = item
            const { rate, growth, base } = amplify_rarity_weigths[rarity]
            const rs = level
                .plus(rate)
                .multiply(growth)
                .multiply(amplify_coeff[upgrade_level].divide(100))
                .plus(base)
                .plus(upgrade_level < 12 ? 0 : 5)
                .floor()

            const fames = item.part == "weapon" ? weapon_fames : item.rarity == "mythic" ? mythic_fames : epic_fames
            const fame = fames[upgrade_level]
            context.changeFame(fame, `upgrade[${item.part}]`)

            console.log(item.part, upgrade_level, fame)

            context.changeStatus({
                [adapt_status]: rs
            })
        }
    }
}

export default function createAmplifySlot(part: string): ItemSlot | undefined {
    if (part != "title") {
        return new Amplify()
    }
}
