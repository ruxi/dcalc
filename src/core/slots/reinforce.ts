import { CalcData } from "@/core/calc"
import { Equip, EquipColumn } from "@/core/equips"
import { isBuffer } from "@/store"
import { Item, ItemRarity, ItemSlot } from "../item"
import data from "@/assets/data/weapon_type_ceoff.json"

const weapon_type_ceoff: {
    [key: string]: number[]
} = data
const earring_reinforce_ceoff = [
    0, 83, 124, 166, 207, 248, 314, 370, 426, 482, 621, 970, 1455, 1941, 2911, 4043, 5175, 7116, 9056, 10997, 13099, 15363, 17627, 19891, 22155, 24420, 26684, 28948, 31212, 33476, 35740, 38004
]
const leftright_refinforce_ceoff = [
    0, 60, 90, 120, 150, 180, 210, 247, 285, 322, 360, 675, 1013, 1350, 2025, 2813, 3600, 4950, 6300, 7650, 9113, 10688, 12263, 13838, 15413, 16988, 18563, 20138, 21713, 23288, 24863, 26438
]

const weapon_reinforce_coeff = [
    0, 2, 2.6, 3.6, 4.7, 5.8, 6.9, 8.2, 11, 14.6, 18.7, 26.9, 36.7, 43, 49.2, 55.4, 61.7, 68, 74.3, 80.6, 86.9, 93.2, 99.5, 105.8, 112.1, 118.3, 124.6, 130.9, 137.1, 143.4, 149.7, 156.0
]

type RarityWeighs = {
    [rarity in ItemRarity]: {
        rate: number
        growth: number
    }
}

const special_rarity_weaights: RarityWeighs = {
    uncommon: {
        rate: 30,
        growth: 0.7
    },
    rare: {
        rate: 26,
        growth: 1
    },
    unique: {
        rate: 28,
        growth: 1.25
    },
    legendary: {
        rate: 29,
        growth: 1.35
    },
    epic: {
        rate: 30,
        growth: 1.45
    },
    mythic: {
        rate: 31,
        growth: 1.55
    }
}

const weapon_rarity_weigths: RarityWeighs = {
    uncommon: {
        rate: 8,
        growth: 0.7
    },
    rare: {
        rate: 10,
        growth: 1
    },
    unique: {
        rate: 12,
        growth: 1.25
    },
    legendary: {
        rate: 13,
        growth: 1.35
    },
    epic: {
        rate: 14,
        growth: 1.45
    },
    mythic: {
        rate: 15,
        growth: 1.55
    }
}

function computeEarringsReinforce(item: Equip, upgrade_level: number, context: CalcData) {
    let { level = 100, rarity = "epic" } = item

    const { rate, growth } = special_rarity_weaights[rarity]

    const triatk = level.plus(rate).divide(2400).multiply(growth).multiply(earring_reinforce_ceoff[upgrade_level])

    context.changeStatus({
        triatk
    })
}

function computeLeftrightReinforce(item: Equip, upgrade_level: number, context: CalcData) {
    let { level = 100, rarity = "epic" } = item

    const { rate, growth } = special_rarity_weaights[rarity]

    const quadra = level.plus(rate).divide(2400).multiply(growth).multiply(leftright_refinforce_ceoff[upgrade_level])

    context.changeStatus({
        quadra
    })
}

function computeWeaponReinforce(item: Equip, upgrade_level: number, context: CalcData) {
    let { level = 100, rarity = "epic" } = item

    const { rate, growth } = weapon_rarity_weigths[rarity]

    let [phyatk, magatk] = weapon_type_ceoff[item.type]

    const count = level.plus(rate).divide(8).multiply(growth).multiply(weapon_reinforce_coeff[upgrade_level])

    phyatk = phyatk.multiply(count)
    magatk = magatk.multiply(count)
    context.changeStatus({
        phyatk,
        magatk
    })
}

/**
 *  强化
 */

class Reinforce implements ItemSlot {
    name: string = "reinforce"

    effect(column: EquipColumn, context: CalcData) {
        const { item, data } = column
        let upgrade_level = (data.get("upgrade_level") as number) ?? 0
        if (item) {
            switch (item.part) {
                case "weapon":
                    computeWeaponReinforce(item, upgrade_level, context)
                    break
                case "subequip":
                case "magicstones":
                    computeLeftrightReinforce(item, upgrade_level, context)
                    break
                case "earrings":
                    computeEarringsReinforce(item, upgrade_level, context)
            }
        }
    }
}

const supports = ["weapon", "subequip", "magicstones", "earrings"]

export default function createRefineSlot(part: string): ItemSlot | undefined {
    if (supports.includes(part)) {
        return new Reinforce()
    }
    return undefined
}
