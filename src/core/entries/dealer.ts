/**
 * @Author: Kritsu
 * @Date:   2021/11/12 14:56:13
 * @Last Modified by:   Kritsu
 * @Last Modified time: 2021/11/17 18:36:03
 */

export type DealerKey = keyof DealerEntry

/** ( 伤害词条)*/
export interface DealerEntry {
    /** (技能攻击力)*/
    skill_attack: number

    /** (最终伤害)*/
    final_damage: number

    /** (百分比三攻)*/
    triatk_per: number

    /** (百分比力智)*/
    intstr_per: number

    /** (暴击伤害)*/
    critical_damage: number

    /** (额外伤害)*/
    additional_damage: number

    /** (附加伤害)*/
    bonus_damage: number

    /** (属性附加伤害)*/
    bonus_elemental_damage: number

    /** (持续伤害)*/
    continuous_damage: number

    /** (独立乘算的伤害(例如药剂和宠物技能))*/
    standalone_damage: number
}

export interface DealerEntryExtra {}

export interface DealerEntryOptions extends Partial<DealerEntry> {}

export function createDelearEntry(options: DealerEntryOptions = {}): DealerEntry {
    let {
        skill_attack = 0,
        final_damage = 0,
        triatk_per = 0,
        intstr_per = 0,
        critical_damage = 0,
        additional_damage = 0,
        bonus_damage = 0,
        bonus_elemental_damage = 0,
        continuous_damage = 0,
        standalone_damage = 0
    } = options

    return {
        skill_attack,
        final_damage,
        triatk_per,
        intstr_per,
        critical_damage,
        additional_damage,
        bonus_damage,
        bonus_elemental_damage,
        continuous_damage,
        standalone_damage
    }
}

export function mergeDealerEntry(dealer_entry: DealerEntry, options: DealerEntryOptions = {}) {
    const new_entry = createDelearEntry(options)
    const keys = new Set<string>(Object.keys(dealer_entry).concat(Object.keys(new_entry)))

    for (let key of keys) {
        let k = key as DealerKey
        let oldValue = dealer_entry[k] ?? 0
        let newValue = new_entry[k] ?? 0
        // 技攻计算公式  skill_attack =(x + 1) * (y+1) - 1 = xy + x +y
        let value = key == "skill_attack" ? oldValue.perMulti(newValue) : oldValue.plus(newValue)
        value = parseFloat(value.toFixed(2))
        dealer_entry[k] = value
    }
}
