import { CalcData } from "../calc"
import { Item } from "../item"

export interface Siroco extends Item {}

export const siroco_parts = ["ring", "pants", "subequip"] as const

export type SirocoPart = typeof siroco_parts[number]

export function fuse_pants(data: CalcData) {
    data.changeBuff({
        buff_intstr_per: 0.03
    })
}
export function fuse_ring(data: CalcData) {
    data.changeBuff({
        awake_intstr_per: 0.03
    })
}
export function fuse_subequip(data: CalcData) {
    data.changeBuff({
        passive_alter: 80
    })
}
