import assassin from "./assassin"
import lodos from "./lodos"
import nameless from "./nameless"
import nex from "./nex"
import roxy from "./roxy"

export const siroco_names = ["nex", "assassin", "roxy", "nameless", "lodos"]

export const sirocos = [nex, assassin, roxy, nameless, lodos]
    .reduce((a, b) => a.concat(b))
    .map(e => {
        e.fame = 116
        return e
    })

export * from "./siroco"
