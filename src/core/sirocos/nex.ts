import { hasSiroco, useCharacterStore } from "@/store"
import { fuse_ring, fuse_subequip } from "."
import { fuse_pants, Siroco } from "./siroco"

const nexs = [
    {
        part: "pants",
        suit_name: "nex",
        effect(data) {
            fuse_pants(data)
            if (hasSiroco("nex_ring")) {
                data.changeBuff({
                    awake_intstr_add: 40
                })
            }
        }
    },
    {
        part: "ring",
        suit_name: "nex",
        effect(data) {
            fuse_ring(data)
            if (hasSiroco("nex_subequip")) {
                data.changeBuff({
                    passive_alter: 80
                })
            }
        }
    },
    {
        suit_name: "nex",
        part: "subequip",
        effect(data) {
            fuse_subequip(data)
            if (hasSiroco("nex_pants")) {
                data.changeBuff({
                    buff_triatk_per: 0.02
                })
            }
        }
    }
] as Siroco[]

export default nexs.map(e => {
    e.name = `${e.suit_name}_${e.part}`
    e.icon = `/icon/siroco/${e.name}.png`
    return e
})
