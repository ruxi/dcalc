import { hasSiroco } from "@/store"
import { fuse_ring, fuse_subequip } from "."
import { fuse_pants, Siroco } from "./siroco"

const assassins = [
    {
        part: "pants",
        suit_name: "assassin",
        effect(data) {
            fuse_pants(data)
            if (hasSiroco("asassin_ring")) {
                data.changeBuff({
                    awake_intstr_add: 28
                })
            }
        }
    },
    {
        part: "ring",
        suit_name: "assassin",
        effect(data) {
            fuse_ring(data)
            if (hasSiroco("asassin_subequip")) {
                data.changeBuff({
                    passive_alter: 55
                })
            }
        }
    },
    {
        part: "subequip",
        suit_name: "assassin",
        effect(data) {
            fuse_subequip(data)
            if (hasSiroco("asassin_pants")) {
                data.changeBuff({
                    buff_intstr_per: 0.01,
                    buff_triatk_per: 0.01
                })
            }
        }
    }
] as Siroco[]

export default assassins.map(e => {
    e.name = `${e.suit_name}_${e.part}`
    e.icon = `/icon/siroco/${e.name}.png`
    return e
})
