import { hasSiroco } from "@/store"
import { fuse_ring, fuse_subequip } from "."
import { fuse_pants, Siroco } from "./siroco"

const lodos = [
    {
        part: "pants",
        suit_name: "lodos",
        effect(data) {
            fuse_pants(data)
            if (hasSiroco("lodos_ring")) {
                data.changeBuff({
                    awake_intstr_per: 0.02
                })
            }
        }
    },
    {
        part: "ring",
        suit_name: "lodos",
        effect(data) {
            fuse_ring(data)
            if (hasSiroco("lodos_subequip")) {
                data.changeBuff({
                    buff_intstr_per: 0.02
                })
            }
        }
    },
    {
        part: "subequip",
        suit_name: "lodos",
        effect(data) {
            fuse_subequip(data)
            if (hasSiroco("lodos_pants")) {
                data.changeBuff({
                    buff_triatk_per: 0.01,
                    passive_alter: 30
                })
            }
        }
    }
] as Siroco[]

export default lodos.map(e => {
    e.name = `${e.suit_name}_${e.part}`
    e.icon = `/icon/siroco/${e.name}.png`
    return e
})
