import { hasSiroco, useCharacterStore } from "@/store"
import { fuse_ring, fuse_subequip } from "."
import { fuse_pants, Siroco } from "./siroco"

const nameless = [
    {
        part: "pants",
        suit_name: "nameless",
        effect(data) {
            fuse_pants(data)
            if (hasSiroco("nameless_ring")) {
                data.changeBuff({
                    awake_intstr_add: 20,
                    awake_intstr_per: 0.01
                })
            }
        }
    },
    {
        part: "ring",
        suit_name: "nameless",
        effect(data) {
            fuse_ring(data)
            if (hasSiroco("nameless_subequip")) {
                data.changeBuff({
                    passive_alter: 55
                })
            }
        }
    },
    {
        part: "subequip",
        suit_name: "nameless",
        effect(data) {
            fuse_subequip(data)
            if (hasSiroco("nameless_pants")) {
                data.changeBuff({
                    buff_triatk_per: 0.01,
                    passive_alter: 30
                })
            }
        }
    }
] as Siroco[]

export default nameless.map(e => {
    e.name = `${e.suit_name}_${e.part}`
    e.icon = `/icon/siroco/${e.name}.png`
    return e
})
