import { hasSiroco, useCharacterStore } from "@/store"
import { fuse_ring } from "."
import { fuse_pants, fuse_subequip, Siroco } from "./siroco"

const roxys = [
    {
        part: "pants",
        suit_name: "roxy",
        effect: fuse_pants
    },
    {
        part: "ring",
        suit_name: "roxy",

        effect: fuse_ring
    },
    {
        part: "subequip",
        suit_name: "roxy",

        effect: fuse_subequip
    }
] as Siroco[]

export default roxys.map(e => {
    e.name = `${e.suit_name}_${e.part}`
    e.icon = `/icon/siroco/${e.name}.png`
    return e
})
