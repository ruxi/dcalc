import { useCharacterStore } from "./../../store/character"
import { CalcData } from "./../calc"
import { CreateItems, Items } from "./../item"
import { Enchant } from "@/core/item"
import { Skill, SkillType } from "@/core/skill"
import { StatusOptions } from "@/core/status"
import { isBuffer } from "@/store"

export default [
    {
        name: "cpw_powatk_4070",
        parts: ["coat", "pants", "weapon"],
        tags: [""],
        fame: 130,
        effect(data) {
            data.changeStatus({
                intstr: 40,
                triatk: 70
            })
        }
    },
    {
        name: "cpw_intelligence_100",
        parts: ["coat", "pants", "weapon"],
        tags: [""],
        fame: 130,
        effect(data) {
            data.changeStatus({
                intelligence: 100
            })
        }
    },
    {
        name: "cpw_vitspi_50",
        parts: ["coat", "pants", "weapon"],
        tags: [""],
        fame: 130,
        effect(data) {
            data.changeStatus({
                vitspi: 50
            })
        }
    },
    {
        name: "cpw_intstr_75",
        fame: 108,
        parts: ["coat", "pants", "weapon"],
        effect(data) {
            data.changeStatus({
                intelligence: 75
            })
        }
    },
    {
        name: "accessory_quadra_70",
        fame: 130,
        parts: ["bracelet", "necklace", "ring"],
        effect(data) {
            data.changeStatus({
                quadra: 70
            })
        }
    },

    {
        name: "accessory_elemental_30",
        fame: 130,
        parts: ["bracelet", "necklace", "ring"],
        effect(data) {
            data.changeStatus({
                elemental_boost: 30
            })
        }
    },
    {
        name: "accessory_quadra_50",
        fame: 108,
        parts: ["bracelet", "necklace", "ring"],
        effect(data) {
            data.changeStatus({
                quadra: 50
            })
        }
    },

    {
        name: "accessory_elemental_25",
        fame: 108,
        parts: ["bracelet", "necklace", "ring"],
        effect(data) {
            data.changeStatus({
                elemental_boost: 25
            })
        }
    },
    {
        name: "subequip_quadra_100",
        fame: 158,
        parts: ["subequip"],
        effect(data) {
            data.changeStatus({
                quadra: 100
            })
        }
    },
    {
        name: "subequip_quadra_100",
        fame: 130,
        parts: ["subequip"],
        effect(data) {
            data.changeStatus({
                triatk: 70
            })
        }
    },

    {
        name: "magicstones_quadra_100",
        fame: 158,
        parts: ["magicstones"],
        effect(data) {
            data.changeStatus({
                quadra: 100
            })
        }
    },
    {
        name: "magicstones_elemental_100",
        fame: 158,
        parts: ["magicstones"],
        effect(data) {
            data.changeStatus({
                elemental_boost: 30
            })
        }
    },
    {
        name: "earrings_quadra_150",
        fame: 130,
        parts: ["earrings"],
        effect(data) {
            data.changeStatus({
                quadra: 150
            })
        }
    },
    {
        name: "earrings_quadra_125",
        fame: 108,
        parts: ["earrings"],
        effect(data) {
            data.changeStatus({
                quadra: 125
            })
        }
    },
    {
        name: "shoulder_skill_level", //春节技能护肩
        fame: 168,
        parts: ["shoulder"],
        effect(data) {
            data.changeSkill({
                start: 1,
                end: 50,
                value: 1,
                condition: {
                    type: SkillType.ACTIVE
                }
            })
            data.changeStatus({
                quadra: 75,
                triatk: 20
            })
        }
    },
    {
        name: "shoulder_skill_attack", // 春节技攻宝珠
        fame: 168,
        parts: ["shoulder"],
        effect(data) {
            data.changeDamage({
                skill_attack: 0.03
            })
            data.changeStatus({
                quadra: 75,
                triatk: 20
            })
        }
    },
    {
        name: "skill_level_add_50", //50技能+1的肩腰鞋
        fame: 108,
        parts: ["shoulder", "belt", "shoes"],
        effect(data) {
            data.changeSkill({
                start: 1,
                end: 50,
                value: 1,
                condition: {
                    type: SkillType.ACTIVE
                }
            })
        }
    },
    {
        name: "skill_level_add_30", //30技能+1的肩腰鞋
        fame: 78,
        parts: ["shoulder", "belt", "shoes"],
        effect(data) {
            data.changeSkill({
                start: 1,
                end: 30,
                value: 1,
                condition: {
                    type: SkillType.ACTIVE
                }
            })
        }
    },
    {
        name: "autumn_skill_add(quadra)", //国庆三攻腰鞋和春节四维腰鞋
        fame: 168,
        parts: ["belt", "shoes"],
        effect(data) {
            data.changeSkill({
                start: 1,
                end: 50,
                value: 1,
                condition: {
                    type: SkillType.ACTIVE
                }
            })
            data.changeStatus({ quadra: 45 })
        }
    },

    {
        name: "holiday_skill_add(triatk)", //国庆三攻腰鞋和春节四维腰鞋
        fame: 168,
        parts: ["belt", "shoes"],
        effect(data) {
            data.changeSkill({
                start: 1,
                end: 50,
                value: 1,
                condition: {
                    type: SkillType.ACTIVE
                }
            })
            data.changeStatus({ triatk: 36 })
        }
    },
    {
        name: "holiday_title_skillatk", //国庆三攻腰鞋和春节四维腰鞋
        fame: 185,
        parts: ["title"],
        effect(data) {
            data.changeStatus({ elemental_boost: 15, triatk: 40 })
            data.changeSkill({
                start: 1,
                end: 50,
                value: 1,
                condition: {
                    type: SkillType.ACTIVE
                }
            })
        }
    },
    {
        name: "holiday_title_skill_add", //国庆三攻腰鞋和春节四维腰鞋
        fame: 185,
        parts: ["title"],
        effect(data) {
            data.changeStatus({ elemental_boost: 15, triatk: 40 })
            data.changeDamage({
                skill_attack: 0.03
            })
        }
    },
    // 称号附魔 动态生成
    () => {
        const { character } = useCharacterStore()
        if (character) {
            const { skills } = character
            const skill_list = skills.map(e => e.skill).filter(e => e.need_level <= 85)
            const fn = function (value = 2) {
                return (e: Skill) => {
                    return {
                        name: `title_skill_${e.name}_add_${value}`,
                        fame: e.need_level == 50 && value == 2 ? 370 : 130,
                        parts: ["title"],
                        effect(data) {
                            data.changeSkill({
                                condition: {
                                    names: [e.name]
                                },
                                value
                            })
                        }
                    } as Enchant
                }
            }
            return [...skill_list.map(fn(2)), ...skill_list.map(fn(1))]
        }
        return []
    },
    ...createEnchants(
        ["pet"],
        ["strength", "intelligence"],
        [
            {
                fame: 158,
                value: 50
            }
        ]
    )
] as Items<Enchant>

function createEnchants(parts: string[], keys: string[], values: { fame: number; value: number }[]) {
    const array: Enchant[] = []
    for (let key of keys) {
        for (let { fame, value } of values) {
            const enchant: Enchant = {
                name: `${parts.map(e => e.charAt(0)).join("")}_${key}_${value}`,
                part: "enchant",
                fame,
                parts,
                effect(data) {
                    data.changeStatus({
                        [key]: value
                    })
                }
            }
            array.push(enchant)
        }
    }
    return array
}
