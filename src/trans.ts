import { defineTranslator } from "@/plugins/translator"
import zhcn from "@/assets/trans/zh-cn.json"

export default defineTranslator({
    locale: "zh-cn",
    data: [zhcn]
})
