import { CalcData } from "@/core/calc"
import { getCommentByContext } from "@/core/equips/comment"
import { effectModifier, getModifiers, modifier_keys, modifier_parts } from "@/core/modifiers"
import { useTrans } from "@/plugins/translator"
import { getEquip, isBuffer, useCharacterStore, useConfigStore } from "@/store"
import { mapWithCount } from "@/utils"
import { computed, defineComponent } from "vue"

export default defineComponent(() => {
    const modifierConfig = computed(() => {
        return useConfigStore().modifers
    })

    function changeModifier(part: string, type: string, key?: string, level = 4) {
        const characterStore = useCharacterStore()
        if (!key) {
            key = modifierConfig.value[part][0]
        }
        characterStore.setColumnData(part, "modifier", [type, key, level])
    }

    const commentsTrans = useTrans("comments")

    function getModifierEffect(part: string, level: number) {
        let key = getKey(part)
        const type = getType(part)
        const equip = getEquip(part)

        if (equip) {
            const defaults = equip.modifier ?? ["none", 0]

            if (key == "none") {
                key = defaults[0]
            }

            const list = getModifiers(part, type)

            const modifier = list.find(e => e.name == key)

            if (modifier) {
                const data = new CalcData()
                const is_buffer = isBuffer()
                data.disable_dealer = is_buffer
                data.disable_buffer = !is_buffer
                effectModifier(modifier, data, defaults[1], level)
                return getCommentByContext(data)
            }
        }
        return ["none"]
    }

    function onChange(part: string) {
        return (level: number) => {
            changeModifier(part, getType(part), getKey(part), level)
        }
    }

    function onChangeType(part: string) {
        return (type: string) => {
            changeModifier(part, type, getKey(part), getLevel(part))
        }
    }

    function onChangeKey(part: string) {
        return (key: string) => {
            const type = getType(part)
            key = type == "awake" ? "awake_".concat(key) : key
            changeModifier(part, type, key, getLevel(part))
        }
    }

    function getKey(part: string) {
        return modifierConfig.value[part][1]
    }

    function getType(part: string) {
        return modifierConfig.value[part][0] ?? "custom"
    }

    function getLevel(part: string) {
        const key = getKey(part)
        return key == "none" ? 0 : Math.max(1, modifierConfig.value[part][2]) //只要key不为none level最低为1
    }

    function isDisabledType(part: string) {
        const type = getType(part)
        return ["auto", "none"].includes(type) || (type == "awake" && isBuffer())
    }

    function isDisableValue(part: string) {
        const key = getKey(part)
        return isDisabledType(part) || key == "none"
    }

    function labelValue(level: number) {
        let value = [`{$${level}}`]
        if (level == 1) {
            value.push("({min_value})")
        } else if (level == 4) {
            value.push("({max_value})")
        }
        return trans(value.join(" "))
    }

    const trans = useTrans()

    return () => {
        return (
            <div>
                {modifier_parts.map(part => (
                    <>
                        <div class="flex h-8 items-center">
                            <span class="text-sm w-15" v-trans={part}></span>
                            <calc-select class="w-15" modelValue={getType(part)} onUpdate:modelValue={onChangeType(part)}>
                                <calc-option value="none" label={trans}></calc-option>
                                <calc-option value="custom" label={trans}></calc-option>
                                <calc-option value="auto" label={trans}></calc-option>
                                {part == "weapon" && <calc-option value="awake" label={trans}></calc-option>}
                            </calc-select>
                            <calc-select class="w-40" disabled={isDisabledType(part)} width={160} modelValue={getKey(part)} onUpdate:modelValue={onChangeKey(part)}>
                                <calc-option value="none" label={trans}></calc-option>
                                {modifier_keys.map(e => (
                                    <calc-option value={e} label={trans}></calc-option>
                                ))}
                            </calc-select>
                            <calc-select class="w-48" disabled={isDisableValue(part)} modelValue={getLevel(part)} onUpdate:modelValue={onChange(part)} width={200}>
                                {mapWithCount(1, 5, i => {
                                    return <calc-option value={i} label={labelValue(i)}></calc-option>
                                })}
                            </calc-select>
                        </div>
                        <div class="w-full text-hex-#937639 text-sm h-auto">
                            {getModifierEffect(part, getLevel(part)).map(e => (
                                <div v-text={commentsTrans(e)}></div>
                            ))}
                        </div>
                    </>
                ))}
            </div>
        )
    }
})
