import { Equip, equips, equip_parts, getEquipImage, getSocketColor } from "@/core/equips"
import { useTrans } from "@/plugins/translator"
import { computed, defineComponent, ref, toRaw, watch } from "vue"
import { useDetailsStore } from "@/store/details"
import { getEquip, useCharacterStore } from "@/store"
import { mapWithCount } from "@/utils"
import { CalcData } from "@/core/calc"
import { getCommentByContext } from "@/core/equips/comment"
import { compareItem, Emblem, Enchant, rateItem, SocketMaps } from "@/core/item"
import { suits } from "@/core/equips"

export default defineComponent({
    name: "overview",
    setup() {
        const detailsStore = useDetailsStore()
        const characterStore = useCharacterStore()

        const equip_list = computed(() => equips.filter(e => e.part == detailsStore.part))

        function last() {
            goto(-1)
        }

        function next() {
            goto(1)
        }

        function goto(step = 1) {
            let i = equip_parts.indexOf(detailsStore.part) + step
            if (i >= equip_parts.length) {
                i = 0
            }
            if (i < 0) {
                i = equip_parts.length - 1
            }
            detailsStore.setPart(equip_parts[i])
        }

        const current_equip = computed<Equip | undefined>({
            get() {
                return getEquip(detailsStore.part)
            },
            set(val) {
                characterStore.setEquip(detailsStore.part, val)
            }
        })

        const current_image = computed(() => {
            return getEquipImage(current_equip.value)
        })

        const can_upgrade_parts = equip_parts.filter(e => e != "title")

        const can_upgrade = computed(() => can_upgrade_parts.includes(detailsStore.part))

        const current_upgrade = computed<number>({
            set(val) {
                let parts: string[] = []
                if (global_change.value) {
                    parts.push(...can_upgrade_parts)
                } else {
                    parts.push(detailsStore.part)
                }
                for (let part of parts) {
                    characterStore.setColumnData(part, "upgrade_level", val)
                }
            },
            get() {
                return (characterStore.getColumnData(detailsStore.part, "upgrade_level") as number) ?? 0
            }
        })

        const has_cursed = computed<boolean>({
            get() {
                return !!characterStore.getColumnData(detailsStore.part, "has_cursed")
            },
            set(val) {
                let parts: string[] = []
                if (global_change.value) {
                    parts = can_upgrade_parts
                } else {
                    parts.push(detailsStore.part)
                }
                for (let part of parts) {
                    characterStore.setColumnData(part, "has_cursed", val)
                }
            }
        })

        // 附魔列表 只包括当前部位可用
        const enchanting_list = computed(() => {
            return characterStore.enchant_list
                .filter(e => e.parts.includes(detailsStore.part) && rateItem(e))
                .sort(compareItem)
                .map(e => {
                    const data = new CalcData()
                    e.effect?.(data)
                    let text = getCommentByContext(data).join(" ")
                    return {
                        ...e,
                        text
                    }
                })
        })

        //当前部位的附魔
        const enchanting = computed<string>({
            get() {
                return characterStore.getColumnData(detailsStore.part, "enchanting")
            },
            set(val) {
                let parts = [detailsStore.part]
                if (global_change.value) {
                    const enchant = characterStore.enchant_list.find(e => e.name == val) as Enchant
                    if (enchant?.parts) {
                        parts = enchant.parts
                    }
                }
                for (let part of parts) {
                    characterStore.setColumnData(part, "enchanting", val)
                }
            }
        })

        const suit_list = computed(() => {
            let list = suits.filter(e => e.parts.includes(detailsStore.part)).map(e => e.name) as string[]
            list = Array.from(new Set(list))
            return list
        })

        const suit_name = computed<string | undefined>({
            get() {
                return current_equip.value?.suit_name
            },
            set(val) {
                if (val) {
                    characterStore.setEquipSuit(val)
                }
            }
        })

        function changeSocket(name: string, left: boolean = true) {
            const glc = global_change.value
            const socket_key = `socket_${left ? "left" : "right"}`
            let parts = [detailsStore.part]
            if (glc) {
                const emblem = characterStore.emblem_list.find(e => e.name == name)
                if (emblem) {
                    parts.length = 0
                    const colors = emblem.colors ?? []
                    for (let color of colors) {
                        const values = SocketMaps[color]
                        parts.push(...values)
                    }
                }
            }
            for (let part of parts) {
                if (glc) {
                    characterStore.setColumnData(part, "socket_left", name)
                    characterStore.setColumnData(part, "socket_right", name)
                } else {
                    characterStore.setColumnData(part, socket_key, name)
                }
            }
        }

        // 镶嵌栏1
        const socket_left = computed<string>({
            get() {
                return characterStore.getColumnData(detailsStore.part, "socket_left")
            },
            set(val) {
                changeSocket(val, true)
                characterStore.setColumnData(detailsStore.part, "socket_left", val)
            }
        })

        const socket_right = computed<string>({
            get() {
                return characterStore.getColumnData(detailsStore.part, "socket_right")
            },
            set(val) {
                changeSocket(val, false)
            }
        })

        const has_socket = computed(() => {
            return !["weapon", "earrings", "title"].includes(current_equip.value?.part ?? "")
        })

        const has_socket_right = computed(() => {
            return !["subequip", "magicstones"].includes(current_equip.value?.part ?? "") && !global_change.value
        })

        const emblem_list = computed<Emblem[]>(() => {
            const color = getSocketColor(detailsStore.part)
            if (color) {
                return characterStore.emblem_list.filter(e => e.colors.includes(color))
            }
            return []
        })

        const global_change = ref(false)

        const trans = useTrans()
        const itemTrans = useTrans("items")
        const abbrCommentsTrans = useTrans("comments.abbr")

        return () => {
            return (
                <div class="flex flex-wrap">
                    <div>
                        <img src={current_image.value} class="w-10 h-10 mr-4 mb-4"></img>
                    </div>
                    <div class="flex w-100 h-8 items-center">
                        <span class="text-sm w-10">装备:</span>
                        <calc-select v-model={current_equip.value} class="w-40 mr-4">
                            <calc-option label={itemTrans("none")} value={null}></calc-option>
                            {equip_list.value.map(equip => (
                                <calc-option label={itemTrans(equip.name)} value={equip}></calc-option>
                            ))}
                        </calc-select>
                        {can_upgrade.value ? (
                            <>
                                <calc-select v-model={has_cursed.value} class="w-8 mr-4">
                                    <calc-option label={itemTrans("reinforce")} value={false}></calc-option>
                                    <calc-option label={itemTrans("amplify")} value={true}></calc-option>
                                </calc-select>
                                <calc-select v-model={current_upgrade.value} class="w-8">
                                    {mapWithCount(0, 32, i => (
                                        <calc-option label={`${i}`} value={i}></calc-option>
                                    ))}
                                </calc-select>
                            </>
                        ) : (
                            <div></div>
                        )}
                    </div>
                    {suit_list.value.length ? (
                        <div class="flex w-100 h-8 items-center">
                            <span class="text-sm w-10">套装:</span>
                            <calc-select v-model={suit_name.value} class="w-80">
                                <calc-option label={itemTrans("none")} value={null}></calc-option>
                                {suit_list.value.map(e => (
                                    <calc-option label={itemTrans} value={e}></calc-option>
                                ))}
                            </calc-select>
                        </div>
                    ) : undefined}
                    <div class="flex w-100 h-8 items-center">
                        <span class="text-sm w-10">附魔:</span>
                        <calc-select v-model={enchanting.value} class="w-80">
                            <calc-option label={trans("none")} value={null}></calc-option>
                            {enchanting_list.value.map(e => (
                                <calc-option label={abbrCommentsTrans(e.text)} value={e.name}></calc-option>
                            ))}
                        </calc-select>
                    </div>

                    {has_socket.value ? (
                        <div class="flex w-120 h-8 items-center flex-wrap">
                            <span class="text-sm w-10">徽章:</span>
                            <calc-select v-model={socket_left.value} class="w-50">
                                <calc-option label={itemTrans("none")} value={null}></calc-option>
                                {emblem_list.value.map(e => (
                                    <calc-option label={itemTrans} value={e.name}></calc-option>
                                ))}
                            </calc-select>
                            {has_socket_right.value ? (
                                <calc-select v-model={socket_right.value} class="w-50 ml-2">
                                    <calc-option label={itemTrans("none")} value={null}></calc-option>
                                    {emblem_list.value.map(e => (
                                        <calc-option label={itemTrans} value={e.name}></calc-option>
                                    ))}
                                </calc-select>
                            ) : (
                                <div></div>
                            )}
                        </div>
                    ) : (
                        <div></div>
                    )}

                    <div class="w-full">
                        <calc-checkbox v-model:checked={global_change.value} label={trans("global_change")}></calc-checkbox>
                    </div>
                </div>
            )
        }
    }
})
