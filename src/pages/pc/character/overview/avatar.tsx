import { Avatar, avatar_parts, suits } from "@/core/avatar"
import { CalcData } from "@/core/calc"
import { getSuitByItems } from "@/core/equips"
import { getCommentByContext } from "@/core/equips/comment"
import { Item, toItems } from "@/core/item"
import { useTrans } from "@/plugins/translator"
import { useCharacterStore } from "@/store"
import { computed, defineComponent, ref, watch } from "vue"

export default defineComponent({
    name: "avatar",
    setup() {
        const characterStore = useCharacterStore()
        const { avatar_columns, avatar_list } = characterStore

        function getAvatarList(part: string) {
            return avatar_list.filter(e => e.part == part)
        }

        function getAvatar(part: string) {
            return avatar_columns[part].item?.name ?? null
        }

        function setAvatar(part: string) {
            return (name: string) => {
                const avatar = avatar_list.find(e => e.name == name)
                characterStore.setAvatar(part, avatar)
            }
        }

        function getOptions(part: string) {
            const avatar = avatar_columns[part].item as Avatar
            if (avatar) {
                let { options } = avatar
                options = toItems(options)
                if (options) {
                    return options.map(e => {
                        const data = new CalcData()
                        if (typeof e == "function") {
                            e(data)
                        } else {
                            data.changeStatus(e)
                        }
                        const line = getCommentByContext(data).join("")
                        return line
                    })
                }
            }
            return []
        }

        function getOptionIndex(part: string) {
            return (avatar_columns[part].data.get("option_index") as number) ?? 0
        }

        function setOptionIndex(part: string) {
            return (index = 0) => {
                avatar_columns[part].data.set("option_index", index)
            }
        }

        function label(e: Avatar) {
            let title = e.title ?? e.name
            if (typeof title == "function") {
                title = title()
            }
            return itemsTrans(title)
        }

        const ignore_parts = ["skin", "aura", "weapon"]

        const suit_parts = avatar_parts.filter(e => !ignore_parts.includes(e))

        const suit_name = computed({
            get() {
                const items: Item[] = []
                for (let part of suit_parts) {
                    if (ignore_parts.includes(part)) {
                        continue
                    }
                    const { item } = avatar_columns[part]
                    if (item) {
                        items.push(item)
                    }
                }
                const suit_list = getSuitByItems(items, suits)

                return suit_list.find(e => e.needCount >= 8)?.name ?? "none"
            },
            set(val: string) {
                characterStore.setAvatarSuit(val)
            }
        })

        const trans = useTrans()

        const itemsTrans = useTrans("items")

        const commentsTrans = useTrans("comments.abbr")

        const is_collapse = ref(false)
        function collapse() {
            is_collapse.value = !is_collapse.value
        }

        return () => {
            function renderOptions(part: string) {
                const options: JSX.Element[] = getOptions(part).map((e, index) => <calc-option label={commentsTrans(e)} value={index}></calc-option>)

                if (options.length == 0) {
                    options.push(<calc-option label={itemsTrans("none")} value={null}></calc-option>)
                }
                return options
            }

            return (
                <div class="flex flex-wrap">
                    {avatar_parts.map(part => (
                        <div class="w-full flex h-8 items-center">
                            <div class="w-10 text-sm" v-trans={part}></div>
                            <calc-select class="mx-2 w-48 text-xs" modelValue={getAvatar(part)} onUpdate:modelValue={setAvatar(part)}>
                                <calc-option label={itemsTrans("none")} value={null}></calc-option>
                                {getAvatarList(part).map(e => (
                                    <calc-option label={label(e)} value={e.name}></calc-option>
                                ))}
                            </calc-select>
                            <calc-select class="w-60 text-xs" modelValue={getOptionIndex(part)} onUpdate:modelValue={setOptionIndex(part)}>
                                {renderOptions(part)}
                            </calc-select>
                        </div>
                    ))}

                    <div class="w-full flex h-8 items-center">
                        <div class="w-10 text-sm" v-trans="suits"></div>
                        <calc-select class="mx-2 w-48 text-xs" v-model={suit_name.value}>
                            <calc-option label={itemsTrans} value="none"></calc-option>
                            <calc-option label={itemsTrans} value="unique_avatar"></calc-option>
                            <calc-option label={itemsTrans} value="rare_avatar"></calc-option>
                            <calc-option label={itemsTrans} value="holiday_avatar"></calc-option>
                        </calc-select>
                    </div>
                </div>
            )
        }
    }
})
