import { defineComponent, inject, ref, Ref, watch } from "vue"

import equip from "./equip"
import avatar from "./avatar"
import fuse from "./fuse"
import { useTrans } from "@/plugins/translator"
import modifiers from "./modifiers"
import { useDetailsStore } from "@/store/details"

export default defineComponent({
    name: "overview",
    components: {
        equip,
        avatar,
        fuse,
        modifiers
    },
    setup() {
        const trans = useTrans()

        const current = ref("equip")

        function setCurrent(val: string) {
            return (modelValue: boolean) => {
                if (modelValue) {
                    current.value = val
                } else {
                    current.value = "none"
                }
            }
        }

        function isCurrent(val: string) {
            return current.value == val
        }

        const detailStore = useDetailsStore()
        watch(
            () => detailStore.part,
            () => {
                setCurrent("equip")(true)
            }
        )

        return () => {
            return (
                <div class="w-128 overflow-hidden">
                    <calc-collapse modelValue={isCurrent("equip")} onUpdate:modelValue={setCurrent("equip")} title={trans("equip")}>
                        <equip />
                    </calc-collapse>
                    <calc-collapse modelValue={isCurrent("avatar")} onUpdate:modelValue={setCurrent("avatar")} title={trans("avatar")}>
                        <avatar />
                    </calc-collapse>

                    <calc-collapse modelValue={isCurrent("fuse")} onUpdate:modelValue={setCurrent("fuse")} title={trans("fuse_property")}>
                        <fuse />
                    </calc-collapse>

                    <calc-collapse modelValue={isCurrent("modifiers")} onUpdate:modelValue={setCurrent("modifiers")} title={trans("modifier_property")}>
                        <modifiers />
                    </calc-collapse>
                </div>
            )
        }
    }
})
