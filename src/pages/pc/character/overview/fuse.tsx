import { loadSuits, findEquipBySuit } from "@/core/equips"
import { useTrans } from "@/plugins/translator"
import { useCharacterStore } from "@/store/character"
import { Siroco, sirocos, siroco_names } from "@/core/sirocos"
import EquipIcon from "@/components/internal/equip-icon"
import { defineComponent } from "vue"
import { hasOzma, hasSiroco } from "@/store"
import { Ozma, ozmas } from "@/core/ozmas"
import { mapWithCount } from "@/utils"
import OzmaImg from "@/assets/img/ozma.png"
import SirocoImg from "@/assets/img/siroco.png"

export default defineComponent({
    name: "raid",
    components: {
        EquipIcon
    },
    setup() {
        const characterStore = useCharacterStore()

        function findBySirocoSuit(suit_name: string) {
            return sirocos.filter(e => e.suit_name == suit_name)
        }

        function useSiroco(siroco: Siroco | undefined, index: number) {
            if (siroco && hasSiroco(siroco.name)) {
                siroco = undefined
            }
            characterStore.setSiroco(index, siroco)
        }

        function useSirocoSuit(suit_name: string) {
            let siroco_array: (Siroco | undefined)[] = findBySirocoSuit(suit_name)
            if (siroco_array.every(e => e && hasSiroco(e.name))) {
                siroco_array = siroco_array.map(_ => undefined)
            }
            siroco_array.forEach((siroco, index) => characterStore.setSiroco(index, siroco))
        }

        function useOzma(ozma: Ozma | undefined, index: number) {
            if (ozma && hasOzma(ozma.name, index)) {
                ozma = undefined
            }
            characterStore.setOzma(index, ozma)
        }

        function useOzmaSuit(name: string) {
            let ozma = ozmas.find(e => e.name === name)
            if (ozma) {
                if (characterStore.ozma_columns.every(e => e?.name == name)) {
                    ozma = undefined
                }
                for (let i = 0; i < 5; i++) {
                    characterStore.setOzma(i, ozma)
                }
            }
        }

        function iconStyle(img: string, x: number, y: number) {
            return {
                backgroundImage: `url(${img})`,
                backgroundPositionX: x == 0 ? 0 : `${-x * 28}px`,
                backgroundPositionY: y == 0 ? 0 : `${-y * 28}px`
            }
        }

        const trans = useTrans()

        return () => {
            return (
                <div class="flex flex-wrap">
                    <div class="w-full flex">
                        <div>
                            {siroco_names.map((siroco_name, j) => (
                                <div class="flex items-center mb-2">
                                    <calc-button class="mr-2" onClick={() => useSirocoSuit(siroco_name)} v-trans={siroco_name}></calc-button>
                                    {findBySirocoSuit(siroco_name).map((siroco, i) => (
                                        <div class="mr-2 relative" onClick={() => useSiroco(siroco, i)}>
                                            <div class={{ "icon-lay": !hasSiroco(siroco.name) }}></div>
                                            <div class="icon" style={iconStyle(SirocoImg, i, j)}></div>
                                        </div>
                                    ))}
                                </div>
                            ))}
                        </div>

                        <div>
                            {ozmas.map((ozma, j) => (
                                <div class="flex items-center mb-2">
                                    <calc-button class="mr-2" onClick={() => useOzmaSuit(ozma.name)} v-trans={ozma.name}></calc-button>
                                    {mapWithCount(0, 5, i => (
                                        <div class="mr-2 relative" onClick={() => useOzma(ozma, i)}>
                                            <div class={{ "icon-lay": !hasOzma(ozma.name, i) }}></div>
                                            <div class="icon" style={iconStyle(OzmaImg, i, j)}></div>
                                        </div>
                                    ))}
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            )
        }
    }
})
