import { SkillResult } from "@/core/skill"
import { useTrans } from "@/plugins/translator"
import { useCalcStore, getSkillImg, useCharacterStore } from "@/store"
import { to_percent } from "@/utils"
import { mapValues } from "lodash"
import { buffer } from "stream/consumers"
import { computed, defineComponent, ref } from "vue"
import "./results.scss"

interface ResultData extends Record<string, number | string> {
    name: string
    level: number
}

export default defineComponent(() => {
    const calcStore = useCalcStore()
    const characterStore = useCharacterStore()

    const show_buffer = ref(true)

    const data = computed(() => {
        const result = calcStore.result
        const list: ResultData[] = []
        const total_headers = show_buffer.value ? ["strength", "intelligence", "phyatk", "magatk", "indatk"] : ["damage"]

        const headers = total_headers.concat(show_buffer.value ? ["adapt_number"] : [])

        const total: Record<string, number> = {}

        if (result) {
            const { skill_datas } = result
            if (!!skill_datas) {
                for (let i = 0; i < skill_datas.length; i++) {
                    const skill = skill_datas[i]
                    const { value = {}, adapt_number, damage } = skill_datas[i]
                    const row: Record<string, number> = { ...value, adapt_number, damage }
                    const filter_data: Record<string, number> = {}
                    let row_total = 0
                    for (let key of headers) {
                        const v = row[key]
                        filter_data[key] = v
                        if (total_headers.includes(key) && key) {
                            total[key] = v.plus(total[key] ?? 0)
                            row_total = v.plus(row_total)
                        }
                    }
                    if (row_total > 0) {
                        list.push({ name: skill.name, level: skill.level, ...filter_data })
                    }
                }
            }
        }
        return { list, headers, total }
    })

    const rate = computed(() => {
        const { result } = useCalcStore()
        return result?.rate ?? 0
    })

    const modifier_list = computed(() => {
        const { result } = useCalcStore()
        if (result) {
            const { modifiers } = result.context
            if (modifiers.length > 0) {
                return modifiers.map(e => {
                    let [k, v] = e
                    return `{${k}}:{${v}}`
                })
            }
        }
        return []
    })

    function getValue(value: number = 0) {
        return value > 0 ? value.round().toLocaleString("zh", {}) : ""
    }

    function filterRow(haders: string[]) {
        return ({ value }: SkillResult) => Object.entries(value).some(([k, v]) => haders.includes(k) && !!v && v > 0)
    }

    const trans = useTrans()

    return () => {
        const { list, headers, total } = data.value
        return (
            <table class="w-auto h-140 bg-hex-#00000078">
                <thead>
                    <tr class="results-header flex w-full">
                        <th class="results-col  text-#b99460 items-center" v-trans="skill"></th>

                        {headers.map(h => {
                            return <th class="results-col text-#b99460 items-center" v-trans={h}></th>
                        })}
                    </tr>
                </thead>
                <tbody class="results-body">
                    {list.map(e => (
                        <tr class="flex">
                            <td class="results-col">
                                <img title={trans(`$skills.${e.name}`)} class="mr-1" src={getSkillImg(e.name)} />
                                <span v-text={`Lv.${e.level}`}></span>
                            </td>
                            {headers.map(v => (
                                <td class="results-col" v-text={getValue(e[v] as number)}></td>
                            ))}
                        </tr>
                    ))}
                </tbody>
                <tfoot>
                    <tr class="flex">
                        <td class="results-col" v-text={to_percent(rate.value, 0, "%")}></td>
                        {headers.map(h => (
                            <td class="results-col" v-text={getValue(total[h])}></td>
                        ))}
                    </tr>
                    <tr class="flex">
                        <td class="results-col" v-trans="modifier_property"></td>
                        {modifier_list.value.map(m => (
                            <td class="results-col" v-trans={m}></td>
                        ))}
                    </tr>
                </tfoot>
            </table>
        )
    }
})
