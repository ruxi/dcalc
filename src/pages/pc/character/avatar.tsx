import { computed, defineComponent, inject, Ref } from "vue"
import avatarSrc from "@/assets/img/avatar_ui.png"
import { backgroundImage } from "@/utils/style"
import { CurrentAlterSymbol } from "@/constants"
import { Avatar, avatar_parts } from "@/core/avatar"
import { useCharacterStore } from "@/store"
import ItemIcon from "@/components/internal/item-icon"
import { getCommentByContext } from "@/core/equips/comment"
import { CalcData } from "@/core/calc"
import ItemComments from "@/components/internal/item-comments"

export default defineComponent({
    name: "avatar_column",
    components: { ItemIcon, ItemComments },

    setup() {
        const alter = inject(CurrentAlterSymbol) as Ref<string>

        const { avatar_columns, pet_column } = useCharacterStore()

        const display_parts = avatar_parts

        function partIconStyle(part: string) {
            let x = 132
            let y = 8
            let index = display_parts.findIndex(p => p == part)

            if (index == 1) {
                y += 32
            } else if (index > 0) {
                index -= 2
                x += 32
                x += (index % 3) * 32
                y += Math.floor(index / 3) * 32
            }

            return {
                left: `${x}px`,
                top: `${y}px`
            }
        }

        const enchantingComments = computed(() => {
            const { slots } = pet_column
            const enchant = slots.find(e => e.name == "enchanting")
            if (enchant) {
                const data = new CalcData()
                enchant.effect(pet_column, data)
                return getCommentByContext(data)
            }
            return []
        })

        function getOptionsComments(part: string) {
            const { item, data } = avatar_columns[part]
            if (item) {
                const avatar = item as Avatar
                let context = new CalcData()
                const index = (data.get("option_index") as number) ?? 0
                const options = avatar.options ?? []
                if (options?.length) {
                    const option = options[index]
                    if (typeof option == "function") {
                        option(context)
                    } else {
                        context.changeStatus(option)
                    }
                    return getCommentByContext(context).join("")
                }
            }
            return "none_option_value"
        }

        return () => {
            return (
                <div class="w-66.5 h-100 relative box-border">
                    <div class="w-66.5 h-116" style={backgroundImage(avatarSrc)}></div>
                    <div class="h-40">
                        <div class="w-266px h-40 bg-left" style={backgroundImage(`/resources/${alter.value}/character.png`)}></div>
                        {display_parts.map(p => (
                            <div class="w-7 h-7 absolute" style={partIconStyle(p)}>
                                <item-icon column={avatar_columns[p]}>
                                    <item-comments item={avatar_columns[p].item}></item-comments>
                                    <div class="text-hex-#5eb6fa mt-2" v-trans:comments={"[{option_value}]"}></div>
                                    <div v-trans:comments={getOptionsComments(p)}></div>
                                </item-icon>
                            </div>
                        ))}
                        {pet_column.item ? (
                            <div
                                class="icon"
                                style={{
                                    left: "25px",
                                    top: "192px"
                                }}
                            >
                                <item-icon column={pet_column}>
                                    <item-comments item={pet_column.item}></item-comments>
                                    <div class="equip-tooltip-comments effect">
                                        {enchantingComments.value.map((comment, index) => (
                                            <div class="enchanting-color" key={index} v-trans={comment}></div>
                                        ))}
                                    </div>
                                </item-icon>
                            </div>
                        ) : (
                            <div></div>
                        )}
                    </div>
                </div>
            )
        }
    }
})
