import { getSkillImg, useCharacterStore } from "@/store"
import { computed, defineComponent } from "vue"

export default defineComponent({
    name: "skill",
    setup() {
        const characterStore = useCharacterStore()
        const skills = computed(() => {
            return (characterStore.character?.skills ?? []).map(e => e.skill).sort((a, b) => a.need_level - b.need_level)
        })

        return () => (
            <div>
                {skills.value.map(e => (
                    <div class="w-auto h-8 flex items-center">
                        <img class="w-7 h-7 mr-2" src={getSkillImg(e.name)}></img>
                        <span class="text-sm w-40" v-trans:skills={e.name}></span>
                    </div>
                ))}
            </div>
        )
    }
})
