import { backgroundImage } from "@/utils/style"
import ProfileSrc from "@/assets/img/profile.png"
import { CurrentAlterSymbol, CurrentPartSymbol } from "@/constants"
import { equip_parts } from "@/core/equips"
import EquipIcon from "@/components/internal/equip-icon"

import { computed, Ref, defineComponent, inject } from "vue"
import { createCharacterStatus, Status } from "@/core/status"
import { useCalcStore, useCharacterStore } from "@/store"
import "./profile.scss"
import { useDetailsStore } from "@/store/details"
import { useRoute, useRouter } from "vue-router"
import { to_percent } from "@/utils/index"
import { BufferEntry } from "@/core/entries/buffer"
import { DealerEntry } from "@/core/entries/dealer"
import { StatusKey } from "../../../core/status"

type ProfileStatus = Partial<Status & BufferEntry & DealerEntry>

export default defineComponent({
    name: "profile",
    components: { EquipIcon },

    setup() {
        const alter = inject(CurrentAlterSymbol) as Ref<string>

        //面板显示的顺序
        const display_parts = ["shoulder", "coat", "pants", "belt", "shoes", "pet", "weapon", "title", "bracelet", "necklace", "subequip", "ring", "earrings", "magicstones"]
        const calcStore = useCalcStore()
        const characterStore = useCharacterStore()
        const detailsStore = useDetailsStore()

        const character = characterStore.character

        function setPart(part: string) {
            const router = useRouter()
            const route = useRoute()

            return () => {
                detailsStore.setPart(part)
                router.replace({
                    path: route.path,
                    query: {
                        tab: 0
                    }
                })
            }
        }

        function partIconStyle(part: string) {
            let x = 13
            let y = 28
            let index = display_parts.findIndex(p => p == part)

            if (index >= 6) {
                x += 180
                index -= 6
            }

            x += (index % 2) * 32
            y += Math.floor(index / 2) * 32

            return {
                left: `${x}px`,
                top: `${y}px`
            }
        }

        const current_status = computed<ProfileStatus>(() => {
            const rs = calcStore.result
            if (rs) {
                const { context } = rs
                const { status, deal_entry, buff_entry } = context
                return { ...status, ...deal_entry, ...buff_entry }
            }
            return createCharacterStatus()
        })

        return () => {
            function renderStatus(key: StatusKey, handle?: (val: number) => string) {
                if (key) {
                    let value: string | number | undefined = current_status.value[key]

                    if (value) {
                        if (handle) {
                            value = handle(value)
                        } else {
                            value = value?.round()
                        }
                        return (
                            <div class="character-status-row flex justify-between px-1 h-5 box-border">
                                <span v-trans={key}></span>
                                <span class="text-hex-#4aa256" v-text={value?.toString()}></span>
                            </div>
                        )
                    }
                }
            }

            function renderElementalStatus(key: "ice" | "fire" | "light" | "dark") {
                const value = current_status.value[`${key}_boost`] ?? 0

                const is_forced = true

                return (
                    <div class={["flex", "justify-center"].concat(is_forced ? "" : "text-hex-#707070")}>
                        <span v-trans={key}></span>
                        <span class={is_forced && "text-hex-#4aa256"} v-text={`(${value})`}></span>
                    </div>
                )
            }

            return (
                <div class="bg-hex-#0000001f bg-no-repeat  relative box-border w-266px h-100 text-hex-#937639 text-xs">
                    <div class="w-266px h-100" style={backgroundImage(ProfileSrc)}></div>
                    <div class="absolute top-182px h-6 flex items-center justify-center w-full  text-xs">
                        <div class="flex items-center h-full">
                            <span style={backgroundImage("/icon/profile/fame.png", { width: "15px", height: "13px", position: "relative" })}></span>
                            <span v-trans="fame"></span>
                            <span class="text-hex-#4aa256 mx-1" v-text={calcStore.fame.toLocaleString()}></span>
                        </div>
                    </div>
                    <div class="h-50">
                        <div class="w-266px h-40 bg-bottom" style={backgroundImage(`/resources/${alter.value}/character.png`)}></div>
                        {equip_parts.map(p => (
                            <div onClick={setPart(p)} class="absolute w-7 h-7" style={partIconStyle(p)}>
                                <equip-icon column={characterStore.equip_columns[p]}></equip-icon>
                            </div>
                        ))}
                    </div>

                    <div class="character-core-status">
                        {renderStatus("strength")}
                        {renderStatus("intelligence")}
                        {renderStatus("vitality")}
                        {renderStatus("spirit")}
                        {renderStatus("phyatk")}
                        {renderStatus("magatk")}
                        {renderStatus("indatk")}
                        <div></div>
                    </div>
                    <div>{renderStatus("skill_attack", val => to_percent(val, 1, "%"))}</div>
                    <div class="flex px-1 box-border justify-between w-full h-5 absolute bottom-1">
                        <span v-trans="attack_elemental"></span>
                        <div class="flex">
                            {renderElementalStatus("fire")}
                            <span>/</span>
                            {renderElementalStatus("ice")}
                            <span>/</span>
                            {renderElementalStatus("light")}
                            <span>/</span>
                            {renderElementalStatus("dark")}
                        </div>
                    </div>
                </div>
            )
        }
    }
})
