import { computed, defineComponent, provide, ref } from "vue"
import { useRoute, useRouter } from "vue-router"
import { CurrentAlterSymbol } from "@/constants"
import OverviewTab from "@/pages/pc/character/overview"
import EquipIcon from "@/components/internal/equip-icon"
import ITab from "@/components/tabs/tab"
import ITabs from "@/components/tabs/tabs"
import Results from "./results"
import { useCharacterStore } from "@/store/character"
import Profile from "./profile"
import Avatar from "./avatar"

import { useTranslator } from "@/plugins/translator"
import SuitsTab from "./suits"
import SkillsTab from "./skills"

export default defineComponent({
    name: "character",
    components: { ITabs, ITab, EquipIcon },
    setup() {
        const router = useRouter()

        const alter = computed(() => {
            const route = useRoute()
            return (route.params.alter as string) ?? ""
        })

        const characterStore = useCharacterStore()

        // 创建新角色
        characterStore.newCharacter(alter.value)

        const backgroundStyle = {
            backgroundImage: `url(/resources/${alter.value}/background.png)`
        }

        provide(CurrentAlterSymbol, alter)

        const tab_index = computed({
            set(tab: number) {
                router.replace({
                    query: { tab }
                })
            },
            get() {
                const route = useRoute()
                return parseInt(route.query.tab as string)
            }
        })

        if (!tab_index.value) {
            tab_index.value = 0
        }

        const ProfileTabs = [Profile, Avatar]

        const profile_tab_index = ref(0)

        const translator = useTranslator()

        function switchLanguage() {
            translator?.setLocale("en")
        }
        const tabs = [OverviewTab, SkillsTab, SuitsTab]

        return () => {
            const TabPane = tabs[tab_index.value] ?? <div></div>
            const ProfileTabPane = ProfileTabs[profile_tab_index.value] ?? <div></div>
            return (
                <div class="text-color w-full h-full p-8">
                    <div class="mask-full character-bg-image bg-no-repeat bg-cover -z-2" style={backgroundStyle}></div>
                    <div class="mask-full bg-hex-#000000cc"></div>
                    <div class="w-full h-full flex z-2">
                        <calc-tabs vertical v-model={tab_index.value}>
                            <calc-tab width={60} to="/pc" v-trans="back"></calc-tab>
                            {tabs.map((tab, index) => (
                                <calc-tab width={60} value={index} v-trans={tab.name}></calc-tab>
                            ))}
                        </calc-tabs>
                        <div class="w-140 p-5">
                            <TabPane></TabPane>
                        </div>

                        <div class="w-80">
                            <calc-tabs v-model={profile_tab_index.value}>
                                {ProfileTabs.map((e, i) => (
                                    <calc-tab width={60} value={i} v-trans={e.name}></calc-tab>
                                ))}
                            </calc-tabs>
                            <ProfileTabPane />
                            <calc-button v-trans="English" onClick={() => switchLanguage()}></calc-button>
                        </div>

                        <Results />
                    </div>
                </div>
            )
        }
    }
})
