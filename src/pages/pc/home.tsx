import { defineComponent } from "vue"
import { useRouter } from "vue-router"
import { useTrans } from "@/plugins/translator"
import { hasCharacter } from "@/store"
import jobs from "@/assets/data/jobs.json"
import "./home.scss"

function job_icon(job: string) {
    return {
        backgroundImage: `url(/icon/jobs/${job}.png)`
    }
}

function job_alter_icon(job: string) {
    return {
        backgroundImage: `url(/img/${job}.png)`
    }
}

export default defineComponent(() => {
    const router = useRouter()
    const trans = useTrans("jobs")

    async function selectCharacter(alter: string) {
        if (!hasCharacter(alter)) {
            alert(`未开放的角色:'${trans(alter)}'`)
            return
        }
        router.push({ name: "pc-character", params: { alter } })
    }

    return () => {
        return (
            <div class="home bg-cover w-full h-full p-5 overflow-auto">
                {jobs.map(job => (
                    <div class="flex flex-row" key={job.name}>
                        <div class="job-icon-box w-30 h-20 flex flex-wrap justify-center items-center relative bg-no-repeat bg-center">
                            <div class="w-12 h-12 bg-center bg-no-repeat" style={job_icon(job.name)}></div>
                            <div class="absolute w-30 text-sm text-center font-bold bottom-0 text-color" v-trans:jobs={job.name}></div>
                        </div>
                        {job.alters.map(alter => (
                            <div onClick={() => selectCharacter(alter)} class="job-box w-30 h-22.5 m-1 box-border relative duration-300 cursor-pointer" key={alter}>
                                <div class="job-border w-full h-full absolute z-2 bg-no-repeat"></div>
                                <div class="job-mask bg-hex-#ffd7002e w-full h-full invisible z-999 absolute"></div>
                                <div class="absolute bottom-1 w-full text-sm text-center text-hex-#bea347" v-trans:jobs={alter}></div>
                                <div class="w-full h-full bg-no-repeat bg-auto bg-clip-content overflow-hidden z-1" style={job_alter_icon(alter)}></div>
                            </div>
                        ))}
                    </div>
                ))}
            </div>
        )
    }
})
