/**
 * @Author: Kritsu
 * @Date:   2021/11/09 10:40:34
 * @Last Modified by:   Kritsu
 * @Last Modified time: 2021/11/17 18:37:32
 */
import { RouterView } from "vue-router"
import { pinia } from "@/store"
import router from "@/router"
import trans from "@/trans"
import components from "@/components"
import { createApp } from "vue"

import "@/utils/number"
import "@/assets/style/app.scss"
import "uno.css"

createApp(RouterView).use(pinia).use(router).use(components).use(trans).mount("#app")
