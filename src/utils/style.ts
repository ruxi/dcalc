import { CSSProperties } from "vue"

export function backgroundImage(src: string, defaults: CSSProperties = {}): CSSProperties {
    return {
        backgroundImage: `url(${src})`,
        backgroundRepeat: "no-repeat",
        zIndex: -1,
        position: "absolute",
        ...defaults
    }
}
