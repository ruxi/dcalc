import decimal from "decimal.js"

const trimFloat = false

Number.prototype.plus = function (num: number = 0) {
    const value = this.valueOf()
    return trimFloat ? new decimal(value).plus(num).toNumber() : value + num
}
Number.prototype.minus = function (num: number = 0) {
    const value = this.valueOf()

    return trimFloat ? new decimal(value).minus(num).toNumber() : value - num
}
Number.prototype.multiply = function (num: number = 1) {
    const value = this.valueOf()

    return trimFloat ? new decimal(value).mul(num).toNumber() : value * num
}
Number.prototype.divide = function (num: number = 1) {
    const value = this.valueOf()

    return trimFloat ? new decimal(value).div(num).toNumber() : value / num
}

//  10% * 10% = 21% 百分比的相乘计算
Number.prototype.perMulti = function (num: number = 0) {
    return this.multiply(num).plus(this.valueOf()).plus(num)
}

Number.prototype.exactlyDivide = function (num: number = 1) {
    return Math.floor(this.valueOf() / num)
}

Number.prototype.round = function (digit = 0) {
    return Number(this.valueOf().toFixed(digit))
}

Number.prototype.floor = function (digit = 0) {
    return Math.floor(this.valueOf())
}
