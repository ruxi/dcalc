export function isMobile() {
    var ua = navigator.userAgent.toLowerCase()
    const arr = ["ipad", "iphone os", "midp", "rv:1.2.3.4", "ucweb", "android", "windows ce", "windows mobile"]
    return arr.some(e => ua.includes(e))
}
