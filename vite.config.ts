/**
 * @Author: Kritsu
 * @Date:   2021/11/09 10:40:34
 * @Last Modified by:   Kritsu
 * @Last Modified time: 2021/11/17 18:38:32
 */
import { defineConfig } from "vite"
import vue from "@vitejs/plugin-vue"
import uno from "unocss/vite"
import { VitePWA as pwa } from "vite-plugin-pwa"

import presetDefault from "@unocss/preset-uno"
import presetIcons from "@unocss/preset-icons"

import compression from "vite-plugin-compression"

import jsx from "@vitejs/plugin-vue-jsx"
import { resolve } from "path"

// https://vitejs.dev/config/
export default defineConfig({
    resolve: {
        alias: {
            "@": resolve(__dirname, "./src")
        }
    },
    server: {
        fs: {
            strict: false // 工作区以外文件的访问
        }
    },
    plugins: [
        vue(),
        jsx(),
        uno({
            presets: [presetDefault(), presetIcons()]
        }),
        compression({
            ext: ".gz", //gz br
            algorithm: "gzip" //brotliCompress gzip
        }),
        pwa({
            base: "/",
            strategies: "generateSW",
            manifest: {
                name: "dcalc",
                short_name: "dcalc",
                theme_color: "#ffffff",
                icons: [
                    {
                        src: "pwa-192x192.png", // <== don't add slash, for testing
                        sizes: "192x192",
                        type: "image/png"
                    },
                    {
                        src: "/pwa-512x512.png", // <== don't remove slash, for testing
                        sizes: "512x512",
                        type: "image/png"
                    },
                    {
                        src: "pwa-512x512.png", // <== don't add slash, for testing
                        sizes: "512x512",
                        type: "image/png",
                        purpose: "any maskable"
                    }
                ]
            }
        })
    ]
})
