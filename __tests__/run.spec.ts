import { useTrans, createTranslator } from "@/plugins/translator"

import { mount, config } from "@vue/test-utils"
import translator from "@/trans"
import equip_ids from "@/assets/data/equip_ids.json"
import { equips } from "@/core/equips"
import fs from "fs"
import { h } from "vue"

const KEY = "HelloWorld"
const VALUE = "你好世界"

config.global.plugins.push(translator)

const ids: any = equip_ids

test("Language Test", () => {
    mount(() => {
        const trans = useTrans("equips")
        const dict: any = {}
        for (let equip of equips) {
            dict[equip.name] = ids[trans(equip.name)]
        }
        //fs.writeFileSync("./equip_ids.json", JSON.stringify(dict))
        return () => h("div")
    })
})
