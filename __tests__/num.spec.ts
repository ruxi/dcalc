import "@/utils/number_prod"

test("number test", () => {
    expect((0.1).plus(0.2)).toEqual(0.3)
    expect((0.4).minus(0.3)).toEqual(0.1)
    expect((1.1).multiply(1.1)).toEqual(1.21)
    expect((1.21).divide(1.1)).toEqual(1.1)
    expect((0.1).perMulti(0.1)).toEqual(0.21)
    expect((0).perMulti(0.21)).toEqual(0.21)
})
