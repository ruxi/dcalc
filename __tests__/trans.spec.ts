import { useTrans, createTranslator } from "@/plugins/translator"

import { defineComponent, h } from "vue"
import { mount, config } from "@vue/test-utils"

const KEY = "HelloWorld"
const VALUE = "你好世界"

const translator = createTranslator({
    locale: "zh-cn",
    data: [
        {
            locale: "zh-cn",
            dictionaries: {
                test: {
                    [KEY]: VALUE
                }
            }
        }
    ]
})

config.global.plugins.push(translator)

test("Language Test", () => {
    mount(() => {
        const trans = useTrans("test")
        expect(trans(KEY)).toEqual(VALUE)

        return () => h("div")
    })
})
