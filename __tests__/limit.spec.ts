import { parallel } from "@/utils/async"
test("Limit Test", async () => {
    let array: string[] = []
    for (let i = 0; i < 2000000; i++) {
        array.push(String.fromCharCode(97 + (i % 26)))
    }

    let c = 0

    let time = 0

    setInterval(() => {
        ++time
        const speed = c / time
    }, 1000)

    function fetch(s: any) {
        return new Promise<string>(resolve => {
            ++c
            resolve(s)
        })
    }
    console.time("s")

    let tasks0 = await parallel(array, 5, fetch)
    console.timeEnd("s")
    console.time("q")

    let tasks1 = await parallel(array, 20, fetch)
    console.timeEnd("q")

    expect(complement(tasks0, tasks1)).toHaveLength(0)
}, 2000000)

function complement<T>(arr0: Array<T>, arr1: Array<T>) {
    return arr0.filter(val => !(arr1.indexOf(val) > -1)).concat(arr1.filter(val => !(arr1.indexOf(val) > -1)))
}
