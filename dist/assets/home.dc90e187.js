import { f as m, x as i, j as e, l as t, n } from "./vendor.5859bc74.js"
import { u as c, h as u } from "./index.7098be47.js"
var _ = [
    { name: "swordman_male", alters: ["weapon_master", "soul_bringer", "berserker", "asura", "sword_ghost"] },
    { name: "swordman_female", alters: ["sword_master", "dark_templar", "demon_slayer", "vegabond", "blade"] },
    { name: "fighter_male", alters: ["nenmaster_male", "striker_male", "street_fighter_male", "grappler_male"] },
    { name: "fighter_female", alters: ["nenmaster_female", "striker_female", "street_fighter_female", "grappler_female"] },
    { name: "gunner_male", alters: ["ranger_male", "launcher_male", "mechanic_male", "spitfire_male"] },
    { name: "gunner_female", alters: ["ranger_female", "launcher_female", "mechanic_female", "spitfire_female"] },
    { name: "mage_male", alters: ["elemental_bomber", "glacial_master", "blood_mage", "swift_master", "dimension_walker"] },
    { name: "mage_female", alters: ["magiciant", "summoner", "battle_mage", "witch", "enchantress"] },
    { name: "priest_male", alters: ["crusader_male", "infighter", "exorcist", "avenger"] },
    { name: "priest_female", alters: ["crusader_female", "inquistor", "sorceress", "mistress"] },
    { name: "thief", alters: ["rogue", "necro", "kunoichi", "shadow_dancer"] },
    { name: "knight", alters: ["elven_knight", "chaos", "paladin", "dragon_knight"] },
    { name: "demonic_lancer", alters: ["vanguard", "duelist", "dragonian_lacner", "dark_lancer"] },
    { name: "gunblader", alters: ["hitman", "agent", "trouble_shooter", "specialist"] },
    { name: "extra", alters: ["creator", "dark_knight"] }
]
function f(r) {
    return { backgroundImage: `url(/icon/jobs/${r}.png)` }
}
function g(r) {
    return { backgroundImage: `url(/img/${r}.png)` }
}
var h = m(() => {
    const r = i(),
        s = c("jobs")
    async function o(a) {
        if (!u(a)) {
            alert(`\u672A\u5F00\u653E\u7684\u89D2\u8272:'${s(a)}'`)
            return
        }
        r.push({ name: "pc-character", params: { alter: a } })
    }
    return () =>
        e("div", { class: "home bg-cover w-full h-full p-5 overflow-auto" }, [
            _.map(a =>
                e("div", { class: "flex flex-row", key: a.name }, [
                    e("div", { class: "job-icon-box w-30 h-20 flex flex-wrap justify-center items-center relative bg-no-repeat bg-center" }, [
                        e("div", { class: "w-12 h-12 bg-center bg-no-repeat", style: f(a.name) }, null),
                        t(e("div", { class: "absolute w-30 text-sm text-center font-bold bottom-0 text-color" }, null), [[n("trans"), a.name, "jobs"]])
                    ]),
                    a.alters.map(l =>
                        e("div", { onClick: () => o(l), class: "job-box w-30 h-22.5 m-1 box-border relative duration-300 cursor-pointer", key: l }, [
                            e("div", { class: "job-border w-full h-full absolute z-2 bg-no-repeat" }, null),
                            e("div", { class: "job-mask bg-hex-#ffd7002e w-full h-full invisible z-999 absolute" }, null),
                            t(e("div", { class: "absolute bottom-1 w-full text-sm text-center text-hex-#bea347" }, null), [[n("trans"), l, "jobs"]]),
                            e("div", { class: "w-full h-full bg-no-repeat bg-auto bg-clip-content overflow-hidden z-1", style: g(l) }, null)
                        ])
                    )
                ])
            )
        ])
})
export { h as default }
