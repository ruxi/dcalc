function rr(e, t) {
    const n = Object.create(null),
        r = e.split(",")
    for (let s = 0; s < r.length; s++) n[r[s]] = !0
    return t ? s => !!n[s.toLowerCase()] : s => !!n[s]
}
const $o = "itemscope,allowfullscreen,formnovalidate,ismap,nomodule,novalidate,readonly",
    Ho = rr($o)
function gs(e) {
    return !!e || e === ""
}
function sr(e) {
    if (V(e)) {
        const t = {}
        for (let n = 0; n < e.length; n++) {
            const r = e[n],
                s = be(r) ? Do(r) : sr(r)
            if (s) for (const i in s) t[i] = s[i]
        }
        return t
    } else {
        if (be(e)) return e
        if (Ce(e)) return e
    }
}
const Bo = /;(?![^(]*\))/g,
    jo = /:(.+)/
function Do(e) {
    const t = {}
    return (
        e.split(Bo).forEach(n => {
            if (n) {
                const r = n.split(jo)
                r.length > 1 && (t[r[0].trim()] = r[1].trim())
            }
        }),
        t
    )
}
function ir(e) {
    let t = ""
    if (be(e)) t = e
    else if (V(e))
        for (let n = 0; n < e.length; n++) {
            const r = ir(e[n])
            r && (t += r + " ")
        }
    else if (Ce(e)) for (const n in e) e[n] && (t += n + " ")
    return t.trim()
}
const ie = {},
    At = [],
    $e = () => {},
    Uo = () => !1,
    qo = /^on[^a-z]/,
    mn = e => qo.test(e),
    or = e => e.startsWith("onUpdate:"),
    Re = Object.assign,
    ms = (e, t) => {
        const n = e.indexOf(t)
        n > -1 && e.splice(n, 1)
    },
    Ko = Object.prototype.hasOwnProperty,
    J = (e, t) => Ko.call(e, t),
    V = Array.isArray,
    Vt = e => wn(e) === "[object Map]",
    Wo = e => wn(e) === "[object Set]",
    Z = e => typeof e == "function",
    be = e => typeof e == "string",
    lr = e => typeof e == "symbol",
    Ce = e => e !== null && typeof e == "object",
    ws = e => Ce(e) && Z(e.then) && Z(e.catch),
    zo = Object.prototype.toString,
    wn = e => zo.call(e),
    Zo = e => wn(e).slice(8, -1),
    Vo = e => wn(e) === "[object Object]",
    cr = e => be(e) && e !== "NaN" && e[0] !== "-" && "" + parseInt(e, 10) === e,
    vn = rr(",key,ref,onVnodeBeforeMount,onVnodeMounted,onVnodeBeforeUpdate,onVnodeUpdated,onVnodeBeforeUnmount,onVnodeUnmounted"),
    _n = e => {
        const t = Object.create(null)
        return n => t[n] || (t[n] = e(n))
    },
    Yo = /-(\w)/g,
    ze = _n(e => e.replace(Yo, (t, n) => (n ? n.toUpperCase() : ""))),
    Qo = /\B([A-Z])/g,
    Ot = _n(e => e.replace(Qo, "-$1").toLowerCase()),
    bn = _n(e => e.charAt(0).toUpperCase() + e.slice(1)),
    ur = _n(e => (e ? `on${bn(e)}` : "")),
    Yt = (e, t) => !Object.is(e, t),
    fr = (e, t) => {
        for (let n = 0; n < e.length; n++) e[n](t)
    },
    En = (e, t, n) => {
        Object.defineProperty(e, t, { configurable: !0, enumerable: !1, value: n })
    },
    Jo = e => {
        const t = parseFloat(e)
        return isNaN(t) ? e : t
    }
let vs
const Xo = () => vs || (vs = typeof globalThis != "undefined" ? globalThis : typeof self != "undefined" ? self : typeof window != "undefined" ? window : typeof global != "undefined" ? global : {})
let Ze
const yn = []
class _s {
    constructor(t = !1) {
        ;(this.active = !0), (this.effects = []), (this.cleanups = []), !t && Ze && ((this.parent = Ze), (this.index = (Ze.scopes || (Ze.scopes = [])).push(this) - 1))
    }
    run(t) {
        if (this.active)
            try {
                return this.on(), t()
            } finally {
                this.off()
            }
    }
    on() {
        this.active && (yn.push(this), (Ze = this))
    }
    off() {
        this.active && (yn.pop(), (Ze = yn[yn.length - 1]))
    }
    stop(t) {
        if (this.active) {
            if ((this.effects.forEach(n => n.stop()), this.cleanups.forEach(n => n()), this.scopes && this.scopes.forEach(n => n.stop(!0)), this.parent && !t)) {
                const n = this.parent.scopes.pop()
                n && n !== this && ((this.parent.scopes[this.index] = n), (n.index = this.index))
            }
            this.active = !1
        }
    }
}
function bs(e) {
    return new _s(e)
}
function Go(e, t) {
    ;(t = t || Ze), t && t.active && t.effects.push(e)
}
function el() {
    return Ze
}
function tl(e) {
    Ze && Ze.cleanups.push(e)
}
const ar = e => {
        const t = new Set(e)
        return (t.w = 0), (t.n = 0), t
    },
    Es = e => (e.w & rt) > 0,
    ys = e => (e.n & rt) > 0,
    nl = ({ deps: e }) => {
        if (e.length) for (let t = 0; t < e.length; t++) e[t].w |= rt
    },
    rl = e => {
        const { deps: t } = e
        if (t.length) {
            let n = 0
            for (let r = 0; r < t.length; r++) {
                const s = t[r]
                Es(s) && !ys(s) ? s.delete(e) : (t[n++] = s), (s.w &= ~rt), (s.n &= ~rt)
            }
            t.length = n
        }
    },
    dr = new WeakMap()
let Qt = 0,
    rt = 1
const hr = 30,
    Jt = []
let gt
const mt = Symbol(""),
    pr = Symbol("")
class gr {
    constructor(t, n = null, r) {
        ;(this.fn = t), (this.scheduler = n), (this.active = !0), (this.deps = []), Go(this, r)
    }
    run() {
        if (!this.active) return this.fn()
        if (!Jt.includes(this))
            try {
                return Jt.push((gt = this)), sl(), (rt = 1 << ++Qt), Qt <= hr ? nl(this) : Cs(this), this.fn()
            } finally {
                Qt <= hr && rl(this), (rt = 1 << --Qt), wt(), Jt.pop()
                const t = Jt.length
                gt = t > 0 ? Jt[t - 1] : void 0
            }
    }
    stop() {
        this.active && (Cs(this), this.onStop && this.onStop(), (this.active = !1))
    }
}
function Cs(e) {
    const { deps: t } = e
    if (t.length) {
        for (let n = 0; n < t.length; n++) t[n].delete(e)
        t.length = 0
    }
}
let xt = !0
const mr = []
function Tt() {
    mr.push(xt), (xt = !1)
}
function sl() {
    mr.push(xt), (xt = !0)
}
function wt() {
    const e = mr.pop()
    xt = e === void 0 ? !0 : e
}
function Te(e, t, n) {
    if (!Ps()) return
    let r = dr.get(e)
    r || dr.set(e, (r = new Map()))
    let s = r.get(n)
    s || r.set(n, (s = ar())), Rs(s)
}
function Ps() {
    return xt && gt !== void 0
}
function Rs(e, t) {
    let n = !1
    Qt <= hr ? ys(e) || ((e.n |= rt), (n = !Es(e))) : (n = !e.has(gt)), n && (e.add(gt), gt.deps.push(e))
}
function Xe(e, t, n, r, s, i) {
    const o = dr.get(e)
    if (!o) return
    let l = []
    if (t === "clear") l = [...o.values()]
    else if (n === "length" && V(e))
        o.forEach((c, u) => {
            ;(u === "length" || u >= r) && l.push(c)
        })
    else
        switch ((n !== void 0 && l.push(o.get(n)), t)) {
            case "add":
                V(e) ? cr(n) && l.push(o.get("length")) : (l.push(o.get(mt)), Vt(e) && l.push(o.get(pr)))
                break
            case "delete":
                V(e) || (l.push(o.get(mt)), Vt(e) && l.push(o.get(pr)))
                break
            case "set":
                Vt(e) && l.push(o.get(mt))
                break
        }
    if (l.length === 1) l[0] && wr(l[0])
    else {
        const c = []
        for (const u of l) u && c.push(...u)
        wr(ar(c))
    }
}
function wr(e, t) {
    for (const n of V(e) ? e : [...e]) (n !== gt || n.allowRecurse) && (n.scheduler ? n.scheduler() : n.run())
}
const il = rr("__proto__,__v_isRef,__isVue"),
    Ns = new Set(
        Object.getOwnPropertyNames(Symbol)
            .map(e => Symbol[e])
            .filter(lr)
    ),
    ol = vr(),
    ll = vr(!1, !0),
    cl = vr(!0),
    As = ul()
function ul() {
    const e = {}
    return (
        ["includes", "indexOf", "lastIndexOf"].forEach(t => {
            e[t] = function (...n) {
                const r = X(this)
                for (let i = 0, o = this.length; i < o; i++) Te(r, "get", i + "")
                const s = r[t](...n)
                return s === -1 || s === !1 ? r[t](...n.map(X)) : s
            }
        }),
        ["push", "pop", "shift", "unshift", "splice"].forEach(t => {
            e[t] = function (...n) {
                Tt()
                const r = X(this)[t].apply(this, n)
                return wt(), r
            }
        }),
        e
    )
}
function vr(e = !1, t = !1) {
    return function (r, s, i) {
        if (s === "__v_isReactive") return !e
        if (s === "__v_isReadonly") return e
        if (s === "__v_raw" && i === (e ? (t ? Rl : Ls) : t ? ks : Fs).get(r)) return r
        const o = V(r)
        if (!e && o && J(As, s)) return Reflect.get(As, s, i)
        const l = Reflect.get(r, s, i)
        return (lr(s) ? Ns.has(s) : il(s)) || (e || Te(r, "get", s), t) ? l : me(l) ? (!o || !cr(s) ? l.value : l) : Ce(l) ? (e ? $s(l) : St(l)) : l
    }
}
const fl = Os(),
    al = Os(!0)
function Os(e = !1) {
    return function (n, r, s, i) {
        let o = n[r]
        if (!e && !yr(s) && ((s = X(s)), (o = X(o)), !V(n) && me(o) && !me(s))) return (o.value = s), !0
        const l = V(n) && cr(r) ? Number(r) < n.length : J(n, r),
            c = Reflect.set(n, r, s, i)
        return n === X(i) && (l ? Yt(s, o) && Xe(n, "set", r, s) : Xe(n, "add", r, s)), c
    }
}
function dl(e, t) {
    const n = J(e, t)
    e[t]
    const r = Reflect.deleteProperty(e, t)
    return r && n && Xe(e, "delete", t, void 0), r
}
function hl(e, t) {
    const n = Reflect.has(e, t)
    return (!lr(t) || !Ns.has(t)) && Te(e, "has", t), n
}
function pl(e) {
    return Te(e, "iterate", V(e) ? "length" : mt), Reflect.ownKeys(e)
}
const xs = { get: ol, set: fl, deleteProperty: dl, has: hl, ownKeys: pl },
    gl = {
        get: cl,
        set(e, t) {
            return !0
        },
        deleteProperty(e, t) {
            return !0
        }
    },
    ml = Re({}, xs, { get: ll, set: al }),
    _r = e => e,
    Cn = e => Reflect.getPrototypeOf(e)
function Pn(e, t, n = !1, r = !1) {
    e = e.__v_raw
    const s = X(e),
        i = X(t)
    t !== i && !n && Te(s, "get", t), !n && Te(s, "get", i)
    const { has: o } = Cn(s),
        l = r ? _r : n ? Cr : Xt
    if (o.call(s, t)) return l(e.get(t))
    if (o.call(s, i)) return l(e.get(i))
    e !== s && e.get(t)
}
function Rn(e, t = !1) {
    const n = this.__v_raw,
        r = X(n),
        s = X(e)
    return e !== s && !t && Te(r, "has", e), !t && Te(r, "has", s), e === s ? n.has(e) : n.has(e) || n.has(s)
}
function Nn(e, t = !1) {
    return (e = e.__v_raw), !t && Te(X(e), "iterate", mt), Reflect.get(e, "size", e)
}
function Ts(e) {
    e = X(e)
    const t = X(this)
    return Cn(t).has.call(t, e) || (t.add(e), Xe(t, "add", e, e)), this
}
function Ss(e, t) {
    t = X(t)
    const n = X(this),
        { has: r, get: s } = Cn(n)
    let i = r.call(n, e)
    i || ((e = X(e)), (i = r.call(n, e)))
    const o = s.call(n, e)
    return n.set(e, t), i ? Yt(t, o) && Xe(n, "set", e, t) : Xe(n, "add", e, t), this
}
function Is(e) {
    const t = X(this),
        { has: n, get: r } = Cn(t)
    let s = n.call(t, e)
    s || ((e = X(e)), (s = n.call(t, e))), r && r.call(t, e)
    const i = t.delete(e)
    return s && Xe(t, "delete", e, void 0), i
}
function Ms() {
    const e = X(this),
        t = e.size !== 0,
        n = e.clear()
    return t && Xe(e, "clear", void 0, void 0), n
}
function An(e, t) {
    return function (r, s) {
        const i = this,
            o = i.__v_raw,
            l = X(o),
            c = t ? _r : e ? Cr : Xt
        return !e && Te(l, "iterate", mt), o.forEach((u, f) => r.call(s, c(u), c(f), i))
    }
}
function On(e, t, n) {
    return function (...r) {
        const s = this.__v_raw,
            i = X(s),
            o = Vt(i),
            l = e === "entries" || (e === Symbol.iterator && o),
            c = e === "keys" && o,
            u = s[e](...r),
            f = n ? _r : t ? Cr : Xt
        return (
            !t && Te(i, "iterate", c ? pr : mt),
            {
                next() {
                    const { value: a, done: d } = u.next()
                    return d ? { value: a, done: d } : { value: l ? [f(a[0]), f(a[1])] : f(a), done: d }
                },
                [Symbol.iterator]() {
                    return this
                }
            }
        )
    }
}
function st(e) {
    return function (...t) {
        return e === "delete" ? !1 : this
    }
}
function wl() {
    const e = {
            get(i) {
                return Pn(this, i)
            },
            get size() {
                return Nn(this)
            },
            has: Rn,
            add: Ts,
            set: Ss,
            delete: Is,
            clear: Ms,
            forEach: An(!1, !1)
        },
        t = {
            get(i) {
                return Pn(this, i, !1, !0)
            },
            get size() {
                return Nn(this)
            },
            has: Rn,
            add: Ts,
            set: Ss,
            delete: Is,
            clear: Ms,
            forEach: An(!1, !0)
        },
        n = {
            get(i) {
                return Pn(this, i, !0)
            },
            get size() {
                return Nn(this, !0)
            },
            has(i) {
                return Rn.call(this, i, !0)
            },
            add: st("add"),
            set: st("set"),
            delete: st("delete"),
            clear: st("clear"),
            forEach: An(!0, !1)
        },
        r = {
            get(i) {
                return Pn(this, i, !0, !0)
            },
            get size() {
                return Nn(this, !0)
            },
            has(i) {
                return Rn.call(this, i, !0)
            },
            add: st("add"),
            set: st("set"),
            delete: st("delete"),
            clear: st("clear"),
            forEach: An(!0, !0)
        }
    return (
        ["keys", "values", "entries", Symbol.iterator].forEach(i => {
            ;(e[i] = On(i, !1, !1)), (n[i] = On(i, !0, !1)), (t[i] = On(i, !1, !0)), (r[i] = On(i, !0, !0))
        }),
        [e, n, t, r]
    )
}
const [vl, _l, bl, El] = wl()
function br(e, t) {
    const n = t ? (e ? El : bl) : e ? _l : vl
    return (r, s, i) => (s === "__v_isReactive" ? !e : s === "__v_isReadonly" ? e : s === "__v_raw" ? r : Reflect.get(J(n, s) && s in r ? n : r, s, i))
}
const yl = { get: br(!1, !1) },
    Cl = { get: br(!1, !0) },
    Pl = { get: br(!0, !1) },
    Fs = new WeakMap(),
    ks = new WeakMap(),
    Ls = new WeakMap(),
    Rl = new WeakMap()
function Nl(e) {
    switch (e) {
        case "Object":
        case "Array":
            return 1
        case "Map":
        case "Set":
        case "WeakMap":
        case "WeakSet":
            return 2
        default:
            return 0
    }
}
function Al(e) {
    return e.__v_skip || !Object.isExtensible(e) ? 0 : Nl(Zo(e))
}
function St(e) {
    return e && e.__v_isReadonly ? e : Er(e, !1, xs, yl, Fs)
}
function Ol(e) {
    return Er(e, !1, ml, Cl, ks)
}
function $s(e) {
    return Er(e, !0, gl, Pl, Ls)
}
function Er(e, t, n, r, s) {
    if (!Ce(e) || (e.__v_raw && !(t && e.__v_isReactive))) return e
    const i = s.get(e)
    if (i) return i
    const o = Al(e)
    if (o === 0) return e
    const l = new Proxy(e, o === 2 ? r : n)
    return s.set(e, l), l
}
function it(e) {
    return yr(e) ? it(e.__v_raw) : !!(e && e.__v_isReactive)
}
function yr(e) {
    return !!(e && e.__v_isReadonly)
}
function Hs(e) {
    return it(e) || yr(e)
}
function X(e) {
    const t = e && e.__v_raw
    return t ? X(t) : e
}
function It(e) {
    return En(e, "__v_skip", !0), e
}
const Xt = e => (Ce(e) ? St(e) : e),
    Cr = e => (Ce(e) ? $s(e) : e)
function Bs(e) {
    Ps() && ((e = X(e)), e.dep || (e.dep = ar()), Rs(e.dep))
}
function js(e, t) {
    ;(e = X(e)), e.dep && wr(e.dep)
}
function me(e) {
    return Boolean(e && e.__v_isRef === !0)
}
function xn(e) {
    return Ds(e, !1)
}
function xl(e) {
    return Ds(e, !0)
}
function Ds(e, t) {
    return me(e) ? e : new Tl(e, t)
}
class Tl {
    constructor(t, n) {
        ;(this._shallow = n), (this.dep = void 0), (this.__v_isRef = !0), (this._rawValue = n ? t : X(t)), (this._value = n ? t : Xt(t))
    }
    get value() {
        return Bs(this), this._value
    }
    set value(t) {
        ;(t = this._shallow ? t : X(t)), Yt(t, this._rawValue) && ((this._rawValue = t), (this._value = this._shallow ? t : Xt(t)), js(this))
    }
}
function vt(e) {
    return me(e) ? e.value : e
}
const Sl = {
    get: (e, t, n) => vt(Reflect.get(e, t, n)),
    set: (e, t, n, r) => {
        const s = e[t]
        return me(s) && !me(n) ? ((s.value = n), !0) : Reflect.set(e, t, n, r)
    }
}
function Us(e) {
    return it(e) ? e : new Proxy(e, Sl)
}
function Il(e) {
    const t = V(e) ? new Array(e.length) : {}
    for (const n in e) t[n] = Fl(e, n)
    return t
}
class Ml {
    constructor(t, n) {
        ;(this._object = t), (this._key = n), (this.__v_isRef = !0)
    }
    get value() {
        return this._object[this._key]
    }
    set value(t) {
        this._object[this._key] = t
    }
}
function Fl(e, t) {
    const n = e[t]
    return me(n) ? n : new Ml(e, t)
}
class kl {
    constructor(t, n, r) {
        ;(this._setter = n),
            (this.dep = void 0),
            (this._dirty = !0),
            (this.__v_isRef = !0),
            (this.effect = new gr(t, () => {
                this._dirty || ((this._dirty = !0), js(this))
            })),
            (this.__v_isReadonly = r)
    }
    get value() {
        const t = X(this)
        return Bs(t), t._dirty && ((t._dirty = !1), (t._value = t.effect.run())), t._value
    }
    set value(t) {
        this._setter(t)
    }
}
function He(e, t) {
    let n, r
    const s = Z(e)
    return s ? ((n = e), (r = $e)) : ((n = e.get), (r = e.set)), new kl(n, r, s || !r)
}
Promise.resolve()
function Ll(e, t, ...n) {
    const r = e.vnode.props || ie
    let s = n
    const i = t.startsWith("update:"),
        o = i && t.slice(7)
    if (o && o in r) {
        const f = `${o === "modelValue" ? "model" : o}Modifiers`,
            { number: a, trim: d } = r[f] || ie
        d ? (s = n.map(g => g.trim())) : a && (s = n.map(Jo))
    }
    let l,
        c = r[(l = ur(t))] || r[(l = ur(ze(t)))]
    !c && i && (c = r[(l = ur(Ot(t)))]), c && De(c, e, 6, s)
    const u = r[l + "Once"]
    if (u) {
        if (!e.emitted) e.emitted = {}
        else if (e.emitted[l]) return
        ;(e.emitted[l] = !0), De(u, e, 6, s)
    }
}
function qs(e, t, n = !1) {
    const r = t.emitsCache,
        s = r.get(e)
    if (s !== void 0) return s
    const i = e.emits
    let o = {},
        l = !1
    if (!Z(e)) {
        const c = u => {
            const f = qs(u, t, !0)
            f && ((l = !0), Re(o, f))
        }
        !n && t.mixins.length && t.mixins.forEach(c), e.extends && c(e.extends), e.mixins && e.mixins.forEach(c)
    }
    return !i && !l ? (r.set(e, null), null) : (V(i) ? i.forEach(c => (o[c] = null)) : Re(o, i), r.set(e, o), o)
}
function Pr(e, t) {
    return !e || !mn(t) ? !1 : ((t = t.slice(2).replace(/Once$/, "")), J(e, t[0].toLowerCase() + t.slice(1)) || J(e, Ot(t)) || J(e, t))
}
let Se = null,
    Ks = null
function Tn(e) {
    const t = Se
    return (Se = e), (Ks = (e && e.type.__scopeId) || null), t
}
function $l(e, t = Se, n) {
    if (!t || e._n) return e
    const r = (...s) => {
        r._d && hi(-1)
        const i = Tn(t),
            o = e(...s)
        return Tn(i), r._d && hi(1), o
    }
    return (r._n = !0), (r._c = !0), (r._d = !0), r
}
function Rr(e) {
    const {
        type: t,
        vnode: n,
        proxy: r,
        withProxy: s,
        props: i,
        propsOptions: [o],
        slots: l,
        attrs: c,
        emit: u,
        render: f,
        renderCache: a,
        data: d,
        setupState: g,
        ctx: v,
        inheritAttrs: C
    } = e
    let x, P
    const L = Tn(e)
    try {
        if (n.shapeFlag & 4) {
            const F = s || r
            ;(x = Ye(f.call(F, F, a, i, g, d, v))), (P = c)
        } else {
            const F = t
            ;(x = Ye(F.length > 1 ? F(i, { attrs: c, slots: l, emit: u }) : F(i, null))), (P = t.props ? c : Hl(c))
        }
    } catch (F) {
        ;(en.length = 0), Dn(F, e, 1), (x = Ie(Mt))
    }
    let S = x
    if (P && C !== !1) {
        const F = Object.keys(P),
            { shapeFlag: G } = S
        F.length && G & (1 | 6) && (o && F.some(or) && (P = Bl(P, o)), (S = nn(S, P)))
    }
    return n.dirs && (S.dirs = S.dirs ? S.dirs.concat(n.dirs) : n.dirs), n.transition && (S.transition = n.transition), (x = S), Tn(L), x
}
const Hl = e => {
        let t
        for (const n in e) (n === "class" || n === "style" || mn(n)) && ((t || (t = {}))[n] = e[n])
        return t
    },
    Bl = (e, t) => {
        const n = {}
        for (const r in e) (!or(r) || !(r.slice(9) in t)) && (n[r] = e[r])
        return n
    }
function jl(e, t, n) {
    const { props: r, children: s, component: i } = e,
        { props: o, children: l, patchFlag: c } = t,
        u = i.emitsOptions
    if (t.dirs || t.transition) return !0
    if (n && c >= 0) {
        if (c & 1024) return !0
        if (c & 16) return r ? Ws(r, o, u) : !!o
        if (c & 8) {
            const f = t.dynamicProps
            for (let a = 0; a < f.length; a++) {
                const d = f[a]
                if (o[d] !== r[d] && !Pr(u, d)) return !0
            }
        }
    } else return (s || l) && (!l || !l.$stable) ? !0 : r === o ? !1 : r ? (o ? Ws(r, o, u) : !0) : !!o
    return !1
}
function Ws(e, t, n) {
    const r = Object.keys(t)
    if (r.length !== Object.keys(e).length) return !0
    for (let s = 0; s < r.length; s++) {
        const i = r[s]
        if (t[i] !== e[i] && !Pr(n, i)) return !0
    }
    return !1
}
function Dl({ vnode: e, parent: t }, n) {
    for (; t && t.subTree === e; ) ((e = t.vnode).el = n), (t = t.parent)
}
const Ul = e => e.__isSuspense
function ql(e, t) {
    t && t.pendingBranch ? (V(e) ? t.effects.push(...e) : t.effects.push(e)) : Kc(e)
}
function Sn(e, t) {
    if (ve) {
        let n = ve.provides
        const r = ve.parent && ve.parent.provides
        r === n && (n = ve.provides = Object.create(r)), (n[e] = t)
    }
}
function Be(e, t, n = !1) {
    const r = ve || Se
    if (r) {
        const s = r.parent == null ? r.vnode.appContext && r.vnode.appContext.provides : r.parent.provides
        if (s && e in s) return s[e]
        if (arguments.length > 1) return n && Z(t) ? t.call(r.proxy) : t
    }
}
function zs(e) {
    return Z(e) ? { setup: e, name: e.name } : e
}
const Nr = e => !!e.type.__asyncLoader,
    Zs = e => e.type.__isKeepAlive
function Kl(e, t) {
    Vs(e, "a", t)
}
function Wl(e, t) {
    Vs(e, "da", t)
}
function Vs(e, t, n = ve) {
    const r =
        e.__wdc ||
        (e.__wdc = () => {
            let s = n
            for (; s; ) {
                if (s.isDeactivated) return
                s = s.parent
            }
            return e()
        })
    if ((In(t, r, n), n)) {
        let s = n.parent
        for (; s && s.parent; ) Zs(s.parent.vnode) && zl(r, t, n, s), (s = s.parent)
    }
}
function zl(e, t, n, r) {
    const s = In(t, e, r, !0)
    Ar(() => {
        ms(r[t], s)
    }, n)
}
function In(e, t, n = ve, r = !1) {
    if (n) {
        const s = n[e] || (n[e] = []),
            i =
                t.__weh ||
                (t.__weh = (...o) => {
                    if (n.isUnmounted) return
                    Tt(), Ft(n)
                    const l = De(t, n, e, o)
                    return yt(), wt(), l
                })
        return r ? s.unshift(i) : s.push(i), i
    }
}
const Ge =
        e =>
        (t, n = ve) =>
            (!jn || e === "sp") && In(e, t, n),
    Zl = Ge("bm"),
    Vl = Ge("m"),
    Yl = Ge("bu"),
    Ql = Ge("u"),
    Jl = Ge("bum"),
    Ar = Ge("um"),
    Xl = Ge("sp"),
    Gl = Ge("rtg"),
    ec = Ge("rtc")
function tc(e, t = ve) {
    In("ec", e, t)
}
let Or = !0
function nc(e) {
    const t = Js(e),
        n = e.proxy,
        r = e.ctx
    ;(Or = !1), t.beforeCreate && Ys(t.beforeCreate, e, "bc")
    const {
        data: s,
        computed: i,
        methods: o,
        watch: l,
        provide: c,
        inject: u,
        created: f,
        beforeMount: a,
        mounted: d,
        beforeUpdate: g,
        updated: v,
        activated: C,
        deactivated: x,
        beforeDestroy: P,
        beforeUnmount: L,
        destroyed: S,
        unmounted: F,
        render: G,
        renderTracked: j,
        renderTriggered: U,
        errorCaptured: ce,
        serverPrefetch: ee,
        expose: ge,
        inheritAttrs: te,
        components: ye,
        directives: _e,
        filters: he
    } = t
    if ((u && rc(u, r, null, e.appContext.config.unwrapInjectedRef), o))
        for (const ue in o) {
            const ne = o[ue]
            Z(ne) && (r[ue] = ne.bind(n))
        }
    if (s) {
        const ue = s.call(n, n)
        Ce(ue) && (e.data = St(ue))
    }
    if (((Or = !0), i))
        for (const ue in i) {
            const ne = i[ue],
                Fe = Z(ne) ? ne.bind(n, n) : Z(ne.get) ? ne.get.bind(n, n) : $e,
                Rt = !Z(ne) && Z(ne.set) ? ne.set.bind(n) : $e,
                Je = He({ get: Fe, set: Rt })
            Object.defineProperty(r, ue, { enumerable: !0, configurable: !0, get: () => Je.value, set: Ke => (Je.value = Ke) })
        }
    if (l) for (const ue in l) Qs(l[ue], r, n, ue)
    if (c) {
        const ue = Z(c) ? c.call(n) : c
        Reflect.ownKeys(ue).forEach(ne => {
            Sn(ne, ue[ne])
        })
    }
    f && Ys(f, e, "c")
    function q(ue, ne) {
        V(ne) ? ne.forEach(Fe => ue(Fe.bind(n))) : ne && ue(ne.bind(n))
    }
    if ((q(Zl, a), q(Vl, d), q(Yl, g), q(Ql, v), q(Kl, C), q(Wl, x), q(tc, ce), q(ec, j), q(Gl, U), q(Jl, L), q(Ar, F), q(Xl, ee), V(ge)))
        if (ge.length) {
            const ue = e.exposed || (e.exposed = {})
            ge.forEach(ne => {
                Object.defineProperty(ue, ne, { get: () => n[ne], set: Fe => (n[ne] = Fe) })
            })
        } else e.exposed || (e.exposed = {})
    G && e.render === $e && (e.render = G), te != null && (e.inheritAttrs = te), ye && (e.components = ye), _e && (e.directives = _e)
}
function rc(e, t, n = $e, r = !1) {
    V(e) && (e = xr(e))
    for (const s in e) {
        const i = e[s]
        let o
        Ce(i) ? ("default" in i ? (o = Be(i.from || s, i.default, !0)) : (o = Be(i.from || s))) : (o = Be(i)),
            me(o) && r ? Object.defineProperty(t, s, { enumerable: !0, configurable: !0, get: () => o.value, set: l => (o.value = l) }) : (t[s] = o)
    }
}
function Ys(e, t, n) {
    De(V(e) ? e.map(r => r.bind(t.proxy)) : e.bind(t.proxy), t, n)
}
function Qs(e, t, n, r) {
    const s = r.includes(".") ? xi(n, r) : () => n[r]
    if (be(e)) {
        const i = t[e]
        Z(i) && $t(s, i)
    } else if (Z(e)) $t(s, e.bind(n))
    else if (Ce(e))
        if (V(e)) e.forEach(i => Qs(i, t, n, r))
        else {
            const i = Z(e.handler) ? e.handler.bind(n) : t[e.handler]
            Z(i) && $t(s, i, e)
        }
}
function Js(e) {
    const t = e.type,
        { mixins: n, extends: r } = t,
        {
            mixins: s,
            optionsCache: i,
            config: { optionMergeStrategies: o }
        } = e.appContext,
        l = i.get(t)
    let c
    return l ? (c = l) : !s.length && !n && !r ? (c = t) : ((c = {}), s.length && s.forEach(u => Mn(c, u, o, !0)), Mn(c, t, o)), i.set(t, c), c
}
function Mn(e, t, n, r = !1) {
    const { mixins: s, extends: i } = t
    i && Mn(e, i, n, !0), s && s.forEach(o => Mn(e, o, n, !0))
    for (const o in t)
        if (!(r && o === "expose")) {
            const l = sc[o] || (n && n[o])
            e[o] = l ? l(e[o], t[o]) : t[o]
        }
    return e
}
const sc = {
    data: Xs,
    props: _t,
    emits: _t,
    methods: _t,
    computed: _t,
    beforeCreate: Ne,
    created: Ne,
    beforeMount: Ne,
    mounted: Ne,
    beforeUpdate: Ne,
    updated: Ne,
    beforeDestroy: Ne,
    beforeUnmount: Ne,
    destroyed: Ne,
    unmounted: Ne,
    activated: Ne,
    deactivated: Ne,
    errorCaptured: Ne,
    serverPrefetch: Ne,
    components: _t,
    directives: _t,
    watch: oc,
    provide: Xs,
    inject: ic
}
function Xs(e, t) {
    return t
        ? e
            ? function () {
                  return Re(Z(e) ? e.call(this, this) : e, Z(t) ? t.call(this, this) : t)
              }
            : t
        : e
}
function ic(e, t) {
    return _t(xr(e), xr(t))
}
function xr(e) {
    if (V(e)) {
        const t = {}
        for (let n = 0; n < e.length; n++) t[e[n]] = e[n]
        return t
    }
    return e
}
function Ne(e, t) {
    return e ? [...new Set([].concat(e, t))] : t
}
function _t(e, t) {
    return e ? Re(Re(Object.create(null), e), t) : t
}
function oc(e, t) {
    if (!e) return t
    if (!t) return e
    const n = Re(Object.create(null), e)
    for (const r in t) n[r] = Ne(e[r], t[r])
    return n
}
function lc(e, t, n, r = !1) {
    const s = {},
        i = {}
    En(i, $n, 1), (e.propsDefaults = Object.create(null)), Gs(e, t, s, i)
    for (const o in e.propsOptions[0]) o in s || (s[o] = void 0)
    n ? (e.props = r ? s : Ol(s)) : e.type.props ? (e.props = s) : (e.props = i), (e.attrs = i)
}
function cc(e, t, n, r) {
    const {
            props: s,
            attrs: i,
            vnode: { patchFlag: o }
        } = e,
        l = X(s),
        [c] = e.propsOptions
    let u = !1
    if ((r || o > 0) && !(o & 16)) {
        if (o & 8) {
            const f = e.vnode.dynamicProps
            for (let a = 0; a < f.length; a++) {
                let d = f[a]
                const g = t[d]
                if (c)
                    if (J(i, d)) g !== i[d] && ((i[d] = g), (u = !0))
                    else {
                        const v = ze(d)
                        s[v] = Tr(c, l, v, g, e, !1)
                    }
                else g !== i[d] && ((i[d] = g), (u = !0))
            }
        }
    } else {
        Gs(e, t, s, i) && (u = !0)
        let f
        for (const a in l) (!t || (!J(t, a) && ((f = Ot(a)) === a || !J(t, f)))) && (c ? n && (n[a] !== void 0 || n[f] !== void 0) && (s[a] = Tr(c, l, a, void 0, e, !0)) : delete s[a])
        if (i !== l) for (const a in i) (!t || !J(t, a)) && (delete i[a], (u = !0))
    }
    u && Xe(e, "set", "$attrs")
}
function Gs(e, t, n, r) {
    const [s, i] = e.propsOptions
    let o = !1,
        l
    if (t)
        for (let c in t) {
            if (vn(c)) continue
            const u = t[c]
            let f
            s && J(s, (f = ze(c))) ? (!i || !i.includes(f) ? (n[f] = u) : ((l || (l = {}))[f] = u)) : Pr(e.emitsOptions, c) || (u !== r[c] && ((r[c] = u), (o = !0)))
        }
    if (i) {
        const c = X(n),
            u = l || ie
        for (let f = 0; f < i.length; f++) {
            const a = i[f]
            n[a] = Tr(s, c, a, u[a], e, !J(u, a))
        }
    }
    return o
}
function Tr(e, t, n, r, s, i) {
    const o = e[n]
    if (o != null) {
        const l = J(o, "default")
        if (l && r === void 0) {
            const c = o.default
            if (o.type !== Function && Z(c)) {
                const { propsDefaults: u } = s
                n in u ? (r = u[n]) : (Ft(s), (r = u[n] = c.call(null, t)), yt())
            } else r = c
        }
        o[0] && (i && !l ? (r = !1) : o[1] && (r === "" || r === Ot(n)) && (r = !0))
    }
    return r
}
function ei(e, t, n = !1) {
    const r = t.propsCache,
        s = r.get(e)
    if (s) return s
    const i = e.props,
        o = {},
        l = []
    let c = !1
    if (!Z(e)) {
        const f = a => {
            c = !0
            const [d, g] = ei(a, t, !0)
            Re(o, d), g && l.push(...g)
        }
        !n && t.mixins.length && t.mixins.forEach(f), e.extends && f(e.extends), e.mixins && e.mixins.forEach(f)
    }
    if (!i && !c) return r.set(e, At), At
    if (V(i))
        for (let f = 0; f < i.length; f++) {
            const a = ze(i[f])
            ti(a) && (o[a] = ie)
        }
    else if (i)
        for (const f in i) {
            const a = ze(f)
            if (ti(a)) {
                const d = i[f],
                    g = (o[a] = V(d) || Z(d) ? { type: d } : d)
                if (g) {
                    const v = si(Boolean, g.type),
                        C = si(String, g.type)
                    ;(g[0] = v > -1), (g[1] = C < 0 || v < C), (v > -1 || J(g, "default")) && l.push(a)
                }
            }
        }
    const u = [o, l]
    return r.set(e, u), u
}
function ti(e) {
    return e[0] !== "$"
}
function ni(e) {
    const t = e && e.toString().match(/^\s*function (\w+)/)
    return t ? t[1] : e === null ? "null" : ""
}
function ri(e, t) {
    return ni(e) === ni(t)
}
function si(e, t) {
    return V(t) ? t.findIndex(n => ri(n, e)) : Z(t) && ri(t, e) ? 0 : -1
}
const ii = e => e[0] === "_" || e === "$stable",
    Sr = e => (V(e) ? e.map(Ye) : [Ye(e)]),
    uc = (e, t, n) => {
        const r = $l((...s) => Sr(t(...s)), n)
        return (r._c = !1), r
    },
    oi = (e, t, n) => {
        const r = e._ctx
        for (const s in e) {
            if (ii(s)) continue
            const i = e[s]
            if (Z(i)) t[s] = uc(s, i, r)
            else if (i != null) {
                const o = Sr(i)
                t[s] = () => o
            }
        }
    },
    li = (e, t) => {
        const n = Sr(t)
        e.slots.default = () => n
    },
    fc = (e, t) => {
        if (e.vnode.shapeFlag & 32) {
            const n = t._
            n ? ((e.slots = X(t)), En(t, "_", n)) : oi(t, (e.slots = {}))
        } else (e.slots = {}), t && li(e, t)
        En(e.slots, $n, 1)
    },
    ac = (e, t, n) => {
        const { vnode: r, slots: s } = e
        let i = !0,
            o = ie
        if (r.shapeFlag & 32) {
            const l = t._
            l ? (n && l === 1 ? (i = !1) : (Re(s, t), !n && l === 1 && delete s._)) : ((i = !t.$stable), oi(t, s)), (o = t)
        } else t && (li(e, t), (o = { default: 1 }))
        if (i) for (const l in s) !ii(l) && !(l in o) && delete s[l]
    }
function Pa(e, t) {
    const n = Se
    if (n === null) return e
    const r = n.proxy,
        s = e.dirs || (e.dirs = [])
    for (let i = 0; i < t.length; i++) {
        let [o, l, c, u = ie] = t[i]
        Z(o) && (o = { mounted: o, updated: o }), o.deep && Ct(l), s.push({ dir: o, instance: r, value: l, oldValue: void 0, arg: c, modifiers: u })
    }
    return e
}
function bt(e, t, n, r) {
    const s = e.dirs,
        i = t && t.dirs
    for (let o = 0; o < s.length; o++) {
        const l = s[o]
        i && (l.oldValue = i[o].value)
        let c = l.dir[r]
        c && (Tt(), De(c, n, 8, [e.el, l, e, t]), wt())
    }
}
function ci() {
    return {
        app: null,
        config: { isNativeTag: Uo, performance: !1, globalProperties: {}, optionMergeStrategies: {}, errorHandler: void 0, warnHandler: void 0, compilerOptions: {} },
        mixins: [],
        components: {},
        directives: {},
        provides: Object.create(null),
        optionsCache: new WeakMap(),
        propsCache: new WeakMap(),
        emitsCache: new WeakMap()
    }
}
let dc = 0
function hc(e, t) {
    return function (r, s = null) {
        s != null && !Ce(s) && (s = null)
        const i = ci(),
            o = new Set()
        let l = !1
        const c = (i.app = {
            _uid: dc++,
            _component: r,
            _props: s,
            _container: null,
            _context: i,
            _instance: null,
            version: Zc,
            get config() {
                return i.config
            },
            set config(u) {},
            use(u, ...f) {
                return o.has(u) || (u && Z(u.install) ? (o.add(u), u.install(c, ...f)) : Z(u) && (o.add(u), u(c, ...f))), c
            },
            mixin(u) {
                return i.mixins.includes(u) || i.mixins.push(u), c
            },
            component(u, f) {
                return f ? ((i.components[u] = f), c) : i.components[u]
            },
            directive(u, f) {
                return f ? ((i.directives[u] = f), c) : i.directives[u]
            },
            mount(u, f, a) {
                if (!l) {
                    const d = Ie(r, s)
                    return (d.appContext = i), f && t ? t(d, u) : e(d, u, a), (l = !0), (c._container = u), (u.__vue_app__ = c), jr(d.component) || d.component.proxy
                }
            },
            unmount() {
                l && (e(null, c._container), delete c._container.__vue_app__)
            },
            provide(u, f) {
                return (i.provides[u] = f), c
            }
        })
        return c
    }
}
const Ae = ql
function pc(e) {
    return gc(e)
}
function gc(e, t) {
    const n = Xo()
    n.__VUE__ = !0
    const {
            insert: r,
            remove: s,
            patchProp: i,
            createElement: o,
            createText: l,
            createComment: c,
            setText: u,
            setElementText: f,
            parentNode: a,
            nextSibling: d,
            setScopeId: g = $e,
            cloneNode: v,
            insertStaticContent: C
        } = e,
        x = (h, p, m, E = null, b = null, A = null, T = !1, R = null, N = !!p.dynamicChildren) => {
            if (h === p) return
            h && !tn(h, p) && ((E = $(h)), ke(h, b, A, !0), (h = null)), p.patchFlag === -2 && ((N = !1), (p.dynamicChildren = null))
            const { type: y, ref: H, shapeFlag: I } = p
            switch (y) {
                case kr:
                    P(h, p, m, E)
                    break
                case Mt:
                    L(h, p, m, E)
                    break
                case Lr:
                    h == null && S(p, m, E, T)
                    break
                case je:
                    _e(h, p, m, E, b, A, T, R, N)
                    break
                default:
                    I & 1 ? j(h, p, m, E, b, A, T, R, N) : I & 6 ? he(h, p, m, E, b, A, T, R, N) : (I & 64 || I & 128) && y.process(h, p, m, E, b, A, T, R, N, fe)
            }
            H != null && b && Ir(H, h && h.ref, A, p || h, !p)
        },
        P = (h, p, m, E) => {
            if (h == null) r((p.el = l(p.children)), m, E)
            else {
                const b = (p.el = h.el)
                p.children !== h.children && u(b, p.children)
            }
        },
        L = (h, p, m, E) => {
            h == null ? r((p.el = c(p.children || "")), m, E) : (p.el = h.el)
        },
        S = (h, p, m, E) => {
            ;[h.el, h.anchor] = C(h.children, p, m, E)
        },
        F = ({ el: h, anchor: p }, m, E) => {
            let b
            for (; h && h !== p; ) (b = d(h)), r(h, m, E), (h = b)
            r(p, m, E)
        },
        G = ({ el: h, anchor: p }) => {
            let m
            for (; h && h !== p; ) (m = d(h)), s(h), (h = m)
            s(p)
        },
        j = (h, p, m, E, b, A, T, R, N) => {
            ;(T = T || p.type === "svg"), h == null ? U(p, m, E, b, A, T, R, N) : ge(h, p, b, A, T, R, N)
        },
        U = (h, p, m, E, b, A, T, R) => {
            let N, y
            const { type: H, props: I, shapeFlag: B, transition: W, patchFlag: Q, dirs: de } = h
            if (h.el && v !== void 0 && Q === -1) N = h.el = v(h.el)
            else {
                if (
                    ((N = h.el = o(h.type, A, I && I.is, I)), B & 8 ? f(N, h.children) : B & 16 && ee(h.children, N, null, E, b, A && H !== "foreignObject", T, R), de && bt(h, null, E, "created"), I)
                ) {
                    for (const ae in I) ae !== "value" && !vn(ae) && i(N, ae, null, I[ae], A, h.children, E, b, O)
                    "value" in I && i(N, "value", null, I.value), (y = I.onVnodeBeforeMount) && Ve(y, E, h)
                }
                ce(N, h, h.scopeId, T, E)
            }
            de && bt(h, null, E, "beforeMount")
            const se = (!b || (b && !b.pendingBranch)) && W && !W.persisted
            se && W.beforeEnter(N),
                r(N, p, m),
                ((y = I && I.onVnodeMounted) || se || de) &&
                    Ae(() => {
                        y && Ve(y, E, h), se && W.enter(N), de && bt(h, null, E, "mounted")
                    }, b)
        },
        ce = (h, p, m, E, b) => {
            if ((m && g(h, m), E)) for (let A = 0; A < E.length; A++) g(h, E[A])
            if (b) {
                let A = b.subTree
                if (p === A) {
                    const T = b.vnode
                    ce(h, T, T.scopeId, T.slotScopeIds, b.parent)
                }
            }
        },
        ee = (h, p, m, E, b, A, T, R, N = 0) => {
            for (let y = N; y < h.length; y++) {
                const H = (h[y] = R ? ot(h[y]) : Ye(h[y]))
                x(null, H, p, m, E, b, A, T, R)
            }
        },
        ge = (h, p, m, E, b, A, T) => {
            const R = (p.el = h.el)
            let { patchFlag: N, dynamicChildren: y, dirs: H } = p
            N |= h.patchFlag & 16
            const I = h.props || ie,
                B = p.props || ie
            let W
            ;(W = B.onVnodeBeforeUpdate) && Ve(W, m, p, h), H && bt(p, h, m, "beforeUpdate")
            const Q = b && p.type !== "foreignObject"
            if ((y ? te(h.dynamicChildren, y, R, m, E, Q, A) : T || Fe(h, p, R, null, m, E, Q, A, !1), N > 0)) {
                if (N & 16) ye(R, p, I, B, m, E, b)
                else if ((N & 2 && I.class !== B.class && i(R, "class", null, B.class, b), N & 4 && i(R, "style", I.style, B.style, b), N & 8)) {
                    const de = p.dynamicProps
                    for (let se = 0; se < de.length; se++) {
                        const ae = de[se],
                            Le = I[ae],
                            Nt = B[ae]
                        ;(Nt !== Le || ae === "value") && i(R, ae, Le, Nt, b, h.children, m, E, O)
                    }
                }
                N & 1 && h.children !== p.children && f(R, p.children)
            } else !T && y == null && ye(R, p, I, B, m, E, b)
            ;((W = B.onVnodeUpdated) || H) &&
                Ae(() => {
                    W && Ve(W, m, p, h), H && bt(p, h, m, "updated")
                }, E)
        },
        te = (h, p, m, E, b, A, T) => {
            for (let R = 0; R < p.length; R++) {
                const N = h[R],
                    y = p[R],
                    H = N.el && (N.type === je || !tn(N, y) || N.shapeFlag & (6 | 64)) ? a(N.el) : m
                x(N, y, H, null, E, b, A, T, !0)
            }
        },
        ye = (h, p, m, E, b, A, T) => {
            if (m !== E) {
                for (const R in E) {
                    if (vn(R)) continue
                    const N = E[R],
                        y = m[R]
                    N !== y && R !== "value" && i(h, R, y, N, T, p.children, b, A, O)
                }
                if (m !== ie) for (const R in m) !vn(R) && !(R in E) && i(h, R, m[R], null, T, p.children, b, A, O)
                "value" in E && i(h, "value", m.value, E.value)
            }
        },
        _e = (h, p, m, E, b, A, T, R, N) => {
            const y = (p.el = h ? h.el : l("")),
                H = (p.anchor = h ? h.anchor : l(""))
            let { patchFlag: I, dynamicChildren: B, slotScopeIds: W } = p
            W && (R = R ? R.concat(W) : W),
                h == null
                    ? (r(y, m, E), r(H, m, E), ee(p.children, m, H, b, A, T, R, N))
                    : I > 0 && I & 64 && B && h.dynamicChildren
                    ? (te(h.dynamicChildren, B, m, b, A, T, R), (p.key != null || (b && p === b.subTree)) && Mr(h, p, !0))
                    : Fe(h, p, m, H, b, A, T, R, N)
        },
        he = (h, p, m, E, b, A, T, R, N) => {
            ;(p.slotScopeIds = R), h == null ? (p.shapeFlag & 512 ? b.ctx.activate(p, m, E, T, N) : pe(p, m, E, b, A, T, N)) : q(h, p, N)
        },
        pe = (h, p, m, E, b, A, T) => {
            const R = (h.component = Fc(h, E, b))
            if ((Zs(h) && (R.ctx.renderer = fe), kc(R), R.asyncDep)) {
                if ((b && b.registerDep(R, ue), !h.el)) {
                    const N = (R.subTree = Ie(Mt))
                    L(null, N, p, m)
                }
                return
            }
            ue(R, h, p, m, b, A, T)
        },
        q = (h, p, m) => {
            const E = (p.component = h.component)
            if (jl(h, p, m))
                if (E.asyncDep && !E.asyncResolved) {
                    ne(E, p, m)
                    return
                } else (E.next = p), Uc(E.update), E.update()
            else (p.component = h.component), (p.el = h.el), (E.vnode = p)
        },
        ue = (h, p, m, E, b, A, T) => {
            const R = () => {
                    if (h.isMounted) {
                        let { next: H, bu: I, u: B, parent: W, vnode: Q } = h,
                            de = H,
                            se
                        ;(N.allowRecurse = !1), H ? ((H.el = Q.el), ne(h, H, T)) : (H = Q), I && fr(I), (se = H.props && H.props.onVnodeBeforeUpdate) && Ve(se, W, H, Q), (N.allowRecurse = !0)
                        const ae = Rr(h),
                            Le = h.subTree
                        ;(h.subTree = ae),
                            x(Le, ae, a(Le.el), $(Le), h, b, A),
                            (H.el = ae.el),
                            de === null && Dl(h, ae.el),
                            B && Ae(B, b),
                            (se = H.props && H.props.onVnodeUpdated) && Ae(() => Ve(se, W, H, Q), b)
                    } else {
                        let H
                        const { el: I, props: B } = p,
                            { bm: W, m: Q, parent: de } = h,
                            se = Nr(p)
                        if (((N.allowRecurse = !1), W && fr(W), !se && (H = B && B.onVnodeBeforeMount) && Ve(H, de, p), (N.allowRecurse = !0), I && z)) {
                            const ae = () => {
                                ;(h.subTree = Rr(h)), z(I, h.subTree, h, b, null)
                            }
                            se ? p.type.__asyncLoader().then(() => !h.isUnmounted && ae()) : ae()
                        } else {
                            const ae = (h.subTree = Rr(h))
                            x(null, ae, m, E, h, b, A), (p.el = ae.el)
                        }
                        if ((Q && Ae(Q, b), !se && (H = B && B.onVnodeMounted))) {
                            const ae = p
                            Ae(() => Ve(H, de, ae), b)
                        }
                        p.shapeFlag & 256 && h.a && Ae(h.a, b), (h.isMounted = !0), (p = m = E = null)
                    }
                },
                N = new gr(R, () => yi(h.update), h.scope),
                y = (h.update = N.run.bind(N))
            ;(y.id = h.uid), (N.allowRecurse = y.allowRecurse = !0), y()
        },
        ne = (h, p, m) => {
            p.component = h
            const E = h.vnode.props
            ;(h.vnode = p), (h.next = null), cc(h, p.props, E, m), ac(h, p.children, m), Tt(), Wr(void 0, h.update), wt()
        },
        Fe = (h, p, m, E, b, A, T, R, N = !1) => {
            const y = h && h.children,
                H = h ? h.shapeFlag : 0,
                I = p.children,
                { patchFlag: B, shapeFlag: W } = p
            if (B > 0) {
                if (B & 128) {
                    Je(y, I, m, E, b, A, T, R, N)
                    return
                } else if (B & 256) {
                    Rt(y, I, m, E, b, A, T, R, N)
                    return
                }
            }
            W & 8 ? (H & 16 && O(y, b, A), I !== y && f(m, I)) : H & 16 ? (W & 16 ? Je(y, I, m, E, b, A, T, R, N) : O(y, b, A, !0)) : (H & 8 && f(m, ""), W & 16 && ee(I, m, E, b, A, T, R, N))
        },
        Rt = (h, p, m, E, b, A, T, R, N) => {
            ;(h = h || At), (p = p || At)
            const y = h.length,
                H = p.length,
                I = Math.min(y, H)
            let B
            for (B = 0; B < I; B++) {
                const W = (p[B] = N ? ot(p[B]) : Ye(p[B]))
                x(h[B], W, m, null, b, A, T, R, N)
            }
            y > H ? O(h, b, A, !0, !1, I) : ee(p, m, E, b, A, T, R, N, I)
        },
        Je = (h, p, m, E, b, A, T, R, N) => {
            let y = 0
            const H = p.length
            let I = h.length - 1,
                B = H - 1
            for (; y <= I && y <= B; ) {
                const W = h[y],
                    Q = (p[y] = N ? ot(p[y]) : Ye(p[y]))
                if (tn(W, Q)) x(W, Q, m, null, b, A, T, R, N)
                else break
                y++
            }
            for (; y <= I && y <= B; ) {
                const W = h[I],
                    Q = (p[B] = N ? ot(p[B]) : Ye(p[B]))
                if (tn(W, Q)) x(W, Q, m, null, b, A, T, R, N)
                else break
                I--, B--
            }
            if (y > I) {
                if (y <= B) {
                    const W = B + 1,
                        Q = W < H ? p[W].el : E
                    for (; y <= B; ) x(null, (p[y] = N ? ot(p[y]) : Ye(p[y])), m, Q, b, A, T, R, N), y++
                }
            } else if (y > B) for (; y <= I; ) ke(h[y], b, A, !0), y++
            else {
                const W = y,
                    Q = y,
                    de = new Map()
                for (y = Q; y <= B; y++) {
                    const xe = (p[y] = N ? ot(p[y]) : Ye(p[y]))
                    xe.key != null && de.set(xe.key, y)
                }
                let se,
                    ae = 0
                const Le = B - Q + 1
                let Nt = !1,
                    ds = 0
                const Zt = new Array(Le)
                for (y = 0; y < Le; y++) Zt[y] = 0
                for (y = W; y <= I; y++) {
                    const xe = h[y]
                    if (ae >= Le) {
                        ke(xe, b, A, !0)
                        continue
                    }
                    let We
                    if (xe.key != null) We = de.get(xe.key)
                    else
                        for (se = Q; se <= B; se++)
                            if (Zt[se - Q] === 0 && tn(xe, p[se])) {
                                We = se
                                break
                            }
                    We === void 0 ? ke(xe, b, A, !0) : ((Zt[We - Q] = y + 1), We >= ds ? (ds = We) : (Nt = !0), x(xe, p[We], m, null, b, A, T, R, N), ae++)
                }
                const hs = Nt ? mc(Zt) : At
                for (se = hs.length - 1, y = Le - 1; y >= 0; y--) {
                    const xe = Q + y,
                        We = p[xe],
                        ps = xe + 1 < H ? p[xe + 1].el : E
                    Zt[y] === 0 ? x(null, We, m, ps, b, A, T, R, N) : Nt && (se < 0 || y !== hs[se] ? Ke(We, m, ps, 2) : se--)
                }
            }
        },
        Ke = (h, p, m, E, b = null) => {
            const { el: A, type: T, transition: R, children: N, shapeFlag: y } = h
            if (y & 6) {
                Ke(h.component.subTree, p, m, E)
                return
            }
            if (y & 128) {
                h.suspense.move(p, m, E)
                return
            }
            if (y & 64) {
                T.move(h, p, m, fe)
                return
            }
            if (T === je) {
                r(A, p, m)
                for (let I = 0; I < N.length; I++) Ke(N[I], p, m, E)
                r(h.anchor, p, m)
                return
            }
            if (T === Lr) {
                F(h, p, m)
                return
            }
            if (E !== 2 && y & 1 && R)
                if (E === 0) R.beforeEnter(A), r(A, p, m), Ae(() => R.enter(A), b)
                else {
                    const { leave: I, delayLeave: B, afterLeave: W } = R,
                        Q = () => r(A, p, m),
                        de = () => {
                            I(A, () => {
                                Q(), W && W()
                            })
                        }
                    B ? B(A, Q, de) : de()
                }
            else r(A, p, m)
        },
        ke = (h, p, m, E = !1, b = !1) => {
            const { type: A, props: T, ref: R, children: N, dynamicChildren: y, shapeFlag: H, patchFlag: I, dirs: B } = h
            if ((R != null && Ir(R, null, m, h, !0), H & 256)) {
                p.ctx.deactivate(h)
                return
            }
            const W = H & 1 && B,
                Q = !Nr(h)
            let de
            if ((Q && (de = T && T.onVnodeBeforeUnmount) && Ve(de, p, h), H & 6)) M(h.component, m, E)
            else {
                if (H & 128) {
                    h.suspense.unmount(m, E)
                    return
                }
                W && bt(h, null, p, "beforeUnmount"),
                    H & 64 ? h.type.remove(h, p, m, b, fe, E) : y && (A !== je || (I > 0 && I & 64)) ? O(y, p, m, !1, !0) : ((A === je && I & (128 | 256)) || (!b && H & 16)) && O(N, p, m),
                    E && nr(h)
            }
            ;((Q && (de = T && T.onVnodeUnmounted)) || W) &&
                Ae(() => {
                    de && Ve(de, p, h), W && bt(h, null, p, "unmounted")
                }, m)
        },
        nr = h => {
            const { type: p, el: m, anchor: E, transition: b } = h
            if (p === je) {
                _(m, E)
                return
            }
            if (p === Lr) {
                G(h)
                return
            }
            const A = () => {
                s(m), b && !b.persisted && b.afterLeave && b.afterLeave()
            }
            if (h.shapeFlag & 1 && b && !b.persisted) {
                const { leave: T, delayLeave: R } = b,
                    N = () => T(m, A)
                R ? R(h.el, A, N) : N()
            } else A()
        },
        _ = (h, p) => {
            let m
            for (; h !== p; ) (m = d(h)), s(h), (h = m)
            s(p)
        },
        M = (h, p, m) => {
            const { bum: E, scope: b, update: A, subTree: T, um: R } = h
            E && fr(E),
                b.stop(),
                A && ((A.active = !1), ke(T, h, p, m)),
                R && Ae(R, p),
                Ae(() => {
                    h.isUnmounted = !0
                }, p),
                p && p.pendingBranch && !p.isUnmounted && h.asyncDep && !h.asyncResolved && h.suspenseId === p.pendingId && (p.deps--, p.deps === 0 && p.resolve())
        },
        O = (h, p, m, E = !1, b = !1, A = 0) => {
            for (let T = A; T < h.length; T++) ke(h[T], p, m, E, b)
        },
        $ = h => (h.shapeFlag & 6 ? $(h.component.subTree) : h.shapeFlag & 128 ? h.suspense.next() : d(h.anchor || h.el)),
        re = (h, p, m) => {
            h == null ? p._vnode && ke(p._vnode, null, null, !0) : x(p._vnode || null, h, p, null, null, null, m), Ri(), (p._vnode = h)
        },
        fe = { p: x, um: ke, m: Ke, r: nr, mt: pe, mc: ee, pc: Fe, pbc: te, n: $, o: e }
    let Y, z
    return t && ([Y, z] = t(fe)), { render: re, hydrate: Y, createApp: hc(re, Y) }
}
function Ir(e, t, n, r, s = !1) {
    if (V(e)) {
        e.forEach((d, g) => Ir(d, t && (V(t) ? t[g] : t), n, r, s))
        return
    }
    if (Nr(r) && !s) return
    const i = r.shapeFlag & 4 ? jr(r.component) || r.component.proxy : r.el,
        o = s ? null : i,
        { i: l, r: c } = e,
        u = t && t.r,
        f = l.refs === ie ? (l.refs = {}) : l.refs,
        a = l.setupState
    if ((u != null && u !== c && (be(u) ? ((f[u] = null), J(a, u) && (a[u] = null)) : me(u) && (u.value = null)), be(c))) {
        const d = () => {
            ;(f[c] = o), J(a, c) && (a[c] = o)
        }
        o ? ((d.id = -1), Ae(d, n)) : d()
    } else if (me(c)) {
        const d = () => {
            c.value = o
        }
        o ? ((d.id = -1), Ae(d, n)) : d()
    } else Z(c) && lt(c, l, 12, [o, f])
}
function Ve(e, t, n, r = null) {
    De(e, t, 7, [n, r])
}
function Mr(e, t, n = !1) {
    const r = e.children,
        s = t.children
    if (V(r) && V(s))
        for (let i = 0; i < r.length; i++) {
            const o = r[i]
            let l = s[i]
            l.shapeFlag & 1 && !l.dynamicChildren && ((l.patchFlag <= 0 || l.patchFlag === 32) && ((l = s[i] = ot(s[i])), (l.el = o.el)), n || Mr(o, l))
        }
}
function mc(e) {
    const t = e.slice(),
        n = [0]
    let r, s, i, o, l
    const c = e.length
    for (r = 0; r < c; r++) {
        const u = e[r]
        if (u !== 0) {
            if (((s = n[n.length - 1]), e[s] < u)) {
                ;(t[r] = s), n.push(r)
                continue
            }
            for (i = 0, o = n.length - 1; i < o; ) (l = (i + o) >> 1), e[n[l]] < u ? (i = l + 1) : (o = l)
            u < e[n[i]] && (i > 0 && (t[r] = n[i - 1]), (n[i] = r))
        }
    }
    for (i = n.length, o = n[i - 1]; i-- > 0; ) (n[i] = o), (o = t[o])
    return n
}
const wc = e => e.__isTeleport,
    Gt = e => e && (e.disabled || e.disabled === ""),
    ui = e => typeof SVGElement != "undefined" && e instanceof SVGElement,
    Fr = (e, t) => {
        const n = e && e.to
        return be(n) ? (t ? t(n) : null) : n
    },
    vc = {
        __isTeleport: !0,
        process(e, t, n, r, s, i, o, l, c, u) {
            const {
                    mc: f,
                    pc: a,
                    pbc: d,
                    o: { insert: g, querySelector: v, createText: C, createComment: x }
                } = u,
                P = Gt(t.props)
            let { shapeFlag: L, children: S, dynamicChildren: F } = t
            if (e == null) {
                const G = (t.el = C("")),
                    j = (t.anchor = C(""))
                g(G, n, r), g(j, n, r)
                const U = (t.target = Fr(t.props, v)),
                    ce = (t.targetAnchor = C(""))
                U && (g(ce, U), (o = o || ui(U)))
                const ee = (ge, te) => {
                    L & 16 && f(S, ge, te, s, i, o, l, c)
                }
                P ? ee(n, j) : U && ee(U, ce)
            } else {
                t.el = e.el
                const G = (t.anchor = e.anchor),
                    j = (t.target = e.target),
                    U = (t.targetAnchor = e.targetAnchor),
                    ce = Gt(e.props),
                    ee = ce ? n : j,
                    ge = ce ? G : U
                if (((o = o || ui(j)), F ? (d(e.dynamicChildren, F, ee, s, i, o, l), Mr(e, t, !0)) : c || a(e, t, ee, ge, s, i, o, l, !1), P)) ce || Fn(t, n, G, u, 1)
                else if ((t.props && t.props.to) !== (e.props && e.props.to)) {
                    const te = (t.target = Fr(t.props, v))
                    te && Fn(t, te, null, u, 0)
                } else ce && Fn(t, j, U, u, 1)
            }
        },
        remove(e, t, n, r, { um: s, o: { remove: i } }, o) {
            const { shapeFlag: l, children: c, anchor: u, targetAnchor: f, target: a, props: d } = e
            if ((a && i(f), (o || !Gt(d)) && (i(u), l & 16)))
                for (let g = 0; g < c.length; g++) {
                    const v = c[g]
                    s(v, t, n, !0, !!v.dynamicChildren)
                }
        },
        move: Fn,
        hydrate: _c
    }
function Fn(e, t, n, { o: { insert: r }, m: s }, i = 2) {
    i === 0 && r(e.targetAnchor, t, n)
    const { el: o, anchor: l, shapeFlag: c, children: u, props: f } = e,
        a = i === 2
    if ((a && r(o, t, n), (!a || Gt(f)) && c & 16)) for (let d = 0; d < u.length; d++) s(u[d], t, n, 2)
    a && r(l, t, n)
}
function _c(e, t, n, r, s, i, { o: { nextSibling: o, parentNode: l, querySelector: c } }, u) {
    const f = (t.target = Fr(t.props, c))
    if (f) {
        const a = f._lpa || f.firstChild
        t.shapeFlag & 16 &&
            (Gt(t.props) ? ((t.anchor = u(o(e), t, l(e), n, r, s, i)), (t.targetAnchor = a)) : ((t.anchor = o(e)), (t.targetAnchor = u(a, t, f, n, r, s, i))),
            (f._lpa = t.targetAnchor && o(t.targetAnchor)))
    }
    return t.anchor && o(t.anchor)
}
const Ra = vc,
    fi = "components",
    bc = "directives"
function Na(e, t) {
    return ai(fi, e, !0, t) || e
}
const Ec = Symbol()
function Aa(e) {
    return ai(bc, e)
}
function ai(e, t, n = !0, r = !1) {
    const s = Se || ve
    if (s) {
        const i = s.type
        if (e === fi) {
            const l = Hc(i)
            if (l && (l === t || l === ze(t) || l === bn(ze(t)))) return i
        }
        const o = di(s[e] || i[e], t) || di(s.appContext[e], t)
        return !o && r ? i : o
    }
}
function di(e, t) {
    return e && (e[t] || e[ze(t)] || e[bn(ze(t))])
}
const je = Symbol(void 0),
    kr = Symbol(void 0),
    Mt = Symbol(void 0),
    Lr = Symbol(void 0),
    en = []
let Et = null
function yc(e = !1) {
    en.push((Et = e ? null : []))
}
function Cc() {
    en.pop(), (Et = en[en.length - 1] || null)
}
let kn = 1
function hi(e) {
    kn += e
}
function Pc(e) {
    return (e.dynamicChildren = kn > 0 ? Et || At : null), Cc(), kn > 0 && Et && Et.push(e), e
}
function Rc(e, t, n, r, s) {
    return Pc(Ie(e, t, n, r, s, !0))
}
function Ln(e) {
    return e ? e.__v_isVNode === !0 : !1
}
function tn(e, t) {
    return e.type === t.type && e.key === t.key
}
const $n = "__vInternal",
    pi = ({ key: e }) => (e != null ? e : null),
    Hn = ({ ref: e }) => (e != null ? (be(e) || me(e) || Z(e) ? { i: Se, r: e } : e) : null)
function Nc(e, t = null, n = null, r = 0, s = null, i = e === je ? 0 : 1, o = !1, l = !1) {
    const c = {
        __v_isVNode: !0,
        __v_skip: !0,
        type: e,
        props: t,
        key: t && pi(t),
        ref: t && Hn(t),
        scopeId: Ks,
        slotScopeIds: null,
        children: n,
        component: null,
        suspense: null,
        ssContent: null,
        ssFallback: null,
        dirs: null,
        transition: null,
        el: null,
        anchor: null,
        target: null,
        targetAnchor: null,
        staticCount: 0,
        shapeFlag: i,
        patchFlag: r,
        dynamicProps: s,
        dynamicChildren: null,
        appContext: null
    }
    return l ? ($r(c, n), i & 128 && e.normalize(c)) : n && (c.shapeFlag |= be(n) ? 8 : 16), kn > 0 && !o && Et && (c.patchFlag > 0 || i & 6) && c.patchFlag !== 32 && Et.push(c), c
}
const Ie = Ac
function Ac(e, t = null, n = null, r = 0, s = null, i = !1) {
    if (((!e || e === Ec) && (e = Mt), Ln(e))) {
        const l = nn(e, t, !0)
        return n && $r(l, n), l
    }
    if ((Bc(e) && (e = e.__vccOpts), t)) {
        t = Oc(t)
        let { class: l, style: c } = t
        l && !be(l) && (t.class = ir(l)), Ce(c) && (Hs(c) && !V(c) && (c = Re({}, c)), (t.style = sr(c)))
    }
    const o = be(e) ? 1 : Ul(e) ? 128 : wc(e) ? 64 : Ce(e) ? 4 : Z(e) ? 2 : 0
    return Nc(e, t, n, r, s, o, i, !0)
}
function Oc(e) {
    return e ? (Hs(e) || $n in e ? Re({}, e) : e) : null
}
function nn(e, t, n = !1) {
    const { props: r, ref: s, patchFlag: i, children: o } = e,
        l = t ? Tc(r || {}, t) : r
    return {
        __v_isVNode: !0,
        __v_skip: !0,
        type: e.type,
        props: l,
        key: l && pi(l),
        ref: t && t.ref ? (n && s ? (V(s) ? s.concat(Hn(t)) : [s, Hn(t)]) : Hn(t)) : s,
        scopeId: e.scopeId,
        slotScopeIds: e.slotScopeIds,
        children: o,
        target: e.target,
        targetAnchor: e.targetAnchor,
        staticCount: e.staticCount,
        shapeFlag: e.shapeFlag,
        patchFlag: t && e.type !== je ? (i === -1 ? 16 : i | 16) : i,
        dynamicProps: e.dynamicProps,
        dynamicChildren: e.dynamicChildren,
        appContext: e.appContext,
        dirs: e.dirs,
        transition: e.transition,
        component: e.component,
        suspense: e.suspense,
        ssContent: e.ssContent && nn(e.ssContent),
        ssFallback: e.ssFallback && nn(e.ssFallback),
        el: e.el,
        anchor: e.anchor
    }
}
function xc(e = " ", t = 0) {
    return Ie(kr, null, e, t)
}
function Ye(e) {
    return e == null || typeof e == "boolean" ? Ie(Mt) : V(e) ? Ie(je, null, e.slice()) : typeof e == "object" ? ot(e) : Ie(kr, null, String(e))
}
function ot(e) {
    return e.el === null || e.memo ? e : nn(e)
}
function $r(e, t) {
    let n = 0
    const { shapeFlag: r } = e
    if (t == null) t = null
    else if (V(t)) n = 16
    else if (typeof t == "object")
        if (r & (1 | 64)) {
            const s = t.default
            s && (s._c && (s._d = !1), $r(e, s()), s._c && (s._d = !0))
            return
        } else {
            n = 32
            const s = t._
            !s && !($n in t) ? (t._ctx = Se) : s === 3 && Se && (Se.slots._ === 1 ? (t._ = 1) : ((t._ = 2), (e.patchFlag |= 1024)))
        }
    else Z(t) ? ((t = { default: t, _ctx: Se }), (n = 32)) : ((t = String(t)), r & 64 ? ((n = 16), (t = [xc(t)])) : (n = 8))
    ;(e.children = t), (e.shapeFlag |= n)
}
function Tc(...e) {
    const t = {}
    for (let n = 0; n < e.length; n++) {
        const r = e[n]
        for (const s in r)
            if (s === "class") t.class !== r.class && (t.class = ir([t.class, r.class]))
            else if (s === "style") t.style = sr([t.style, r.style])
            else if (mn(s)) {
                const i = t[s],
                    o = r[s]
                i !== o && !(V(i) && i.includes(o)) && (t[s] = i ? [].concat(i, o) : o)
            } else s !== "" && (t[s] = r[s])
    }
    return t
}
function Oa(e, t, n = {}, r, s) {
    if (Se.isCE) return Ie("slot", t === "default" ? null : { name: t }, r && r())
    let i = e[t]
    i && i._c && (i._d = !1), yc()
    const o = i && gi(i(n)),
        l = Rc(je, { key: n.key || `_${t}` }, o || (r ? r() : []), o && e._ === 1 ? 64 : -2)
    return !s && l.scopeId && (l.slotScopeIds = [l.scopeId + "-s"]), i && i._c && (i._d = !0), l
}
function gi(e) {
    return e.some(t => (Ln(t) ? !(t.type === Mt || (t.type === je && !gi(t.children))) : !0)) ? e : null
}
const Hr = e => (e ? (mi(e) ? jr(e) || e.proxy : Hr(e.parent)) : null),
    Bn = Re(Object.create(null), {
        $: e => e,
        $el: e => e.vnode.el,
        $data: e => e.data,
        $props: e => e.props,
        $attrs: e => e.attrs,
        $slots: e => e.slots,
        $refs: e => e.refs,
        $parent: e => Hr(e.parent),
        $root: e => Hr(e.root),
        $emit: e => e.emit,
        $options: e => Js(e),
        $forceUpdate: e => () => yi(e.update),
        $nextTick: e => Kr.bind(e.proxy),
        $watch: e => Wc.bind(e)
    }),
    Sc = {
        get({ _: e }, t) {
            const { ctx: n, setupState: r, data: s, props: i, accessCache: o, type: l, appContext: c } = e
            let u
            if (t[0] !== "$") {
                const g = o[t]
                if (g !== void 0)
                    switch (g) {
                        case 1:
                            return r[t]
                        case 2:
                            return s[t]
                        case 4:
                            return n[t]
                        case 3:
                            return i[t]
                    }
                else {
                    if (r !== ie && J(r, t)) return (o[t] = 1), r[t]
                    if (s !== ie && J(s, t)) return (o[t] = 2), s[t]
                    if ((u = e.propsOptions[0]) && J(u, t)) return (o[t] = 3), i[t]
                    if (n !== ie && J(n, t)) return (o[t] = 4), n[t]
                    Or && (o[t] = 0)
                }
            }
            const f = Bn[t]
            let a, d
            if (f) return t === "$attrs" && Te(e, "get", t), f(e)
            if ((a = l.__cssModules) && (a = a[t])) return a
            if (n !== ie && J(n, t)) return (o[t] = 4), n[t]
            if (((d = c.config.globalProperties), J(d, t))) return d[t]
        },
        set({ _: e }, t, n) {
            const { data: r, setupState: s, ctx: i } = e
            if (s !== ie && J(s, t)) s[t] = n
            else if (r !== ie && J(r, t)) r[t] = n
            else if (J(e.props, t)) return !1
            return t[0] === "$" && t.slice(1) in e ? !1 : ((i[t] = n), !0)
        },
        has({ _: { data: e, setupState: t, accessCache: n, ctx: r, appContext: s, propsOptions: i } }, o) {
            let l
            return !!n[o] || (e !== ie && J(e, o)) || (t !== ie && J(t, o)) || ((l = i[0]) && J(l, o)) || J(r, o) || J(Bn, o) || J(s.config.globalProperties, o)
        }
    },
    Ic = ci()
let Mc = 0
function Fc(e, t, n) {
    const r = e.type,
        s = (t ? t.appContext : e.appContext) || Ic,
        i = {
            uid: Mc++,
            vnode: e,
            type: r,
            parent: t,
            appContext: s,
            root: null,
            next: null,
            subTree: null,
            update: null,
            scope: new _s(!0),
            render: null,
            proxy: null,
            exposed: null,
            exposeProxy: null,
            withProxy: null,
            provides: t ? t.provides : Object.create(s.provides),
            accessCache: null,
            renderCache: [],
            components: null,
            directives: null,
            propsOptions: ei(r, s),
            emitsOptions: qs(r, s),
            emit: null,
            emitted: null,
            propsDefaults: ie,
            inheritAttrs: r.inheritAttrs,
            ctx: ie,
            data: ie,
            props: ie,
            attrs: ie,
            slots: ie,
            refs: ie,
            setupState: ie,
            setupContext: null,
            suspense: n,
            suspenseId: n ? n.pendingId : 0,
            asyncDep: null,
            asyncResolved: !1,
            isMounted: !1,
            isUnmounted: !1,
            isDeactivated: !1,
            bc: null,
            c: null,
            bm: null,
            m: null,
            bu: null,
            u: null,
            um: null,
            bum: null,
            da: null,
            a: null,
            rtg: null,
            rtc: null,
            ec: null,
            sp: null
        }
    return (i.ctx = { _: i }), (i.root = t ? t.root : i), (i.emit = Ll.bind(null, i)), e.ce && e.ce(i), i
}
let ve = null
const Br = () => ve || Se,
    Ft = e => {
        ;(ve = e), e.scope.on()
    },
    yt = () => {
        ve && ve.scope.off(), (ve = null)
    }
function mi(e) {
    return e.vnode.shapeFlag & 4
}
let jn = !1
function kc(e, t = !1) {
    jn = t
    const { props: n, children: r } = e.vnode,
        s = mi(e)
    lc(e, n, s, t), fc(e, r)
    const i = s ? Lc(e, t) : void 0
    return (jn = !1), i
}
function Lc(e, t) {
    const n = e.type
    ;(e.accessCache = Object.create(null)), (e.proxy = It(new Proxy(e.ctx, Sc)))
    const { setup: r } = n
    if (r) {
        const s = (e.setupContext = r.length > 1 ? bi(e) : null)
        Ft(e), Tt()
        const i = lt(r, e, 0, [e.props, s])
        if ((wt(), yt(), ws(i))) {
            if ((i.then(yt, yt), t))
                return i
                    .then(o => {
                        wi(e, o, t)
                    })
                    .catch(o => {
                        Dn(o, e, 0)
                    })
            e.asyncDep = i
        } else wi(e, i, t)
    } else _i(e, t)
}
function wi(e, t, n) {
    Z(t) ? (e.type.__ssrInlineRender ? (e.ssrRender = t) : (e.render = t)) : Ce(t) && (e.setupState = Us(t)), _i(e, n)
}
let vi
function _i(e, t, n) {
    const r = e.type
    if (!e.render) {
        if (!t && vi && !r.render) {
            const s = r.template
            if (s) {
                const { isCustomElement: i, compilerOptions: o } = e.appContext.config,
                    { delimiters: l, compilerOptions: c } = r,
                    u = Re(Re({ isCustomElement: i, delimiters: l }, o), c)
                r.render = vi(s, u)
            }
        }
        e.render = r.render || $e
    }
    Ft(e), Tt(), nc(e), wt(), yt()
}
function $c(e) {
    return new Proxy(e.attrs, {
        get(t, n) {
            return Te(e, "get", "$attrs"), t[n]
        }
    })
}
function bi(e) {
    const t = r => {
        e.exposed = r || {}
    }
    let n
    return {
        get attrs() {
            return n || (n = $c(e))
        },
        slots: e.slots,
        emit: e.emit,
        expose: t
    }
}
function jr(e) {
    if (e.exposed)
        return (
            e.exposeProxy ||
            (e.exposeProxy = new Proxy(Us(It(e.exposed)), {
                get(t, n) {
                    if (n in t) return t[n]
                    if (n in Bn) return Bn[n](e)
                }
            }))
        )
}
function Hc(e) {
    return (Z(e) && e.displayName) || e.name
}
function Bc(e) {
    return Z(e) && "__vccOpts" in e
}
function lt(e, t, n, r) {
    let s
    try {
        s = r ? e(...r) : e()
    } catch (i) {
        Dn(i, t, n)
    }
    return s
}
function De(e, t, n, r) {
    if (Z(e)) {
        const i = lt(e, t, n, r)
        return (
            i &&
                ws(i) &&
                i.catch(o => {
                    Dn(o, t, n)
                }),
            i
        )
    }
    const s = []
    for (let i = 0; i < e.length; i++) s.push(De(e[i], t, n, r))
    return s
}
function Dn(e, t, n, r = !0) {
    const s = t ? t.vnode : null
    if (t) {
        let i = t.parent
        const o = t.proxy,
            l = n
        for (; i; ) {
            const u = i.ec
            if (u) {
                for (let f = 0; f < u.length; f++) if (u[f](e, o, l) === !1) return
            }
            i = i.parent
        }
        const c = t.appContext.config.errorHandler
        if (c) {
            lt(c, null, 10, [e, o, l])
            return
        }
    }
    jc(e, n, s, r)
}
function jc(e, t, n, r = !0) {
    console.error(e)
}
let Un = !1,
    Dr = !1
const Me = []
let et = 0
const rn = []
let sn = null,
    kt = 0
const on = []
let ct = null,
    Lt = 0
const Ei = Promise.resolve()
let Ur = null,
    qr = null
function Kr(e) {
    const t = Ur || Ei
    return e ? t.then(this ? e.bind(this) : e) : t
}
function Dc(e) {
    let t = et + 1,
        n = Me.length
    for (; t < n; ) {
        const r = (t + n) >>> 1
        ln(Me[r]) < e ? (t = r + 1) : (n = r)
    }
    return t
}
function yi(e) {
    ;(!Me.length || !Me.includes(e, Un && e.allowRecurse ? et + 1 : et)) && e !== qr && (e.id == null ? Me.push(e) : Me.splice(Dc(e.id), 0, e), Ci())
}
function Ci() {
    !Un && !Dr && ((Dr = !0), (Ur = Ei.then(Ni)))
}
function Uc(e) {
    const t = Me.indexOf(e)
    t > et && Me.splice(t, 1)
}
function Pi(e, t, n, r) {
    V(e) ? n.push(...e) : (!t || !t.includes(e, e.allowRecurse ? r + 1 : r)) && n.push(e), Ci()
}
function qc(e) {
    Pi(e, sn, rn, kt)
}
function Kc(e) {
    Pi(e, ct, on, Lt)
}
function Wr(e, t = null) {
    if (rn.length) {
        for (qr = t, sn = [...new Set(rn)], rn.length = 0, kt = 0; kt < sn.length; kt++) sn[kt]()
        ;(sn = null), (kt = 0), (qr = null), Wr(e, t)
    }
}
function Ri(e) {
    if (on.length) {
        const t = [...new Set(on)]
        if (((on.length = 0), ct)) {
            ct.push(...t)
            return
        }
        for (ct = t, ct.sort((n, r) => ln(n) - ln(r)), Lt = 0; Lt < ct.length; Lt++) ct[Lt]()
        ;(ct = null), (Lt = 0)
    }
}
const ln = e => (e.id == null ? 1 / 0 : e.id)
function Ni(e) {
    ;(Dr = !1), (Un = !0), Wr(e), Me.sort((n, r) => ln(n) - ln(r))
    const t = $e
    try {
        for (et = 0; et < Me.length; et++) {
            const n = Me[et]
            n && n.active !== !1 && lt(n, null, 14)
        }
    } finally {
        ;(et = 0), (Me.length = 0), Ri(), (Un = !1), (Ur = null), (Me.length || rn.length || on.length) && Ni(e)
    }
}
const Ai = {}
function $t(e, t, n) {
    return Oi(e, t, n)
}
function Oi(e, t, { immediate: n, deep: r, flush: s, onTrack: i, onTrigger: o } = ie) {
    const l = ve
    let c,
        u = !1,
        f = !1
    if (
        (me(e)
            ? ((c = () => e.value), (u = !!e._shallow))
            : it(e)
            ? ((c = () => e), (r = !0))
            : V(e)
            ? ((f = !0),
              (u = e.some(it)),
              (c = () =>
                  e.map(P => {
                      if (me(P)) return P.value
                      if (it(P)) return Ct(P)
                      if (Z(P)) return lt(P, l, 2)
                  })))
            : Z(e)
            ? t
                ? (c = () => lt(e, l, 2))
                : (c = () => {
                      if (!(l && l.isUnmounted)) return a && a(), De(e, l, 3, [d])
                  })
            : (c = $e),
        t && r)
    ) {
        const P = c
        c = () => Ct(P())
    }
    let a,
        d = P => {
            a = x.onStop = () => {
                lt(P, l, 4)
            }
        }
    if (jn) return (d = $e), t ? n && De(t, l, 3, [c(), f ? [] : void 0, d]) : c(), $e
    let g = f ? [] : Ai
    const v = () => {
        if (!!x.active)
            if (t) {
                const P = x.run()
                ;(r || u || (f ? P.some((L, S) => Yt(L, g[S])) : Yt(P, g))) && (a && a(), De(t, l, 3, [P, g === Ai ? void 0 : g, d]), (g = P))
            } else x.run()
    }
    v.allowRecurse = !!t
    let C
    s === "sync"
        ? (C = v)
        : s === "post"
        ? (C = () => Ae(v, l && l.suspense))
        : (C = () => {
              !l || l.isMounted ? qc(v) : v()
          })
    const x = new gr(c, C)
    return (
        t ? (n ? v() : (g = x.run())) : s === "post" ? Ae(x.run.bind(x), l && l.suspense) : x.run(),
        () => {
            x.stop(), l && l.scope && ms(l.scope.effects, x)
        }
    )
}
function Wc(e, t, n) {
    const r = this.proxy,
        s = be(e) ? (e.includes(".") ? xi(r, e) : () => r[e]) : e.bind(r, r)
    let i
    Z(t) ? (i = t) : ((i = t.handler), (n = t))
    const o = ve
    Ft(this)
    const l = Oi(s, i.bind(r), n)
    return o ? Ft(o) : yt(), l
}
function xi(e, t) {
    const n = t.split(".")
    return () => {
        let r = e
        for (let s = 0; s < n.length && r; s++) r = r[n[s]]
        return r
    }
}
function Ct(e, t) {
    if (!Ce(e) || e.__v_skip || ((t = t || new Set()), t.has(e))) return e
    if ((t.add(e), me(e))) Ct(e.value, t)
    else if (V(e)) for (let n = 0; n < e.length; n++) Ct(e[n], t)
    else if (Wo(e) || Vt(e))
        e.forEach(n => {
            Ct(n, t)
        })
    else if (Vo(e)) for (const n in e) Ct(e[n], t)
    return e
}
function xa() {
    return zc().slots
}
function zc() {
    const e = Br()
    return e.setupContext || (e.setupContext = bi(e))
}
function Ti(e, t, n) {
    const r = arguments.length
    return r === 2 ? (Ce(t) && !V(t) ? (Ln(t) ? Ie(e, null, [t]) : Ie(e, t)) : Ie(e, null, t)) : (r > 3 ? (n = Array.prototype.slice.call(arguments, 2)) : r === 3 && Ln(n) && (n = [n]), Ie(e, t, n))
}
const Zc = "3.2.23",
    Vc = "http://www.w3.org/2000/svg",
    Ht = typeof document != "undefined" ? document : null,
    Si = new Map(),
    Yc = {
        insert: (e, t, n) => {
            t.insertBefore(e, n || null)
        },
        remove: e => {
            const t = e.parentNode
            t && t.removeChild(e)
        },
        createElement: (e, t, n, r) => {
            const s = t ? Ht.createElementNS(Vc, e) : Ht.createElement(e, n ? { is: n } : void 0)
            return e === "select" && r && r.multiple != null && s.setAttribute("multiple", r.multiple), s
        },
        createText: e => Ht.createTextNode(e),
        createComment: e => Ht.createComment(e),
        setText: (e, t) => {
            e.nodeValue = t
        },
        setElementText: (e, t) => {
            e.textContent = t
        },
        parentNode: e => e.parentNode,
        nextSibling: e => e.nextSibling,
        querySelector: e => Ht.querySelector(e),
        setScopeId(e, t) {
            e.setAttribute(t, "")
        },
        cloneNode(e) {
            const t = e.cloneNode(!0)
            return "_value" in e && (t._value = e._value), t
        },
        insertStaticContent(e, t, n, r) {
            const s = n ? n.previousSibling : t.lastChild
            let i = Si.get(e)
            if (!i) {
                const o = Ht.createElement("template")
                if (((o.innerHTML = r ? `<svg>${e}</svg>` : e), (i = o.content), r)) {
                    const l = i.firstChild
                    for (; l.firstChild; ) i.appendChild(l.firstChild)
                    i.removeChild(l)
                }
                Si.set(e, i)
            }
            return t.insertBefore(i.cloneNode(!0), n), [s ? s.nextSibling : t.firstChild, n ? n.previousSibling : t.lastChild]
        }
    }
function Qc(e, t, n) {
    const r = e._vtc
    r && (t = (t ? [t, ...r] : [...r]).join(" ")), t == null ? e.removeAttribute("class") : n ? e.setAttribute("class", t) : (e.className = t)
}
function Jc(e, t, n) {
    const r = e.style,
        s = be(n)
    if (n && !s) {
        for (const i in n) zr(r, i, n[i])
        if (t && !be(t)) for (const i in t) n[i] == null && zr(r, i, "")
    } else {
        const i = r.display
        s ? t !== n && (r.cssText = n) : t && e.removeAttribute("style"), "_vod" in e && (r.display = i)
    }
}
const Ii = /\s*!important$/
function zr(e, t, n) {
    if (V(n)) n.forEach(r => zr(e, t, r))
    else if (t.startsWith("--")) e.setProperty(t, n)
    else {
        const r = Xc(e, t)
        Ii.test(n) ? e.setProperty(Ot(r), n.replace(Ii, ""), "important") : (e[r] = n)
    }
}
const Mi = ["Webkit", "Moz", "ms"],
    Zr = {}
function Xc(e, t) {
    const n = Zr[t]
    if (n) return n
    let r = ze(t)
    if (r !== "filter" && r in e) return (Zr[t] = r)
    r = bn(r)
    for (let s = 0; s < Mi.length; s++) {
        const i = Mi[s] + r
        if (i in e) return (Zr[t] = i)
    }
    return t
}
const Fi = "http://www.w3.org/1999/xlink"
function Gc(e, t, n, r, s) {
    if (r && t.startsWith("xlink:")) n == null ? e.removeAttributeNS(Fi, t.slice(6, t.length)) : e.setAttributeNS(Fi, t, n)
    else {
        const i = Ho(t)
        n == null || (i && !gs(n)) ? e.removeAttribute(t) : e.setAttribute(t, i ? "" : n)
    }
}
function eu(e, t, n, r, s, i, o) {
    if (t === "innerHTML" || t === "textContent") {
        r && o(r, s, i), (e[t] = n == null ? "" : n)
        return
    }
    if (t === "value" && e.tagName !== "PROGRESS" && !e.tagName.includes("-")) {
        e._value = n
        const l = n == null ? "" : n
        ;(e.value !== l || e.tagName === "OPTION") && (e.value = l), n == null && e.removeAttribute(t)
        return
    }
    if (n === "" || n == null) {
        const l = typeof e[t]
        if (l === "boolean") {
            e[t] = gs(n)
            return
        } else if (n == null && l === "string") {
            ;(e[t] = ""), e.removeAttribute(t)
            return
        } else if (l === "number") {
            try {
                e[t] = 0
            } catch {}
            e.removeAttribute(t)
            return
        }
    }
    try {
        e[t] = n
    } catch {}
}
let qn = Date.now,
    ki = !1
if (typeof window != "undefined") {
    qn() > document.createEvent("Event").timeStamp && (qn = () => performance.now())
    const e = navigator.userAgent.match(/firefox\/(\d+)/i)
    ki = !!(e && Number(e[1]) <= 53)
}
let Vr = 0
const tu = Promise.resolve(),
    nu = () => {
        Vr = 0
    },
    ru = () => Vr || (tu.then(nu), (Vr = qn()))
function su(e, t, n, r) {
    e.addEventListener(t, n, r)
}
function iu(e, t, n, r) {
    e.removeEventListener(t, n, r)
}
function ou(e, t, n, r, s = null) {
    const i = e._vei || (e._vei = {}),
        o = i[t]
    if (r && o) o.value = r
    else {
        const [l, c] = lu(t)
        if (r) {
            const u = (i[t] = cu(r, s))
            su(e, l, u, c)
        } else o && (iu(e, l, o, c), (i[t] = void 0))
    }
}
const Li = /(?:Once|Passive|Capture)$/
function lu(e) {
    let t
    if (Li.test(e)) {
        t = {}
        let n
        for (; (n = e.match(Li)); ) (e = e.slice(0, e.length - n[0].length)), (t[n[0].toLowerCase()] = !0)
    }
    return [Ot(e.slice(2)), t]
}
function cu(e, t) {
    const n = r => {
        const s = r.timeStamp || qn()
        ;(ki || s >= n.attached - 1) && De(uu(r, n.value), t, 5, [r])
    }
    return (n.value = e), (n.attached = ru()), n
}
function uu(e, t) {
    if (V(t)) {
        const n = e.stopImmediatePropagation
        return (
            (e.stopImmediatePropagation = () => {
                n.call(e), (e._stopped = !0)
            }),
            t.map(r => s => !s._stopped && r(s))
        )
    } else return t
}
const $i = /^on[a-z]/,
    fu = (e, t, n, r, s = !1, i, o, l, c) => {
        t === "class"
            ? Qc(e, r, s)
            : t === "style"
            ? Jc(e, n, r)
            : mn(t)
            ? or(t) || ou(e, t, n, r, o)
            : (t[0] === "." ? ((t = t.slice(1)), !0) : t[0] === "^" ? ((t = t.slice(1)), !1) : au(e, t, r, s))
            ? eu(e, t, r, i, o, l, c)
            : (t === "true-value" ? (e._trueValue = r) : t === "false-value" && (e._falseValue = r), Gc(e, t, r, s))
    }
function au(e, t, n, r) {
    return r
        ? !!(t === "innerHTML" || t === "textContent" || (t in e && $i.test(t) && Z(n)))
        : t === "spellcheck" || t === "draggable" || t === "form" || (t === "list" && e.tagName === "INPUT") || (t === "type" && e.tagName === "TEXTAREA") || ($i.test(t) && be(n))
        ? !1
        : t in e
}
const Ta = {
    beforeMount(e, { value: t }, { transition: n }) {
        ;(e._vod = e.style.display === "none" ? "" : e.style.display), n && t ? n.beforeEnter(e) : cn(e, t)
    },
    mounted(e, { value: t }, { transition: n }) {
        n && t && n.enter(e)
    },
    updated(e, { value: t, oldValue: n }, { transition: r }) {
        !t != !n &&
            (r
                ? t
                    ? (r.beforeEnter(e), cn(e, !0), r.enter(e))
                    : r.leave(e, () => {
                          cn(e, !1)
                      })
                : cn(e, t))
    },
    beforeUnmount(e, { value: t }) {
        cn(e, t)
    }
}
function cn(e, t) {
    e.style.display = t ? e._vod : "none"
}
const du = Re({ patchProp: fu }, Yc)
let Hi
function hu() {
    return Hi || (Hi = pc(du))
}
const Sa = (...e) => {
    const t = hu().createApp(...e),
        { mount: n } = t
    return (
        (t.mount = r => {
            const s = pu(r)
            if (!s) return
            const i = t._component
            !Z(i) && !i.render && !i.template && (i.template = s.innerHTML), (s.innerHTML = "")
            const o = n(s, !1, s instanceof SVGElement)
            return s instanceof Element && (s.removeAttribute("v-cloak"), s.setAttribute("data-v-app", "")), o
        }),
        t
    )
}
function pu(e) {
    return be(e) ? document.querySelector(e) : e
}
/*!
 * vue-router v4.0.12
 * (c) 2021 Eduardo San Martin Morote
 * @license MIT
 */ const Bi = typeof Symbol == "function" && typeof Symbol.toStringTag == "symbol",
    Bt = e => (Bi ? Symbol(e) : "_vr_" + e),
    gu = Bt("rvlm"),
    ji = Bt("rvd"),
    Kn = Bt("r"),
    Yr = Bt("rl"),
    Qr = Bt("rvl"),
    jt = typeof window != "undefined"
function mu(e) {
    return e.__esModule || (Bi && e[Symbol.toStringTag] === "Module")
}
const oe = Object.assign
function Jr(e, t) {
    const n = {}
    for (const r in t) {
        const s = t[r]
        n[r] = Array.isArray(s) ? s.map(e) : e(s)
    }
    return n
}
const un = () => {},
    wu = /\/$/,
    vu = e => e.replace(wu, "")
function Xr(e, t, n = "/") {
    let r,
        s = {},
        i = "",
        o = ""
    const l = t.indexOf("?"),
        c = t.indexOf("#", l > -1 ? l : 0)
    return (
        l > -1 && ((r = t.slice(0, l)), (i = t.slice(l + 1, c > -1 ? c : t.length)), (s = e(i))),
        c > -1 && ((r = r || t.slice(0, c)), (o = t.slice(c, t.length))),
        (r = yu(r != null ? r : t, n)),
        { fullPath: r + (i && "?") + i + o, path: r, query: s, hash: o }
    )
}
function _u(e, t) {
    const n = t.query ? e(t.query) : ""
    return t.path + (n && "?") + n + (t.hash || "")
}
function Di(e, t) {
    return !t || !e.toLowerCase().startsWith(t.toLowerCase()) ? e : e.slice(t.length) || "/"
}
function bu(e, t, n) {
    const r = t.matched.length - 1,
        s = n.matched.length - 1
    return r > -1 && r === s && Dt(t.matched[r], n.matched[s]) && Ui(t.params, n.params) && e(t.query) === e(n.query) && t.hash === n.hash
}
function Dt(e, t) {
    return (e.aliasOf || e) === (t.aliasOf || t)
}
function Ui(e, t) {
    if (Object.keys(e).length !== Object.keys(t).length) return !1
    for (const n in e) if (!Eu(e[n], t[n])) return !1
    return !0
}
function Eu(e, t) {
    return Array.isArray(e) ? qi(e, t) : Array.isArray(t) ? qi(t, e) : e === t
}
function qi(e, t) {
    return Array.isArray(t) ? e.length === t.length && e.every((n, r) => n === t[r]) : e.length === 1 && e[0] === t
}
function yu(e, t) {
    if (e.startsWith("/")) return e
    if (!e) return t
    const n = t.split("/"),
        r = e.split("/")
    let s = n.length - 1,
        i,
        o
    for (i = 0; i < r.length; i++)
        if (((o = r[i]), !(s === 1 || o === ".")))
            if (o === "..") s--
            else break
    return n.slice(0, s).join("/") + "/" + r.slice(i - (i === r.length ? 1 : 0)).join("/")
}
var fn
;(function (e) {
    ;(e.pop = "pop"), (e.push = "push")
})(fn || (fn = {}))
var an
;(function (e) {
    ;(e.back = "back"), (e.forward = "forward"), (e.unknown = "")
})(an || (an = {}))
function Cu(e) {
    if (!e)
        if (jt) {
            const t = document.querySelector("base")
            ;(e = (t && t.getAttribute("href")) || "/"), (e = e.replace(/^\w+:\/\/[^\/]+/, ""))
        } else e = "/"
    return e[0] !== "/" && e[0] !== "#" && (e = "/" + e), vu(e)
}
const Pu = /^[^#]+#/
function Ru(e, t) {
    return e.replace(Pu, "#") + t
}
function Nu(e, t) {
    const n = document.documentElement.getBoundingClientRect(),
        r = e.getBoundingClientRect()
    return { behavior: t.behavior, left: r.left - n.left - (t.left || 0), top: r.top - n.top - (t.top || 0) }
}
const Wn = () => ({ left: window.pageXOffset, top: window.pageYOffset })
function Au(e) {
    let t
    if ("el" in e) {
        const n = e.el,
            r = typeof n == "string" && n.startsWith("#"),
            s = typeof n == "string" ? (r ? document.getElementById(n.slice(1)) : document.querySelector(n)) : n
        if (!s) return
        t = Nu(s, e)
    } else t = e
    "scrollBehavior" in document.documentElement.style ? window.scrollTo(t) : window.scrollTo(t.left != null ? t.left : window.pageXOffset, t.top != null ? t.top : window.pageYOffset)
}
function Ki(e, t) {
    return (history.state ? history.state.position - t : -1) + e
}
const Gr = new Map()
function Ou(e, t) {
    Gr.set(e, t)
}
function xu(e) {
    const t = Gr.get(e)
    return Gr.delete(e), t
}
let Tu = () => location.protocol + "//" + location.host
function Wi(e, t) {
    const { pathname: n, search: r, hash: s } = t,
        i = e.indexOf("#")
    if (i > -1) {
        let l = s.includes(e.slice(i)) ? e.slice(i).length : 1,
            c = s.slice(l)
        return c[0] !== "/" && (c = "/" + c), Di(c, "")
    }
    return Di(n, e) + r + s
}
function Su(e, t, n, r) {
    let s = [],
        i = [],
        o = null
    const l = ({ state: d }) => {
        const g = Wi(e, location),
            v = n.value,
            C = t.value
        let x = 0
        if (d) {
            if (((n.value = g), (t.value = d), o && o === v)) {
                o = null
                return
            }
            x = C ? d.position - C.position : 0
        } else r(g)
        s.forEach(P => {
            P(n.value, v, { delta: x, type: fn.pop, direction: x ? (x > 0 ? an.forward : an.back) : an.unknown })
        })
    }
    function c() {
        o = n.value
    }
    function u(d) {
        s.push(d)
        const g = () => {
            const v = s.indexOf(d)
            v > -1 && s.splice(v, 1)
        }
        return i.push(g), g
    }
    function f() {
        const { history: d } = window
        !d.state || d.replaceState(oe({}, d.state, { scroll: Wn() }), "")
    }
    function a() {
        for (const d of i) d()
        ;(i = []), window.removeEventListener("popstate", l), window.removeEventListener("beforeunload", f)
    }
    return window.addEventListener("popstate", l), window.addEventListener("beforeunload", f), { pauseListeners: c, listen: u, destroy: a }
}
function zi(e, t, n, r = !1, s = !1) {
    return { back: e, current: t, forward: n, replaced: r, position: window.history.length, scroll: s ? Wn() : null }
}
function Iu(e) {
    const { history: t, location: n } = window,
        r = { value: Wi(e, n) },
        s = { value: t.state }
    s.value || i(r.value, { back: null, current: r.value, forward: null, position: t.length - 1, replaced: !0, scroll: null }, !0)
    function i(c, u, f) {
        const a = e.indexOf("#"),
            d = a > -1 ? (n.host && document.querySelector("base") ? e : e.slice(a)) + c : Tu() + e + c
        try {
            t[f ? "replaceState" : "pushState"](u, "", d), (s.value = u)
        } catch (g) {
            console.error(g), n[f ? "replace" : "assign"](d)
        }
    }
    function o(c, u) {
        const f = oe({}, t.state, zi(s.value.back, c, s.value.forward, !0), u, { position: s.value.position })
        i(c, f, !0), (r.value = c)
    }
    function l(c, u) {
        const f = oe({}, s.value, t.state, { forward: c, scroll: Wn() })
        i(f.current, f, !0)
        const a = oe({}, zi(r.value, c, null), { position: f.position + 1 }, u)
        i(c, a, !1), (r.value = c)
    }
    return { location: r, state: s, push: l, replace: o }
}
function Mu(e) {
    e = Cu(e)
    const t = Iu(e),
        n = Su(e, t.state, t.location, t.replace)
    function r(i, o = !0) {
        o || n.pauseListeners(), history.go(i)
    }
    const s = oe({ location: "", base: e, go: r, createHref: Ru.bind(null, e) }, t, n)
    return Object.defineProperty(s, "location", { enumerable: !0, get: () => t.location.value }), Object.defineProperty(s, "state", { enumerable: !0, get: () => t.state.value }), s
}
function Ia(e) {
    return (e = location.host ? e || location.pathname + location.search : ""), e.includes("#") || (e += "#"), Mu(e)
}
function Fu(e) {
    return typeof e == "string" || (e && typeof e == "object")
}
function Zi(e) {
    return typeof e == "string" || typeof e == "symbol"
}
const ut = { path: "/", name: void 0, params: {}, query: {}, hash: "", fullPath: "/", matched: [], meta: {}, redirectedFrom: void 0 },
    Vi = Bt("nf")
var Yi
;(function (e) {
    ;(e[(e.aborted = 4)] = "aborted"), (e[(e.cancelled = 8)] = "cancelled"), (e[(e.duplicated = 16)] = "duplicated")
})(Yi || (Yi = {}))
function Ut(e, t) {
    return oe(new Error(), { type: e, [Vi]: !0 }, t)
}
function Pt(e, t) {
    return e instanceof Error && Vi in e && (t == null || !!(e.type & t))
}
const Qi = "[^/]+?",
    ku = { sensitive: !1, strict: !1, start: !0, end: !0 },
    Lu = /[.+*?^${}()[\]/\\]/g
function $u(e, t) {
    const n = oe({}, ku, t),
        r = []
    let s = n.start ? "^" : ""
    const i = []
    for (const u of e) {
        const f = u.length ? [] : [90]
        n.strict && !u.length && (s += "/")
        for (let a = 0; a < u.length; a++) {
            const d = u[a]
            let g = 40 + (n.sensitive ? 0.25 : 0)
            if (d.type === 0) a || (s += "/"), (s += d.value.replace(Lu, "\\$&")), (g += 40)
            else if (d.type === 1) {
                const { value: v, repeatable: C, optional: x, regexp: P } = d
                i.push({ name: v, repeatable: C, optional: x })
                const L = P || Qi
                if (L !== Qi) {
                    g += 10
                    try {
                        new RegExp(`(${L})`)
                    } catch (F) {
                        throw new Error(`Invalid custom RegExp for param "${v}" (${L}): ` + F.message)
                    }
                }
                let S = C ? `((?:${L})(?:/(?:${L}))*)` : `(${L})`
                a || (S = x && u.length < 2 ? `(?:/${S})` : "/" + S), x && (S += "?"), (s += S), (g += 20), x && (g += -8), C && (g += -20), L === ".*" && (g += -50)
            }
            f.push(g)
        }
        r.push(f)
    }
    if (n.strict && n.end) {
        const u = r.length - 1
        r[u][r[u].length - 1] += 0.7000000000000001
    }
    n.strict || (s += "/?"), n.end ? (s += "$") : n.strict && (s += "(?:/|$)")
    const o = new RegExp(s, n.sensitive ? "" : "i")
    function l(u) {
        const f = u.match(o),
            a = {}
        if (!f) return null
        for (let d = 1; d < f.length; d++) {
            const g = f[d] || "",
                v = i[d - 1]
            a[v.name] = g && v.repeatable ? g.split("/") : g
        }
        return a
    }
    function c(u) {
        let f = "",
            a = !1
        for (const d of e) {
            ;(!a || !f.endsWith("/")) && (f += "/"), (a = !1)
            for (const g of d)
                if (g.type === 0) f += g.value
                else if (g.type === 1) {
                    const { value: v, repeatable: C, optional: x } = g,
                        P = v in u ? u[v] : ""
                    if (Array.isArray(P) && !C) throw new Error(`Provided param "${v}" is an array but it is not repeatable (* or + modifiers)`)
                    const L = Array.isArray(P) ? P.join("/") : P
                    if (!L)
                        if (x) d.length < 2 && (f.endsWith("/") ? (f = f.slice(0, -1)) : (a = !0))
                        else throw new Error(`Missing required param "${v}"`)
                    f += L
                }
        }
        return f
    }
    return { re: o, score: r, keys: i, parse: l, stringify: c }
}
function Hu(e, t) {
    let n = 0
    for (; n < e.length && n < t.length; ) {
        const r = t[n] - e[n]
        if (r) return r
        n++
    }
    return e.length < t.length ? (e.length === 1 && e[0] === 40 + 40 ? -1 : 1) : e.length > t.length ? (t.length === 1 && t[0] === 40 + 40 ? 1 : -1) : 0
}
function Bu(e, t) {
    let n = 0
    const r = e.score,
        s = t.score
    for (; n < r.length && n < s.length; ) {
        const i = Hu(r[n], s[n])
        if (i) return i
        n++
    }
    return s.length - r.length
}
const ju = { type: 0, value: "" },
    Du = /[a-zA-Z0-9_]/
function Uu(e) {
    if (!e) return [[]]
    if (e === "/") return [[ju]]
    if (!e.startsWith("/")) throw new Error(`Invalid path "${e}"`)
    function t(g) {
        throw new Error(`ERR (${n})/"${u}": ${g}`)
    }
    let n = 0,
        r = n
    const s = []
    let i
    function o() {
        i && s.push(i), (i = [])
    }
    let l = 0,
        c,
        u = "",
        f = ""
    function a() {
        !u ||
            (n === 0
                ? i.push({ type: 0, value: u })
                : n === 1 || n === 2 || n === 3
                ? (i.length > 1 && (c === "*" || c === "+") && t(`A repeatable param (${u}) must be alone in its segment. eg: '/:ids+.`),
                  i.push({ type: 1, value: u, regexp: f, repeatable: c === "*" || c === "+", optional: c === "*" || c === "?" }))
                : t("Invalid state to consume buffer"),
            (u = ""))
    }
    function d() {
        u += c
    }
    for (; l < e.length; ) {
        if (((c = e[l++]), c === "\\" && n !== 2)) {
            ;(r = n), (n = 4)
            continue
        }
        switch (n) {
            case 0:
                c === "/" ? (u && a(), o()) : c === ":" ? (a(), (n = 1)) : d()
                break
            case 4:
                d(), (n = r)
                break
            case 1:
                c === "(" ? (n = 2) : Du.test(c) ? d() : (a(), (n = 0), c !== "*" && c !== "?" && c !== "+" && l--)
                break
            case 2:
                c === ")" ? (f[f.length - 1] == "\\" ? (f = f.slice(0, -1) + c) : (n = 3)) : (f += c)
                break
            case 3:
                a(), (n = 0), c !== "*" && c !== "?" && c !== "+" && l--, (f = "")
                break
            default:
                t("Unknown state")
                break
        }
    }
    return n === 2 && t(`Unfinished custom RegExp for param "${u}"`), a(), o(), s
}
function qu(e, t, n) {
    const r = $u(Uu(e.path), n),
        s = oe(r, { record: e, parent: t, children: [], alias: [] })
    return t && !s.record.aliasOf == !t.record.aliasOf && t.children.push(s), s
}
function Ku(e, t) {
    const n = [],
        r = new Map()
    t = Xi({ strict: !1, end: !0, sensitive: !1 }, t)
    function s(f) {
        return r.get(f)
    }
    function i(f, a, d) {
        const g = !d,
            v = zu(f)
        v.aliasOf = d && d.record
        const C = Xi(t, f),
            x = [v]
        if ("alias" in f) {
            const S = typeof f.alias == "string" ? [f.alias] : f.alias
            for (const F of S) x.push(oe({}, v, { components: d ? d.record.components : v.components, path: F, aliasOf: d ? d.record : v }))
        }
        let P, L
        for (const S of x) {
            const { path: F } = S
            if (a && F[0] !== "/") {
                const G = a.record.path,
                    j = G[G.length - 1] === "/" ? "" : "/"
                S.path = a.record.path + (F && j + F)
            }
            if (((P = qu(S, a, C)), d ? d.alias.push(P) : ((L = L || P), L !== P && L.alias.push(P), g && f.name && !Ji(P) && o(f.name)), "children" in v)) {
                const G = v.children
                for (let j = 0; j < G.length; j++) i(G[j], P, d && d.children[j])
            }
            ;(d = d || P), c(P)
        }
        return L
            ? () => {
                  o(L)
              }
            : un
    }
    function o(f) {
        if (Zi(f)) {
            const a = r.get(f)
            a && (r.delete(f), n.splice(n.indexOf(a), 1), a.children.forEach(o), a.alias.forEach(o))
        } else {
            const a = n.indexOf(f)
            a > -1 && (n.splice(a, 1), f.record.name && r.delete(f.record.name), f.children.forEach(o), f.alias.forEach(o))
        }
    }
    function l() {
        return n
    }
    function c(f) {
        let a = 0
        for (; a < n.length && Bu(f, n[a]) >= 0; ) a++
        n.splice(a, 0, f), f.record.name && !Ji(f) && r.set(f.record.name, f)
    }
    function u(f, a) {
        let d,
            g = {},
            v,
            C
        if ("name" in f && f.name) {
            if (((d = r.get(f.name)), !d)) throw Ut(1, { location: f })
            ;(C = d.record.name),
                (g = oe(
                    Wu(
                        a.params,
                        d.keys.filter(L => !L.optional).map(L => L.name)
                    ),
                    f.params
                )),
                (v = d.stringify(g))
        } else if ("path" in f) (v = f.path), (d = n.find(L => L.re.test(v))), d && ((g = d.parse(v)), (C = d.record.name))
        else {
            if (((d = a.name ? r.get(a.name) : n.find(L => L.re.test(a.path))), !d)) throw Ut(1, { location: f, currentLocation: a })
            ;(C = d.record.name), (g = oe({}, a.params, f.params)), (v = d.stringify(g))
        }
        const x = []
        let P = d
        for (; P; ) x.unshift(P.record), (P = P.parent)
        return { name: C, path: v, params: g, matched: x, meta: Vu(x) }
    }
    return e.forEach(f => i(f)), { addRoute: i, resolve: u, removeRoute: o, getRoutes: l, getRecordMatcher: s }
}
function Wu(e, t) {
    const n = {}
    for (const r of t) r in e && (n[r] = e[r])
    return n
}
function zu(e) {
    return {
        path: e.path,
        redirect: e.redirect,
        name: e.name,
        meta: e.meta || {},
        aliasOf: void 0,
        beforeEnter: e.beforeEnter,
        props: Zu(e),
        children: e.children || [],
        instances: {},
        leaveGuards: new Set(),
        updateGuards: new Set(),
        enterCallbacks: {},
        components: "components" in e ? e.components || {} : { default: e.component }
    }
}
function Zu(e) {
    const t = {},
        n = e.props || !1
    if ("component" in e) t.default = n
    else for (const r in e.components) t[r] = typeof n == "boolean" ? n : n[r]
    return t
}
function Ji(e) {
    for (; e; ) {
        if (e.record.aliasOf) return !0
        e = e.parent
    }
    return !1
}
function Vu(e) {
    return e.reduce((t, n) => oe(t, n.meta), {})
}
function Xi(e, t) {
    const n = {}
    for (const r in e) n[r] = r in t ? t[r] : e[r]
    return n
}
const Gi = /#/g,
    Yu = /&/g,
    Qu = /\//g,
    Ju = /=/g,
    Xu = /\?/g,
    eo = /\+/g,
    Gu = /%5B/g,
    ef = /%5D/g,
    to = /%5E/g,
    tf = /%60/g,
    no = /%7B/g,
    nf = /%7C/g,
    ro = /%7D/g,
    rf = /%20/g
function es(e) {
    return encodeURI("" + e)
        .replace(nf, "|")
        .replace(Gu, "[")
        .replace(ef, "]")
}
function sf(e) {
    return es(e).replace(no, "{").replace(ro, "}").replace(to, "^")
}
function ts(e) {
    return es(e).replace(eo, "%2B").replace(rf, "+").replace(Gi, "%23").replace(Yu, "%26").replace(tf, "`").replace(no, "{").replace(ro, "}").replace(to, "^")
}
function of(e) {
    return ts(e).replace(Ju, "%3D")
}
function lf(e) {
    return es(e).replace(Gi, "%23").replace(Xu, "%3F")
}
function cf(e) {
    return e == null ? "" : lf(e).replace(Qu, "%2F")
}
function zn(e) {
    try {
        return decodeURIComponent("" + e)
    } catch {}
    return "" + e
}
function uf(e) {
    const t = {}
    if (e === "" || e === "?") return t
    const r = (e[0] === "?" ? e.slice(1) : e).split("&")
    for (let s = 0; s < r.length; ++s) {
        const i = r[s].replace(eo, " "),
            o = i.indexOf("="),
            l = zn(o < 0 ? i : i.slice(0, o)),
            c = o < 0 ? null : zn(i.slice(o + 1))
        if (l in t) {
            let u = t[l]
            Array.isArray(u) || (u = t[l] = [u]), u.push(c)
        } else t[l] = c
    }
    return t
}
function so(e) {
    let t = ""
    for (let n in e) {
        const r = e[n]
        if (((n = of(n)), r == null)) {
            r !== void 0 && (t += (t.length ? "&" : "") + n)
            continue
        }
        ;(Array.isArray(r) ? r.map(i => i && ts(i)) : [r && ts(r)]).forEach(i => {
            i !== void 0 && ((t += (t.length ? "&" : "") + n), i != null && (t += "=" + i))
        })
    }
    return t
}
function ff(e) {
    const t = {}
    for (const n in e) {
        const r = e[n]
        r !== void 0 && (t[n] = Array.isArray(r) ? r.map(s => (s == null ? null : "" + s)) : r == null ? r : "" + r)
    }
    return t
}
function dn() {
    let e = []
    function t(r) {
        return (
            e.push(r),
            () => {
                const s = e.indexOf(r)
                s > -1 && e.splice(s, 1)
            }
        )
    }
    function n() {
        e = []
    }
    return { add: t, list: () => e, reset: n }
}
function ft(e, t, n, r, s) {
    const i = r && (r.enterCallbacks[s] = r.enterCallbacks[s] || [])
    return () =>
        new Promise((o, l) => {
            const c = a => {
                    a === !1
                        ? l(Ut(4, { from: n, to: t }))
                        : a instanceof Error
                        ? l(a)
                        : Fu(a)
                        ? l(Ut(2, { from: t, to: a }))
                        : (i && r.enterCallbacks[s] === i && typeof a == "function" && i.push(a), o())
                },
                u = e.call(r && r.instances[s], t, n, c)
            let f = Promise.resolve(u)
            e.length < 3 && (f = f.then(c)), f.catch(a => l(a))
        })
}
function ns(e, t, n, r) {
    const s = []
    for (const i of e)
        for (const o in i.components) {
            let l = i.components[o]
            if (!(t !== "beforeRouteEnter" && !i.instances[o]))
                if (af(l)) {
                    const u = (l.__vccOpts || l)[t]
                    u && s.push(ft(u, n, r, i, o))
                } else {
                    let c = l()
                    s.push(() =>
                        c.then(u => {
                            if (!u) return Promise.reject(new Error(`Couldn't resolve component "${o}" at "${i.path}"`))
                            const f = mu(u) ? u.default : u
                            i.components[o] = f
                            const d = (f.__vccOpts || f)[t]
                            return d && ft(d, n, r, i, o)()
                        })
                    )
                }
        }
    return s
}
function af(e) {
    return typeof e == "object" || "displayName" in e || "props" in e || "__vccOpts" in e
}
function io(e) {
    const t = Be(Kn),
        n = Be(Yr),
        r = He(() => t.resolve(vt(e.to))),
        s = He(() => {
            const { matched: c } = r.value,
                { length: u } = c,
                f = c[u - 1],
                a = n.matched
            if (!f || !a.length) return -1
            const d = a.findIndex(Dt.bind(null, f))
            if (d > -1) return d
            const g = oo(c[u - 2])
            return u > 1 && oo(f) === g && a[a.length - 1].path !== g ? a.findIndex(Dt.bind(null, c[u - 2])) : d
        }),
        i = He(() => s.value > -1 && gf(n.params, r.value.params)),
        o = He(() => s.value > -1 && s.value === n.matched.length - 1 && Ui(n.params, r.value.params))
    function l(c = {}) {
        return pf(c) ? t[vt(e.replace) ? "replace" : "push"](vt(e.to)).catch(un) : Promise.resolve()
    }
    return { route: r, href: He(() => r.value.href), isActive: i, isExactActive: o, navigate: l }
}
const df = zs({
        name: "RouterLink",
        props: { to: { type: [String, Object], required: !0 }, replace: Boolean, activeClass: String, exactActiveClass: String, custom: Boolean, ariaCurrentValue: { type: String, default: "page" } },
        useLink: io,
        setup(e, { slots: t }) {
            const n = St(io(e)),
                { options: r } = Be(Kn),
                s = He(() => ({
                    [lo(e.activeClass, r.linkActiveClass, "router-link-active")]: n.isActive,
                    [lo(e.exactActiveClass, r.linkExactActiveClass, "router-link-exact-active")]: n.isExactActive
                }))
            return () => {
                const i = t.default && t.default(n)
                return e.custom ? i : Ti("a", { "aria-current": n.isExactActive ? e.ariaCurrentValue : null, href: n.href, onClick: n.navigate, class: s.value }, i)
            }
        }
    }),
    hf = df
function pf(e) {
    if (!(e.metaKey || e.altKey || e.ctrlKey || e.shiftKey) && !e.defaultPrevented && !(e.button !== void 0 && e.button !== 0)) {
        if (e.currentTarget && e.currentTarget.getAttribute) {
            const t = e.currentTarget.getAttribute("target")
            if (/\b_blank\b/i.test(t)) return
        }
        return e.preventDefault && e.preventDefault(), !0
    }
}
function gf(e, t) {
    for (const n in t) {
        const r = t[n],
            s = e[n]
        if (typeof r == "string") {
            if (r !== s) return !1
        } else if (!Array.isArray(s) || s.length !== r.length || r.some((i, o) => i !== s[o])) return !1
    }
    return !0
}
function oo(e) {
    return e ? (e.aliasOf ? e.aliasOf.path : e.path) : ""
}
const lo = (e, t, n) => (e != null ? e : t != null ? t : n),
    mf = zs({
        name: "RouterView",
        inheritAttrs: !1,
        props: { name: { type: String, default: "default" }, route: Object },
        setup(e, { attrs: t, slots: n }) {
            const r = Be(Qr),
                s = He(() => e.route || r.value),
                i = Be(ji, 0),
                o = He(() => s.value.matched[i])
            Sn(ji, i + 1), Sn(gu, o), Sn(Qr, s)
            const l = xn()
            return (
                $t(
                    () => [l.value, o.value, e.name],
                    ([c, u, f], [a, d, g]) => {
                        u && ((u.instances[f] = c), d && d !== u && c && c === a && (u.leaveGuards.size || (u.leaveGuards = d.leaveGuards), u.updateGuards.size || (u.updateGuards = d.updateGuards))),
                            c && u && (!d || !Dt(u, d) || !a) && (u.enterCallbacks[f] || []).forEach(v => v(c))
                    },
                    { flush: "post" }
                ),
                () => {
                    const c = s.value,
                        u = o.value,
                        f = u && u.components[e.name],
                        a = e.name
                    if (!f) return co(n.default, { Component: f, route: c })
                    const d = u.props[e.name],
                        g = d ? (d === !0 ? c.params : typeof d == "function" ? d(c) : d) : null,
                        C = Ti(
                            f,
                            oe({}, g, t, {
                                onVnodeUnmounted: x => {
                                    x.component.isUnmounted && (u.instances[a] = null)
                                },
                                ref: l
                            })
                        )
                    return co(n.default, { Component: C, route: c }) || C
                }
            )
        }
    })
function co(e, t) {
    if (!e) return null
    const n = e(t)
    return n.length === 1 ? n[0] : n
}
const wf = mf
function Ma(e) {
    const t = Ku(e.routes, e),
        n = e.parseQuery || uf,
        r = e.stringifyQuery || so,
        s = e.history,
        i = dn(),
        o = dn(),
        l = dn(),
        c = xl(ut)
    let u = ut
    jt && e.scrollBehavior && "scrollRestoration" in history && (history.scrollRestoration = "manual")
    const f = Jr.bind(null, _ => "" + _),
        a = Jr.bind(null, cf),
        d = Jr.bind(null, zn)
    function g(_, M) {
        let O, $
        return Zi(_) ? ((O = t.getRecordMatcher(_)), ($ = M)) : ($ = _), t.addRoute($, O)
    }
    function v(_) {
        const M = t.getRecordMatcher(_)
        M && t.removeRoute(M)
    }
    function C() {
        return t.getRoutes().map(_ => _.record)
    }
    function x(_) {
        return !!t.getRecordMatcher(_)
    }
    function P(_, M) {
        if (((M = oe({}, M || c.value)), typeof _ == "string")) {
            const z = Xr(n, _, M.path),
                h = t.resolve({ path: z.path }, M),
                p = s.createHref(z.fullPath)
            return oe(z, h, { params: d(h.params), hash: zn(z.hash), redirectedFrom: void 0, href: p })
        }
        let O
        if ("path" in _) O = oe({}, _, { path: Xr(n, _.path, M.path).path })
        else {
            const z = oe({}, _.params)
            for (const h in z) z[h] == null && delete z[h]
            ;(O = oe({}, _, { params: a(_.params) })), (M.params = a(M.params))
        }
        const $ = t.resolve(O, M),
            re = _.hash || ""
        $.params = f(d($.params))
        const fe = _u(r, oe({}, _, { hash: sf(re), path: $.path })),
            Y = s.createHref(fe)
        return oe({ fullPath: fe, hash: re, query: r === so ? ff(_.query) : _.query || {} }, $, { redirectedFrom: void 0, href: Y })
    }
    function L(_) {
        return typeof _ == "string" ? Xr(n, _, c.value.path) : oe({}, _)
    }
    function S(_, M) {
        if (u !== _) return Ut(8, { from: M, to: _ })
    }
    function F(_) {
        return U(_)
    }
    function G(_) {
        return F(oe(L(_), { replace: !0 }))
    }
    function j(_) {
        const M = _.matched[_.matched.length - 1]
        if (M && M.redirect) {
            const { redirect: O } = M
            let $ = typeof O == "function" ? O(_) : O
            return typeof $ == "string" && (($ = $.includes("?") || $.includes("#") ? ($ = L($)) : { path: $ }), ($.params = {})), oe({ query: _.query, hash: _.hash, params: _.params }, $)
        }
    }
    function U(_, M) {
        const O = (u = P(_)),
            $ = c.value,
            re = _.state,
            fe = _.force,
            Y = _.replace === !0,
            z = j(O)
        if (z) return U(oe(L(z), { state: re, force: fe, replace: Y }), M || O)
        const h = O
        h.redirectedFrom = M
        let p
        return (
            !fe && bu(r, $, O) && ((p = Ut(16, { to: h, from: $ })), Rt($, $, !0, !1)),
            (p ? Promise.resolve(p) : ee(h, $))
                .catch(m => (Pt(m) ? m : ue(m, h, $)))
                .then(m => {
                    if (m) {
                        if (Pt(m, 2)) return U(oe(L(m.to), { state: re, force: fe, replace: Y }), M || h)
                    } else m = te(h, $, !0, Y, re)
                    return ge(h, $, m), m
                })
        )
    }
    function ce(_, M) {
        const O = S(_, M)
        return O ? Promise.reject(O) : Promise.resolve()
    }
    function ee(_, M) {
        let O
        const [$, re, fe] = vf(_, M)
        O = ns($.reverse(), "beforeRouteLeave", _, M)
        for (const z of $)
            z.leaveGuards.forEach(h => {
                O.push(ft(h, _, M))
            })
        const Y = ce.bind(null, _, M)
        return (
            O.push(Y),
            qt(O)
                .then(() => {
                    O = []
                    for (const z of i.list()) O.push(ft(z, _, M))
                    return O.push(Y), qt(O)
                })
                .then(() => {
                    O = ns(re, "beforeRouteUpdate", _, M)
                    for (const z of re)
                        z.updateGuards.forEach(h => {
                            O.push(ft(h, _, M))
                        })
                    return O.push(Y), qt(O)
                })
                .then(() => {
                    O = []
                    for (const z of _.matched)
                        if (z.beforeEnter && !M.matched.includes(z))
                            if (Array.isArray(z.beforeEnter)) for (const h of z.beforeEnter) O.push(ft(h, _, M))
                            else O.push(ft(z.beforeEnter, _, M))
                    return O.push(Y), qt(O)
                })
                .then(() => (_.matched.forEach(z => (z.enterCallbacks = {})), (O = ns(fe, "beforeRouteEnter", _, M)), O.push(Y), qt(O)))
                .then(() => {
                    O = []
                    for (const z of o.list()) O.push(ft(z, _, M))
                    return O.push(Y), qt(O)
                })
                .catch(z => (Pt(z, 8) ? z : Promise.reject(z)))
        )
    }
    function ge(_, M, O) {
        for (const $ of l.list()) $(_, M, O)
    }
    function te(_, M, O, $, re) {
        const fe = S(_, M)
        if (fe) return fe
        const Y = M === ut,
            z = jt ? history.state : {}
        O && ($ || Y ? s.replace(_.fullPath, oe({ scroll: Y && z && z.scroll }, re)) : s.push(_.fullPath, re)), (c.value = _), Rt(_, M, O, Y), Fe()
    }
    let ye
    function _e() {
        ye = s.listen((_, M, O) => {
            const $ = P(_),
                re = j($)
            if (re) {
                U(oe(re, { replace: !0 }), $).catch(un)
                return
            }
            u = $
            const fe = c.value
            jt && Ou(Ki(fe.fullPath, O.delta), Wn()),
                ee($, fe)
                    .catch(Y =>
                        Pt(Y, 4 | 8)
                            ? Y
                            : Pt(Y, 2)
                            ? (U(Y.to, $)
                                  .then(z => {
                                      Pt(z, 4 | 16) && !O.delta && O.type === fn.pop && s.go(-1, !1)
                                  })
                                  .catch(un),
                              Promise.reject())
                            : (O.delta && s.go(-O.delta, !1), ue(Y, $, fe))
                    )
                    .then(Y => {
                        ;(Y = Y || te($, fe, !1)), Y && (O.delta ? s.go(-O.delta, !1) : O.type === fn.pop && Pt(Y, 4 | 16) && s.go(-1, !1)), ge($, fe, Y)
                    })
                    .catch(un)
        })
    }
    let he = dn(),
        pe = dn(),
        q
    function ue(_, M, O) {
        Fe(_)
        const $ = pe.list()
        return $.length ? $.forEach(re => re(_, M, O)) : console.error(_), Promise.reject(_)
    }
    function ne() {
        return q && c.value !== ut
            ? Promise.resolve()
            : new Promise((_, M) => {
                  he.add([_, M])
              })
    }
    function Fe(_) {
        q || ((q = !0), _e(), he.list().forEach(([M, O]) => (_ ? O(_) : M())), he.reset())
    }
    function Rt(_, M, O, $) {
        const { scrollBehavior: re } = e
        if (!jt || !re) return Promise.resolve()
        const fe = (!O && xu(Ki(_.fullPath, 0))) || (($ || !O) && history.state && history.state.scroll) || null
        return Kr()
            .then(() => re(_, M, fe))
            .then(Y => Y && Au(Y))
            .catch(Y => ue(Y, _, M))
    }
    const Je = _ => s.go(_)
    let Ke
    const ke = new Set()
    return {
        currentRoute: c,
        addRoute: g,
        removeRoute: v,
        hasRoute: x,
        getRoutes: C,
        resolve: P,
        options: e,
        push: F,
        replace: G,
        go: Je,
        back: () => Je(-1),
        forward: () => Je(1),
        beforeEach: i.add,
        beforeResolve: o.add,
        afterEach: l.add,
        onError: pe.add,
        isReady: ne,
        install(_) {
            const M = this
            _.component("RouterLink", hf),
                _.component("RouterView", wf),
                (_.config.globalProperties.$router = M),
                Object.defineProperty(_.config.globalProperties, "$route", { enumerable: !0, get: () => vt(c) }),
                jt && !Ke && c.value === ut && ((Ke = !0), F(s.location).catch(re => {}))
            const O = {}
            for (const re in ut) O[re] = He(() => c.value[re])
            _.provide(Kn, M), _.provide(Yr, St(O)), _.provide(Qr, c)
            const $ = _.unmount
            ke.add(_),
                (_.unmount = function () {
                    ke.delete(_), ke.size < 1 && ((u = ut), ye && ye(), (c.value = ut), (Ke = !1), (q = !1)), $()
                })
        }
    }
}
function qt(e) {
    return e.reduce((t, n) => t.then(() => n()), Promise.resolve())
}
function vf(e, t) {
    const n = [],
        r = [],
        s = [],
        i = Math.max(t.matched.length, e.matched.length)
    for (let o = 0; o < i; o++) {
        const l = t.matched[o]
        l && (e.matched.find(u => Dt(u, l)) ? r.push(l) : n.push(l))
        const c = e.matched[o]
        c && (t.matched.find(u => Dt(u, c)) || s.push(c))
    }
    return [n, r, s]
}
function Fa() {
    return Be(Kn)
}
function ka() {
    return Be(Yr)
}
var _f = !1
/*!
 * pinia v2.0.9
 * (c) 2021 Eduardo San Martin Morote
 * @license MIT
 */ let uo
const Zn = e => (uo = e),
    fo = Symbol()
function rs(e) {
    return e && typeof e == "object" && Object.prototype.toString.call(e) === "[object Object]" && typeof e.toJSON != "function"
}
var hn
;(function (e) {
    ;(e.direct = "direct"), (e.patchObject = "patch object"), (e.patchFunction = "patch function")
})(hn || (hn = {}))
function La() {
    const e = bs(!0),
        t = e.run(() => xn({}))
    let n = [],
        r = []
    const s = It({
        install(i) {
            Zn(s), (s._a = i), i.provide(fo, s), (i.config.globalProperties.$pinia = s), r.forEach(o => n.push(o)), (r = [])
        },
        use(i) {
            return !this._a && !_f ? r.push(i) : n.push(i), this
        },
        _p: n,
        _a: null,
        _e: e,
        _s: new Map(),
        state: t
    })
    return s
}
const ao = () => {}
function ho(e, t, n, r = ao) {
    e.push(t)
    const s = () => {
        const i = e.indexOf(t)
        i > -1 && (e.splice(i, 1), r())
    }
    return !n && Br() && Ar(s), s
}
function Kt(e, ...t) {
    e.forEach(n => {
        n(...t)
    })
}
function ss(e, t) {
    for (const n in t) {
        const r = t[n],
            s = e[n]
        rs(s) && rs(r) && !me(r) && !it(r) ? (e[n] = ss(s, r)) : (e[n] = r)
    }
    return e
}
const bf = Symbol()
function Ef(e) {
    return !rs(e) || !e.hasOwnProperty(bf)
}
const { assign: tt } = Object
function yf(e) {
    return !!(me(e) && e.effect)
}
function Cf(e, t, n, r) {
    const { state: s, actions: i, getters: o } = t,
        l = n.state.value[e]
    let c
    function u() {
        l || (n.state.value[e] = s ? s() : {})
        const f = Il(n.state.value[e])
        return tt(
            f,
            i,
            Object.keys(o || {}).reduce(
                (a, d) => (
                    (a[d] = It(
                        He(() => {
                            Zn(n)
                            const g = n._s.get(e)
                            return o[d].call(g, g)
                        })
                    )),
                    a
                ),
                {}
            )
        )
    }
    return (
        (c = po(e, u, t, n)),
        (c.$reset = function () {
            const a = s ? s() : {}
            this.$patch(d => {
                tt(d, a)
            })
        }),
        c
    )
}
function po(e, t, n = {}, r, s) {
    let i
    const o = n.state,
        l = tt({ actions: {} }, n),
        c = { deep: !0 }
    let u,
        f,
        a = It([]),
        d = It([]),
        g
    const v = r.state.value[e]
    !o && !v && (r.state.value[e] = {}), xn({})
    function C(j) {
        let U
        ;(u = f = !1),
            typeof j == "function"
                ? (j(r.state.value[e]), (U = { type: hn.patchFunction, storeId: e, events: g }))
                : (ss(r.state.value[e], j), (U = { type: hn.patchObject, payload: j, storeId: e, events: g })),
            Kr().then(() => {
                u = !0
            }),
            (f = !0),
            Kt(a, U, r.state.value[e])
    }
    const x = ao
    function P() {
        i.stop(), (a = []), (d = []), r._s.delete(e)
    }
    function L(j, U) {
        return function () {
            Zn(r)
            const ce = Array.from(arguments),
                ee = [],
                ge = []
            function te(he) {
                ee.push(he)
            }
            function ye(he) {
                ge.push(he)
            }
            Kt(d, { args: ce, name: j, store: F, after: te, onError: ye })
            let _e
            try {
                _e = U.apply(this && this.$id === e ? this : F, ce)
            } catch (he) {
                throw (Kt(ge, he), he)
            }
            return _e instanceof Promise ? _e.then(he => (Kt(ee, he), he)).catch(he => (Kt(ge, he), Promise.reject(he))) : (Kt(ee, _e), _e)
        }
    }
    const S = {
            _p: r,
            $id: e,
            $onAction: ho.bind(null, d),
            $patch: C,
            $reset: x,
            $subscribe(j, U = {}) {
                const ce = ho(a, j, U.detached, () => ee()),
                    ee = i.run(() =>
                        $t(
                            () => r.state.value[e],
                            ge => {
                                ;(U.flush === "sync" ? f : u) && j({ storeId: e, type: hn.direct, events: g }, ge)
                            },
                            tt({}, c, U)
                        )
                    )
                return ce
            },
            $dispose: P
        },
        F = St(tt({}, S))
    r._s.set(e, F)
    const G = r._e.run(() => ((i = bs()), i.run(() => t())))
    for (const j in G) {
        const U = G[j]
        if ((me(U) && !yf(U)) || it(U)) o || (v && Ef(U) && (me(U) ? (U.value = v[j]) : ss(U, v[j])), (r.state.value[e][j] = U))
        else if (typeof U == "function") {
            const ce = L(j, U)
            ;(G[j] = ce), (l.actions[j] = U)
        }
    }
    return (
        tt(F, G),
        tt(X(F), G),
        Object.defineProperty(F, "$state", {
            get: () => r.state.value[e],
            set: j => {
                C(U => {
                    tt(U, j)
                })
            }
        }),
        r._p.forEach(j => {
            tt(
                F,
                i.run(() => j({ store: F, app: r._a, pinia: r, options: l }))
            )
        }),
        v && o && n.hydrate && n.hydrate(F.$state, v),
        (u = !0),
        (f = !0),
        F
    )
}
function $a(e, t, n) {
    let r, s
    const i = typeof t == "function"
    typeof e == "string" ? ((r = e), (s = i ? n : t)) : ((s = e), (r = e.id))
    function o(l, c) {
        const u = Br()
        return (l = l || (u && Be(fo))), l && Zn(l), (l = uo), l._s.has(r) || (i ? po(r, t, s, l) : Cf(r, s, l)), l._s.get(r)
    }
    return (o.$id = r), o
}
function Pf(e) {
    return el() ? (tl(e), !0) : !1
}
const pn = typeof window != "undefined",
    Rf = e => typeof e == "string",
    is = () => {}
function go(e) {
    var t
    const n = vt(e)
    return (t = n == null ? void 0 : n.$el) != null ? t : n
}
const mo = pn ? window : void 0
pn && window.document
pn && window.navigator
pn && window.location
function wo(...e) {
    let t, n, r, s
    if ((Rf(e[0]) ? (([n, r, s] = e), (t = mo)) : ([t, n, r, s] = e), !t)) return is
    let i = is
    const o = $t(
            () => vt(t),
            c => {
                i(),
                    !!c &&
                        (c.addEventListener(n, r, s),
                        (i = () => {
                            c.removeEventListener(n, r, s), (i = is)
                        }))
            },
            { immediate: !0, flush: "post" }
        ),
        l = () => {
            o(), i()
        }
    return Pf(l), l
}
function Ha(e, t, n = {}) {
    const { window: r = mo } = n
    if (!r) return
    const s = xn(!0),
        o = [
            wo(
                r,
                "click",
                c => {
                    const u = go(e)
                    !u || u === c.target || c.composedPath().includes(u) || !s.value || t(c)
                },
                { passive: !0, capture: !0 }
            ),
            wo(
                r,
                "pointerdown",
                c => {
                    const u = go(e)
                    s.value = !!u && !c.composedPath().includes(u)
                },
                { passive: !0 }
            )
        ]
    return () => o.forEach(c => c())
}
const vo = "__vueuse_ssr_handlers__"
globalThis[vo] = globalThis[vo] || {}
var _o, bo
pn &&
    (window == null ? void 0 : window.navigator) &&
    ((_o = window == null ? void 0 : window.navigator) == null ? void 0 : _o.platform) &&
    /iP(ad|hone|od)/.test((bo = window == null ? void 0 : window.navigator) == null ? void 0 : bo.platform)
var Nf = Object.defineProperty,
    Eo = Object.getOwnPropertySymbols,
    Af = Object.prototype.hasOwnProperty,
    Of = Object.prototype.propertyIsEnumerable,
    yo = (e, t, n) => (t in e ? Nf(e, t, { enumerable: !0, configurable: !0, writable: !0, value: n }) : (e[t] = n)),
    xf = (e, t) => {
        for (var n in t || (t = {})) Af.call(t, n) && yo(e, n, t[n])
        if (Eo) for (var n of Eo(t)) Of.call(t, n) && yo(e, n, t[n])
        return e
    }
const Tf = { top: 0, left: 0, bottom: 0, right: 0, height: 0, width: 0 }
xf({ text: "" }, Tf)
var Wt = 9e15,
    at = 1e9,
    os = "0123456789abcdef",
    Vn =
        "2.3025850929940456840179914546843642076011014886287729760333279009675726096773524802359972050895982983419677840422862486334095254650828067566662873690987816894829072083255546808437998948262331985283935053089653777326288461633662222876982198867465436674744042432743651550489343149393914796194044002221051017141748003688084012647080685567743216228355220114804663715659121373450747856947683463616792101806445070648000277502684916746550586856935673420670581136429224554405758925724208241314695689016758940256776311356919292033376587141660230105703089634572075440370847469940168269282808481184289314848524948644871927809676271275775397027668605952496716674183485704422507197965004714951050492214776567636938662976979522110718264549734772662425709429322582798502585509785265383207606726317164309505995087807523710333101197857547331541421808427543863591778117054309827482385045648019095610299291824318237525357709750539565187697510374970888692180205189339507238539205144634197265287286965110862571492198849978748873771345686209167058",
    Yn =
        "3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679821480865132823066470938446095505822317253594081284811174502841027019385211055596446229489549303819644288109756659334461284756482337867831652712019091456485669234603486104543266482133936072602491412737245870066063155881748815209209628292540917153643678925903600113305305488204665213841469519415116094330572703657595919530921861173819326117931051185480744623799627495673518857527248912279381830119491298336733624406566430860213949463952247371907021798609437027705392171762931767523846748184676694051320005681271452635608277857713427577896091736371787214684409012249534301465495853710507922796892589235420199561121290219608640344181598136297747713099605187072113499999983729780499510597317328160963185950244594553469083026425223082533446850352619311881710100031378387528865875332083814206171776691473035982534904287554687311595628638823537875937519577818577805321712268066130019278766111959092164201989380952572010654858632789",
    ls = { precision: 20, rounding: 4, modulo: 1, toExpNeg: -7, toExpPos: 21, minE: -Wt, maxE: Wt, crypto: !1 },
    Co,
    nt,
    K = !0,
    Qn = "[DecimalError] ",
    dt = Qn + "Invalid argument: ",
    Po = Qn + "Precision limit exceeded",
    Ro = Qn + "crypto unavailable",
    No = "[object Decimal]",
    Pe = Math.floor,
    we = Math.pow,
    Sf = /^0b([01]+(\.[01]*)?|\.[01]+)(p[+-]?\d+)?$/i,
    If = /^0x([0-9a-f]+(\.[0-9a-f]*)?|\.[0-9a-f]+)(p[+-]?\d+)?$/i,
    Mf = /^0o([0-7]+(\.[0-7]*)?|\.[0-7]+)(p[+-]?\d+)?$/i,
    Ao = /^(\d+(\.\d*)?|\.\d+)(e[+-]?\d+)?$/i,
    Ue = 1e7,
    D = 7,
    Ff = 9007199254740991,
    kf = Vn.length - 1,
    cs = Yn.length - 1,
    w = { toStringTag: No }
w.absoluteValue = w.abs = function () {
    var e = new this.constructor(this)
    return e.s < 0 && (e.s = 1), k(e)
}
w.ceil = function () {
    return k(new this.constructor(this), this.e + 1, 2)
}
w.clampedTo = w.clamp = function (e, t) {
    var n,
        r = this,
        s = r.constructor
    if (((e = new s(e)), (t = new s(t)), !e.s || !t.s)) return new s(NaN)
    if (e.gt(t)) throw Error(dt + t)
    return (n = r.cmp(e)), n < 0 ? e : r.cmp(t) > 0 ? t : new s(r)
}
w.comparedTo = w.cmp = function (e) {
    var t,
        n,
        r,
        s,
        i = this,
        o = i.d,
        l = (e = new i.constructor(e)).d,
        c = i.s,
        u = e.s
    if (!o || !l) return !c || !u ? NaN : c !== u ? c : o === l ? 0 : !o ^ (c < 0) ? 1 : -1
    if (!o[0] || !l[0]) return o[0] ? c : l[0] ? -u : 0
    if (c !== u) return c
    if (i.e !== e.e) return (i.e > e.e) ^ (c < 0) ? 1 : -1
    for (r = o.length, s = l.length, t = 0, n = r < s ? r : s; t < n; ++t) if (o[t] !== l[t]) return (o[t] > l[t]) ^ (c < 0) ? 1 : -1
    return r === s ? 0 : (r > s) ^ (c < 0) ? 1 : -1
}
w.cosine = w.cos = function () {
    var e,
        t,
        n = this,
        r = n.constructor
    return n.d
        ? n.d[0]
            ? ((e = r.precision),
              (t = r.rounding),
              (r.precision = e + Math.max(n.e, n.sd()) + D),
              (r.rounding = 1),
              (n = Lf(r, Mo(r, n))),
              (r.precision = e),
              (r.rounding = t),
              k(nt == 2 || nt == 3 ? n.neg() : n, e, t, !0))
            : new r(1)
        : new r(NaN)
}
w.cubeRoot = w.cbrt = function () {
    var e,
        t,
        n,
        r,
        s,
        i,
        o,
        l,
        c,
        u,
        f = this,
        a = f.constructor
    if (!f.isFinite() || f.isZero()) return new a(f)
    for (
        K = !1,
            i = f.s * we(f.s * f, 1 / 3),
            !i || Math.abs(i) == 1 / 0
                ? ((n = Ee(f.d)),
                  (e = f.e),
                  (i = (e - n.length + 1) % 3) && (n += i == 1 || i == -2 ? "0" : "00"),
                  (i = we(n, 1 / 3)),
                  (e = Pe((e + 1) / 3) - (e % 3 == (e < 0 ? -1 : 2))),
                  i == 1 / 0 ? (n = "5e" + e) : ((n = i.toExponential()), (n = n.slice(0, n.indexOf("e") + 1) + e)),
                  (r = new a(n)),
                  (r.s = f.s))
                : (r = new a(i.toString())),
            o = (e = a.precision) + 3;
        ;

    )
        if (((l = r), (c = l.times(l).times(l)), (u = c.plus(f)), (r = le(u.plus(f).times(l), u.plus(c), o + 2, 1)), Ee(l.d).slice(0, o) === (n = Ee(r.d)).slice(0, o)))
            if (((n = n.slice(o - 3, o + 1)), n == "9999" || (!s && n == "4999"))) {
                if (!s && (k(l, e + 1, 0), l.times(l).times(l).eq(f))) {
                    r = l
                    break
                }
                ;(o += 4), (s = 1)
            } else {
                ;(!+n || (!+n.slice(1) && n.charAt(0) == "5")) && (k(r, e + 1, 1), (t = !r.times(r).times(r).eq(f)))
                break
            }
    return (K = !0), k(r, e, a.rounding, t)
}
w.decimalPlaces = w.dp = function () {
    var e,
        t = this.d,
        n = NaN
    if (t) {
        if (((e = t.length - 1), (n = (e - Pe(this.e / D)) * D), (e = t[e]), e)) for (; e % 10 == 0; e /= 10) n--
        n < 0 && (n = 0)
    }
    return n
}
w.dividedBy = w.div = function (e) {
    return le(this, new this.constructor(e))
}
w.dividedToIntegerBy = w.divToInt = function (e) {
    var t = this,
        n = t.constructor
    return k(le(t, new n(e), 0, 1, 1), n.precision, n.rounding)
}
w.equals = w.eq = function (e) {
    return this.cmp(e) === 0
}
w.floor = function () {
    return k(new this.constructor(this), this.e + 1, 3)
}
w.greaterThan = w.gt = function (e) {
    return this.cmp(e) > 0
}
w.greaterThanOrEqualTo = w.gte = function (e) {
    var t = this.cmp(e)
    return t == 1 || t === 0
}
w.hyperbolicCosine = w.cosh = function () {
    var e,
        t,
        n,
        r,
        s,
        i = this,
        o = i.constructor,
        l = new o(1)
    if (!i.isFinite()) return new o(i.s ? 1 / 0 : NaN)
    if (i.isZero()) return l
    ;(n = o.precision),
        (r = o.rounding),
        (o.precision = n + Math.max(i.e, i.sd()) + 4),
        (o.rounding = 1),
        (s = i.d.length),
        s < 32 ? ((e = Math.ceil(s / 3)), (t = (1 / er(4, e)).toString())) : ((e = 16), (t = "2.3283064365386962890625e-10")),
        (i = zt(o, 1, i.times(t), new o(1), !0))
    for (var c, u = e, f = new o(8); u--; ) (c = i.times(i)), (i = l.minus(c.times(f.minus(c.times(f)))))
    return k(i, (o.precision = n), (o.rounding = r), !0)
}
w.hyperbolicSine = w.sinh = function () {
    var e,
        t,
        n,
        r,
        s = this,
        i = s.constructor
    if (!s.isFinite() || s.isZero()) return new i(s)
    if (((t = i.precision), (n = i.rounding), (i.precision = t + Math.max(s.e, s.sd()) + 4), (i.rounding = 1), (r = s.d.length), r < 3)) s = zt(i, 2, s, s, !0)
    else {
        ;(e = 1.4 * Math.sqrt(r)), (e = e > 16 ? 16 : e | 0), (s = s.times(1 / er(5, e))), (s = zt(i, 2, s, s, !0))
        for (var o, l = new i(5), c = new i(16), u = new i(20); e--; ) (o = s.times(s)), (s = s.times(l.plus(o.times(c.times(o).plus(u)))))
    }
    return (i.precision = t), (i.rounding = n), k(s, t, n, !0)
}
w.hyperbolicTangent = w.tanh = function () {
    var e,
        t,
        n = this,
        r = n.constructor
    return n.isFinite()
        ? n.isZero()
            ? new r(n)
            : ((e = r.precision), (t = r.rounding), (r.precision = e + 7), (r.rounding = 1), le(n.sinh(), n.cosh(), (r.precision = e), (r.rounding = t)))
        : new r(n.s)
}
w.inverseCosine = w.acos = function () {
    var e,
        t = this,
        n = t.constructor,
        r = t.abs().cmp(1),
        s = n.precision,
        i = n.rounding
    return r !== -1
        ? r === 0
            ? t.isNeg()
                ? qe(n, s, i)
                : new n(0)
            : new n(NaN)
        : t.isZero()
        ? qe(n, s + 4, i).times(0.5)
        : ((n.precision = s + 6), (n.rounding = 1), (t = t.asin()), (e = qe(n, s + 4, i).times(0.5)), (n.precision = s), (n.rounding = i), e.minus(t))
}
w.inverseHyperbolicCosine = w.acosh = function () {
    var e,
        t,
        n = this,
        r = n.constructor
    return n.lte(1)
        ? new r(n.eq(1) ? 0 : NaN)
        : n.isFinite()
        ? ((e = r.precision),
          (t = r.rounding),
          (r.precision = e + Math.max(Math.abs(n.e), n.sd()) + 4),
          (r.rounding = 1),
          (K = !1),
          (n = n.times(n).minus(1).sqrt().plus(n)),
          (K = !0),
          (r.precision = e),
          (r.rounding = t),
          n.ln())
        : new r(n)
}
w.inverseHyperbolicSine = w.asinh = function () {
    var e,
        t,
        n = this,
        r = n.constructor
    return !n.isFinite() || n.isZero()
        ? new r(n)
        : ((e = r.precision),
          (t = r.rounding),
          (r.precision = e + 2 * Math.max(Math.abs(n.e), n.sd()) + 6),
          (r.rounding = 1),
          (K = !1),
          (n = n.times(n).plus(1).sqrt().plus(n)),
          (K = !0),
          (r.precision = e),
          (r.rounding = t),
          n.ln())
}
w.inverseHyperbolicTangent = w.atanh = function () {
    var e,
        t,
        n,
        r,
        s = this,
        i = s.constructor
    return s.isFinite()
        ? s.e >= 0
            ? new i(s.abs().eq(1) ? s.s / 0 : s.isZero() ? s : NaN)
            : ((e = i.precision),
              (t = i.rounding),
              (r = s.sd()),
              Math.max(r, e) < 2 * -s.e - 1
                  ? k(new i(s), e, t, !0)
                  : ((i.precision = n = r - s.e),
                    (s = le(s.plus(1), new i(1).minus(s), n + e, 1)),
                    (i.precision = e + 4),
                    (i.rounding = 1),
                    (s = s.ln()),
                    (i.precision = e),
                    (i.rounding = t),
                    s.times(0.5)))
        : new i(NaN)
}
w.inverseSine = w.asin = function () {
    var e,
        t,
        n,
        r,
        s = this,
        i = s.constructor
    return s.isZero()
        ? new i(s)
        : ((t = s.abs().cmp(1)),
          (n = i.precision),
          (r = i.rounding),
          t !== -1
              ? t === 0
                  ? ((e = qe(i, n + 4, r).times(0.5)), (e.s = s.s), e)
                  : new i(NaN)
              : ((i.precision = n + 6), (i.rounding = 1), (s = s.div(new i(1).minus(s.times(s)).sqrt().plus(1)).atan()), (i.precision = n), (i.rounding = r), s.times(2)))
}
w.inverseTangent = w.atan = function () {
    var e,
        t,
        n,
        r,
        s,
        i,
        o,
        l,
        c,
        u = this,
        f = u.constructor,
        a = f.precision,
        d = f.rounding
    if (u.isFinite()) {
        if (u.isZero()) return new f(u)
        if (u.abs().eq(1) && a + 4 <= cs) return (o = qe(f, a + 4, d).times(0.25)), (o.s = u.s), o
    } else {
        if (!u.s) return new f(NaN)
        if (a + 4 <= cs) return (o = qe(f, a + 4, d).times(0.5)), (o.s = u.s), o
    }
    for (f.precision = l = a + 10, f.rounding = 1, n = Math.min(28, (l / D + 2) | 0), e = n; e; --e) u = u.div(u.times(u).plus(1).sqrt().plus(1))
    for (K = !1, t = Math.ceil(l / D), r = 1, c = u.times(u), o = new f(u), s = u; e !== -1; )
        if (((s = s.times(c)), (i = o.minus(s.div((r += 2)))), (s = s.times(c)), (o = i.plus(s.div((r += 2)))), o.d[t] !== void 0)) for (e = t; o.d[e] === i.d[e] && e--; );
    return n && (o = o.times(2 << (n - 1))), (K = !0), k(o, (f.precision = a), (f.rounding = d), !0)
}
w.isFinite = function () {
    return !!this.d
}
w.isInteger = w.isInt = function () {
    return !!this.d && Pe(this.e / D) > this.d.length - 2
}
w.isNaN = function () {
    return !this.s
}
w.isNegative = w.isNeg = function () {
    return this.s < 0
}
w.isPositive = w.isPos = function () {
    return this.s > 0
}
w.isZero = function () {
    return !!this.d && this.d[0] === 0
}
w.lessThan = w.lt = function (e) {
    return this.cmp(e) < 0
}
w.lessThanOrEqualTo = w.lte = function (e) {
    return this.cmp(e) < 1
}
w.logarithm = w.log = function (e) {
    var t,
        n,
        r,
        s,
        i,
        o,
        l,
        c,
        u = this,
        f = u.constructor,
        a = f.precision,
        d = f.rounding,
        g = 5
    if (e == null) (e = new f(10)), (t = !0)
    else {
        if (((e = new f(e)), (n = e.d), e.s < 0 || !n || !n[0] || e.eq(1))) return new f(NaN)
        t = e.eq(10)
    }
    if (((n = u.d), u.s < 0 || !n || !n[0] || u.eq(1))) return new f(n && !n[0] ? -1 / 0 : u.s != 1 ? NaN : n ? 0 : 1 / 0)
    if (t)
        if (n.length > 1) i = !0
        else {
            for (s = n[0]; s % 10 == 0; ) s /= 10
            i = s !== 1
        }
    if (((K = !1), (l = a + g), (o = pt(u, l)), (r = t ? Gn(f, l + 10) : pt(e, l)), (c = le(o, r, l, 1)), gn(c.d, (s = a), d)))
        do
            if (((l += 10), (o = pt(u, l)), (r = t ? Gn(f, l + 10) : pt(e, l)), (c = le(o, r, l, 1)), !i)) {
                ;+Ee(c.d).slice(s + 1, s + 15) + 1 == 1e14 && (c = k(c, a + 1, 0))
                break
            }
        while (gn(c.d, (s += 10), d))
    return (K = !0), k(c, a, d)
}
w.minus = w.sub = function (e) {
    var t,
        n,
        r,
        s,
        i,
        o,
        l,
        c,
        u,
        f,
        a,
        d,
        g = this,
        v = g.constructor
    if (((e = new v(e)), !g.d || !e.d)) return !g.s || !e.s ? (e = new v(NaN)) : g.d ? (e.s = -e.s) : (e = new v(e.d || g.s !== e.s ? g : NaN)), e
    if (g.s != e.s) return (e.s = -e.s), g.plus(e)
    if (((u = g.d), (d = e.d), (l = v.precision), (c = v.rounding), !u[0] || !d[0])) {
        if (d[0]) e.s = -e.s
        else if (u[0]) e = new v(g)
        else return new v(c === 3 ? -0 : 0)
        return K ? k(e, l, c) : e
    }
    if (((n = Pe(e.e / D)), (f = Pe(g.e / D)), (u = u.slice()), (i = f - n), i)) {
        for (
            a = i < 0, a ? ((t = u), (i = -i), (o = d.length)) : ((t = d), (n = f), (o = u.length)), r = Math.max(Math.ceil(l / D), o) + 2, i > r && ((i = r), (t.length = 1)), t.reverse(), r = i;
            r--;

        )
            t.push(0)
        t.reverse()
    } else {
        for (r = u.length, o = d.length, a = r < o, a && (o = r), r = 0; r < o; r++)
            if (u[r] != d[r]) {
                a = u[r] < d[r]
                break
            }
        i = 0
    }
    for (a && ((t = u), (u = d), (d = t), (e.s = -e.s)), o = u.length, r = d.length - o; r > 0; --r) u[o++] = 0
    for (r = d.length; r > i; ) {
        if (u[--r] < d[r]) {
            for (s = r; s && u[--s] === 0; ) u[s] = Ue - 1
            --u[s], (u[r] += Ue)
        }
        u[r] -= d[r]
    }
    for (; u[--o] === 0; ) u.pop()
    for (; u[0] === 0; u.shift()) --n
    return u[0] ? ((e.d = u), (e.e = Xn(u, n)), K ? k(e, l, c) : e) : new v(c === 3 ? -0 : 0)
}
w.modulo = w.mod = function (e) {
    var t,
        n = this,
        r = n.constructor
    return (
        (e = new r(e)),
        !n.d || !e.s || (e.d && !e.d[0])
            ? new r(NaN)
            : !e.d || (n.d && !n.d[0])
            ? k(new r(n), r.precision, r.rounding)
            : ((K = !1), r.modulo == 9 ? ((t = le(n, e.abs(), 0, 3, 1)), (t.s *= e.s)) : (t = le(n, e, 0, r.modulo, 1)), (t = t.times(e)), (K = !0), n.minus(t))
    )
}
w.naturalExponential = w.exp = function () {
    return us(this)
}
w.naturalLogarithm = w.ln = function () {
    return pt(this)
}
w.negated = w.neg = function () {
    var e = new this.constructor(this)
    return (e.s = -e.s), k(e)
}
w.plus = w.add = function (e) {
    var t,
        n,
        r,
        s,
        i,
        o,
        l,
        c,
        u,
        f,
        a = this,
        d = a.constructor
    if (((e = new d(e)), !a.d || !e.d)) return !a.s || !e.s ? (e = new d(NaN)) : a.d || (e = new d(e.d || a.s === e.s ? a : NaN)), e
    if (a.s != e.s) return (e.s = -e.s), a.minus(e)
    if (((u = a.d), (f = e.d), (l = d.precision), (c = d.rounding), !u[0] || !f[0])) return f[0] || (e = new d(a)), K ? k(e, l, c) : e
    if (((i = Pe(a.e / D)), (r = Pe(e.e / D)), (u = u.slice()), (s = i - r), s)) {
        for (s < 0 ? ((n = u), (s = -s), (o = f.length)) : ((n = f), (r = i), (o = u.length)), i = Math.ceil(l / D), o = i > o ? i + 1 : o + 1, s > o && ((s = o), (n.length = 1)), n.reverse(); s--; )
            n.push(0)
        n.reverse()
    }
    for (o = u.length, s = f.length, o - s < 0 && ((s = o), (n = f), (f = u), (u = n)), t = 0; s; ) (t = ((u[--s] = u[s] + f[s] + t) / Ue) | 0), (u[s] %= Ue)
    for (t && (u.unshift(t), ++r), o = u.length; u[--o] == 0; ) u.pop()
    return (e.d = u), (e.e = Xn(u, r)), K ? k(e, l, c) : e
}
w.precision = w.sd = function (e) {
    var t,
        n = this
    if (e !== void 0 && e !== !!e && e !== 1 && e !== 0) throw Error(dt + e)
    return n.d ? ((t = Oo(n.d)), e && n.e + 1 > t && (t = n.e + 1)) : (t = NaN), t
}
w.round = function () {
    var e = this,
        t = e.constructor
    return k(new t(e), e.e + 1, t.rounding)
}
w.sine = w.sin = function () {
    var e,
        t,
        n = this,
        r = n.constructor
    return n.isFinite()
        ? n.isZero()
            ? new r(n)
            : ((e = r.precision),
              (t = r.rounding),
              (r.precision = e + Math.max(n.e, n.sd()) + D),
              (r.rounding = 1),
              (n = Hf(r, Mo(r, n))),
              (r.precision = e),
              (r.rounding = t),
              k(nt > 2 ? n.neg() : n, e, t, !0))
        : new r(NaN)
}
w.squareRoot = w.sqrt = function () {
    var e,
        t,
        n,
        r,
        s,
        i,
        o = this,
        l = o.d,
        c = o.e,
        u = o.s,
        f = o.constructor
    if (u !== 1 || !l || !l[0]) return new f(!u || (u < 0 && (!l || l[0])) ? NaN : l ? o : 1 / 0)
    for (
        K = !1,
            u = Math.sqrt(+o),
            u == 0 || u == 1 / 0
                ? ((t = Ee(l)),
                  (t.length + c) % 2 == 0 && (t += "0"),
                  (u = Math.sqrt(t)),
                  (c = Pe((c + 1) / 2) - (c < 0 || c % 2)),
                  u == 1 / 0 ? (t = "5e" + c) : ((t = u.toExponential()), (t = t.slice(0, t.indexOf("e") + 1) + c)),
                  (r = new f(t)))
                : (r = new f(u.toString())),
            n = (c = f.precision) + 3;
        ;

    )
        if (((i = r), (r = i.plus(le(o, i, n + 2, 1)).times(0.5)), Ee(i.d).slice(0, n) === (t = Ee(r.d)).slice(0, n)))
            if (((t = t.slice(n - 3, n + 1)), t == "9999" || (!s && t == "4999"))) {
                if (!s && (k(i, c + 1, 0), i.times(i).eq(o))) {
                    r = i
                    break
                }
                ;(n += 4), (s = 1)
            } else {
                ;(!+t || (!+t.slice(1) && t.charAt(0) == "5")) && (k(r, c + 1, 1), (e = !r.times(r).eq(o)))
                break
            }
    return (K = !0), k(r, c, f.rounding, e)
}
w.tangent = w.tan = function () {
    var e,
        t,
        n = this,
        r = n.constructor
    return n.isFinite()
        ? n.isZero()
            ? new r(n)
            : ((e = r.precision),
              (t = r.rounding),
              (r.precision = e + 10),
              (r.rounding = 1),
              (n = n.sin()),
              (n.s = 1),
              (n = le(n, new r(1).minus(n.times(n)).sqrt(), e + 10, 0)),
              (r.precision = e),
              (r.rounding = t),
              k(nt == 2 || nt == 4 ? n.neg() : n, e, t, !0))
        : new r(NaN)
}
w.times = w.mul = function (e) {
    var t,
        n,
        r,
        s,
        i,
        o,
        l,
        c,
        u,
        f = this,
        a = f.constructor,
        d = f.d,
        g = (e = new a(e)).d
    if (((e.s *= f.s), !d || !d[0] || !g || !g[0])) return new a(!e.s || (d && !d[0] && !g) || (g && !g[0] && !d) ? NaN : !d || !g ? e.s / 0 : e.s * 0)
    for (n = Pe(f.e / D) + Pe(e.e / D), c = d.length, u = g.length, c < u && ((i = d), (d = g), (g = i), (o = c), (c = u), (u = o)), i = [], o = c + u, r = o; r--; ) i.push(0)
    for (r = u; --r >= 0; ) {
        for (t = 0, s = c + r; s > r; ) (l = i[s] + g[r] * d[s - r - 1] + t), (i[s--] = l % Ue | 0), (t = (l / Ue) | 0)
        i[s] = (i[s] + t) % Ue | 0
    }
    for (; !i[--o]; ) i.pop()
    return t ? ++n : i.shift(), (e.d = i), (e.e = Xn(i, n)), K ? k(e, a.precision, a.rounding) : e
}
w.toBinary = function (e, t) {
    return as(this, 2, e, t)
}
w.toDecimalPlaces = w.toDP = function (e, t) {
    var n = this,
        r = n.constructor
    return (n = new r(n)), e === void 0 ? n : (Oe(e, 0, at), t === void 0 ? (t = r.rounding) : Oe(t, 0, 8), k(n, e + n.e + 1, t))
}
w.toExponential = function (e, t) {
    var n,
        r = this,
        s = r.constructor
    return e === void 0 ? (n = Qe(r, !0)) : (Oe(e, 0, at), t === void 0 ? (t = s.rounding) : Oe(t, 0, 8), (r = k(new s(r), e + 1, t)), (n = Qe(r, !0, e + 1))), r.isNeg() && !r.isZero() ? "-" + n : n
}
w.toFixed = function (e, t) {
    var n,
        r,
        s = this,
        i = s.constructor
    return (
        e === void 0 ? (n = Qe(s)) : (Oe(e, 0, at), t === void 0 ? (t = i.rounding) : Oe(t, 0, 8), (r = k(new i(s), e + s.e + 1, t)), (n = Qe(r, !1, e + r.e + 1))),
        s.isNeg() && !s.isZero() ? "-" + n : n
    )
}
w.toFraction = function (e) {
    var t,
        n,
        r,
        s,
        i,
        o,
        l,
        c,
        u,
        f,
        a,
        d,
        g = this,
        v = g.d,
        C = g.constructor
    if (!v) return new C(g)
    if (((u = n = new C(1)), (r = c = new C(0)), (t = new C(r)), (i = t.e = Oo(v) - g.e - 1), (o = i % D), (t.d[0] = we(10, o < 0 ? D + o : o)), e == null)) e = i > 0 ? t : u
    else {
        if (((l = new C(e)), !l.isInt() || l.lt(u))) throw Error(dt + l)
        e = l.gt(t) ? (i > 0 ? t : u) : l
    }
    for (K = !1, l = new C(Ee(v)), f = C.precision, C.precision = i = v.length * D * 2; (a = le(l, t, 0, 1, 1)), (s = n.plus(a.times(r))), s.cmp(e) != 1; )
        (n = r), (r = s), (s = u), (u = c.plus(a.times(s))), (c = s), (s = t), (t = l.minus(a.times(s))), (l = s)
    return (
        (s = le(e.minus(n), r, 0, 1, 1)),
        (c = c.plus(s.times(u))),
        (n = n.plus(s.times(r))),
        (c.s = u.s = g.s),
        (d = le(u, r, i, 1).minus(g).abs().cmp(le(c, n, i, 1).minus(g).abs()) < 1 ? [u, r] : [c, n]),
        (C.precision = f),
        (K = !0),
        d
    )
}
w.toHexadecimal = w.toHex = function (e, t) {
    return as(this, 16, e, t)
}
w.toNearest = function (e, t) {
    var n = this,
        r = n.constructor
    if (((n = new r(n)), e == null)) {
        if (!n.d) return n
        ;(e = new r(1)), (t = r.rounding)
    } else {
        if (((e = new r(e)), t === void 0 ? (t = r.rounding) : Oe(t, 0, 8), !n.d)) return e.s ? n : e
        if (!e.d) return e.s && (e.s = n.s), e
    }
    return e.d[0] ? ((K = !1), (n = le(n, e, 0, t, 1).times(e)), (K = !0), k(n)) : ((e.s = n.s), (n = e)), n
}
w.toNumber = function () {
    return +this
}
w.toOctal = function (e, t) {
    return as(this, 8, e, t)
}
w.toPower = w.pow = function (e) {
    var t,
        n,
        r,
        s,
        i,
        o,
        l = this,
        c = l.constructor,
        u = +(e = new c(e))
    if (!l.d || !e.d || !l.d[0] || !e.d[0]) return new c(we(+l, u))
    if (((l = new c(l)), l.eq(1))) return l
    if (((r = c.precision), (i = c.rounding), e.eq(1))) return k(l, r, i)
    if (((t = Pe(e.e / D)), t >= e.d.length - 1 && (n = u < 0 ? -u : u) <= Ff)) return (s = xo(c, l, n, r)), e.s < 0 ? new c(1).div(s) : k(s, r, i)
    if (((o = l.s), o < 0)) {
        if (t < e.d.length - 1) return new c(NaN)
        if (((e.d[t] & 1) == 0 && (o = 1), l.e == 0 && l.d[0] == 1 && l.d.length == 1)) return (l.s = o), l
    }
    return (
        (n = we(+l, u)),
        (t = n == 0 || !isFinite(n) ? Pe(u * (Math.log("0." + Ee(l.d)) / Math.LN10 + l.e + 1)) : new c(n + "").e),
        t > c.maxE + 1 || t < c.minE - 1
            ? new c(t > 0 ? o / 0 : 0)
            : ((K = !1),
              (c.rounding = l.s = 1),
              (n = Math.min(12, (t + "").length)),
              (s = us(e.times(pt(l, r + n)), r)),
              s.d && ((s = k(s, r + 5, 1)), gn(s.d, r, i) && ((t = r + 10), (s = k(us(e.times(pt(l, t + n)), t), t + 5, 1)), +Ee(s.d).slice(r + 1, r + 15) + 1 == 1e14 && (s = k(s, r + 1, 0)))),
              (s.s = o),
              (K = !0),
              (c.rounding = i),
              k(s, r, i))
    )
}
w.toPrecision = function (e, t) {
    var n,
        r = this,
        s = r.constructor
    return (
        e === void 0
            ? (n = Qe(r, r.e <= s.toExpNeg || r.e >= s.toExpPos))
            : (Oe(e, 1, at), t === void 0 ? (t = s.rounding) : Oe(t, 0, 8), (r = k(new s(r), e, t)), (n = Qe(r, e <= r.e || r.e <= s.toExpNeg, e))),
        r.isNeg() && !r.isZero() ? "-" + n : n
    )
}
w.toSignificantDigits = w.toSD = function (e, t) {
    var n = this,
        r = n.constructor
    return e === void 0 ? ((e = r.precision), (t = r.rounding)) : (Oe(e, 1, at), t === void 0 ? (t = r.rounding) : Oe(t, 0, 8)), k(new r(n), e, t)
}
w.toString = function () {
    var e = this,
        t = e.constructor,
        n = Qe(e, e.e <= t.toExpNeg || e.e >= t.toExpPos)
    return e.isNeg() && !e.isZero() ? "-" + n : n
}
w.truncated = w.trunc = function () {
    return k(new this.constructor(this), this.e + 1, 1)
}
w.valueOf = w.toJSON = function () {
    var e = this,
        t = e.constructor,
        n = Qe(e, e.e <= t.toExpNeg || e.e >= t.toExpPos)
    return e.isNeg() ? "-" + n : n
}
function Ee(e) {
    var t,
        n,
        r,
        s = e.length - 1,
        i = "",
        o = e[0]
    if (s > 0) {
        for (i += o, t = 1; t < s; t++) (r = e[t] + ""), (n = D - r.length), n && (i += ht(n)), (i += r)
        ;(o = e[t]), (r = o + ""), (n = D - r.length), n && (i += ht(n))
    } else if (o === 0) return "0"
    for (; o % 10 == 0; ) o /= 10
    return i + o
}
function Oe(e, t, n) {
    if (e !== ~~e || e < t || e > n) throw Error(dt + e)
}
function gn(e, t, n, r) {
    var s, i, o, l
    for (i = e[0]; i >= 10; i /= 10) --t
    return (
        --t < 0 ? ((t += D), (s = 0)) : ((s = Math.ceil((t + 1) / D)), (t %= D)),
        (i = we(10, D - t)),
        (l = e[s] % i | 0),
        r == null
            ? t < 3
                ? (t == 0 ? (l = (l / 100) | 0) : t == 1 && (l = (l / 10) | 0), (o = (n < 4 && l == 99999) || (n > 3 && l == 49999) || l == 5e4 || l == 0))
                : (o = (((n < 4 && l + 1 == i) || (n > 3 && l + 1 == i / 2)) && ((e[s + 1] / i / 100) | 0) == we(10, t - 2) - 1) || ((l == i / 2 || l == 0) && ((e[s + 1] / i / 100) | 0) == 0))
            : t < 4
            ? (t == 0 ? (l = (l / 1e3) | 0) : t == 1 ? (l = (l / 100) | 0) : t == 2 && (l = (l / 10) | 0), (o = ((r || n < 4) && l == 9999) || (!r && n > 3 && l == 4999)))
            : (o = (((r || n < 4) && l + 1 == i) || (!r && n > 3 && l + 1 == i / 2)) && ((e[s + 1] / i / 1e3) | 0) == we(10, t - 3) - 1),
        o
    )
}
function Jn(e, t, n) {
    for (var r, s = [0], i, o = 0, l = e.length; o < l; ) {
        for (i = s.length; i--; ) s[i] *= t
        for (s[0] += os.indexOf(e.charAt(o++)), r = 0; r < s.length; r++) s[r] > n - 1 && (s[r + 1] === void 0 && (s[r + 1] = 0), (s[r + 1] += (s[r] / n) | 0), (s[r] %= n))
    }
    return s.reverse()
}
function Lf(e, t) {
    var n, r, s
    if (t.isZero()) return t
    ;(r = t.d.length), r < 32 ? ((n = Math.ceil(r / 3)), (s = (1 / er(4, n)).toString())) : ((n = 16), (s = "2.3283064365386962890625e-10")), (e.precision += n), (t = zt(e, 1, t.times(s), new e(1)))
    for (var i = n; i--; ) {
        var o = t.times(t)
        t = o.times(o).minus(o).times(8).plus(1)
    }
    return (e.precision -= n), t
}
var le = (function () {
    function e(r, s, i) {
        var o,
            l = 0,
            c = r.length
        for (r = r.slice(); c--; ) (o = r[c] * s + l), (r[c] = o % i | 0), (l = (o / i) | 0)
        return l && r.unshift(l), r
    }
    function t(r, s, i, o) {
        var l, c
        if (i != o) c = i > o ? 1 : -1
        else
            for (l = c = 0; l < i; l++)
                if (r[l] != s[l]) {
                    c = r[l] > s[l] ? 1 : -1
                    break
                }
        return c
    }
    function n(r, s, i, o) {
        for (var l = 0; i--; ) (r[i] -= l), (l = r[i] < s[i] ? 1 : 0), (r[i] = l * o + r[i] - s[i])
        for (; !r[0] && r.length > 1; ) r.shift()
    }
    return function (r, s, i, o, l, c) {
        var u,
            f,
            a,
            d,
            g,
            v,
            C,
            x,
            P,
            L,
            S,
            F,
            G,
            j,
            U,
            ce,
            ee,
            ge,
            te,
            ye,
            _e = r.constructor,
            he = r.s == s.s ? 1 : -1,
            pe = r.d,
            q = s.d
        if (!pe || !pe[0] || !q || !q[0]) return new _e(!r.s || !s.s || (pe ? q && pe[0] == q[0] : !q) ? NaN : (pe && pe[0] == 0) || !q ? he * 0 : he / 0)
        for (c ? ((g = 1), (f = r.e - s.e)) : ((c = Ue), (g = D), (f = Pe(r.e / g) - Pe(s.e / g))), te = q.length, ee = pe.length, P = new _e(he), L = P.d = [], a = 0; q[a] == (pe[a] || 0); a++);
        if ((q[a] > (pe[a] || 0) && f--, i == null ? ((j = i = _e.precision), (o = _e.rounding)) : l ? (j = i + (r.e - s.e) + 1) : (j = i), j < 0)) L.push(1), (v = !0)
        else {
            if (((j = (j / g + 2) | 0), (a = 0), te == 1)) {
                for (d = 0, q = q[0], j++; (a < ee || d) && j--; a++) (U = d * c + (pe[a] || 0)), (L[a] = (U / q) | 0), (d = U % q | 0)
                v = d || a < ee
            } else {
                for (d = (c / (q[0] + 1)) | 0, d > 1 && ((q = e(q, d, c)), (pe = e(pe, d, c)), (te = q.length), (ee = pe.length)), ce = te, S = pe.slice(0, te), F = S.length; F < te; ) S[F++] = 0
                ;(ye = q.slice()), ye.unshift(0), (ge = q[0]), q[1] >= c / 2 && ++ge
                do
                    (d = 0),
                        (u = t(q, S, te, F)),
                        u < 0
                            ? ((G = S[0]),
                              te != F && (G = G * c + (S[1] || 0)),
                              (d = (G / ge) | 0),
                              d > 1
                                  ? (d >= c && (d = c - 1), (C = e(q, d, c)), (x = C.length), (F = S.length), (u = t(C, S, x, F)), u == 1 && (d--, n(C, te < x ? ye : q, x, c)))
                                  : (d == 0 && (u = d = 1), (C = q.slice())),
                              (x = C.length),
                              x < F && C.unshift(0),
                              n(S, C, F, c),
                              u == -1 && ((F = S.length), (u = t(q, S, te, F)), u < 1 && (d++, n(S, te < F ? ye : q, F, c))),
                              (F = S.length))
                            : u === 0 && (d++, (S = [0])),
                        (L[a++] = d),
                        u && S[0] ? (S[F++] = pe[ce] || 0) : ((S = [pe[ce]]), (F = 1))
                while ((ce++ < ee || S[0] !== void 0) && j--)
                v = S[0] !== void 0
            }
            L[0] || L.shift()
        }
        if (g == 1) (P.e = f), (Co = v)
        else {
            for (a = 1, d = L[0]; d >= 10; d /= 10) a++
            ;(P.e = a + f * g - 1), k(P, l ? i + P.e + 1 : i, o, v)
        }
        return P
    }
})()
function k(e, t, n, r) {
    var s,
        i,
        o,
        l,
        c,
        u,
        f,
        a,
        d,
        g = e.constructor
    e: if (t != null) {
        if (((a = e.d), !a)) return e
        for (s = 1, l = a[0]; l >= 10; l /= 10) s++
        if (((i = t - s), i < 0)) (i += D), (o = t), (f = a[(d = 0)]), (c = (f / we(10, s - o - 1)) % 10 | 0)
        else if (((d = Math.ceil((i + 1) / D)), (l = a.length), d >= l))
            if (r) {
                for (; l++ <= d; ) a.push(0)
                ;(f = c = 0), (s = 1), (i %= D), (o = i - D + 1)
            } else break e
        else {
            for (f = l = a[d], s = 1; l >= 10; l /= 10) s++
            ;(i %= D), (o = i - D + s), (c = o < 0 ? 0 : (f / we(10, s - o - 1)) % 10 | 0)
        }
        if (
            ((r = r || t < 0 || a[d + 1] !== void 0 || (o < 0 ? f : f % we(10, s - o - 1))),
            (u =
                n < 4
                    ? (c || r) && (n == 0 || n == (e.s < 0 ? 3 : 2))
                    : c > 5 || (c == 5 && (n == 4 || r || (n == 6 && (i > 0 ? (o > 0 ? f / we(10, s - o) : 0) : a[d - 1]) % 10 & 1) || n == (e.s < 0 ? 8 : 7)))),
            t < 1 || !a[0])
        )
            return (a.length = 0), u ? ((t -= e.e + 1), (a[0] = we(10, (D - (t % D)) % D)), (e.e = -t || 0)) : (a[0] = e.e = 0), e
        if ((i == 0 ? ((a.length = d), (l = 1), d--) : ((a.length = d + 1), (l = we(10, D - i)), (a[d] = o > 0 ? ((f / we(10, s - o)) % we(10, o) | 0) * l : 0)), u))
            for (;;)
                if (d == 0) {
                    for (i = 1, o = a[0]; o >= 10; o /= 10) i++
                    for (o = a[0] += l, l = 1; o >= 10; o /= 10) l++
                    i != l && (e.e++, a[0] == Ue && (a[0] = 1))
                    break
                } else {
                    if (((a[d] += l), a[d] != Ue)) break
                    ;(a[d--] = 0), (l = 1)
                }
        for (i = a.length; a[--i] === 0; ) a.pop()
    }
    return K && (e.e > g.maxE ? ((e.d = null), (e.e = NaN)) : e.e < g.minE && ((e.e = 0), (e.d = [0]))), e
}
function Qe(e, t, n) {
    if (!e.isFinite()) return Io(e)
    var r,
        s = e.e,
        i = Ee(e.d),
        o = i.length
    return (
        t
            ? (n && (r = n - o) > 0 ? (i = i.charAt(0) + "." + i.slice(1) + ht(r)) : o > 1 && (i = i.charAt(0) + "." + i.slice(1)), (i = i + (e.e < 0 ? "e" : "e+") + e.e))
            : s < 0
            ? ((i = "0." + ht(-s - 1) + i), n && (r = n - o) > 0 && (i += ht(r)))
            : s >= o
            ? ((i += ht(s + 1 - o)), n && (r = n - s - 1) > 0 && (i = i + "." + ht(r)))
            : ((r = s + 1) < o && (i = i.slice(0, r) + "." + i.slice(r)), n && (r = n - o) > 0 && (s + 1 === o && (i += "."), (i += ht(r)))),
        i
    )
}
function Xn(e, t) {
    var n = e[0]
    for (t *= D; n >= 10; n /= 10) t++
    return t
}
function Gn(e, t, n) {
    if (t > kf) throw ((K = !0), n && (e.precision = n), Error(Po))
    return k(new e(Vn), t, 1, !0)
}
function qe(e, t, n) {
    if (t > cs) throw Error(Po)
    return k(new e(Yn), t, n, !0)
}
function Oo(e) {
    var t = e.length - 1,
        n = t * D + 1
    if (((t = e[t]), t)) {
        for (; t % 10 == 0; t /= 10) n--
        for (t = e[0]; t >= 10; t /= 10) n++
    }
    return n
}
function ht(e) {
    for (var t = ""; e--; ) t += "0"
    return t
}
function xo(e, t, n, r) {
    var s,
        i = new e(1),
        o = Math.ceil(r / D + 4)
    for (K = !1; ; ) {
        if ((n % 2 && ((i = i.times(t)), Fo(i.d, o) && (s = !0)), (n = Pe(n / 2)), n === 0)) {
            ;(n = i.d.length - 1), s && i.d[n] === 0 && ++i.d[n]
            break
        }
        ;(t = t.times(t)), Fo(t.d, o)
    }
    return (K = !0), i
}
function To(e) {
    return e.d[e.d.length - 1] & 1
}
function So(e, t, n) {
    for (var r, s = new e(t[0]), i = 0; ++i < t.length; )
        if (((r = new e(t[i])), r.s)) s[n](r) && (s = r)
        else {
            s = r
            break
        }
    return s
}
function us(e, t) {
    var n,
        r,
        s,
        i,
        o,
        l,
        c,
        u = 0,
        f = 0,
        a = 0,
        d = e.constructor,
        g = d.rounding,
        v = d.precision
    if (!e.d || !e.d[0] || e.e > 17) return new d(e.d ? (e.d[0] ? (e.s < 0 ? 0 : 1 / 0) : 1) : e.s ? (e.s < 0 ? 0 : e) : 0 / 0)
    for (t == null ? ((K = !1), (c = v)) : (c = t), l = new d(0.03125); e.e > -2; ) (e = e.times(l)), (a += 5)
    for (r = ((Math.log(we(2, a)) / Math.LN10) * 2 + 5) | 0, c += r, n = i = o = new d(1), d.precision = c; ; ) {
        if (((i = k(i.times(e), c, 1)), (n = n.times(++f)), (l = o.plus(le(i, n, c, 1))), Ee(l.d).slice(0, c) === Ee(o.d).slice(0, c))) {
            for (s = a; s--; ) o = k(o.times(o), c, 1)
            if (t == null)
                if (u < 3 && gn(o.d, c - r, g, u)) (d.precision = c += 10), (n = i = l = new d(1)), (f = 0), u++
                else return k(o, (d.precision = v), g, (K = !0))
            else return (d.precision = v), o
        }
        o = l
    }
}
function pt(e, t) {
    var n,
        r,
        s,
        i,
        o,
        l,
        c,
        u,
        f,
        a,
        d,
        g = 1,
        v = 10,
        C = e,
        x = C.d,
        P = C.constructor,
        L = P.rounding,
        S = P.precision
    if (C.s < 0 || !x || !x[0] || (!C.e && x[0] == 1 && x.length == 1)) return new P(x && !x[0] ? -1 / 0 : C.s != 1 ? NaN : x ? 0 : C)
    if ((t == null ? ((K = !1), (f = S)) : (f = t), (P.precision = f += v), (n = Ee(x)), (r = n.charAt(0)), Math.abs((i = C.e)) < 15e14)) {
        for (; (r < 7 && r != 1) || (r == 1 && n.charAt(1) > 3); ) (C = C.times(e)), (n = Ee(C.d)), (r = n.charAt(0)), g++
        ;(i = C.e), r > 1 ? ((C = new P("0." + n)), i++) : (C = new P(r + "." + n.slice(1)))
    } else return (u = Gn(P, f + 2, S).times(i + "")), (C = pt(new P(r + "." + n.slice(1)), f - v).plus(u)), (P.precision = S), t == null ? k(C, S, L, (K = !0)) : C
    for (a = C, c = o = C = le(C.minus(1), C.plus(1), f, 1), d = k(C.times(C), f, 1), s = 3; ; ) {
        if (((o = k(o.times(d), f, 1)), (u = c.plus(le(o, new P(s), f, 1))), Ee(u.d).slice(0, f) === Ee(c.d).slice(0, f)))
            if (((c = c.times(2)), i !== 0 && (c = c.plus(Gn(P, f + 2, S).times(i + ""))), (c = le(c, new P(g), f, 1)), t == null))
                if (gn(c.d, f - v, L, l)) (P.precision = f += v), (u = o = C = le(a.minus(1), a.plus(1), f, 1)), (d = k(C.times(C), f, 1)), (s = l = 1)
                else return k(c, (P.precision = S), L, (K = !0))
            else return (P.precision = S), c
        ;(c = u), (s += 2)
    }
}
function Io(e) {
    return String((e.s * e.s) / 0)
}
function fs(e, t) {
    var n, r, s
    for (
        (n = t.indexOf(".")) > -1 && (t = t.replace(".", "")), (r = t.search(/e/i)) > 0 ? (n < 0 && (n = r), (n += +t.slice(r + 1)), (t = t.substring(0, r))) : n < 0 && (n = t.length), r = 0;
        t.charCodeAt(r) === 48;
        r++
    );
    for (s = t.length; t.charCodeAt(s - 1) === 48; --s);
    if (((t = t.slice(r, s)), t)) {
        if (((s -= r), (e.e = n = n - r - 1), (e.d = []), (r = (n + 1) % D), n < 0 && (r += D), r < s)) {
            for (r && e.d.push(+t.slice(0, r)), s -= D; r < s; ) e.d.push(+t.slice(r, (r += D)))
            ;(t = t.slice(r)), (r = D - t.length)
        } else r -= s
        for (; r--; ) t += "0"
        e.d.push(+t), K && (e.e > e.constructor.maxE ? ((e.d = null), (e.e = NaN)) : e.e < e.constructor.minE && ((e.e = 0), (e.d = [0])))
    } else (e.e = 0), (e.d = [0])
    return e
}
function $f(e, t) {
    var n, r, s, i, o, l, c, u, f
    if (t.indexOf("_") > -1) {
        if (((t = t.replace(/(\d)_(?=\d)/g, "$1")), Ao.test(t))) return fs(e, t)
    } else if (t === "Infinity" || t === "NaN") return +t || (e.s = NaN), (e.e = NaN), (e.d = null), e
    if (If.test(t)) (n = 16), (t = t.toLowerCase())
    else if (Sf.test(t)) n = 2
    else if (Mf.test(t)) n = 8
    else throw Error(dt + t)
    for (
        i = t.search(/p/i),
            i > 0 ? ((c = +t.slice(i + 1)), (t = t.substring(2, i))) : (t = t.slice(2)),
            i = t.indexOf("."),
            o = i >= 0,
            r = e.constructor,
            o && ((t = t.replace(".", "")), (l = t.length), (i = l - i), (s = xo(r, new r(n), i, i * 2))),
            u = Jn(t, n, Ue),
            f = u.length - 1,
            i = f;
        u[i] === 0;
        --i
    )
        u.pop()
    return i < 0 ? new r(e.s * 0) : ((e.e = Xn(u, f)), (e.d = u), (K = !1), o && (e = le(e, s, l * 4)), c && (e = e.times(Math.abs(c) < 54 ? we(2, c) : tr.pow(2, c))), (K = !0), e)
}
function Hf(e, t) {
    var n,
        r = t.d.length
    if (r < 3) return t.isZero() ? t : zt(e, 2, t, t)
    ;(n = 1.4 * Math.sqrt(r)), (n = n > 16 ? 16 : n | 0), (t = t.times(1 / er(5, n))), (t = zt(e, 2, t, t))
    for (var s, i = new e(5), o = new e(16), l = new e(20); n--; ) (s = t.times(t)), (t = t.times(i.plus(s.times(o.times(s).minus(l)))))
    return t
}
function zt(e, t, n, r, s) {
    var i,
        o,
        l,
        c,
        u = e.precision,
        f = Math.ceil(u / D)
    for (K = !1, c = n.times(n), l = new e(r); ; ) {
        if (((o = le(l.times(c), new e(t++ * t++), u, 1)), (l = s ? r.plus(o) : r.minus(o)), (r = le(o.times(c), new e(t++ * t++), u, 1)), (o = l.plus(r)), o.d[f] !== void 0)) {
            for (i = f; o.d[i] === l.d[i] && i--; );
            if (i == -1) break
        }
        ;(i = l), (l = r), (r = o), (o = i)
    }
    return (K = !0), (o.d.length = f + 1), o
}
function er(e, t) {
    for (var n = e; --t; ) n *= e
    return n
}
function Mo(e, t) {
    var n,
        r = t.s < 0,
        s = qe(e, e.precision, 1),
        i = s.times(0.5)
    if (((t = t.abs()), t.lte(i))) return (nt = r ? 4 : 1), t
    if (((n = t.divToInt(s)), n.isZero())) nt = r ? 3 : 2
    else {
        if (((t = t.minus(n.times(s))), t.lte(i))) return (nt = To(n) ? (r ? 2 : 3) : r ? 4 : 1), t
        nt = To(n) ? (r ? 1 : 4) : r ? 3 : 2
    }
    return t.minus(s).abs()
}
function as(e, t, n, r) {
    var s,
        i,
        o,
        l,
        c,
        u,
        f,
        a,
        d,
        g = e.constructor,
        v = n !== void 0
    if ((v ? (Oe(n, 1, at), r === void 0 ? (r = g.rounding) : Oe(r, 0, 8)) : ((n = g.precision), (r = g.rounding)), !e.isFinite())) f = Io(e)
    else {
        for (
            f = Qe(e),
                o = f.indexOf("."),
                v ? ((s = 2), t == 16 ? (n = n * 4 - 3) : t == 8 && (n = n * 3 - 2)) : (s = t),
                o >= 0 && ((f = f.replace(".", "")), (d = new g(1)), (d.e = f.length - o), (d.d = Jn(Qe(d), 10, s)), (d.e = d.d.length)),
                a = Jn(f, 10, s),
                i = c = a.length;
            a[--c] == 0;

        )
            a.pop()
        if (!a[0]) f = v ? "0p+0" : "0"
        else {
            if (
                (o < 0 ? i-- : ((e = new g(e)), (e.d = a), (e.e = i), (e = le(e, d, n, r, 0, s)), (a = e.d), (i = e.e), (u = Co)),
                (o = a[n]),
                (l = s / 2),
                (u = u || a[n + 1] !== void 0),
                (u = r < 4 ? (o !== void 0 || u) && (r === 0 || r === (e.s < 0 ? 3 : 2)) : o > l || (o === l && (r === 4 || u || (r === 6 && a[n - 1] & 1) || r === (e.s < 0 ? 8 : 7)))),
                (a.length = n),
                u)
            )
                for (; ++a[--n] > s - 1; ) (a[n] = 0), n || (++i, a.unshift(1))
            for (c = a.length; !a[c - 1]; --c);
            for (o = 0, f = ""; o < c; o++) f += os.charAt(a[o])
            if (v) {
                if (c > 1)
                    if (t == 16 || t == 8) {
                        for (o = t == 16 ? 4 : 3, --c; c % o; c++) f += "0"
                        for (a = Jn(f, s, t), c = a.length; !a[c - 1]; --c);
                        for (o = 1, f = "1."; o < c; o++) f += os.charAt(a[o])
                    } else f = f.charAt(0) + "." + f.slice(1)
                f = f + (i < 0 ? "p" : "p+") + i
            } else if (i < 0) {
                for (; ++i; ) f = "0" + f
                f = "0." + f
            } else if (++i > c) for (i -= c; i--; ) f += "0"
            else i < c && (f = f.slice(0, i) + "." + f.slice(i))
        }
        f = (t == 16 ? "0x" : t == 2 ? "0b" : t == 8 ? "0o" : "") + f
    }
    return e.s < 0 ? "-" + f : f
}
function Fo(e, t) {
    if (e.length > t) return (e.length = t), !0
}
function Bf(e) {
    return new this(e).abs()
}
function jf(e) {
    return new this(e).acos()
}
function Df(e) {
    return new this(e).acosh()
}
function Uf(e, t) {
    return new this(e).plus(t)
}
function qf(e) {
    return new this(e).asin()
}
function Kf(e) {
    return new this(e).asinh()
}
function Wf(e) {
    return new this(e).atan()
}
function zf(e) {
    return new this(e).atanh()
}
function Zf(e, t) {
    ;(e = new this(e)), (t = new this(t))
    var n,
        r = this.precision,
        s = this.rounding,
        i = r + 4
    return (
        !e.s || !t.s
            ? (n = new this(NaN))
            : !e.d && !t.d
            ? ((n = qe(this, i, 1).times(t.s > 0 ? 0.25 : 0.75)), (n.s = e.s))
            : !t.d || e.isZero()
            ? ((n = t.s < 0 ? qe(this, r, s) : new this(0)), (n.s = e.s))
            : !e.d || t.isZero()
            ? ((n = qe(this, i, 1).times(0.5)), (n.s = e.s))
            : t.s < 0
            ? ((this.precision = i), (this.rounding = 1), (n = this.atan(le(e, t, i, 1))), (t = qe(this, i, 1)), (this.precision = r), (this.rounding = s), (n = e.s < 0 ? n.minus(t) : n.plus(t)))
            : (n = this.atan(le(e, t, i, 1))),
        n
    )
}
function Vf(e) {
    return new this(e).cbrt()
}
function Yf(e) {
    return k((e = new this(e)), e.e + 1, 2)
}
function Qf(e, t, n) {
    return new this(e).clamp(t, n)
}
function Jf(e) {
    if (!e || typeof e != "object") throw Error(Qn + "Object expected")
    var t,
        n,
        r,
        s = e.defaults === !0,
        i = ["precision", 1, at, "rounding", 0, 8, "toExpNeg", -Wt, 0, "toExpPos", 0, Wt, "maxE", 0, Wt, "minE", -Wt, 0, "modulo", 0, 9]
    for (t = 0; t < i.length; t += 3)
        if (((n = i[t]), s && (this[n] = ls[n]), (r = e[n]) !== void 0))
            if (Pe(r) === r && r >= i[t + 1] && r <= i[t + 2]) this[n] = r
            else throw Error(dt + n + ": " + r)
    if (((n = "crypto"), s && (this[n] = ls[n]), (r = e[n]) !== void 0))
        if (r === !0 || r === !1 || r === 0 || r === 1)
            if (r)
                if (typeof crypto != "undefined" && crypto && (crypto.getRandomValues || crypto.randomBytes)) this[n] = !0
                else throw Error(Ro)
            else this[n] = !1
        else throw Error(dt + n + ": " + r)
    return this
}
function Xf(e) {
    return new this(e).cos()
}
function Gf(e) {
    return new this(e).cosh()
}
function ko(e) {
    var t, n, r
    function s(i) {
        var o,
            l,
            c,
            u = this
        if (!(u instanceof s)) return new s(i)
        if (((u.constructor = s), Lo(i))) {
            ;(u.s = i.s),
                K ? (!i.d || i.e > s.maxE ? ((u.e = NaN), (u.d = null)) : i.e < s.minE ? ((u.e = 0), (u.d = [0])) : ((u.e = i.e), (u.d = i.d.slice()))) : ((u.e = i.e), (u.d = i.d ? i.d.slice() : i.d))
            return
        }
        if (((c = typeof i), c === "number")) {
            if (i === 0) {
                ;(u.s = 1 / i < 0 ? -1 : 1), (u.e = 0), (u.d = [0])
                return
            }
            if ((i < 0 ? ((i = -i), (u.s = -1)) : (u.s = 1), i === ~~i && i < 1e7)) {
                for (o = 0, l = i; l >= 10; l /= 10) o++
                K ? (o > s.maxE ? ((u.e = NaN), (u.d = null)) : o < s.minE ? ((u.e = 0), (u.d = [0])) : ((u.e = o), (u.d = [i]))) : ((u.e = o), (u.d = [i]))
                return
            } else if (i * 0 != 0) {
                i || (u.s = NaN), (u.e = NaN), (u.d = null)
                return
            }
            return fs(u, i.toString())
        } else if (c !== "string") throw Error(dt + i)
        return (l = i.charCodeAt(0)) === 45 ? ((i = i.slice(1)), (u.s = -1)) : (l === 43 && (i = i.slice(1)), (u.s = 1)), Ao.test(i) ? fs(u, i) : $f(u, i)
    }
    if (
        ((s.prototype = w),
        (s.ROUND_UP = 0),
        (s.ROUND_DOWN = 1),
        (s.ROUND_CEIL = 2),
        (s.ROUND_FLOOR = 3),
        (s.ROUND_HALF_UP = 4),
        (s.ROUND_HALF_DOWN = 5),
        (s.ROUND_HALF_EVEN = 6),
        (s.ROUND_HALF_CEIL = 7),
        (s.ROUND_HALF_FLOOR = 8),
        (s.EUCLID = 9),
        (s.config = s.set = Jf),
        (s.clone = ko),
        (s.isDecimal = Lo),
        (s.abs = Bf),
        (s.acos = jf),
        (s.acosh = Df),
        (s.add = Uf),
        (s.asin = qf),
        (s.asinh = Kf),
        (s.atan = Wf),
        (s.atanh = zf),
        (s.atan2 = Zf),
        (s.cbrt = Vf),
        (s.ceil = Yf),
        (s.clamp = Qf),
        (s.cos = Xf),
        (s.cosh = Gf),
        (s.div = ea),
        (s.exp = ta),
        (s.floor = na),
        (s.hypot = ra),
        (s.ln = sa),
        (s.log = ia),
        (s.log10 = la),
        (s.log2 = oa),
        (s.max = ca),
        (s.min = ua),
        (s.mod = fa),
        (s.mul = aa),
        (s.pow = da),
        (s.random = ha),
        (s.round = pa),
        (s.sign = ga),
        (s.sin = ma),
        (s.sinh = wa),
        (s.sqrt = va),
        (s.sub = _a),
        (s.sum = ba),
        (s.tan = Ea),
        (s.tanh = ya),
        (s.trunc = Ca),
        e === void 0 && (e = {}),
        e && e.defaults !== !0)
    )
        for (r = ["precision", "rounding", "toExpNeg", "toExpPos", "maxE", "minE", "modulo", "crypto"], t = 0; t < r.length; ) e.hasOwnProperty((n = r[t++])) || (e[n] = this[n])
    return s.config(e), s
}
function ea(e, t) {
    return new this(e).div(t)
}
function ta(e) {
    return new this(e).exp()
}
function na(e) {
    return k((e = new this(e)), e.e + 1, 3)
}
function ra() {
    var e,
        t,
        n = new this(0)
    for (K = !1, e = 0; e < arguments.length; )
        if (((t = new this(arguments[e++])), t.d)) n.d && (n = n.plus(t.times(t)))
        else {
            if (t.s) return (K = !0), new this(1 / 0)
            n = t
        }
    return (K = !0), n.sqrt()
}
function Lo(e) {
    return e instanceof tr || (e && e.toStringTag === No) || !1
}
function sa(e) {
    return new this(e).ln()
}
function ia(e, t) {
    return new this(e).log(t)
}
function oa(e) {
    return new this(e).log(2)
}
function la(e) {
    return new this(e).log(10)
}
function ca() {
    return So(this, arguments, "lt")
}
function ua() {
    return So(this, arguments, "gt")
}
function fa(e, t) {
    return new this(e).mod(t)
}
function aa(e, t) {
    return new this(e).mul(t)
}
function da(e, t) {
    return new this(e).pow(t)
}
function ha(e) {
    var t,
        n,
        r,
        s,
        i = 0,
        o = new this(1),
        l = []
    if ((e === void 0 ? (e = this.precision) : Oe(e, 1, at), (r = Math.ceil(e / D)), this.crypto))
        if (crypto.getRandomValues) for (t = crypto.getRandomValues(new Uint32Array(r)); i < r; ) (s = t[i]), s >= 429e7 ? (t[i] = crypto.getRandomValues(new Uint32Array(1))[0]) : (l[i++] = s % 1e7)
        else if (crypto.randomBytes) {
            for (t = crypto.randomBytes((r *= 4)); i < r; )
                (s = t[i] + (t[i + 1] << 8) + (t[i + 2] << 16) + ((t[i + 3] & 127) << 24)), s >= 214e7 ? crypto.randomBytes(4).copy(t, i) : (l.push(s % 1e7), (i += 4))
            i = r / 4
        } else throw Error(Ro)
    else for (; i < r; ) l[i++] = (Math.random() * 1e7) | 0
    for (r = l[--i], e %= D, r && e && ((s = we(10, D - e)), (l[i] = ((r / s) | 0) * s)); l[i] === 0; i--) l.pop()
    if (i < 0) (n = 0), (l = [0])
    else {
        for (n = -1; l[0] === 0; n -= D) l.shift()
        for (r = 1, s = l[0]; s >= 10; s /= 10) r++
        r < D && (n -= D - r)
    }
    return (o.e = n), (o.d = l), o
}
function pa(e) {
    return k((e = new this(e)), e.e + 1, this.rounding)
}
function ga(e) {
    return (e = new this(e)), e.d ? (e.d[0] ? e.s : 0 * e.s) : e.s || NaN
}
function ma(e) {
    return new this(e).sin()
}
function wa(e) {
    return new this(e).sinh()
}
function va(e) {
    return new this(e).sqrt()
}
function _a(e, t) {
    return new this(e).sub(t)
}
function ba() {
    var e = 0,
        t = arguments,
        n = new this(t[e])
    for (K = !1; n.s && ++e < t.length; ) n = n.plus(t[e])
    return (K = !0), k(n, this.precision, this.rounding)
}
function Ea(e) {
    return new this(e).tan()
}
function ya(e) {
    return new this(e).tanh()
}
function Ca(e) {
    return k((e = new this(e)), e.e + 1, 1)
}
w[Symbol.for("nodejs.util.inspect.custom")] = w.toString
w[Symbol.toStringTag] = "Decimal"
var tr = (w.constructor = ko(ls))
Vn = new tr(Vn)
Yn = new tr(Yn)
export {
    Ln as A,
    Vl as B,
    Ar as C,
    ka as D,
    je as F,
    hf as R,
    Ra as T,
    Ma as a,
    Ia as b,
    La as c,
    $a as d,
    He as e,
    zs as f,
    Oa as g,
    Ti as h,
    Be as i,
    Ie as j,
    Wl as k,
    Pa as l,
    It as m,
    Aa as n,
    Ha as o,
    Sn as p,
    Sa as q,
    xn as r,
    wf as s,
    X as t,
    xa as u,
    Ta as v,
    $t as w,
    Fa as x,
    xc as y,
    Na as z
}
