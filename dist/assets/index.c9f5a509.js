var Ke = Object.defineProperty,
    We = Object.defineProperties
var Ne = Object.getOwnPropertyDescriptors
var pe = Object.getOwnPropertySymbols
var Ge = Object.prototype.hasOwnProperty,
    He = Object.prototype.propertyIsEnumerable
var ve = (n, t, u) => (t in n ? Ke(n, t, { enumerable: !0, configurable: !0, writable: !0, value: u }) : (n[t] = u)),
    K = (n, t) => {
        for (var u in t || (t = {})) Ge.call(t, u) && ve(n, u, t[u])
        if (pe) for (var u of pe(t)) He.call(t, u) && ve(n, u, t[u])
        return n
    },
    J = (n, t) => We(n, Ne(t))
import {
    d as Je,
    f as D,
    e as k,
    r as M,
    j as e,
    y as Q,
    z as g,
    A as X,
    F as ne,
    l as w,
    n as C,
    w as le,
    g as ae,
    T as Qe,
    B as Xe,
    C as Ye,
    i as he,
    x as _e,
    D as se,
    p as Ze
} from "./vendor.5859bc74.js"
import {
    C as B,
    S as et,
    a as tt,
    t as W,
    L as nt,
    g as N,
    b as G,
    c as ge,
    d as R,
    u as P,
    m as oe,
    e as Y,
    f as be,
    i as ue,
    r as lt,
    j as at,
    s as ye,
    k as xe,
    l as Z,
    n as st,
    o as ce,
    p as ot,
    q as ut,
    v as ct,
    w as rt,
    x as ke,
    y as it,
    z as ft,
    A as mt,
    B as dt,
    D as re,
    E as we,
    F as Ce,
    G as pt,
    H as Se,
    I as vt,
    J as ht,
    K as _t,
    M as $e,
    N as gt,
    O as bt,
    P as yt,
    Q as Ve,
    R as xt,
    T as kt,
    U as wt,
    V as Ct,
    W as St
} from "./index.7098be47.js"
const ie = Symbol("[App]CurrentAlter"),
    fe = Je("details", {
        state() {
            return { part: "pants" }
        },
        actions: {
            setPart(n) {
                this.part = n
            }
        }
    })
function $t(n) {
    const t = {},
        { item: u, slots: d = [] } = n
    if (!u) return t
    if (u.effect) {
        const o = new B()
        u.effect(o)
        const m = A(o)
        t.effect = m
    }
    if (u.dungeon_effect) {
        let o = new B()
        u.dungeon_effect(o)
        const m = A(o)
        t.dungeon_effect = m
    }
    for (let o of d) {
        let m = new B()
        o.effect(n, m)
        let p = Array.from(m.history())
        for (let a = 0; a < p.length; a++) {
            const s = a == 0 ? o.name : `${o.name}_${a}`
            t[s] = A(p[a])
        }
    }
    return t
}
function Vt(n) {
    const t = $t(n),
        { item: u } = n
    if (((u == null ? void 0 : u.modifier) && (t.modifier = qt(n)), (u == null ? void 0 : u.rarity) == "mythic")) {
        const d = [],
            { mythic_properties: o } = u
        for (let m of o) {
            const { deal_data: p, buff_data: a, effect: s } = m,
                l = N(p),
                i = N(a),
                v = new B()
            s(v, [l, i]), d.push(...A(v))
        }
        t.mythic = d
    }
    return t
}
function qt(n) {
    var m
    const { item: t, part: u, data: d } = n,
        o = []
    if (t == null ? void 0 : t.modifier) {
        let p = 0,
            [a, s, l = 3] = (m = d.get("modifier")) != null ? m : ["none", "none", 0]
        if (a == "auto") {
            const { result: f } = G()
            if (f == null ? void 0 : f.context.modifiers) {
                let c = f.context.modifiers.find(r => r[0] == u)
                c && ((s = c[1]), (l = c[2]))
            }
        }
        ;(a == "none" || s == "none") && ((s = t.modifier[0]), (l = 0)), (p = t.modifier[1])
        const i = u == "weapon",
            x = ge(u, a).find(f => f.name == s)
        if (x) {
            let { buff_data: f, key: c, name: r } = x,
                [h, $] = c
            if (h != "awake") {
                const V = i ? 0.04 : 0.02
                p = r.startsWith("awake_") ? 0 : p
                const E = p,
                    T = p.plus(V.multiply(4)),
                    _ = [V, E, T],
                    S = l > 0 ? N(_, l) : E,
                    y = qe(h, S)
                        .concat(" ")
                        .concat(`(${ee(E.plus(V), "%")}~${ee(T, "%")})`)
                if ((o.push(y), f)) {
                    const I = l > 0 ? N(f, l - 1) : 0
                    if (I > 0) {
                        const [z, F, L] = f,
                            O = new B()
                        O.changeBuff({ [$]: I })
                        let j = A(O)
                        ;(j = j.map(U => U.concat(" ").concat(`(${ee(F, "%")}~${ee(L, "%")})`))), o.push(...j)
                    }
                }
                if (t.name == "weapon_epic_broom_01") {
                    const I = new B()
                    I.changeBuff({ awake_level_add: 2 })
                    let z = A(I)
                    o.push(...z)
                }
            }
            const q = new B()
            x.effect(q, l)
            const b = A(q)
            o.push(...b)
        }
    }
    return o
}
function ee(n, t = "") {
    return n > 0 && n < 1 ? W(n, 0, t) : n.toString()
}
function It(n) {
    if (n == null ? void 0 : n.effect) {
        const t = new B()
        return n.effect(t), A(t)
    }
    return []
}
function A({ name: n, buff_entry: t, deal_entry: u, skill_entries: d, status: o, listeners: m }) {
    const p = []
    return n && p.push(n), p.push(...jt(o)), p.push(...Dt(u)), p.push(...Et(d)), p.push(...Bt(t)), p.push(...Ot(m)), p
}
function Ot(n) {
    let t = []
    for (let [u, d] of n) {
        const o = u.split(":")
        t.push(`{on.${o[0]}}${o[1]}`)
        for (let m of d) {
            const p = new B()
            m(p), t.push(...A(p))
        }
    }
    return t
}
function jt(n) {
    let { ice_boost: t = 0, fire_boost: u, light_boost: d, dark_boost: o, intelligence: m = 0, strength: p, vitality: a = 0, spirit: s, phyatk: l = 0, magatk: i, indatk: v } = n
    return (
        l > 0 && [i, v].every(x => x == l) && ((n.triatk = l), delete n.phyatk, delete n.magatk, delete n.indatk),
        m > 0 && m == p && ((n.intstr = m), delete n.intelligence, delete n.strength),
        a > 0 && a == s && ((n.vitspi = a), delete n.vitality, delete n.spirit),
        n.vitspi && n.vitspi == n.intstr && ((n.quadra = n.vitspi), delete n.vitspi, delete n.intstr),
        t > 0 && [u, d, o].every(x => x == t) && ((n.elemental_boost = t), delete n.ice_boost, delete n.fire_boost, delete n.light_boost, delete n.dark_boost),
        H(n, void 0, (x, f) => {
            let c = `{${x}} +${f}`
            return (x.endsWith("_rate") || x.endsWith("_speed")) && (c = c.concat("%")), c
        })
    )
}
function Et(n) {
    const t = [],
        u = " "
    for (let d of n) {
        const { start: o, end: m, type: p, condition: a, value: s } = d,
            l = [],
            { levels: i, ignores: v, jobs: x = [], names: f } = a
        if (o + m > 1 || i.length > 0) {
            switch ((l.push(u), o > 0 && l.push(`Lv${o}`), o < m && (l.push(`~${m}`), l.push(u), l.push("{total}")), i.length > 0 && l.push(i.map(c => `Lv${c}`).join("\u3001")), l.push(u), a.type)) {
                case et.ACTIVE:
                    l.push("{active_skill}")
                    break
                default:
                    l.push("{skill}")
                    break
            }
            l.push(u)
        } else f.length == 0 && l.push("{all_skill}")
        if ((f.length > 0 && l.push(...f.map(c => `[{$skills.${c}}]{skill}`)), s > 0))
            switch (p) {
                case nt:
                    l.push("Lv"), l.push(u), l.push(`+${s}`)
                    break
                case tt:
                    l.push("{cd}"), l.push(u), l.push(`-${W(s, 0, "%")}`)
                    break
            }
        t.push(l.join(""))
    }
    return t
}
function Dt(n) {
    const t = []
    return t.push(...H(n)), t
}
function Bt(n) {
    const t = [],
        { buff_phyatk_per: u, buff_magatk_per: d, buff_indatk_per: o, buff_int_per: m, buff_str_per: p } = n
    m > 0 && p > 0 ? t.push(`{buff_intstr_per[${W(p)}]} `) : t.push(...H({ buff_int_per: m, buff_str_per: p })),
        u > 0 && d > 0 && o > 0 ? t.push(`{buff_triatk_per[${W(u)}]} `) : t.push(...H({ buff_phyatk_per: u, buff_magatk_per: d, buff_indatk_per: o }))
    const a = ["buff_int_per", "buff_str_per", "buff_phyatk_per", "buff_magatk_per", "buff_indatk_per"]
    return t.push(...H(n, s => !a.includes(s))), t
}
function qe(n, t, u) {
    var o
    const d = (o = t > 0 && t < 1 ? W(t) : t.toString()) != null ? o : ""
    return u || (u = (m, p) => `{${m}[${p}]}`), u(n, d)
}
function H(n, t, u) {
    const d = []
    let o = Object.keys(n)
    t && (o = o.filter(t))
    for (let m of o) {
        let p = n[m]
        p && d.push(qe(m, p, u))
    }
    return d
}
function Ie(n) {
    return typeof n == "function" || (Object.prototype.toString.call(n) === "[object Object]" && !X(n))
}
var Oe = D({
    name: "overview",
    setup() {
        const n = fe(),
            t = R(),
            u = k(() => Y.filter(_ => _.part == n.part)),
            d = k({
                get() {
                    return be(n.part)
                },
                set(_) {
                    t.setEquip(n.part, _)
                }
            }),
            o = k(() => ue(d.value)),
            m = Z.filter(_ => _ != "title"),
            p = k(() => m.includes(n.part)),
            a = k({
                set(_) {
                    let S = []
                    b.value ? S.push(...m) : S.push(n.part)
                    for (let y of S) t.setColumnData(y, "upgrade_level", _)
                },
                get() {
                    var _
                    return (_ = t.getColumnData(n.part, "upgrade_level")) != null ? _ : 0
                }
            }),
            s = k({
                get() {
                    return !!t.getColumnData(n.part, "has_cursed")
                },
                set(_) {
                    let S = []
                    b.value ? (S = m) : S.push(n.part)
                    for (let y of S) t.setColumnData(y, "has_cursed", _)
                }
            }),
            l = k(() =>
                t.enchant_list
                    .filter(_ => _.parts.includes(n.part) && lt(_))
                    .sort(at)
                    .map(_ => {
                        var I
                        const S = new B()
                        ;(I = _.effect) == null || I.call(_, S)
                        let y = A(S).join(" ")
                        return J(K({}, _), { text: y })
                    })
            ),
            i = k({
                get() {
                    return t.getColumnData(n.part, "enchanting")
                },
                set(_) {
                    let S = [n.part]
                    if (b.value) {
                        const y = t.enchant_list.find(I => I.name == _)
                        ;(y == null ? void 0 : y.parts) && (S = y.parts)
                    }
                    for (let y of S) t.setColumnData(y, "enchanting", _)
                }
            }),
            v = k(() => {
                let _ = ye.filter(S => S.parts.includes(n.part)).map(S => S.name)
                return (_ = Array.from(new Set(_))), _
            }),
            x = k({
                get() {
                    var _
                    return (_ = d.value) == null ? void 0 : _.suit_name
                },
                set(_) {
                    _ && t.setEquipSuit(_)
                }
            })
        function f(_, S = !0) {
            var F
            const y = b.value,
                I = `socket_${S ? "left" : "right"}`
            let z = [n.part]
            if (y) {
                const L = t.emblem_list.find(O => O.name == _)
                if (L) {
                    z.length = 0
                    const O = (F = L.colors) != null ? F : []
                    for (let j of O) {
                        const U = st[j]
                        z.push(...U)
                    }
                }
            }
            for (let L of z) y ? (t.setColumnData(L, "socket_left", _), t.setColumnData(L, "socket_right", _)) : t.setColumnData(L, I, _)
        }
        const c = k({
                get() {
                    return t.getColumnData(n.part, "socket_left")
                },
                set(_) {
                    f(_, !0), t.setColumnData(n.part, "socket_left", _)
                }
            }),
            r = k({
                get() {
                    return t.getColumnData(n.part, "socket_right")
                },
                set(_) {
                    f(_, !1)
                }
            }),
            h = k(() => {
                var _, S
                return !["weapon", "earrings", "title"].includes((S = (_ = d.value) == null ? void 0 : _.part) != null ? S : "")
            }),
            $ = k(() => {
                var _, S
                return !["subequip", "magicstones"].includes((S = (_ = d.value) == null ? void 0 : _.part) != null ? S : "") && !b.value
            }),
            q = k(() => {
                const _ = xe(n.part)
                return _ ? t.emblem_list.filter(S => S.colors.includes(_)) : []
            }),
            b = M(!1),
            V = P(),
            E = P("items"),
            T = P("comments.abbr")
        return () => {
            let _, S
            return e("div", { class: "flex flex-wrap" }, [
                e("div", null, [e("img", { src: o.value, class: "w-10 h-10 mr-4 mb-4" }, null)]),
                e("div", { class: "flex w-100 h-8 items-center" }, [
                    e("span", { class: "text-sm w-10" }, [Q("\u88C5\u5907:")]),
                    e(
                        g("calc-select"),
                        { modelValue: d.value, "onUpdate:modelValue": y => (d.value = y), class: "w-40 mr-4" },
                        Ie((_ = u.value.map(y => e(g("calc-option"), { label: E(y.name), value: y }, null)))) ? _ : { default: () => [_] }
                    ),
                    p.value
                        ? e(ne, null, [
                              e(
                                  g("calc-select"),
                                  { modelValue: s.value, "onUpdate:modelValue": y => (s.value = y), class: "w-8 mr-4" },
                                  { default: () => [e(g("calc-option"), { label: E("reinforce"), value: !1 }, null), e(g("calc-option"), { label: E("amplify"), value: !0 }, null)] }
                              ),
                              e(
                                  g("calc-select"),
                                  { modelValue: a.value, "onUpdate:modelValue": y => (a.value = y), class: "w-8" },
                                  Ie((S = oe(0, 32, y => e(g("calc-option"), { label: `${y}`, value: y }, null)))) ? S : { default: () => [S] }
                              )
                          ])
                        : e("div", null, null)
                ]),
                v.value.length
                    ? e("div", { class: "flex w-100 h-8 items-center" }, [
                          e("span", { class: "text-sm w-10" }, [Q("\u5957\u88C5:")]),
                          e(
                              g("calc-select"),
                              { modelValue: x.value, "onUpdate:modelValue": y => (x.value = y), class: "w-80" },
                              { default: () => [e(g("calc-option"), { label: E("none"), value: null }, null), v.value.map(y => e(g("calc-option"), { label: E, value: y }, null))] }
                          )
                      ])
                    : void 0,
                e("div", { class: "flex w-100 h-8 items-center" }, [
                    e("span", { class: "text-sm w-10" }, [Q("\u9644\u9B54:")]),
                    e(
                        g("calc-select"),
                        { modelValue: i.value, "onUpdate:modelValue": y => (i.value = y), class: "w-80" },
                        { default: () => [e(g("calc-option"), { label: V("none"), value: null }, null), l.value.map(y => e(g("calc-option"), { label: T(y.text), value: y.name }, null))] }
                    )
                ]),
                h.value
                    ? e("div", { class: "flex w-120 h-8 items-center flex-wrap" }, [
                          e("span", { class: "text-sm w-10" }, [Q("\u5FBD\u7AE0:")]),
                          e(
                              g("calc-select"),
                              { modelValue: c.value, "onUpdate:modelValue": y => (c.value = y), class: "w-50" },
                              { default: () => [e(g("calc-option"), { label: E("none"), value: null }, null), q.value.map(y => e(g("calc-option"), { label: E, value: y.name }, null))] }
                          ),
                          $.value
                              ? e(
                                    g("calc-select"),
                                    { modelValue: r.value, "onUpdate:modelValue": y => (r.value = y), class: "w-50 ml-2" },
                                    { default: () => [e(g("calc-option"), { label: E("none"), value: null }, null), q.value.map(y => e(g("calc-option"), { label: E, value: y.name }, null))] }
                                )
                              : e("div", null, null)
                      ])
                    : e("div", null, null),
                e("div", { class: "w-full" }, [e(g("calc-checkbox"), { checked: b.value, "onUpdate:checked": y => (b.value = y), label: V("global_change") }, null)])
            ])
        }
    }
})
function Tt(n) {
    return typeof n == "function" || (Object.prototype.toString.call(n) === "[object Object]" && !X(n))
}
var je = D({
    name: "avatar",
    setup() {
        const n = R(),
            { avatar_columns: t, avatar_list: u } = n
        function d(r) {
            return u.filter(h => h.part == r)
        }
        function o(r) {
            var h, $
            return ($ = (h = t[r].item) == null ? void 0 : h.name) != null ? $ : null
        }
        function m(r) {
            return h => {
                const $ = u.find(q => q.name == h)
                n.setAvatar(r, $)
            }
        }
        function p(r) {
            const h = t[r].item
            if (h) {
                let { options: $ } = h
                if ((($ = ut($)), $))
                    return $.map(q => {
                        const b = new B()
                        return typeof q == "function" ? q(b) : b.changeStatus(q), A(b).join("")
                    })
            }
            return []
        }
        function a(r) {
            var h
            return (h = t[r].data.get("option_index")) != null ? h : 0
        }
        function s(r) {
            return (h = 0) => {
                t[r].data.set("option_index", h)
            }
        }
        function l(r) {
            var $
            let h = ($ = r.title) != null ? $ : r.name
            return typeof h == "function" && (h = h()), f(h)
        }
        const i = ["skin", "aura", "weapon"],
            v = ce.filter(r => !i.includes(r)),
            x = k({
                get() {
                    var $, q
                    const r = []
                    for (let b of v) {
                        if (i.includes(b)) continue
                        const { item: V } = t[b]
                        V && r.push(V)
                    }
                    return (q = ($ = ot(r, ct).find(b => b.needCount >= 8)) == null ? void 0 : $.name) != null ? q : "none"
                },
                set(r) {
                    n.setAvatarSuit(r)
                }
            })
        P()
        const f = P("items"),
            c = P("comments.abbr")
        return (
            M(!1),
            () => {
                function r(h) {
                    const $ = p(h).map((q, b) => e(g("calc-option"), { label: c(q), value: b }, null))
                    return $.length == 0 && $.push(e(g("calc-option"), { label: f("none"), value: null }, null)), $
                }
                return e("div", { class: "flex flex-wrap" }, [
                    ce.map(h => {
                        let $
                        return e("div", { class: "w-full flex h-8 items-center" }, [
                            w(e("div", { class: "w-10 text-sm" }, null), [[C("trans"), h]]),
                            e(
                                g("calc-select"),
                                { class: "mx-2 w-48 text-xs", modelValue: o(h), "onUpdate:modelValue": m(h) },
                                { default: () => [e(g("calc-option"), { label: f("none"), value: null }, null), d(h).map(q => e(g("calc-option"), { label: l(q), value: q.name }, null))] }
                            ),
                            e(g("calc-select"), { class: "w-60 text-xs", modelValue: a(h), "onUpdate:modelValue": s(h) }, Tt(($ = r(h))) ? $ : { default: () => [$] })
                        ])
                    }),
                    e("div", { class: "w-full flex h-8 items-center" }, [
                        w(e("div", { class: "w-10 text-sm" }, null), [[C("trans"), "suits"]]),
                        e(
                            g("calc-select"),
                            { class: "mx-2 w-48 text-xs", modelValue: x.value, "onUpdate:modelValue": h => (x.value = h) },
                            {
                                default: () => [
                                    e(g("calc-option"), { label: f, value: "none" }, null),
                                    e(g("calc-option"), { label: f, value: "unique_avatar" }, null),
                                    e(g("calc-option"), { label: f, value: "rare_avatar" }, null),
                                    e(g("calc-option"), { label: f, value: "holiday_avatar" }, null)
                                ]
                            }
                        )
                    ])
                ])
            }
        )
    }
})
var Ut = D({
        props: { suitname: { type: String } },
        setup(n) {
            const t = k(() => Y.filter(o => o.suit_name == n.suitname).map(o => ({ name: o.name, active: rt(o.name) }))),
                u = k(() => t.value.filter(o => o.active).length),
                d = k(() =>
                    ye
                        .filter(o => o.name == n.suitname)
                        .map(o => {
                            var a
                            const m = new B()
                            return (a = o.effect) == null || a.call(o, m), { comments: A(m), active: u.value >= o.needCount, count: o.needCount }
                        })
                )
            return () =>
                e("div", { class: "suit-tooltip" }, [
                    e("div", { class: "suit-equip-list" }, [t.value.map(o => w(e("div", { class: { "suit-color": o.active, "suit-equip": !0 } }, null), [[C("trans"), o.name, "items"]]))]),
                    e("div", { class: "suit-effects" }, [
                        d.value.map(o =>
                            e("div", { class: "suit-effect-block" }, [
                                w(e("div", { class: { "suit-effect-title": !0, "suit-color": o.active } }, null), [[C("trans"), `{suit_effect}[${o.count}]`]]),
                                o.comments.map(m => w(e("div", { class: { "suit-effect-comment": !0, active: o.active } }, null), [[C("trans"), m, "comments"]]))
                            ])
                        )
                    ])
                ])
        }
    }),
    At = D({
        name: "equip-comments",
        props: { column: { type: Object } },
        setup(n) {
            const t = k(() => n.column),
                u = k(() => Vt(t.value)),
                d = k(() => [c("socket"), c("socket_1")].filter(V => !!V && V.length > 0)),
                o = k(() => (!u.value || !t.value ? [] : t.value.slots)),
                m = k(() => {
                    var b
                    if (t.value.item) {
                        let { type: V, part: E } = t.value.item
                        return it.includes(E) && (V = (b = t.value.data.get("armor_type")) != null ? b : V), V
                    }
                    return "light"
                }),
                p = ["cloth", "leather", "light", "heavy", "plate"],
                a = k(() => {
                    let b = m.value
                    if (p.includes(b)) return b.concat("_armor")
                }),
                s = k(() => {
                    if (t.value.item) {
                        let { part: b } = t.value.item
                        return xe(b)
                    }
                }),
                l = ke(),
                i = k(() => {
                    if (t.value.item) {
                        let b = t.value.item
                        return ft(J(K({}, b), { type: m.value }))
                    }
                    return l
                }),
                v = ["reinforce", "amplify", "refine"],
                x = k(() => {
                    var V
                    const b = {}
                    if (t.value) {
                        const E = ((V = o.value) != null ? V : []).filter(T => v.includes(T.name))
                        for (let T of E) {
                            let _ = new B()
                            T.effect(t.value, _)
                            const S = Array.from(_.history())
                            for (let y = 0; y < S.length; y++) {
                                const I = y == 0 ? T.name : `${T.name}_${y}`
                                b[I] = S[y].status
                            }
                        }
                    }
                    return b
                }),
                f = k(() => {
                    const b = x.value.refine_1
                    if (b && b.intelligence > 0) return `{strength}\u3001{intelligence}\u3001{vitality}\u3001{spirit} +${b.intelligence}`
                })
            function c(b) {
                var V
                return u.value ? ((V = u.value[b]) != null ? V : []) : [e("div", null, null)]
            }
            const r = k(() => Object.keys(l).filter(h))
            function h(b) {
                for (let V in x.value) {
                    if (V.includes("_")) continue
                    if (x.value[V][b]) return !0
                }
                return !!i.value[b]
            }
            const $ = k(() => {
                    var V
                    const b = (V = t.value.item) == null ? void 0 : V.modifier
                    if (b) return b
                }),
                q = k(() => {
                    const b = t.value.item
                    if ((b == null ? void 0 : b.rarity) == "mythic") return b.mythic_properties
                })
            return () => {
                if (!t.value.item) return e("span", null, null)
                const { name: b, icon: V, suit_name: E, rarity: T, part: _, type: S } = t.value.item,
                    y = x.value,
                    I = []
                if (
                    (s.value &&
                        I.push(
                            e("div", { class: "comment" }, [
                                d.value.map((O, j) =>
                                    e("div", { key: j }, [
                                        w(e("div", { class: `${s.value}-socket` }, null), [[C("trans"), `[{${s.value}_socket_column}]`, "comments"]]),
                                        O.map(U => w(e("div", { class: "socket-value" }, null), [[C("trans"), U]]))
                                    ])
                                )
                            ])
                        ),
                    r.value.length > 0)
                ) {
                    const O = r.value.map(j =>
                        e("div", { key: j }, [
                            w(e("span", null, null), [[C("trans"), j]]),
                            e("span", { class: "num", textContent: i.value[j] || "" }, null),
                            Object.keys(y)
                                .filter(U => !U.includes("_"))
                                .map(U => e("span", { key: U }, [y[U][j] ? e("span", { class: `num ${U}-color`, textContent: `+${y[U][j].round()}` }, null) : e("span", null, null)]))
                        ])
                    )
                    I.push(e("div", { class: "comment status" }, [O]))
                }
                const z = c("enchanting")
                z.length > 0 && I.push(e("div", { class: "comment effect" }, [z.map((O, j) => w(e("div", { class: "enchanting-color", key: j }, null), [[C("trans"), O]]))])),
                    f.value && I.push(e("div", { class: "comment" }, [w(e("div", { class: "refine-color" }, null), [[C("trans"), f.value]])]))
                const F = c("effect")
                F.length && I.push(e("div", { class: "comment effect" }, [F.map((O, j) => w(e("div", { key: j }, null), [[C("trans"), O, "comments"]]))]))
                const L = q.value
                if (L) {
                    const O = []
                    let j = 1
                    for (let U of L) {
                        const { deal_data: Ue, buff_data: Ae, effect: Le } = U,
                            Me = N(Ue),
                            Pe = N(Ae),
                            de = new B()
                        Le(de, [Me, Pe])
                        const ze = A(de)
                        O.push(
                            e("div", null, [
                                w(e("span", null, null), [[C("trans"), `{property}${j++}`, "comments"]]),
                                ze.map((Re, Fe) => w(e("div", { key: Fe }, null), [[C("trans"), Re, "comments"]]))
                            ])
                        )
                    }
                    O.length > 0 && I.push(e("div", { class: "comment" }, [w(e("div", { class: "property-title" }, null), [[C("trans"), "<{mythic_property}>"]]), O]))
                }
                if ($.value) {
                    const O = c("modifier")
                    O.length > 0 &&
                        I.push(
                            e("div", { class: "comment modifier" }, [
                                w(e("div", { class: "property-title" }, null), [[C("trans"), "<{modifier_property}>"]]),
                                O.map((j, U) => w(e("div", { key: U }, null), [[C("trans"), j, "comments"]]))
                            ])
                        )
                }
                return e("div", null, [
                    e("div", { class: "divider" }, null),
                    e("div", { class: "info-row" }, [w(e("span", { class: `${T}-color` }, null), [[C("trans"), T]])]),
                    e("div", { class: "info-row" }, [
                        w(e("span", { class: m.value != S ? "deep-green" : "" }, null), [[C("trans"), a.value]]),
                        w(e("span", { class: "part-text" }, null), [[C("trans"), a.value ? `({${_}})` : _]])
                    ]),
                    I
                ])
            }
        }
    })
var Ee = D({
    name: "calc-tooltip",
    emits: ["change"],
    props: ["popperClass"],
    setup(n, { slots: t, emit: u }) {
        const d = M(!1),
            o = M(),
            m = M({ x: 0, y: 0 }),
            p = k(() => ({ left: `${m.value.x}px`, top: `${m.value.y}px`, visibility: d.value ? "visible" : "hidden" }))
        function a(l) {
            if (((d.value = !0), o.value)) {
                const { width: i, height: v, left: x, top: f } = o.value.getBoundingClientRect()
                m.value = { x, y: f + v + 4 }
            }
        }
        function s() {
            d.value = !1
        }
        return (
            le(d, () => u("change", d.value)),
            () =>
                e("div", { class: "i-tooltip" }, [
                    e("div", { onMouseover: a, onMouseout: s, ref: o, class: "i-tooltip-content" }, [ae(t, "default")]),
                    e(Qe, { to: "body" }, { default: () => [e("div", { class: ["i-popper", n.popperClass], style: p.value }, [ae(t, "popper")])] })
                ])
        )
    }
})
var De = D({
        name: "item-icon",
        components: { Tooltip: Ee },
        emits: ["change"],
        props: ["column", "titleColor"],
        setup(n, { slots: t, emit: u }) {
            const d = k(() => n.column),
                o = k(() => {
                    var s
                    return (s = d.value) == null ? void 0 : s.item
                }),
                m = k(() => {
                    if (!o.value) return ""
                    let { name: s, title: l } = o.value
                    return l ? (typeof l == "function" ? l() : l) : s
                }),
                p = k(() => {
                    if (n.titleColor) return n.titleColor
                    if (o.value) {
                        const { rarity: s } = o.value
                        if (s) return `${s}-color`
                    }
                    return ""
                })
            function a(...s) {
                u("change", ...s)
            }
            return () => {
                if (!o.value) return e("div", null, null)
                const { name: s, icon: l, suit_name: i, rarity: v, part: x } = o.value
                return e(
                    Ee,
                    { onChange: a, "popper-class": "item-tooltip" },
                    {
                        default: () => {
                            var c, r
                            return (r = (c = t.icon) == null ? void 0 : c.call(t)) != null ? r : e("img", { src: l }, null)
                        },
                        popper: () => {
                            var c
                            return e(ne, null, [
                                t.header
                                    ? (c = t.header) == null
                                        ? void 0
                                        : c.call(t)
                                    : e("div", { class: "item-tooltip-title" }, [
                                          e("div", { class: `${p.value} item-name` }, [w(e("span", null, null), [[C("trans"), m.value, "items"]])]),
                                          i
                                              ? e("div", { class: "item-suit-name suit-color" }, [
                                                    w(e("span", null, null), [[C("trans"), i, "items"]]),
                                                    w(e("span", { class: "suffix" }, null), [[C("trans"), "suits"]])
                                                ])
                                              : e("div", null, null)
                                      ]),
                                e("div", { class: "item-tooltip-comments" }, [ae(t, "default")])
                            ])
                        }
                    }
                )
            }
        }
    }),
    me = D({
        name: "equip-icon",
        components: { equipComments: At, itemIcon: De },
        props: { column: { type: Object }, showIcon: { type: Boolean, default: () => !0 } },
        setup(n) {
            const t = k(() => n.column),
                u = k(() => {
                    var s, l
                    const { data: p, slots: a = [] } = t.value
                    if (t.value.item) {
                        const { name: i } = t.value.item,
                            v = (s = p == null ? void 0 : p.get("upgrade_level")) != null ? s : 0,
                            x = (l = p == null ? void 0 : p.get("refine_level")) != null ? l : 0
                        let f = ""
                        return a.some(c => c.name == "amplify") && v > 0 && (f = `+${v}`), x > 0 && (f = f.concat(`(${x})`)), f.length > 0 && (f = f.concat(" ")), (f = f.concat(`{${i}}`)), f
                    }
                }),
                d = M(!1)
            Xe(() => {
                document.addEventListener("keyup", o)
            }),
                Ye(() => {
                    document.removeEventListener("keyup", o)
                })
            function o(p) {
                p.code == "F8" && (d.value = !d.value)
            }
            function m() {
                d.value = !1
            }
            return () => {
                if (!t.value.item) return e("span", null, null)
                const p = t.value.item,
                    { suit_name: a, rarity: s } = p,
                    l = () => e("div", { class: "icon" }, [e("img", { src: ue(p) }, null)]),
                    i = {
                        icon: l,
                        header() {
                            return e("div", { class: "equip-tooltip-header" }, [
                                n.showIcon ? e("div", { class: "equip-tooltip-icon" }, [l()]) : e("div", null, null),
                                e("div", { class: "equip-tooltip-title" }, [
                                    e("div", { class: `${s}-color equip-name` }, [w(e("span", null, null), [[C("trans"), u.value, "items"]])]),
                                    a
                                        ? e("div", { class: "equip-suit-name suit-color" }, [
                                              w(e("span", null, null), [[C("trans"), a, "items"]]),
                                              w(e("span", { class: "suffix" }, null), [[C("trans"), "suits"]])
                                          ])
                                        : e("div", null, null)
                                ])
                            ])
                        },
                        default() {
                            return e("div", { onKeyup: o }, [e(g("equip-comments"), { column: t.value }, null)])
                        }
                    },
                    v = {
                        header: () => w(e("div", { key: "show-suit", class: "suit-color" }, null), [[C("trans"), a, "items"]]),
                        default() {
                            return e(Ut, { suitname: a }, null)
                        }
                    },
                    x = d.value ? v : i
                return e(g("item-icon"), { onChange: m, column: t.value }, x)
            }
        }
    }),
    Lt = "/assets/ozma.a2364f2e.png",
    Mt = "/assets/siroco.7d3561a6.png",
    Be = D({
        name: "raid",
        components: { EquipIcon: me },
        setup() {
            const n = R()
            function t(a) {
                return dt.filter(s => s.suit_name == a)
            }
            function u(a, s) {
                a && re(a.name) && (a = void 0), n.setSiroco(s, a)
            }
            function d(a) {
                let s = t(a)
                s.every(l => l && re(l.name)) && (s = s.map(l => {})), s.forEach((l, i) => n.setSiroco(i, l))
            }
            function o(a, s) {
                a && Ce(a.name, s) && (a = void 0), n.setOzma(s, a)
            }
            function m(a) {
                let s = we.find(l => l.name === a)
                if (s) {
                    n.ozma_columns.every(l => (l == null ? void 0 : l.name) == a) && (s = void 0)
                    for (let l = 0; l < 5; l++) n.setOzma(l, s)
                }
            }
            function p(a, s, l) {
                return { backgroundImage: `url(${a})`, backgroundPositionX: s == 0 ? 0 : `${-s * 28}px`, backgroundPositionY: l == 0 ? 0 : `${-l * 28}px` }
            }
            return (
                P(),
                () =>
                    e("div", { class: "flex flex-wrap" }, [
                        e("div", { class: "w-full flex" }, [
                            e("div", null, [
                                mt.map((a, s) =>
                                    e("div", { class: "flex items-center mb-2" }, [
                                        w(e(g("calc-button"), { class: "mr-2", onClick: () => d(a) }, null), [[C("trans"), a]]),
                                        t(a).map((l, i) =>
                                            e("div", { class: "mr-2 relative", onClick: () => u(l, i) }, [
                                                e("div", { class: { "icon-lay": !re(l.name) } }, null),
                                                e("div", { class: "icon", style: p(Mt, i, s) }, null)
                                            ])
                                        )
                                    ])
                                )
                            ]),
                            e("div", null, [
                                we.map((a, s) =>
                                    e("div", { class: "flex items-center mb-2" }, [
                                        w(e(g("calc-button"), { class: "mr-2", onClick: () => m(a.name) }, null), [[C("trans"), a.name]]),
                                        oe(0, 5, l =>
                                            e("div", { class: "mr-2 relative", onClick: () => o(a, l) }, [
                                                e("div", { class: { "icon-lay": !Ce(a.name, l) } }, null),
                                                e("div", { class: "icon", style: p(Lt, l, s) }, null)
                                            ])
                                        )
                                    ])
                                )
                            ])
                        ])
                    ])
            )
        }
    })
function Pt(n) {
    return typeof n == "function" || (Object.prototype.toString.call(n) === "[object Object]" && !X(n))
}
var Te = D(() => {
        const n = k(() => ht().modifers)
        function t(c, r, h, $ = 4) {
            const q = R()
            h || (h = n.value[c][0]), q.setColumnData(c, "modifier", [r, h, $])
        }
        const u = P("comments")
        function d(c, r) {
            var b
            let h = a(c)
            const $ = s(c),
                q = be(c)
            if (q) {
                const V = (b = q.modifier) != null ? b : ["none", 0]
                h == "none" && (h = V[0])
                const T = ge(c, $).find(_ => _.name == h)
                if (T) {
                    const _ = new B(),
                        S = Se()
                    return (_.disable_dealer = S), (_.disable_buffer = !S), _t(T, _, V[1], r), A(_)
                }
            }
            return ["none"]
        }
        function o(c) {
            return r => {
                t(c, s(c), a(c), r)
            }
        }
        function m(c) {
            return r => {
                t(c, r, a(c), l(c))
            }
        }
        function p(c) {
            return r => {
                const h = s(c)
                ;(r = h == "awake" ? "awake_".concat(r) : r), t(c, h, r, l(c))
            }
        }
        function a(c) {
            return n.value[c][1]
        }
        function s(c) {
            var r
            return (r = n.value[c][0]) != null ? r : "custom"
        }
        function l(c) {
            return a(c) == "none" ? 0 : Math.max(1, n.value[c][2])
        }
        function i(c) {
            const r = s(c)
            return ["auto", "none"].includes(r) || (r == "awake" && Se())
        }
        function v(c) {
            const r = a(c)
            return i(c) || r == "none"
        }
        function x(c) {
            let r = [`{${c}}`]
            return c == 1 ? r.push("({min_value})") : c == 4 && r.push("({max_value})"), f(r.join(" "))
        }
        const f = P()
        return () =>
            e("div", null, [
                pt.map(c => {
                    let r
                    return e(ne, null, [
                        e("div", { class: "flex h-8 items-center" }, [
                            w(e("span", { class: "text-sm w-15" }, null), [[C("trans"), c]]),
                            e(
                                g("calc-select"),
                                { class: "w-15", modelValue: s(c), "onUpdate:modelValue": m(c) },
                                {
                                    default: () => [
                                        e(g("calc-option"), { value: "none", label: f }, null),
                                        e(g("calc-option"), { value: "custom", label: f }, null),
                                        e(g("calc-option"), { value: "auto", label: f }, null),
                                        c == "weapon" ? e(g("calc-option"), { value: "awake", label: f }, null) : void 0
                                    ]
                                }
                            ),
                            e(
                                g("calc-select"),
                                { class: "w-40", disabled: i(c), width: 160, modelValue: a(c), "onUpdate:modelValue": p(c) },
                                { default: () => [e(g("calc-option"), { value: "none", label: f }, null), vt.map(h => e(g("calc-option"), { value: h, label: f }, null))] }
                            ),
                            e(
                                g("calc-select"),
                                { class: "w-48", disabled: v(c), modelValue: l(c), "onUpdate:modelValue": o(c), width: 200 },
                                Pt((r = oe(1, 5, h => e(g("calc-option"), { value: h, label: x(h) }, null)))) ? r : { default: () => [r] }
                            )
                        ]),
                        e("div", { class: "w-full text-hex-#937639 text-sm h-auto" }, [d(c, l(c)).map(h => e("div", { textContent: u(h) }, null))])
                    ])
                })
            ])
    }),
    zt = D({
        name: "overview",
        components: { equip: Oe, avatar: je, fuse: Be, modifiers: Te },
        setup() {
            const n = P(),
                t = M("equip")
            function u(m) {
                return p => {
                    p ? (t.value = m) : (t.value = "none")
                }
            }
            function d(m) {
                return t.value == m
            }
            const o = fe()
            return (
                le(
                    () => o.part,
                    () => {
                        u("equip")(!0)
                    }
                ),
                () =>
                    e("div", { class: "w-128 overflow-hidden" }, [
                        e(g("calc-collapse"), { modelValue: d("equip"), "onUpdate:modelValue": u("equip"), title: n("equip") }, { default: () => [e(Oe, null, null)] }),
                        e(g("calc-collapse"), { modelValue: d("avatar"), "onUpdate:modelValue": u("avatar"), title: n("avatar") }, { default: () => [e(je, null, null)] }),
                        e(g("calc-collapse"), { modelValue: d("fuse"), "onUpdate:modelValue": u("fuse"), title: n("fuse_property") }, { default: () => [e(Be, null, null)] }),
                        e(g("calc-collapse"), { modelValue: d("modifiers"), "onUpdate:modelValue": u("modifiers"), title: n("modifier_property") }, { default: () => [e(Te, null, null)] })
                    ])
            )
        }
    })
var Rt = D(() => {
    const n = G()
    R()
    const t = M(!0),
        u = k(() => {
            var x
            const s = n.result,
                l = [],
                i = t.value ? ["strength", "intelligence", "phyatk", "magatk", "indatk"] : ["damage"],
                v = {}
            if (s) {
                const { skill_datas: f } = s
                if ((l.push(...f), f))
                    for (let c = 0; c < f.length; c++) {
                        const { value: r = {} } = f[c]
                        for (let h in r) {
                            const $ = r[h]
                            $ && (v[h] = $.plus((x = v[h]) != null ? x : 0))
                        }
                    }
                t.value ||
                    l.sort((c, r) => {
                        var h, $, q, b
                        return (($ = (h = r.value) == null ? void 0 : h.damage) != null ? $ : 0) - ((b = (q = c.value) == null ? void 0 : q.damage) != null ? b : 0)
                    })
            }
            return { list: l, headers: i, total: v }
        }),
        d = k(() => {
            var l
            const { result: s } = G()
            return (l = s == null ? void 0 : s.rate) != null ? l : 0
        }),
        o = k(() => {
            const { result: s } = G()
            if (s) {
                const { modifiers: l } = s.context
                if (l.length > 0)
                    return l.map(i => {
                        let [v, x] = i
                        return `{${v}}:{${x}}`
                    })
            }
            return []
        })
    function m(s = 0) {
        return s > 0 ? s.round().toLocaleString("zh", {}) : ""
    }
    function p(s) {
        return ({ value: l }) => Object.entries(l).some(([i, v]) => s.includes(i) && !!v && v > 0)
    }
    const a = P()
    return () => {
        const { list: s, headers: l, total: i } = u.value
        return e("table", { class: "w-auto h-140 bg-hex-#00000078" }, [
            e("thead", null, [
                e("tr", { class: "results-header flex w-full" }, [
                    w(e("th", { class: "results-col  text-#b99460 items-center" }, null), [[C("trans"), "skill"]]),
                    l.map(v => w(e("th", { class: "results-col text-#b99460 items-center" }, null), [[C("trans"), v]]))
                ])
            ]),
            e("tbody", { class: "results-body" }, [
                s
                    .filter(p(l))
                    .map(v =>
                        e("tr", { class: "flex" }, [
                            e("td", { class: "results-col" }, [e("img", { title: a(`$skills.${v.name}`), class: "mr-1", src: $e(v.name) }, null), e("span", { textContent: `Lv.${v.level}` }, null)]),
                            l.map(x => e("td", { class: "results-col", textContent: m(v.value[x]) }, null))
                        ])
                    )
            ]),
            e("tfoot", null, [
                e("tr", { class: "flex" }, [e("td", { class: "results-col", textContent: W(d.value, 0, "%") }, null), l.map(v => e("td", { class: "results-col", textContent: m(i[v]) }, null))]),
                e("tr", { class: "flex" }, [
                    w(e("td", { class: "results-col" }, null), [[C("trans"), "modifier_property"]]),
                    o.value.map(v => w(e("td", { class: "results-col" }, null), [[C("trans"), v]]))
                ])
            ])
        ])
    }
})
function te(n, t = {}) {
    return K({ backgroundImage: `url(${n})`, backgroundRepeat: "no-repeat", zIndex: -1, position: "absolute" }, t)
}
var Ft = "/assets/profile.39cb8ae8.png"
var Kt = D({
        name: "profile",
        components: { EquipIcon: me },
        setup() {
            const n = he(ie),
                t = ["shoulder", "coat", "pants", "belt", "shoes", "pet", "weapon", "title", "bracelet", "necklace", "subequip", "ring", "earrings", "magicstones"],
                u = G(),
                d = R(),
                o = fe(),
                m = d.character
            function p(l) {
                const i = _e(),
                    v = se()
                return () => {
                    o.setPart(l), i.replace({ path: v.path, query: { tab: 0 } })
                }
            }
            function a(l) {
                let i = 13,
                    v = 28,
                    x = t.findIndex(f => f == l)
                return x >= 6 && ((i += 180), (x -= 6)), (i += (x % 2) * 32), (v += Math.floor(x / 2) * 32), { left: `${i}px`, top: `${v}px` }
            }
            const s = k(() => {
                const l = u.result
                if (l) {
                    const { adapt_number: i } = l,
                        { context: v } = l,
                        { status: x, deal_entry: f, buff_entry: c } = v
                    return console.log(f), J(K(K(K({}, x), f), c), { adapt_number: i })
                }
                return ke()
            })
            return () => {
                const l = m == null ? void 0 : m.adapt_status
                function i(v, x) {
                    if (v) {
                        let f = s.value[v]
                        return (
                            x ? (f = x(f)) : (f = f == null ? void 0 : f.round()),
                            e("div", { class: "character-status-row flex justify-between px-1 h-5 box-border" }, [
                                w(e("span", null, null), [[C("trans"), v]]),
                                e("span", { class: "text-hex-#4aa256", textContent: f == null ? void 0 : f.toString() }, null)
                            ])
                        )
                    }
                }
                return e("div", { class: "bg-hex-#0000001f bg-no-repeat  relative box-border w-266px h-100 text-hex-#937639 text-xs" }, [
                    e("div", { class: "w-266px h-100", style: te(Ft) }, null),
                    e("div", { class: "absolute top-182px h-6 flex items-center justify-center w-full  text-xs" }, [u.fame]),
                    e("div", { class: "h-50" }, [
                        e("div", { class: "w-266px h-40 bg-bottom", style: te(`/resources/${n.value}/character.png`) }, null),
                        Z.map(v => e("div", { onClick: p(v), class: "absolute w-7 h-7", style: a(v) }, [e(g("equip-icon"), { column: d.equip_columns[v] }, null)]))
                    ]),
                    e("div", { class: "character-core-status" }, [i(l), i("adapt_number")]),
                    e("div", { class: "w-full flex absolute bottom-10" }, [i("ice_boost"), i("fire_boost"), i("light_boost"), i("dark_boost")]),
                    e("div", null, [i("skill_attack", v => W(v, 1, "%")), i("fire_boost"), i("light_boost"), i("dark_boost")])
                ])
            }
        }
    }),
    Wt = "/assets/avatar_ui.a751f573.png",
    Nt = D({
        name: "item-comments",
        props: ["item"],
        setup(n) {
            const t = k(() => n.item),
                u = k(() => It(t.value))
            return () => {
                const d = []
                t.value.fame && d.push(w(e("div", null, null), [[C("trans"), `{adventurer_fame} ${t.value.fame}`, "comments"]]))
                for (let o of u.value) d.push(w(e("div", { class: "item-comment" }, null), [[C("trans"), o, "comments"]]))
                return e("div", { class: "item-comments" }, [d])
            }
        }
    }),
    Gt = D({
        name: "avatar_column",
        components: { ItemIcon: De, ItemComments: Nt },
        setup() {
            const n = he(ie),
                { avatar_columns: t, pet_column: u } = R(),
                d = ce
            function o(a) {
                let s = 132,
                    l = 8,
                    i = d.findIndex(v => v == a)
                return i == 1 ? (l += 32) : i > 0 && ((i -= 2), (s += 32), (s += (i % 3) * 32), (l += Math.floor(i / 3) * 32)), { left: `${s}px`, top: `${l}px` }
            }
            const m = k(() => {
                const { slots: a } = u,
                    s = a.find(l => l.name == "enchanting")
                if (s) {
                    const l = new B()
                    return s.effect(u, l), A(l)
                }
                return []
            })
            function p(a) {
                var i, v
                const { item: s, data: l } = t[a]
                if (s) {
                    const x = s
                    let f = new B()
                    const c = (i = l.get("option_index")) != null ? i : 0,
                        r = (v = x.options) != null ? v : []
                    if (r == null ? void 0 : r.length) {
                        const h = r[c]
                        return typeof h == "function" ? h(f) : f.changeStatus(h), A(f).join("")
                    }
                }
                return "none_option_value"
            }
            return () =>
                e("div", { class: "w-66.5 h-100 relative box-border" }, [
                    e("div", { class: "w-66.5 h-116", style: te(Wt) }, null),
                    e("div", { class: "h-40" }, [
                        e("div", { class: "w-266px h-40 bg-left", style: te(`/resources/${n.value}/character.png`) }, null),
                        d.map(a =>
                            e("div", { class: "w-7 h-7 absolute", style: o(a) }, [
                                e(
                                    g("item-icon"),
                                    { column: t[a] },
                                    {
                                        default: () => [
                                            e(g("item-comments"), { item: t[a].item }, null),
                                            w(e("div", { class: "text-hex-#5eb6fa mt-2" }, null), [[C("trans"), "[{option_value}]", "comments"]]),
                                            w(e("div", null, null), [[C("trans"), p(a), "comments"]])
                                        ]
                                    }
                                )
                            ])
                        ),
                        u.item
                            ? e("div", { class: "icon", style: { left: "25px", top: "192px" } }, [
                                  e(
                                      g("item-icon"),
                                      { column: u },
                                      {
                                          default: () => [
                                              e(g("item-comments"), { item: u.item }, null),
                                              e("div", { class: "equip-tooltip-comments effect" }, [m.value.map((a, s) => w(e("div", { class: "enchanting-color", key: s }, null), [[C("trans"), a]]))])
                                          ]
                                      }
                                  )
                              ])
                            : e("div", null, null)
                    ])
                ])
        }
    }),
    Ht = D({
        name: "suits",
        components: { IButton: gt },
        setup() {
            const n = bt(),
                t = M(new Set())
            function u(a) {
                t.value.has(a) ? t.value.delete(a) : t.value.add(a)
            }
            const d = M(0)
            le(
                t,
                a => {
                    const s = Y.filter(i => a.has(i.name))
                    let l = xt(s, kt())
                    d.value = l.length
                },
                { deep: !0 }
            )
            function o(a) {
                Y.filter(l => l.suit_name == a)
                    .map(l => l.name)
                    .forEach(u)
            }
            function m(a) {
                return t.value.has(a.name)
            }
            function p(a, s) {
                const l = Ve.indexOf(a.rarity || "epic"),
                    v = Ve.indexOf(s.rarity || "epic") - l
                if (v) return v
                const x = Z.indexOf(a.part),
                    f = Z.indexOf(s.part)
                return x - f
            }
            return () =>
                e("div", { class: "flex-wrap" }, [
                    n.map(a =>
                        e("div", { class: "flex mt-2 items-center", key: a }, [
                            w(e(g("calc-button"), { onClick: () => o(a), class: "mr-3 w-35 font-sm" }, null), [[C("trans"), a, "items"]]),
                            yt(a)
                                .sort(p)
                                .map(s => e("div", { class: "icon mr-2", onClick: () => u(s.name) }, [e("div", { class: { "icon-lay": !m(s) } }, null), e("img", { src: ue(s) }, null)]))
                        ])
                    )
                ])
        }
    }),
    Jt = D({
        name: "skill",
        setup() {
            const n = R(),
                t = k(() => {
                    var u, d
                    return ((d = (u = n.character) == null ? void 0 : u.skills) != null ? d : []).map(o => o.skill).sort((o, m) => o.need_level - m.need_level)
                })
            return () =>
                e("div", null, [
                    t.value.map(u =>
                        e("div", { class: "w-auto h-8 flex items-center" }, [
                            e("img", { class: "w-7 h-7 mr-2", src: $e(u.name) }, null),
                            w(e("span", { class: "text-sm w-40" }, null), [[C("trans"), u.name, "skills"]])
                        ])
                    )
                ])
        }
    })
function Qt(n) {
    return typeof n == "function" || (Object.prototype.toString.call(n) === "[object Object]" && !X(n))
}
var en = D({
    name: "character",
    components: { ITabs: wt, ITab: Ct, EquipIcon: me },
    setup() {
        const n = _e(),
            t = k(() => {
                var v
                return (v = se().params.alter) != null ? v : ""
            })
        R().newCharacter(t.value)
        const d = { backgroundImage: `url(/resources/${t.value}/background.png)` }
        Ze(ie, t)
        const o = k({
            set(i) {
                n.replace({ query: { tab: i } })
            },
            get() {
                const i = se()
                return parseInt(i.query.tab)
            }
        })
        o.value || (o.value = 0)
        const m = [Kt, Gt],
            p = M(0),
            a = St()
        function s() {
            a == null || a.setLocale("en")
        }
        const l = [zt, Jt, Ht]
        return () => {
            var f, c
            let i
            const v = (f = l[o.value]) != null ? f : e("div", null, null),
                x = (c = m[p.value]) != null ? c : e("div", null, null)
            return e("div", { class: "text-color w-full h-full p-8" }, [
                e("div", { class: "mask-full character-bg-image bg-no-repeat bg-cover -z-2", style: d }, null),
                e("div", { class: "mask-full bg-hex-#000000cc" }, null),
                e("div", { class: "w-full h-full flex z-2" }, [
                    e(
                        g("calc-tabs"),
                        { vertical: !0, modelValue: o.value, "onUpdate:modelValue": r => (o.value = r) },
                        {
                            default: () => [
                                w(e(g("calc-tab"), { width: 60, to: "/pc" }, null), [[C("trans"), "back"]]),
                                l.map((r, h) => w(e(g("calc-tab"), { width: 60, value: h }, null), [[C("trans"), r.name]]))
                            ]
                        }
                    ),
                    e("div", { class: "w-140 p-5" }, [e(v, null, null)]),
                    e("div", { class: "w-80" }, [
                        e(
                            g("calc-tabs"),
                            { modelValue: p.value, "onUpdate:modelValue": r => (p.value = r) },
                            Qt((i = m.map((r, h) => w(e(g("calc-tab"), { width: 60, value: h }, null), [[C("trans"), r.name]])))) ? i : { default: () => [i] }
                        ),
                        e(x, null, null),
                        w(e(g("calc-button"), { onClick: () => s() }, null), [[C("trans"), "English"]])
                    ]),
                    e(Rt, null, null)
                ])
            ])
        }
    }
})
export { en as default }
