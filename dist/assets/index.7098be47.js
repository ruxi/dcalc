var Lh = Object.defineProperty,
    Oh = Object.defineProperties
var qh = Object.getOwnPropertyDescriptors
var ju = Object.getOwnPropertySymbols
var Mh = Object.prototype.hasOwnProperty,
    Ph = Object.prototype.propertyIsEnumerable
var li = (r, s, a) => (s in r ? Lh(r, s, { enumerable: !0, configurable: !0, writable: !0, value: a }) : (r[s] = a)),
    ot = (r, s) => {
        for (var a in s || (s = {})) Mh.call(s, a) && li(r, a, s[a])
        if (ju) for (var a of ju(s)) Ph.call(s, a) && li(r, a, s[a])
        return r
    },
    ft = (r, s) => Oh(r, qh(s))
var Z = (r, s, a) => (li(r, typeof s != "symbol" ? s + "" : s, a), a)
import {
    m as Fh,
    d as ci,
    t as Wh,
    c as $h,
    a as Nh,
    b as Uh,
    i as Mt,
    r as dt,
    e as je,
    f as Pt,
    h as oi,
    g as Mn,
    R as el,
    p as fn,
    j as pe,
    u as Kh,
    w as fi,
    o as zh,
    k as tl,
    T as Gh,
    l as nl,
    v as Hh,
    n as Vh,
    q as Yh,
    s as Zh
} from "./vendor.5859bc74.js"
const Xh = function () {
    const s = document.createElement("link").relList
    if (s && s.supports && s.supports("modulepreload")) return
    for (const f of document.querySelectorAll('link[rel="modulepreload"]')) c(f)
    new MutationObserver(f => {
        for (const h of f) if (h.type === "childList") for (const p of h.addedNodes) p.tagName === "LINK" && p.rel === "modulepreload" && c(p)
    }).observe(document, { childList: !0, subtree: !0 })
    function a(f) {
        const h = {}
        return (
            f.integrity && (h.integrity = f.integrity),
            f.referrerpolicy && (h.referrerPolicy = f.referrerpolicy),
            f.crossorigin === "use-credentials" ? (h.credentials = "include") : f.crossorigin === "anonymous" ? (h.credentials = "omit") : (h.credentials = "same-origin"),
            h
        )
    }
    function c(f) {
        if (f.ep) return
        f.ep = !0
        const h = a(f)
        fetch(f.href, h)
    }
}
Xh()
var Jh = [
        {
            name: "aura_2021_autumn_buff",
            fame: 464,
            part: "aura",
            icon: "/icon/avatar/aura/2021_autumn_buff.png",
            has_socket: !0,
            options() {
                return Ue()
                    ? [a => a.changeBuff({ buff_intstr_per: 0.03 })]
                    : ["additional_damage", "critical_damage", "triatk_per"].map(a => c => {
                          c.changeDamage({ [a]: 0.05 })
                      })
            },
            effect(r) {
                r.changeSkill({ start: 1, end: 80, value: 1 })
            }
        }
    ],
    V
;(function (r) {
    ;(r[(r.PASSIVE = 1)] = "PASSIVE"), (r[(r.BUFF = 2)] = "BUFF"), (r[(r.ACTIVE = 4)] = "ACTIVE"), (r[(r.AWAKE = 8)] = "AWAKE")
})(V || (V = {}))
const Qh = "cd_raduction",
    jh = "attack_add",
    rl = "level_add"
function e5(r = {}) {
    var s, a, c, f, h, p, m, y, S, T, C, D, I, L
    return {
        start: (s = r.start) != null ? s : 0,
        end: (a = r.end) != null ? a : 0,
        value: (c = r.value) != null ? c : 0,
        type: (f = r.type) != null ? f : rl,
        condition: {
            levels: (p = (h = r.condition) == null ? void 0 : h.levels) != null ? p : [],
            names: (y = (m = r.condition) == null ? void 0 : m.names) != null ? y : [],
            ignores: (T = (S = r.condition) == null ? void 0 : S.ignores) != null ? T : [],
            type: (D = (C = r.condition) == null ? void 0 : C.type) != null ? D : 1,
            jobs: (L = (I = r.condition) == null ? void 0 : I.jobs) != null ? L : []
        }
    }
}
function t5(r, s) {
    const { skill_entries: a } = s,
        { character_level: c } = Y()
    return function (f) {
        const { skill: h } = f,
            { need_level: p, name: m, level_master: y, level_max: S, level_spacing: T } = h
        let C = 0,
            D = 0,
            I = 1
        for (let Q of a) {
            let { start: F, end: K, condition: ne, type: X, value: se } = Q
            const { ignores: ae = [], names: ie = [], levels: J = [], type: ye = 1 } = ne
            if (!ae.includes(p) && !(ye > h.type) && ((p >= F && (p <= K || K < 0)) || ie.includes(m) || J.includes(p)))
                switch (X) {
                    case rl:
                        C = C.plus(se)
                        break
                    case jh:
                        D = D.perMulti(se)
                        break
                    case Qh:
                        I = I.multiply((1).minus(se))
                        break
                }
        }
        let L = c.plus(5).minus(p).exactlyDivide(T).plus(1)
        return (L = Math.max(L, 0)), (L = Math.min(L, y)), (L = Math.min(L.plus(C), S)), ft(ot({}, r), { attack: D, cdr: I, add_level: C, level: L, skill_name: h.name })
    }
}
function n5(r) {
    return { skill: r, add_level: 0, enabled: !0 }
}
var r5 = [
    ...[40, 45, 60, 70, 80].map(r => ({
        fame: 166,
        part: "weapon",
        name: `rare_avatar_clone_weapon(Lv${r})`,
        title: `{rare_avatar_clone_weapon}(Lv${r}{skill})`,
        icon: "/icon/avatar/weapon/rare_clone.png",
        rarity: "rare",
        effect(s) {
            s.changeSkill({ start: r, end: r, condition: { type: V.ACTIVE, jobs: !1 }, value: 1 })
        },
        options() {
            return ["intelligence", "strength", "vitality", "spirit"].map(a => ({ [a]: 55 }))
        }
    }))
]
function Hm(r, s = 0, a = "") {
    return (r = r != null ? r : s), r == null ? void 0 : r.multiply(100).round(2).toString().concat(a)
}
function _i(...r) {
    return r.length < 2
        ? r
        : r.reduce(
              (s, a) => {
                  const c = []
                  return (
                      s.forEach(f => {
                          a.forEach(h => {
                              c.push(f.concat([h]))
                          })
                      }),
                      c
                  )
              },
              [[]]
          )
}
function a5(r, s, a) {
    const c = new Map()
    for (let f = 0; f < r.length; f++) {
        const h = r[f],
            p = s(h)
        let m = c.get(p) || []
        c.has(p) || c.set(p, m), m.push(h)
    }
    return Array.from(c.values())
}
function al(r, s) {
    const a = {}
    for (let c in r) {
        const f = s(r[c], c)
        f && (a[c] = f)
    }
    return a
}
function et(r, s) {
    return s || (s = Object.keys(r)), s.map(a => ({ [a]: r[a] }))
}
function Vm(r, s, a) {
    const c = []
    for (let f = r; f < s; f++) c.push(a(f))
    return c
}
Array.prototype.max = function (r) {
    let s = [-1, void 0],
        a = -1
    for (let c = 0; c < this.length; c++) {
        const f = this.at(c),
            h = r(f)
        h > a && ((a = h), (s = [c, f]))
    }
    return s
}
Array.prototype.min = function (r) {
    let s = [-1, void 0],
        a = -1
    for (let c = 0; c < this.length; c++) {
        const f = this.at(c),
            h = r(f)
        h < a && ((a = h), (s = [c, f]))
    }
    return s
}
const i5 = { rare: 76, unique: 116 }
function il() {
    const { character: r } = Y()
    if (r) {
        const { skills: s } = r
        return s
            .map(a => a.skill)
            .filter(a => a.need_level >= 15 && a.need_level <= 85)
            .map(a => c => {
                c.changeSkill({ condition: { names: [a.name] }, value: 1 })
            })
    }
    return []
}
function sl(r, s) {
    return [
        { part: "hair", options: et(r, ["intelligence", "spirit", "casting_speed"]) },
        { part: "cap", options: et(r, ["intelligence", "spirit", "casting_speed"]) },
        { part: "face", options: et(r, ["attack_speed"]) },
        { part: "neck", options: et(r, ["attack_speed"]) },
        { part: "coat", options: il },
        { part: "belt", options: et(r, ["strength", "vitality"]) },
        { part: "pants", options: [] },
        { part: "shoes", options: et(r, ["strength", "vitality", "movement_speed"]) }
    ].map(c => ft(ot({}, c), { fame: i5[s], name: `${s}_avatar_${c.part}`, title: `{${s}_avatar}{${c.part}}`, icon: `/icon/avatar/${s}/${c.part}.png`, suit_name: `${s}_avatar`, rarity: s }))
}
const _n = { intelligence: 45, strength: 45, spirit: 45, vitality: 45, casting_speed: 12, attack_speed: 5, movement_speed: 5 },
    s5 = [
        { part: "hair", options: et(_n, ["intelligence", "spirit", "casting_speed"]) },
        { part: "cap", options: et(_n, ["intelligence", "spirit", "casting_speed"]) },
        { part: "face", options: et(_n, ["attack_speed"]) },
        { part: "neck", options: et(_n, ["attack_speed"]) },
        {
            part: "coat",
            effect(r) {
                r.changeStatus({ elemental_boost: 6 })
            },
            options: il
        },
        { part: "belt", options: et(_n, ["strength", "vitality"]) },
        { part: "pants", options: [] },
        { part: "shoes", options: et(_n, ["strength", "vitality", "movement_speed"]) }
    ]
var u5 = s5.map(r =>
    ft(ot({}, r), { name: `holiday_avatar_${r.part}`, icon: `/icon/avatar/holiday/${r.part}.png`, title: `{holiday_avatar}{${r.part}}`, suit_name: "holiday_avatar", rarity: "uncommon" })
)
const l5 = { intelligence: 55, strength: 55, spirit: 55, vitality: 55, casting_speed: 14, attack_speed: 6, movement_speed: 6 }
var c5 = sl(l5, "rare")
const o5 = { intelligence: 65, strength: 65, spirit: 65, vitality: 65, casting_speed: 14, attack_speed: 7, movement_speed: 7 }
var f5 = sl(o5, "unique"),
    _5 = [
        {
            name: "unique_avatar",
            compatible: "rare_avatar",
            needCount: 3,
            effect(r) {
                r.changeStatus({ quadra: 50, speed: 3 })
            }
        },
        {
            name: "unique_avatar",
            compatible: "rare_avatar",
            needCount: 8,
            effect(r) {
                r.changeStatus({ quadra: 50, speed: 3, elemental_boost: 10 })
            }
        },
        {
            name: "rare_avatar",
            needCount: 3,
            effect(r) {
                r.changeStatus({ quadra: 40, speed: 2 })
            }
        },
        {
            name: "rare_avatar",
            needCount: 8,
            effect(r) {
                r.changeStatus({ quadra: 40, speed: 2, elemental_boost: 6 })
            }
        },
        {
            name: "holiday_avatar",
            needCount: 8,
            effect(r) {
                r.changeStatus({ quadra: 25, speed: 2 })
            }
        },
        {
            name: "uncommon_avatar",
            needCount: 3,
            effect(r) {
                r.changeStatus({ quadra: 10 })
            }
        },
        {
            name: "uncommon_avatar",
            needCount: 8,
            effect(r) {
                r.changeStatus({ quadra: 10, speed: 1 })
            }
        }
    ]
const p5 = [...u5, ...c5, ...f5, ...Jh, ...r5],
    ul = ["weapon", "aura", "hair", "cap", "face", "neck", "coat", "skin", "belt", "pants", "shoes"]
var h5 = {
    "100-legendary-plate-coat": [47, 47, 52, 0],
    "100-legendary-plate-shoulder": [39, 39, 42, 0],
    "100-legendary-plate-pants": [47, 47, 52, 0],
    "100-legendary-plate-shoes": [29, 29, 31, 0],
    "100-legendary-plate-belt": [29, 29, 31, 0],
    "100-legendary-cloth-coat": [0, 57, 0, 57],
    "100-legendary-cloth-shoulder": [0, 45, 0, 45],
    "100-legendary-cloth-pants": [0, 57, 0, 57],
    "100-legendary-cloth-shoes": [0, 34, 0, 34],
    "100-legendary-cloth-belt": [0, 34, 0, 34],
    "100-legendary-leather-coat": [47, 47, 0, 52],
    "100-legendary-leather-shoulder": [39, 39, 0, 42],
    "100-legendary-leather-pants": [47, 47, 0, 52],
    "100-legendary-leather-shoes": [29, 29, 0, 31],
    "100-legendary-leather-belt": [29, 29, 0, 31],
    "100-legendary-light-coat": [57, 37, 0, 0],
    "100-legendary-light-shoulder": [45, 31, 0, 0],
    "100-legendary-light-pants": [57, 37, 0, 0],
    "100-legendary-light-shoes": [34, 23, 0, 0],
    "100-legendary-light-belt": [34, 23, 0, 0],
    "100-legendary-heavy-coat": [52, 37, 47, 0],
    "100-legendary-heavy-shoulder": [42, 31, 39, 0],
    "100-legendary-heavy-pants": [52, 37, 47, 0],
    "100-legendary-heavy-shoes": [31, 23, 29, 0],
    "100-legendary-heavy-belt": [31, 23, 29, 0],
    "100-mythic-plate-coat": [150, 150, 55, 0],
    "100-mythic-cloth-coat": [100, 160, 0, 59],
    "100-mythic-leather-coat": [150, 150, 0, 55],
    "100-mythic-light-coat": [160, 140, 0, 0],
    "100-mythic-heavy-coat": [155, 140, 50, 0],
    "100-unique-plate-coat": [46, 46, 51, 0],
    "100-unique-plate-shoulder": [37, 37, 41, 0],
    "100-unique-plate-pants": [46, 46, 51, 0],
    "100-unique-plate-shoes": [28, 28, 31, 0],
    "100-unique-plate-belt": [28, 28, 31, 0],
    "100-unique-cloth-coat": [0, 55, 0, 55],
    "100-unique-cloth-shoulder": [0, 44, 0, 44],
    "100-unique-cloth-pants": [0, 55, 0, 55],
    "100-unique-cloth-shoes": [0, 33, 0, 33],
    "100-unique-cloth-belt": [0, 33, 0, 33],
    "100-unique-leather-coat": [46, 46, 0, 51],
    "100-unique-leather-shoulder": [37, 37, 0, 41],
    "100-unique-leather-pants": [46, 46, 0, 51],
    "100-unique-leather-shoes": [28, 28, 0, 31],
    "100-unique-leather-belt": [28, 28, 0, 31],
    "100-unique-light-coat": [55, 37, 0, 0],
    "100-unique-light-shoulder": [44, 30, 0, 0],
    "100-unique-light-pants": [55, 37, 0, 0],
    "100-unique-light-shoes": [33, 22, 0, 0],
    "100-unique-light-belt": [33, 22, 0, 0],
    "100-unique-heavy-coat": [51, 37, 46, 0],
    "100-unique-heavy-shoulder": [41, 30, 37, 0],
    "100-unique-heavy-pants": [51, 37, 46, 0],
    "100-unique-heavy-shoes": [31, 22, 28, 0],
    "100-unique-heavy-belt": [31, 22, 28, 0],
    "100-epic-plate-coat": [149, 149, 54, 0],
    "100-epic-plate-shoulder": [139, 139, 43, 0],
    "100-epic-plate-pants": [149, 149, 54, 0],
    "100-epic-plate-shoes": [130, 130, 32, 0],
    "100-epic-plate-belt": [130, 130, 32, 0],
    "100-epic-cloth-coat": [100, 158, 0, 58],
    "100-epic-cloth-shoulder": [100, 146, 0, 46],
    "100-epic-cloth-pants": [100, 158, 0, 58],
    "100-epic-cloth-shoes": [100, 135, 0, 35],
    "100-epic-cloth-belt": [100, 135, 0, 35],
    "100-epic-leather-coat": [149, 149, 0, 54],
    "100-epic-leather-shoulder": [139, 139, 0, 43],
    "100-epic-leather-pants": [149, 149, 0, 54],
    "100-epic-leather-shoes": [130, 130, 0, 32],
    "100-epic-leather-belt": [130, 130, 0, 32],
    "100-epic-light-coat": [158, 139, 0, 0],
    "100-epic-light-shoulder": [146, 131, 0, 0],
    "100-epic-light-pants": [158, 139, 0, 0],
    "100-epic-light-shoes": [135, 123, 0, 0],
    "100-epic-light-belt": [135, 123, 0, 0],
    "100-epic-heavy-coat": [154, 139, 48, 0],
    "100-epic-heavy-shoulder": [143, 131, 39, 0],
    "100-epic-heavy-pants": [154, 139, 48, 0],
    "100-epic-heavy-shoes": [132, 123, 30, 0],
    "100-epic-heavy-belt": [132, 123, 30, 0],
    "100-epic-accessory-bracelet": [147, 100, 117, 0],
    "100-epic-accessory-necklace": [100, 147, 0, 117],
    "100-epic-accessory-ring": [171, 171, 0, 0],
    "100-epic-special-earrings": [169, 169, 69, 69],
    "100-epic-special-subequip": [146, 146, 46, 46],
    "100-epic-special-magicstones": [169, 169, 69, 69],
    "100-mythic-special-earrings": [171, 171, 70, 70],
    "95-legendary-plate-coat": [45, 45, 50, 0],
    "95-legendary-plate-shoulder": [36, 36, 40, 0],
    "95-legendary-plate-pants": [45, 45, 50, 0],
    "95-legendary-plate-shoes": [28, 28, 30, 0],
    "95-legendary-plate-belt": [28, 28, 30, 0],
    "95-legendary-cloth-coat": [0, 54, 0, 54],
    "95-legendary-cloth-shoulder": [0, 43, 0, 43],
    "95-legendary-cloth-pants": [0, 54, 0, 54],
    "95-legendary-cloth-shoes": [0, 33, 0, 33],
    "95-legendary-cloth-belt": [0, 33, 0, 33],
    "95-legendary-leather-coat": [45, 45, 0, 50],
    "95-legendary-leather-shoulder": [36, 36, 0, 40],
    "95-legendary-leather-pants": [45, 45, 0, 50],
    "95-legendary-leather-shoes": [28, 28, 0, 30],
    "95-legendary-leather-belt": [28, 28, 0, 30],
    "95-legendary-light-coat": [54, 36, 0, 0],
    "95-legendary-light-shoulder": [43, 29, 0, 0],
    "95-legendary-light-pants": [54, 36, 0, 0],
    "95-legendary-light-shoes": [33, 22, 0, 0],
    "95-legendary-light-belt": [33, 22, 0, 0],
    "95-legendary-heavy-coat": [50, 36, 45, 0],
    "95-legendary-heavy-shoulder": [40, 29, 36, 0],
    "95-legendary-heavy-pants": [50, 36, 45, 0],
    "95-legendary-heavy-shoes": [30, 22, 28, 0],
    "95-legendary-heavy-belt": [30, 22, 28, 0],
    "95-epic-plate-coat": [46, 46, 52, 0],
    "95-epic-plate-shoulder": [37, 37, 41, 0],
    "95-epic-plate-pants": [46, 46, 52, 0],
    "95-epic-plate-shoes": [28, 28, 31, 0],
    "95-epic-plate-belt": [28, 28, 31, 0],
    "95-epic-cloth-coat": [0, 56, 0, 56],
    "95-epic-cloth-shoulder": [0, 45, 0, 45],
    "95-epic-cloth-pants": [0, 56, 0, 56],
    "95-epic-cloth-shoes": [0, 33, 0, 33],
    "95-epic-cloth-belt": [0, 33, 0, 33],
    "95-epic-leather-coat": [46, 46, 0, 52],
    "95-epic-leather-shoulder": [37, 37, 0, 41],
    "95-epic-leather-pants": [46, 46, 0, 52],
    "95-epic-leather-shoes": [28, 28, 0, 31],
    "95-epic-leather-belt": [28, 28, 0, 31],
    "95-epic-light-coat": [56, 37, 0, 0],
    "95-epic-light-shoulder": [45, 30, 0, 0],
    "95-epic-light-pants": [56, 37, 0, 0],
    "95-epic-light-shoes": [33, 22, 0, 0],
    "95-epic-light-belt": [33, 22, 0, 0],
    "95-epic-heavy-coat": [52, 37, 46, 0],
    "95-epic-heavy-shoulder": [41, 30, 37, 0],
    "95-epic-heavy-pants": [52, 37, 46, 0],
    "95-epic-heavy-shoes": [31, 22, 28, 0],
    "95-epic-heavy-belt": [31, 22, 28, 0]
}
function pn(r = {}) {
    let {
        strength: s = 0,
        intelligence: a = 0,
        vitality: c = 0,
        spirit: f = 0,
        phyatk: h = 0,
        magatk: p = 0,
        indatk: m = 0,
        phycri_rate: y = 0,
        magcri_rate: S = 0,
        crirate: T = 0,
        ice_boost: C = 0,
        fire_boost: D = 0,
        light_boost: I = 0,
        dark_boost: L = 0,
        attack_speed: Q = 0,
        casting_speed: F = 0,
        movement_speed: K = 0,
        defense: ne = 0,
        phydefense: X = 0,
        magdefense: se = 0,
        fire_resistance: ae = 0,
        ice_resistance: ie = 0,
        light_resistance: J = 0,
        dark_resistance: ye = 0,
        intstr: ke = 0,
        vitspi: j = 0,
        quadra: Ke = 0,
        triatk: Le = 0,
        speed: nt = 0,
        elemental_boost: Oe = 0,
        elemental_resistance: ze = 0
    } = r
    return (
        ke > 0 && ((s = ke.plus(s)), (a = ke.plus(a))),
        Le > 0 && ((h = Le.plus(h)), (p = Le.plus(p)), (m = Le.plus(m))),
        j > 0 && ((c = j.plus(c)), (f = j.plus(f))),
        Ke > 0 && ((s = Ke.plus(s)), (a = Ke.plus(a)), (c = Ke.plus(c)), (f = Ke.plus(f))),
        Oe > 0 && ((C = Oe.plus(C)), (D = Oe.plus(D)), (I = Oe.plus(I)), (L = Oe.plus(L))),
        ze > 0 && ((ie = ze.plus(ie)), (ae = ze.plus(ae)), (J = ze.plus(J)), (ye = ze.plus(ye))),
        T > 0 && ((y = y.plus(y)), (S = S.plus(S))),
        nt > 0 && ((Q = nt.plus(Q)), (F = nt.plus(F)), (K = nt.plus(K))),
        ne > 0 && ((X = ne.plus(X)), (se = ne.plus(se))),
        {
            phyatk: h,
            magatk: p,
            indatk: m,
            strength: s,
            intelligence: a,
            vitality: c,
            spirit: f,
            phycri_rate: y,
            magcri_rate: S,
            attack_speed: Q,
            casting_speed: F,
            movement_speed: K,
            fire_boost: D,
            ice_boost: C,
            light_boost: I,
            dark_boost: L,
            ice_resistance: ie,
            fire_resistance: ae,
            light_resistance: J,
            dark_resistance: ye,
            phydefense: X,
            magdefense: se
        }
    )
}
function pi(r, s = {}) {
    const a = pn(s)
    return g5(r, a)
}
function g5(r, s) {
    var c, f
    const a = new Set(Object.keys(r).concat(Object.keys(s)))
    for (let h of a) {
        let p = (c = r[h]) != null ? c : 0,
            m = (f = s[h]) != null ? f : 0
        r[h] = p.plus(m)
    }
    return r
}
function d5({ level: r, rarity: s, type: a, part: c, status: f }) {
    const h = `${r}-${s}-${a}-${c}`,
        p = h5[h]
    let m = {}
    return !!p && p.length > 3 && (m = { strength: p[0], intelligence: p[1], vitality: p[2], spirit: p[3] }), pi(pn(m), f)
}
const m5 = [4.6, 4.6, 4.4, 4.4, 7, 7, 5, 5],
    v5 = [5, 5, 4, 4, 7.5, 7.5, 4.5, 4.5],
    y5 = [5, 5, 4, 4, 7.5, 7.5, 4.5, 4.5],
    b5 = [4.6, 4.6, 4.4, 4.4, 7, 7, 5, 5],
    w5 = [4.5, 4.5, 4.5, 4.5, 6.5, 6.5, 5.5, 5.5],
    k5 = [4.5, 4.5, 4.5, 4.5, 6.5, 6.5, 5.5, 5.5],
    A5 = [4.8, 4.8, 4.2, 4.2, 7, 7, 5, 5],
    S5 = [4, 4, 5, 5, 4.5, 4.5, 7.5, 7.5],
    x5 = [3.5, 4, 5.5, 5, 4, 4.5, 8, 7.5],
    T5 = [5, 4, 5, 4, 7.5, 5.5, 6.5, 4.5],
    E5 = [4, 4, 5, 5, 4.5, 4.5, 7.5, 7.5],
    C5 = [4.8, 4.8, 4.2, 4.2, 7.5, 7.5, 4.5, 4.5],
    B5 = [4.8, 4.8, 4.2, 4.2, 7.5, 7.5, 4.5, 4.5],
    I5 = [4.6, 4.6, 4.4, 4.4, 7, 7, 5, 5],
    R5 = [4.7, 4.8, 4, 4.5, 6.5, 6.5, 4.5, 6.5],
    D5 = [4.7, 4.8, 4, 4.5, 6.5, 6.5, 4.5, 6.5],
    L5 = [5.5, 5.5, 3.5, 3.5],
    O5 = [5, 5, 4, 4],
    q5 = [3, 4.5, 6, 4.5],
    M5 = [3.5, 3.5, 5.5, 5.5],
    P5 = [3.5, 3.5, 5.5, 5.5],
    F5 = [3.5, 3.5, 5.5, 5.5],
    W5 = [5.5, 5.5, 3.5, 3.5],
    $5 = [5.5, 5.5, 3.5, 3.5],
    N5 = [5, 4, 5, 4],
    U5 = [5, 4, 5, 4],
    K5 = [5, 6, 3.5, 3.5],
    z5 = [5, 6, 3.5, 3.5],
    G5 = [5.5, 5.5, 3.5, 3.5],
    H5 = [5, 5, 4, 4],
    V5 = [5.2, 5, 3.5, 4.3],
    Y5 = [3.5, 3.5, 5.5, 5.5],
    Z5 = [5.5, 5.5, 3.5, 3.5],
    X5 = [5.5, 5.5, 3.5, 3.5],
    J5 = [5.5, 5.5, 3.5, 3.5],
    Q5 = [5.5, 5.5, 3.5, 3.5],
    j5 = [3.5, 3.5, 5.5, 5.5],
    e2 = [3.5, 3.5, 5.5, 5.5],
    t2 = [4.7, 4.3, 4.7, 4.3],
    n2 = [4.7, 4.3, 4.7, 4.3],
    r2 = [5, 5, 4, 4],
    a2 = [3.5, 3.5, 5.5, 5.5],
    i2 = [5, 5.5, 2, 5.5],
    s2 = [5, 5, 4, 4],
    u2 = [3.5, 3.5, 5.5, 5.5],
    l2 = [3.8, 3.7, 5.3, 5.2],
    c2 = [5, 4, 5, 4],
    o2 = [4.5, 4, 5.2, 4.3],
    f2 = [4.7, 4.8, 5.5, 4.5],
    _2 = [3.5, 3.5, 5.5, 5.5],
    p2 = [3.5, 3.7, 5.5, 5.2],
    h2 = [5.5, 5.5, 3.5, 3.5],
    g2 = [5.5, 5.2, 3.5, 3.8],
    d2 = [3.5, 3.5, 5.5, 5.5],
    m2 = [5.3, 4.5, 4.2, 4],
    v2 = [4.7, 3.5, 5.3, 4.5],
    y2 = [4.5, 3.5, 5.5, 4.5],
    b2 = [5.5, 4.5, 4, 4],
    w2 = [5, 5, 4, 4],
    k2 = [3.5, 3.5, 5.5, 5.5],
    A2 = [5.5, 5.5, 3.5, 3.5],
    S2 = [3, 4.5, 6, 4.5],
    x2 = [5, 5, 4, 4],
    T2 = [5, 5, 4, 4],
    E2 = [5, 5, 4, 4],
    C2 = [3.5, 3.5, 5.5, 5.5],
    B2 = [5.5, 5.5, 3.5, 3.5],
    I2 = [5.5, 5.5, 3.5, 3.5],
    R2 = [3.5, 5.5, 3.5, 5.5],
    D2 = [3.5, 5.5, 3.5, 5.5],
    L2 = [5.2, 5, 3.9, 3.9],
    O2 = [5, 4, 5, 4],
    q2 = [3.5, 3.5, 5.5, 5.5],
    M2 = [5.2, 5, 3.5, 4.3],
    P2 = [3.5, 3.5, 5.5, 5.5],
    F2 = [3.5, 3.5, 5.5, 5.5]
var W2 = {
    demonic_lancer: m5,
    fighter_female: v5,
    fighter_male: y5,
    gunblader: b5,
    gunner_female: w5,
    gunner_male: k5,
    knight: A5,
    mage_female: S5,
    mage_male: x5,
    thief: T5,
    creator: E5,
    dark_knight: C5,
    swordman_male: B5,
    swordman_female: I5,
    priest_male: R5,
    priest_female: D5,
    vanguard: L5,
    duelist: O5,
    dragonian_lacner: q5,
    dark_lancer: M5,
    nenmaster_male: P5,
    nenmaster_female: F5,
    striker_male: W5,
    striker_female: $5,
    street_fighter_male: N5,
    street_fighter_female: U5,
    grappler_male: K5,
    grappler_female: z5,
    hitman: G5,
    agent: H5,
    trouble_shooter: V5,
    specialist: Y5,
    ranger_male: Z5,
    ranger_female: X5,
    launcher_male: J5,
    launcher_female: Q5,
    mechanic_male: j5,
    mechanic_female: e2,
    spitfire_male: t2,
    spitfire_female: n2,
    elven_knight: r2,
    chaos: a2,
    paladin: i2,
    dragon_knight: s2,
    magiciant: u2,
    summoner: l2,
    battle_mage: c2,
    witch: o2,
    enchantress: f2,
    elemental_bomber: _2,
    glacial_master: p2,
    blood_mage: h2,
    swift_master: g2,
    dimension_walker: d2,
    rogue: m2,
    necro: v2,
    kunoichi: y2,
    shadow_dancer: b2,
    weapon_master: w2,
    soul_bringer: k2,
    berserker: A2,
    asura: S2,
    sword_ghost: x2,
    blade: T2,
    sword_master: E2,
    dark_templar: C2,
    demon_slayer: B2,
    vegabond: I2,
    crusader_male: R2,
    crusader_female: D2,
    infighter: L2,
    exorcist: O2,
    avenger: q2,
    inquistor: M2,
    sorceress: P2,
    mistress: F2
}
function $2(r, s = {}) {
    var f, h
    const a = ll(s),
        c = new Set(Object.keys(r).concat(Object.keys(a)))
    for (let p of c) {
        let m = (f = r[p]) != null ? f : 0,
            y = (h = a[p]) != null ? h : 0,
            S = p.endsWith("_per") ? m.perMulti(y) : m.plus(y)
        r[p] = S
    }
}
function ll(r = {}) {
    var se, ae, ie, J, ye, ke, j, Ke, Le, nt, Oe, ze, Nn, _t, rt, Un, Ft, qe, Kn
    let s = (se = r.buff_str_per) != null ? se : 0,
        a = (ae = r.buff_int_per) != null ? ae : 0,
        c = (ie = r.buff_intstr_per) != null ? ie : 0
    c > 0 && ((s = c.perMulti(s)), (a = c.perMulti(a)))
    let f = (J = r.buff_indatk_per) != null ? J : 0,
        h = (ye = r.buff_magatk_per) != null ? ye : 0,
        p = (ke = r.buff_phyatk_per) != null ? ke : 0,
        m = (j = r.buff_triatk_per) != null ? j : 0
    m > 0 && ((f = m.perMulti(f)), (h = m.perMulti(h)), (p = m.perMulti(p)))
    let y = (Ke = r.buff_intstr_add) != null ? Ke : 0,
        S = (Le = r.buff_attack_add) != null ? Le : 0,
        T = (nt = r.buff_level_add) != null ? nt : 0,
        C = (Oe = r.awake_intstr_per) != null ? Oe : 0,
        D = (ze = r.awake_intstr_add) != null ? ze : 0,
        I = (Nn = r.awake_level_add) != null ? Nn : 0,
        L = (_t = r.passive_int_alter) != null ? _t : 0,
        Q = (rt = r.passive_int_awake) != null ? rt : 0,
        F = (Un = r.passive_alter_level_add) != null ? Un : 0,
        K = (Ft = r.passive_vitspi_alter) != null ? Ft : 0,
        ne = (qe = r.passive_vitspi_awake) != null ? qe : 0,
        X = (Kn = r.passive_alter) != null ? Kn : 0
    return (
        X > 0 && ((L = X.plus(L)), (K = X.plus(K))),
        {
            buff_str_per: s,
            buff_int_per: a,
            buff_magatk_per: h,
            buff_phyatk_per: p,
            buff_indatk_per: f,
            buff_intstr_add: y,
            buff_attack_add: S,
            buff_level_add: T,
            awake_intstr_per: C,
            awake_intstr_add: D,
            awake_level_add: I,
            passive_int_alter: L,
            passive_int_awake: Q,
            passive_vitspi_alter: K,
            passive_vitspi_awake: ne,
            passive_alter_level_add: F
        }
    )
}
function cl(r = {}) {
    let {
        skill_attack: s = 0,
        final_damage: a = 0,
        triatk_per: c = 0,
        intstr_per: f = 0,
        critical_damage: h = 0,
        additional_damage: p = 0,
        bonus_damage: m = 0,
        bonus_elemental_damage: y = 0,
        continuous_damage: S = 0,
        standalone_damage: T = 0
    } = r
    return {
        skill_attack: s,
        final_damage: a,
        triatk_per: c,
        intstr_per: f,
        critical_damage: h,
        additional_damage: p,
        bonus_damage: m,
        bonus_elemental_damage: y,
        continuous_damage: S,
        standalone_damage: T
    }
}
function N2(r, s = {}) {
    var f, h
    const a = cl(s),
        c = new Set(Object.keys(r).concat(Object.keys(a)))
    for (let p of c) {
        let m = p,
            y = (f = r[m]) != null ? f : 0,
            S = (h = a[m]) != null ? h : 0,
            T = p == "skill_attack" ? y.perMulti(S) : y.plus(S)
        ;(T = parseFloat(T.toFixed(2))), (r[m] = T)
    }
}
class De {
    constructor({ status: s, buff_entry: a, deal_entry: c, skill_entries: f } = {}) {
        Z(this, "name")
        Z(this, "fame")
        Z(this, "buff_entry")
        Z(this, "deal_entry")
        Z(this, "skill_entries", [])
        Z(this, "status")
        Z(this, "is_buffer")
        Z(this, "next")
        Z(this, "is_record")
        Z(this, "custom_comments", [])
        Z(this, "modifiers", [])
        Z(this, "disable_buffer", !1)
        Z(this, "disable_dealer", !1)
        Z(this, "listeners")
        Z(this, "waits")
        ;(this.deal_entry = c != null ? c : cl()),
            (this.buff_entry = a != null ? a : ll()),
            (this.status = s != null ? s : pn()),
            (this.is_buffer = Ue()),
            (this.is_record = !1),
            (this.listeners = new Map()),
            (this.waits = new Map()),
            (this.fame = 0)
    }
    on(s, a) {
        var h, p
        let c = (h = this.listeners.get(s)) != null ? h : new Set()
        c.add(a), this.listeners.set(s, c)
        let f = (p = this.waits.get(s)) != null ? p : []
        for (let m of f) this.emit(s, ...m)
    }
    remove(s, a) {
        const c = this.listeners.get(s)
        c && c.delete(a)
    }
    once(s, a, c = []) {
        s = s.split(":")[0]
        let h = 0
        const p = (m, ...y) => {
            c.includes(y[0]) && ++h, h == c.length && (a(m, y), this.remove(s, p))
        }
        this.on(s, p)
    }
    emit(s, ...a) {
        const c = this.listeners.get(s)
        if (c) for (let f of c) f(this, ...a)
    }
    listen(s, ...a) {
        var f
        this.emit(s, ...a)
        const c = (f = this.waits.get(s)) != null ? f : []
        c.push(a), this.waits.set(s, c)
    }
    force(s) {
        var a, c
        s && (this.changeFame(s.fame), (a = s.effect) == null || a.call(s, this), (c = s.dungeon_effect) == null || c.call(s, this))
    }
    changeFame(s = 0) {
        s && (this.fame = this.fame.plus(s))
    }
    changeStatus(s) {
        s && this.proxy(a => pi(a.status, s))
    }
    changeDamage(s) {
        this.proxy(a => N2(a.deal_entry, s))
    }
    changeBuff(s) {
        this.is_buffer && this.proxy(a => $2(a.buff_entry, s))
    }
    changeSkill(...s) {
        this.proxy(a => a.skill_entries.push(...s.map(e5)))
    }
    customComment(s) {
        this.proxy(a => a.custom_comments.push(s))
    }
    apply(s) {
        this.changeBuff(s.buff_entry),
            this.changeDamage(s.deal_entry),
            this.changeStatus(s.status),
            this.changeSkill(...s.skill_entries),
            this.changeFame(s.fame),
            this.proxy(a => a.modifiers.push(...s.modifiers))
    }
    record() {
        ;(this.is_record = !this.is_record), this.is_record && (this.next = void 0)
    }
    proxy(s) {
        let a = this
        if (this.is_record) {
            for (; a.next; ) a = a.next
            ;(a.next = new De()), s(a.next)
        }
        s(this)
    }
    toString() {
        return this.buff_entry + ""
    }
    *history() {
        var a
        let s = (a = this.next) != null ? a : this
        do yield s
        while ((s = s.next))
    }
}
const U2 = [
    { name: "siroco", ceoff: 1.35, base: 7664 },
    { name: "ozma", ceoff: 2.31, base: 4581 }
]
function ol(r, s) {
    const a = U2.find(c => c.name == r)
    if (a) {
        const { ceoff: c, base: f } = a
        return s.minus(950).multiply(c).plus(f)
    }
    return s
}
var K2 = { name: "green-sandbags(120)", magdefense: 443243, ice_resistance: 0, fire_resistance: 0, light_resistance: 0, dark_resistance: 0 }
class z2 {
    constructor({ job: s, alter: a, care_status: c = {}, skills: f = [], adapt_status: h = "intelligence", is_buffer: p, dealer_type: m, armor_type: y }) {
        Z(this, "job")
        Z(this, "alter")
        Z(this, "care_status")
        Z(this, "adapt_status")
        Z(this, "dealer_type")
        Z(this, "is_buffer")
        Z(this, "level", 100)
        Z(this, "skills")
        Z(this, "armor_type")
        ;(this.job = s),
            (this.alter = a),
            (this.care_status = c),
            (this.adapt_status = h),
            (this.dealer_type = m),
            (this.is_buffer = p),
            (this.armor_type = y),
            (this.skills = f
                .sort((S, T) => S.type - T.type)
                .map(S => ft(ot({}, S), { name: this.get_name(S.name) }))
                .map(n5))
    }
    get_name(s) {
        return [this.job, this.alter, s].join(".")
    }
    compute_buffer(s) {
        const a = this.skills,
            c = F => {
                const K = a.find(ne => {
                    let X = ne.skill.name
                    return X == F || X == this.get_name(F)
                })
                if (K) return () => h(K)
            },
            f = new Map()
        function h(F) {
            var X, se, ae
            const K = F.skill.name
            let ne = f.get(K)
            if (!ne) {
                const ie = T[F.skill.name],
                    J = (X = F.skill.effect(ie, m)) != null ? X : {},
                    ye = ((se = J == null ? void 0 : J.frames) != null ? se : []).map(ke => ({ hits: ke.hits, attack: ke.attack.multiply(ie.attack) }))
                ;(ne = ft(ot({ dungeon_status: {}, value: {} }, J), { name: F.skill.name, level: (ae = J == null ? void 0 : J.level) != null ? ae : ie.level, frames: ye })), f.set(K, ne)
            }
            return ne
        }
        const p = H2(this.job, this.alter, this.level),
            m = new De({ status: p })
        for (let F of s) m.apply(F)
        const y = t5({ skill_name: "", level: 0, adapt_number: 0, cdr: 1, attack: 1, add_level: 0, useSkill: c }, m),
            S = this.createDamage(m),
            T = {},
            C = []
        let D = pn(),
            I = 0
        for (let F of a) {
            let K = y(F)
            ;(K.adapt_number = m.status[this.adapt_status].plus(I)), (T[F.skill.name] = K)
            let { value: ne = {}, dungeon_status: X = {}, level: se, name: ae, frames: ie } = h(F)
            const J = X[this.adapt_status]
            J && (I = I.plus(J)), (ne = pn(ne)), (ne.damage = S(ie)), pi(D, ne), C.push({ name: ae, value: ne, level: se, frames: ie, dungeon_status: X })
        }
        const L = m.status[this.adapt_status].plus(I),
            Q = G2(D)
        return { context: m, final_results: D, rate: Q, skill_datas: C, adapt_number: L }
    }
    createDamage({ status: s, deal_entry: a }) {
        const { base_setting: c } = Y(),
            { ice_boost: f, fire_boost: h, light_boost: p, dark_boost: m } = s,
            {
                intstr_per: y,
                triatk_per: S,
                critical_damage: T,
                bonus_damage: C,
                bonus_elemental_damage: D,
                skill_attack: I,
                standalone_damage: L,
                final_damage: Q,
                additional_damage: F,
                continuous_damage: K
            } = a,
            { phydefense: ne, magdefense: X } = K2,
            se = this.dealer_type.startsWith("phy") ? ne : X,
            ae = se.divide(se.multiply(200))
        let [ie, J] = fl(s, this.dealer_type)
        ie = ol(c.dungeon_buff, ie)
        const ke = Math.max(f, h, p, m, 0).divide(220)
        ;(ie = ie.multiply(y.plus(1))), (J = J.multiply(S.plus(1)))
        let j = hi(ie, J)
        return (
            (j = j.minus(j.multiply(ae))),
            (j = j.multiply(1.5).multiply(T.plus(1))),
            (j = j.multiply(I.plus(1))),
            (j = j.multiply(L.plus(1))),
            (j = j.multiply(ke.plus(0.05).plus(1))),
            (j = j.multiply(F.plus(1))),
            (j = j.multiply(K.plus(1))),
            (j = j.multiply(Q.plus(1))),
            (j = j.multiply(C.plus(D.perMulti(ke)).plus(1))),
            (j = j.divide(1e3).divide(100)),
            Ke => {
                let Le = 0
                for (let nt of Ke) {
                    let { attack: Oe, hits: ze } = nt
                    Le = Le.plus(Oe.multiply(j).multiply(ze))
                }
                return Le
            }
        )
    }
}
function fl(r, s) {
    const { strength: a, intelligence: c, phyatk: f, magatk: h, indatk: p } = r
    let m = 0,
        y = 0
    switch (s) {
        case "phyatk":
            ;(m = f), (y = a)
            break
        case "magatk":
            ;(m = h), (y = c)
            break
        case "phyind":
            ;(m = p), (y = a)
            break
        case "magind":
            ;(m = p), (y = c)
            break
    }
    return [y, m]
}
function G2(r) {
    const { base_setting: s } = Y(),
        { apc: a, dungeon_buff: c } = s
    let [f, h] = fl(r, a.type)
    const p = ol(c, a.intstr),
        m = hi(a.intstr.plus(p), a.attack)
    return hi(a.intstr.plus(p).plus(f), h.plus(a.attack)).divide(m)
}
function hi(r, s) {
    return r.divide(250).plus(1).multiply(s)
}
function H2(r, s, a = 100) {
    let c = 0
    a >= 75 ? (c = 275) : a >= 15 && (c = 145)
    const f = W2,
        h = f[s],
        p = f[r],
        m = Math.min(14, a - 1),
        y = Math.max(a - 15, 0),
        S = Math.min(71, a)
    let T = p[4].plus(p[0].multiply(m).plus(h[0].multiply(y)).plus(c).plus(S.multiply(2.1))),
        C = p[5].plus(p[1].multiply(m).plus(h[1].multiply(y)).plus(c).plus(S.multiply(2))),
        D = p[6].plus(p[2].multiply(m).plus(h[2].multiply(y)).plus(c).plus(S.multiply(2.1))),
        I = p[7].plus(p[3].multiply(m).plus(h[3].multiply(y)).plus(c).plus(S.multiply(2)))
    return pn({ strength: T, intelligence: C, spirit: I, vitality: D })
}
const V2 = 74,
    Y2 = 112,
    Z2 = 131,
    X2 = 165,
    J2 = 146,
    Q2 = 79,
    j2 = 117,
    eg = 136,
    tg = 151,
    ng = 170,
    rg = 180,
    ag = 196,
    ig = 204,
    sg = 182,
    ug = 198,
    lg = 206,
    cg = 212,
    og = 220,
    fg = 228,
    _g = 56,
    pg = 93
var hg = {
    cloth_epic_coat_01: V2,
    cloth_epic_pants_01: Y2,
    cloth_epic_shoulder_01: Z2,
    cloth_epic_belt_01: X2,
    cloth_epic_shoes_01: J2,
    cloth_epic_coat_02: Q2,
    cloth_epic_pants_02: j2,
    cloth_epic_shoulder_02: eg,
    cloth_epic_shoes_02: tg,
    cloth_epic_belt_02: ng,
    accessory_epic_bracelet_01: rg,
    accessory_epic_necklace_01: ag,
    accessory_epic_ring_01: ig,
    accessory_epic_bracelet_02: sg,
    accessory_epic_necklace_02: ug,
    accessory_epic_ring_02: lg,
    special_epic_subequip_01: cg,
    special_epic_magicstones_01: og,
    special_epic_earrings_01: fg,
    weapon_epic_broom_01: _g,
    cloth_mythic_coat_01: pg
}
const gg = [
    {
        name: "cloth_mythic_coat_01",
        suit_name: "cloth_suit_01",
        part: "coat",
        rarity: "mythic",
        mythic_properties: [
            {
                deal_data: [0.01, 0.01, 0.11],
                buff_data: [0.01, 0.01, 0.11],
                effect(r, [s, a]) {
                    r.changeDamage({ bonus_damage: s }), r.changeBuff({ buff_intstr_per: a })
                }
            },
            {
                deal_data: [5, 5, 45],
                buff_data: [10, 10, 90],
                effect(r, [s, a]) {
                    r.changeSkill({ start: 1, end: s, value: 1 }), r.changeBuff({ awake_intstr_add: a })
                }
            },
            {
                deal_data: [1, 3, 12],
                buff_data: [20, 20, 200],
                effect(r, [s, a]) {
                    r.changeDamage({ intstr_per: s }), r.changeBuff({ passive_alter: a })
                }
            }
        ],
        effect(r) {
            r.changeDamage({ final_damage: 0.14, triatk_per: 0.1, intstr_per: 0.05 }),
                r.changeSkill({ start: 1, end: 45, value: 1 }),
                r.changeBuff({ passive_alter: 80, passive_vitspi_alter: 100, buff_phyatk_per: 0.25 })
        }
    }
]
var dg = [
        {
            name: "cloth_epic_coat_01",
            suit_name: "cloth_suit_01",
            type: "cloth",
            part: "coat",
            rarity: "epic",
            effect(r) {
                r.changeBuff({ buff_phyatk_per: 0.25, passive_vitspi_alter: 180, passive_int_alter: 80 }),
                    r.changeDamage({ intstr_per: 0.05, triatk_per: 0.1, final_damage: 0.12 }),
                    r.changeSkill({ start: 1, end: 45, value: 1 })
            },
            dungeon_effect(r) {}
        },
        {
            name: "cloth_epic_pants_01",
            suit_name: "cloth_suit_01",
            part: "pants",
            type: "cloth",
            modifier: ["additional_damage", 0.12],
            effect(r) {
                r.changeBuff({ buff_magatk_per: 0.25, passive_int_alter: 150, passive_vitspi_alter: 250 }),
                    r.changeDamage({ intstr_per: 0.05, triatk_per: 0.1 }),
                    r.changeSkill({ start: 1, end: 45, value: 1 })
            },
            dungeon_effect(r) {}
        },
        {
            name: "cloth_epic_shoulder_01",
            suit_name: "cloth_suit_01",
            part: "shoulder",
            type: "cloth",
            effect(r) {
                r.changeBuff({ buff_intstr_per: 0.04, passive_int_alter: 200, passive_vitspi_alter: 300 }),
                    r.changeDamage({ intstr_per: 0.05, triatk_per: 0.1, skill_attack: 0.13 }),
                    r.changeStatus({})
            },
            dungeon_effect(r) {}
        },
        {
            name: "cloth_epic_belt_01",
            suit_name: "cloth_suit_01",
            part: "belt",
            type: "cloth",
            effect(r) {
                r.changeBuff({ buff_indatk_per: 0.25, passive_int_alter: 150, passive_vitspi_alter: 250 }),
                    r.changeDamage({ intstr_per: 0.05, triatk_per: 0.1, additional_damage: 0.14 }),
                    r.changeStatus({})
            },
            dungeon_effect(r) {}
        },
        {
            name: "cloth_epic_shoes_01",
            suit_name: "cloth_suit_01",
            part: "shoes",
            type: "cloth",
            effect(r) {
                r.changeBuff({ buff_intstr_per: 0.04, passive_int_alter: 200, passive_vitspi_alter: 300 }),
                    r.changeDamage({ intstr_per: 0.05, triatk_per: 0.1, critical_damage: 0.16 }),
                    r.changeStatus({})
            },
            dungeon_effect(r) {}
        },
        {
            name: "cloth_epic_coat_02",
            suit_name: "cloth_suit_02",
            part: "coat",
            type: "cloth",
            effect(r) {
                r.changeBuff({ buff_phyatk_per: 0.25, passive_int_alter: 100, passive_vitspi_alter: 200 }),
                    r.changeDamage({ additional_damage: 0.17, critical_damage: 0.14 }),
                    r.changeSkill({ start: 1, end: 45, type: "cd_raduction", value: 0.15 })
            }
        },
        {
            name: "cloth_epic_pants_02",
            suit_name: "cloth_suit_02",
            part: "pants",
            type: "cloth",
            modifier: ["intstr_per", 0.14],
            effect(r) {
                r.changeBuff({ buff_magatk_per: 0.25, passive_int_alter: 100, passive_vitspi_alter: 200 }),
                    r.changeDamage({ final_damage: 0.17 }),
                    r.changeSkill({ start: 35, end: 45, type: "cd_raduction", value: 0.15 })
            }
        },
        {
            name: "cloth_epic_shoulder_02",
            suit_name: "cloth_suit_02",
            part: "shoulder",
            type: "cloth",
            effect(r) {
                r.changeBuff({ buff_intstr_per: 0.04, passive_int_alter: 160, passive_vitspi_alter: 260 }),
                    r.changeDamage({ bonus_damage: 0.17, skill_attack: 0.14 }),
                    r.changeSkill({ start: 75, end: 85, type: "cd_raduction", value: 0.15 })
            }
        },
        {
            name: "cloth_epic_shoes_02",
            suit_name: "cloth_suit_02",
            part: "shoes",
            type: "cloth",
            effect(r) {
                r.changeBuff({ buff_intstr_per: 0.04, passive_int_alter: 80, passive_vitspi_alter: 180 }),
                    r.changeDamage({ triatk_per: 0.17, bonus_damage: 0.14 }),
                    r.changeSkill({ start: 1, end: 30, type: "cd_raduction", value: 0.15 })
            }
        },
        {
            name: "cloth_epic_belt_02",
            suit_name: "cloth_suit_02",
            part: "belt",
            type: "cloth",
            effect(r) {
                r.changeBuff({ buff_indatk_per: 0.25, passive_int_alter: 200, passive_vitspi_alter: 100 }),
                    r.changeDamage({ intstr_per: 0.17, final_damage: 0.14 }),
                    r.changeSkill({ start: 60, end: 70, type: "cd_raduction", value: 0.15 })
            }
        },
        ...gg
    ],
    mg = [...dg],
    vg = [
        {
            name: "accessory_epic_bracelet_01",
            suit_name: "accessory_suit_01",
            type: "accessory",
            part: "bracelet",
            effect(r) {
                r.changeBuff({ buff_intstr_per: 0.08, passive_vitspi_alter: 230, passive_int_alter: 250 }),
                    r.changeDamage({ bonus_damage: 0.12, skill_attack: 0.2 }),
                    r.changeStatus({ elemental_boost: 25 })
            }
        },
        {
            name: "accessory_epic_necklace_01",
            suit_name: "accessory_suit_01",
            type: "accessory",
            part: "necklace",
            effect(r) {
                r.changeBuff({ buff_phyatk_per: 0.06, passive_vitspi_alter: 277, passive_int_alter: 151 }),
                    r.changeDamage({ triatk_per: 0.2, bonus_damage: 0.12 }),
                    r.changeStatus({ elemental_boost: 25 })
            }
        },
        {
            name: "accessory_epic_ring_01",
            suit_name: "accessory_suit_01",
            type: "accessory",
            part: "ring",
            modifier: ["intstr_per", 0.2],
            effect(r) {
                r.changeBuff({ buff_intstr_per: 0.08, passive_vitspi_alter: 266, passive_int_alter: 86 }),
                    r.changeDamage({ bonus_damage: 0.12, intstr_per: 0.2 }),
                    r.changeStatus({ elemental_boost: 25 })
            }
        },
        {
            name: "accessory_epic_bracelet_02",
            suit_name: "accessory_suit_02",
            type: "accessory",
            part: "bracelet",
            effect(r) {
                r.changeBuff({ buff_intstr_per: 0.08, passive_vitspi_alter: 220, passive_int_alter: 250 }),
                    r.changeDamage({ additional_damage: 0.07, critical_damage: 0.07, bonus_damage: 0.07, intstr_per: 0.07, skill_attack: 0.07 }),
                    r.changeStatus({})
            }
        },
        {
            name: "accessory_epic_necklace_02",
            suit_name: "accessory_suit_02",
            type: "accessory",
            part: "necklace",
            effect(r) {
                r.changeBuff({ buff_phyatk_per: 0.06, passive_vitspi_alter: 264, passive_int_alter: 151 }),
                    r.changeDamage({ additional_damage: 0.07, critical_damage: 0.07, triatk_per: 0.07, intstr_per: 0.07, final_damage: 0.07 })
            }
        },
        {
            name: "accessory_epic_ring_02",
            suit_name: "accessory_suit_02",
            type: "accessory",
            part: "ring",
            modifier: ["critical_damage", 0.07],
            effect(r) {
                r.changeBuff({ buff_intstr_per: 0.08, passive_vitspi_alter: 266, passive_int_alter: 86 }),
                    r.changeDamage({ additional_damage: 0.07, triatk_per: 0.07, intstr_per: 0.07, final_damage: 0.07 })
            }
        }
    ],
    yg = [
        {
            name: "special_epic_subequip_01",
            suit_name: "special_suit_01",
            type: "special",
            part: "subequip",
            modifier: ["additional_damage", 0.25],
            effect(r) {
                r.changeBuff({ buff_magatk_per: 0.06, passive_vitspi_alter: 208, passive_int_alter: 124, awake_level_add: 1 }), r.changeDamage({})
            }
        },
        {
            name: "special_epic_magicstones_01",
            suit_name: "special_suit_01",
            type: "special",
            part: "magicstones",
            effect(r) {
                r.changeBuff({ buff_intstr_per: 0.08, buff_level_add: 1, passive_vitspi_alter: 177, passive_int_alter: 80, awake_level_add: 1 }),
                    r.changeDamage({ critical_damage: 0.21, skill_attack: 0.17 })
            }
        },
        {
            name: "special_epic_earrings_01",
            suit_name: "special_suit_01",
            type: "special",
            part: "earrings",
            effect(r) {
                r.changeBuff({ buff_indatk_per: 0.06, passive_vitspi_alter: 176, passive_int_alter: 88 }),
                    r.once(
                        "use_equip",
                        s => {
                            console.log("use_equip:secial_epic_subequip_01"), s.changeDamage({ intstr_per: 0.1 })
                        },
                        ["special_epic_subequip_01"]
                    ),
                    r.changeDamage({ triatk_per: 0.15, final_damage: 0.17 })
            }
        }
    ],
    bg = [
        {
            name: "weapon_epic_broom_01",
            type: "broom",
            part: "weapon",
            modifier: ["awake", 0.14],
            status: { intelligence: 81, phyatk: 1229, magatk: 1352, indatk: 770 },
            effect(r) {
                r.changeBuff({ buff_intstr_per: 0.08, passive_int_alter: 152, buff_level_add: 5, buff_triatk_per: 0.12, awake_level_add: 1, awake_intstr_add: 26 }),
                    r.changeDamage({ final_damage: 0.26, skill_attack: 0.26, bonus_elemental_damage: 0.15 }),
                    r.changeStatus({})
            },
            dungeon_effect(r) {}
        }
    ],
    wg = [...bg],
    kg = [
        {
            name: "title_2021_labour_supremacy",
            icon: "/icon/title/451.png",
            part: "title",
            rarity: "legendary",
            status: { quadra: 80 },
            effect(r) {
                r.changeSkill({ start: 1, end: 35, value: 1 })
            }
        },
        {
            name: "title_2021_spring_supremacy",
            icon: "/icon/title/446.png",
            part: "title",
            status: { quadra: 100, triatk: 65, crirate: 15, elemental_boost: 22 },
            effect(r) {
                r.changeDamage({ triatk_per: 0.12, bonus_damage: 0.1 })
            }
        }
    ],
    Ag = [...mg, ...vg, ...yg, ...wg, ...kg]
const Ym = ["uncommon", "rare", "unique", "legendary", "epic", "mythic"],
    _l = { yellow: ["shoulder", "necklace"], green: ["coat", "pants"], red: ["belt", "ring"], blue: ["bracelet", "shoes"], platinum: ["subequip", "magicstones"] }
function pl(r) {
    return typeof r == "function" && (r = r()), r
}
function hl(r) {
    const s = []
    r = pl(r)
    for (let a of r) typeof a == "function" ? ((a = a), s.push(...a())) : s.push(a)
    return s
}
function Zm(r, s) {
    return Mr(s) - Mr(r)
}
function Mr(r) {
    var h
    const s = new De()
    ;(h = r.effect) == null || h.call(r, s)
    const { status: a, skill_entries: c } = s
    let f = 0
    for (let p in a) f += T3(p) * a[p]
    for (let p of c) p.type != "none" && (f += 200)
    return f
}
const gi = ["coat", "shoulder", "pants", "belt", "shoes"],
    gl = [...gi, "bracelet", "necklace", "ring", "subequip", "earrings", "magicstones", "weapon", "title"],
    di = new Map(),
    Pr = Ag.map(r => (Sg(r), (r = Fh(r)), r))
function Sg(r) {
    if ((r.rarity || (r.rarity = "epic"), r.level || (r.level = 100), !r.fame))
        switch (r.rarity) {
            case "epic":
                r.fame = 464
                break
            case "mythic":
                r.fame = 580
        }
    return r.part == "weapon", r
}
function xg(r) {
    return Pr.filter(s => (r == "all" ? !!s.suit_name : s.suit_name == r))
}
function Tg(r) {
    var s
    if (!r) return []
    if (!di.has(r)) {
        const a = Pr.filter(c => c.part == r)
        di.set(r, a)
    }
    return (s = di.get(r)) != null ? s : []
}
function Xm() {
    const r = []
    for (let s = 0; s < Pr.length; s++) {
        const a = Pr[s]
        !!a.suit_name && !r.includes(a.suit_name) && r.push(a.suit_name)
    }
    return r
}
function dl(r, s) {
    var f, h, p
    const a = new Map()
    for (let m of r)
        if (!!m && !!m.suit_name) {
            let y = (f = a.get(m.suit_name)) != null ? f : 0
            a.set(m.suit_name, y + 1)
        }
    let c = []
    for (let m of s) {
        const y = (h = a.get(m.name)) != null ? h : 0
        if (y > 0) {
            const { compatible: S } = m
            if (y >= m.needCount) c.find(T => T.compatible == m.name && T.needCount == m.needCount) || c.push(m)
            else if (S) {
                let T = (p = a.get(S)) != null ? p : 0
                a.set(S, y + T), a.delete(m.name)
            }
        }
    }
    return Array.from(c)
}
function Fr(r, s = -1) {
    const [a, c, f] = r
    return s < 0 && (s = f.minus(c).exactlyDivide(a)), c.plus(a.multiply(s))
}
function Jm(r, ...s) {
    let a = a5(r, f => (f == null ? void 0 : f.part)),
        c = _i(...a)
    return (c = c.filter(f => s.every(h => h(f)))), c
}
function Qm(r = 11) {
    return s => {
        var f
        const a = new Map()
        for (let h = 0; h < s.length; h++) {
            const p = s[h]
            if (p.suit_name) {
                let m = (f = a.get(p.suit_name)) != null ? f : 0
                a.set(p.suit_name, m + 1)
            }
        }
        const c = Array.from(a.values()).reduce((h, p) => {
            let m = 0
            return p >= 5 ? (m = 5) : p >= 3 ? (m = 3) : p >= 2 && (m = 2), (h += m), h
        }, 0)
        return a.clear(), c >= r
    }
}
function jm(r) {
    if (r) {
        if (!r.icon) {
            const s = r.name,
                a = hg
            if (!s) return ""
            let c = a[s]
            c && (r.icon = `/icon/equips/${c}.gif`)
        }
        return r.icon
    }
}
function Eg(r) {
    for (let s in _l) {
        const a = s
        if (_l[a].includes(r)) return a
    }
}
const Cg = [0, 1, 2, 3, 4, 6, 8, 10, 22, 28, 35, 42, 50, 67, 108, 150, 192, 267, 342, 417, 500, 583, 667, 750, 833, 917, 1e3, 1083, 1167, 1250, 1333, 1417],
    Bg = {
        uncommon: { rate: 45, growth: 1, base: 10 },
        rare: { rate: 45, growth: 1, base: 10 },
        unique: { rate: 45, growth: 1.3, base: 11 },
        legendary: { rate: 46, growth: 1.4, base: 11 },
        epic: { rate: 46, growth: 1.5, base: 12 },
        mythic: { rate: 46, growth: 1.6, base: 13 }
    }
class Ig {
    constructor() {
        Z(this, "name", "amplify")
    }
    effect(s, a) {
        var h
        const { item: c, data: f } = s
        if (c) {
            if (!!!f.get("has_cursed")) return
            const m = (h = f.get("upgrade_level")) != null ? h : 0,
                y = f.get("adapt_status")
            let { level: S = 100, rarity: T = "epic" } = c
            const { rate: C, growth: D, base: I } = Bg[T],
                L = S.plus(C)
                    .multiply(D)
                    .multiply(Cg[m].divide(100))
                    .plus(I)
                    .plus(m < 12 ? 0 : 5)
                    .floor(),
                Q = { [y]: L }
            a.changeStatus(Q)
        }
    }
}
function Rg(r) {
    if (r != "title") return new Ig()
}
const ml = [0, 2, 3, 4, 6, 8, 13, 18, 25, 32, 41],
    Dg = {
        uncommon: { rate: 8, growth: 0.8 },
        rare: { rate: 10, growth: 1 },
        unique: { rate: 12, growth: 1.25 },
        legendary: { rate: 13, growth: 1.35 },
        epic: { rate: 14, growth: 1.45 },
        mythic: { rate: 15, growth: 1.55 }
    }
class Lg {
    constructor() {
        Z(this, "name", "refine")
    }
    effect(s, a) {
        var p
        const { item: c, data: f } = s
        let h = (p = f.get("refine_level")) != null ? p : 0
        if (((h = Math.max(Math.min(h, 8), 0)), c)) {
            let { level: m = 100, rarity: y = "epic" } = c
            const { rate: S, growth: T } = Dg[y],
                C = m.plus(S).divide(8).multiply(T).multiply(ml[h]).round()
            if ((a.record(), a.changeStatus({ indatk: C }), Ue())) {
                const D = m.plus(S).divide(80).multiply(T).multiply(ml[h]).round()
                a.changeStatus({ quadra: D })
            }
        }
    }
}
function Og(r) {
    if (r == "weapon") return new Lg()
}
const qg = { coat: 0.3, shoulder: 0.2, pants: 0.25, belt: 0.1, shoes: 0.15 },
    Mg = { uncommon: 4, rare: 5, unique: 8, legendary: 14, epic: 17, mythic: 18 }
class Pg {
    constructor() {
        Z(this, "name", "armor_mastery")
    }
    effect(s, a) {
        var h
        const { item: c, data: f } = s
        if (c) {
            const p = (h = f.get("upgrade_level")) != null ? h : 0,
                m = f.get("adapt_status"),
                y = Ue()
            let { level: S = 100, rarity: T = "epic", part: C } = c
            const D = y ? 4 : 20,
                I = y ? 1 : 2.5
            let L = S.plus(Mg[T]).plus(p.exactlyDivide(3)).multiply(I).plus(D).multiply(qg[C])
            m != "vitality" && (L *= 2), a.changeStatus({ [m]: L })
        }
    }
}
function Fg(r) {
    if (gi.includes(r)) return new Pg()
}
function Wg(r, s) {
    const { enchant_list: a } = Y()
    for (let c of a) if (c.parts.includes(r) && s && c.name == s) return c
}
class $g {
    constructor() {
        Z(this, "name", "enchanting")
    }
    effect(s, a) {
        var p
        const { item: c, data: f, part: h } = s
        if (c) {
            const m = f.get("enchanting"),
                y = Wg(h, m)
            ;(p = y == null ? void 0 : y.effect) == null || p.call(y, a)
        }
    }
}
function vl(r) {
    return new $g()
}
function yl(r, s) {
    const { emblem_list: a } = Y()
    if (s && r)
        for (let c of a) {
            const { colors: f = [] } = c
            if (f.includes(r) && c.name == s) return c
        }
}
class Ng {
    constructor() {
        Z(this, "name", "socket")
    }
    effect(s, a) {
        var p, m
        const { item: c, data: f, part: h } = s
        if (c) {
            const y = Eg(h)
            if (y) {
                let S = (p = f.get("socket_left")) != null ? p : "all"
                const T = yl(y, S)
                if ((a.record(), a.force(T), y != "platinum")) {
                    let C = (m = f.get("socket_right")) != null ? m : "all"
                    const D = yl(y, C)
                    a.force(D)
                }
                a.record()
            }
        }
    }
}
const Ug = ["weapon", "earrings", "title"]
function Kg(r) {
    if (!Ug.includes(r)) return new Ng()
}
const zg = [1.1, 1.11]
var Gg = {
    短剑: [1.095, 1.115],
    太刀: [1.095, 1.105],
    钝器: [1.11, 1.095],
    巨剑: [1.12, 1.09],
    光剑: [1.093, 1.09],
    手套: [1.095, 1.115],
    臂铠: [1.12, 1.09],
    爪: [1.1, 1.1],
    拳套: [1.105, 1.095],
    东方棍: [1.095, 1.1],
    左轮枪: [1.087, 1.077],
    自动手枪: [1.064, 1.094],
    步枪: [1.1, 1.085],
    手炮: [1.106, 1.064],
    手弩: [1.075, 1.085],
    矛: [1.12, 1.085],
    棍棒: [1.108, 1.09],
    魔杖: [1.09, 1.11],
    法杖: [1.095, 1.12],
    broom: zg,
    十字架: [1.1, 1.095],
    念珠: [1.09, 1.115],
    图腾: [1.105, 1.09],
    镰刀: [1.105, 1.105],
    战斧: [1.12, 1.085],
    匕首: [1.09, 1.089],
    双剑: [1.102, 1.08],
    手杖: [1.081, 1.115],
    苦无: [1.09, 1.11],
    长枪: [1.105, 1.09],
    战戟: [1.12, 1.085],
    光枪: [1.095, 1.115],
    暗矛: [1.095, 1.105],
    长刀: [1.108, 1.09],
    小太刀: [1.1, 1.1],
    重剑: [1.12, 1.09],
    源力剑: [1.095, 1.115]
}
const Hg = Gg,
    Vg = [0, 83, 124, 166, 207, 248, 314, 370, 426, 482, 621, 970, 1455, 1941, 2911, 4043, 5175, 7116, 9056, 10997, 13099, 15363, 17627, 19891, 22155, 24420, 26684, 28948, 31212, 33476, 35740, 38004],
    Yg = [0, 60, 90, 120, 150, 180, 210, 247, 285, 322, 360, 675, 1013, 1350, 2025, 2813, 3600, 4950, 6300, 7650, 9113, 10688, 12263, 13838, 15413, 16988, 18563, 20138, 21713, 23288, 24863, 26438],
    Zg = [0, 2, 2.6, 3.6, 4.7, 5.8, 6.9, 8.2, 11, 14.6, 18.7, 26.9, 36.7, 43, 49.2, 55.4, 61.7, 68, 74.3, 80.6, 86.9, 93.2, 99.5, 105.8, 112.1, 118.3, 124.6, 130.9, 137.1, 143.4, 149.7, 156],
    bl = {
        uncommon: { rate: 30, growth: 0.7 },
        rare: { rate: 26, growth: 1 },
        unique: { rate: 28, growth: 1.25 },
        legendary: { rate: 29, growth: 1.35 },
        epic: { rate: 30, growth: 1.45 },
        mythic: { rate: 31, growth: 1.55 }
    },
    Xg = {
        uncommon: { rate: 8, growth: 0.7 },
        rare: { rate: 10, growth: 1 },
        unique: { rate: 12, growth: 1.25 },
        legendary: { rate: 13, growth: 1.35 },
        epic: { rate: 14, growth: 1.45 },
        mythic: { rate: 15, growth: 1.55 }
    }
function Jg(r, s, a) {
    let { level: c = 100, rarity: f = "epic" } = r
    const { rate: h, growth: p } = bl[f],
        m = c.plus(h).divide(2400).multiply(p).multiply(Vg[s])
    a.changeStatus({ triatk: m })
}
function Qg(r, s, a) {
    let { level: c = 100, rarity: f = "epic" } = r
    const { rate: h, growth: p } = bl[f],
        m = c.plus(h).divide(2400).multiply(p).multiply(Yg[s])
    a.changeStatus({ quadra: m })
}
function jg(r, s, a) {
    let { level: c = 100, rarity: f = "epic" } = r
    const { rate: h, growth: p } = Xg[f]
    let [m, y] = Hg[r.type]
    const S = c.plus(h).divide(8).multiply(p).multiply(Zg[s])
    ;(m = m.multiply(S)), (y = y.multiply(S)), a.changeStatus({ phyatk: m, magatk: y })
}
class ed {
    constructor() {
        Z(this, "name", "reinforce")
    }
    effect(s, a) {
        var p
        const { item: c, data: f } = s
        let h = (p = f.get("upgrade_level")) != null ? p : 0
        if (c)
            switch (c.part) {
                case "weapon":
                    jg(c, h, a)
                    break
                case "subequip":
                case "magicstones":
                    Qg(c, h, a)
                    break
                case "earrings":
                    Jg(c, h, a)
            }
    }
}
const td = ["weapon", "subequip", "magicstones", "earrings"]
function nd(r) {
    if (td.includes(r)) return new ed()
}
var rd = [nd, Rg, Og, Fg, vl, Kg]
const ad = [
        0, 2978, 3279, 3582, 3884, 4186, 4488, 4791, 5092, 5395, 5697, 5999, 6301, 6604, 6906, 7208, 7510, 7813, 8114, 8417, 8719, 9021, 9323, 9626, 9927, 10230, 10532, 10834, 11136, 11439, 11740,
        12043, 12345, 12647, 12949, 13252, 13553, 13856, 14158, 14459, 14762, 15065, 15366, 15668, 15971, 16272, 16575, 16877, 17179, 17481, 17784, 18085, 18388, 18690, 18992, 19294, 19597, 19898,
        20201, 20503, 20805, 21107, 21410, 21711, 22014, 22316, 22618, 22920, 23223, 23524, 23827
    ],
    id = {
        name: "keep_away",
        level_spacing: 2,
        level_master: 50,
        level_max: 60,
        need_level: 5,
        cooldown: 6,
        point: 15,
        type: V.ACTIVE,
        effect({ level: r }) {
            return { frames: [{ attack: ad[r], hits: 3 }] }
        }
    },
    sd = [
        0, 1570, 1729, 1889, 2048, 2206, 2366, 2526, 2685, 2844, 3003, 3164, 3323, 3481, 3641, 3800, 3959, 4119, 4279, 4438, 4596, 4755, 4915, 5075, 5234, 5394, 5553, 5711, 5871, 6031, 6190, 6349,
        6508, 6668, 6828, 6986, 7145, 7305, 7464, 7624, 7784
    ],
    ud = [
        0, 6280, 6918, 7555, 8191, 8830, 9468, 10105, 10741, 11379, 12016, 12654, 13290, 13928, 14565, 15201, 15839, 16478, 17115, 17751, 18389, 19026, 19663, 20300, 20938, 21575, 22211, 22849, 23486,
        24123, 24761, 25399, 26036, 26673, 27310, 27948, 28585, 29221, 29859, 30496, 31133
    ],
    ld = {
        name: "hex",
        level_spacing: 2,
        level_master: 30,
        level_max: 40,
        need_level: 60,
        cooldown: 45,
        point: 15,
        type: V.ACTIVE,
        effect({ level: r }) {
            return {
                frames: [
                    { attack: sd[r], hits: 4 },
                    { attack: ud[r], hits: 1 }
                ]
            }
        }
    }
var cd = [id, ld]
const od = [
        0, 43, 57, 74, 91, 111, 131, 153, 176, 201, 228, 255, 284, 315, 346, 379, 414, 449, 487, 526, 567, 608, 651, 696, 741, 789, 838, 888, 939, 993, 1047, 1103, 1160, 1219, 1278, 1340, 1403, 1467,
        1533, 1600, 1668
    ],
    fd = [
        0, 68740, 84680, 100619, 116559, 132498, 148440, 164380, 180320, 196259, 212200, 228140, 244079, 260019, 275958, 291898, 307839, 323778, 339718, 355658, 371597, 387537, 403479, 419419, 435358,
        451298, 467237, 483177, 499118, 515057, 530997, 546937, 562876, 578816, 594757, 610696, 626636, 642576, 658516, 674457, 690397
    ],
    wl = {
        name: "marionette",
        level_spacing: 5,
        level_master: 30,
        level_max: 40,
        need_level: 50,
        cooldown: 170,
        type: V.AWAKE,
        effect({ level: r, adapt_number: s }, { buff_entry: a }) {
            const { awake_level_add: c, awake_intstr_add: f, awake_intstr_per: h } = a
            r = r.plus(c)
            const p = s.divide(750).plus(1).multiply(h.plus(1)),
                m = od[r].plus(f).multiply(p)
            return { level: r, value: { intstr: m }, frames: [{ attack: fd[r], hits: 1 }], data: [m] }
        }
    },
    _d = [
        0, 8358, 10296, 12235, 14173, 16110, 18049, 19987, 21925, 23863, 25802, 27740, 29677, 31617, 33554, 35492, 37431, 39369, 41307, 43245, 45184, 47121, 49059, 50998, 52936, 54874, 56813, 58751,
        60689, 62626, 64565, 66503, 68441, 70380, 72318, 74256, 76195, 78132, 80070, 82008, 83947, 85885, 87823, 89762, 91700, 93637, 95576, 97514, 99452, 101390, 103329, 105267, 107204, 109143,
        111081, 113019, 114957, 116896, 118834, 120772, 122711, 124648, 126586, 128525, 130463, 132401, 134339, 136278, 138215, 140153, 142092
    ],
    pd = [
        0, 29255, 36038, 42822, 49605, 56389, 63172, 69956, 76739, 83523, 90306, 97090, 103874, 110657, 117441, 124224, 131008, 137791, 144576, 151358, 158143, 164926, 171710, 178494, 185277, 192061,
        198844, 205628, 212411, 219195, 225978, 232762, 239545, 246329, 253114, 259897, 266681, 273464, 280248, 287031, 293815, 300598, 307382, 314165, 320949, 327732, 334516, 341300, 348083, 354867,
        361650, 368435, 375217, 382002, 388785, 395569, 402352, 409136, 415920, 422703, 429487, 436270, 443054, 449837, 456621, 463404, 470188, 476971, 483755, 490540, 497323
    ],
    hd = [
        0, 78011, 96101, 114190, 132279, 150369, 168460, 186548, 204637, 222728, 240817, 258907, 276996, 295086, 313175, 331266, 349354, 367444, 385534, 403624, 421715, 439803, 457892, 475983, 494072,
        512162, 530251, 548341, 566430, 584521, 602609, 620699, 638789, 656879, 674968, 693058, 711147, 729237, 747327, 765416, 783505, 801596, 819685, 837775, 855864, 873954, 892043, 910134, 928222,
        946311, 964402, 982492, 1000581, 1018671, 1036760, 1054849, 1072940, 1091030, 1109118, 1127209, 1145298, 1163389, 1181477, 1199566, 1217657, 1235747, 1253836, 1271925, 1290015, 1308104,
        1326195
    ],
    gd = {
        name: "forest_of_dolls",
        level_spacing: 5,
        level_master: 30,
        level_max: 40,
        need_level: 85,
        cooldown: 180,
        point: 200,
        type: V.AWAKE,
        effect({ level: r }) {
            return {
                frames: [
                    { attack: _d[r], hits: 7 },
                    { attack: pd[r], hits: 2 },
                    { attack: hd[r], hits: 1 }
                ]
            }
        }
    },
    dd = [
        0, 5726, 7053, 8382, 9709, 11036, 12364, 13693, 15021, 16348, 17675, 19003, 20331, 21660, 22987, 24314, 25643, 26970, 28298, 29626, 30953, 32282, 33609, 34937, 36264, 37593, 38921, 40248,
        41576, 42903, 44232, 45560, 46887, 48215, 49542, 50871, 52198, 53527, 54854, 56181, 57510, 58837, 60166, 61493, 62821, 64149, 65476, 66803, 68132, 69461, 70788, 72115, 73442, 74771, 76100,
        77427, 78754, 80082, 81410, 82737, 84066, 85393, 86721, 88049, 89376, 90704, 92032, 93361, 94688, 96016, 97343
    ],
    md = [
        0, 22903, 28215, 33526, 38837, 44149, 49460, 54771, 60083, 65392, 70704, 76016, 81326, 86638, 91950, 97260, 102572, 107882, 113194, 118505, 123816, 129128, 134439, 139750, 145061, 150371,
        155683, 160995, 166305, 171617, 176929, 182239, 187550, 192862, 198172, 203484, 208796, 214106, 219418, 224728, 230040, 235351, 240662, 245974, 251285, 256596, 261908, 267217, 272529, 277841,
        283151, 288463, 293775, 299085, 304396, 309708, 315018, 320330, 325642, 330952, 336264, 341576, 346886, 352197, 357508, 362820, 368131, 373442, 378754, 384063, 389375
    ],
    vd = [
        0, 343566, 423233, 502902, 582569, 662237, 741904, 821572, 901240, 980908, 1060575, 1140243, 1219910, 1299579, 1379246, 1458915, 1538582, 1618250, 1697918, 1777586, 1857253, 1936921, 2016588,
        2096257, 2175924, 2255592, 2335259, 2414927, 2494595, 2574263, 2653930, 2733598, 2813265, 2892934, 2972601, 3052269, 3131936, 3211605, 3291272, 3370940, 3450607, 3530274, 3609943, 3689611,
        3769278, 3848945, 3928613, 4008282, 4087949, 4167616, 4247284, 4326951, 4406620, 4486287, 4565955, 4645622, 4725290, 4804958, 4884626, 4964293, 5043961, 5123629, 5203297, 5282964, 5362632,
        5442299, 5521968, 5601635, 5681303, 5760970, 5840638
    ],
    yd = {
        name: "curtain_call",
        level_spacing: 5,
        level_master: 30,
        level_max: 40,
        need_level: 100,
        cooldown: 290,
        type: V.AWAKE,
        effect({ level: r, useSkill: s }) {
            var f, h
            const a = s(wl.name),
                c = r.multiply(0.01).plus(0.23)
            if (a) {
                let p = (h = (f = a().value) == null ? void 0 : f.intstr) != null ? h : 0
                return (
                    (p = p.multiply(c)),
                    {
                        value: { intstr: p },
                        frames: [
                            { attack: dd[r], hits: 8 },
                            { attack: md[r], hits: 3 },
                            { attack: vd[r], hits: 1 }
                        ]
                    }
                )
            }
        }
    }
var bd = [wl, gd, yd]
const wd = [
        966, 1065, 1161, 1260, 1359, 1457, 1554, 1652, 1751, 1850, 1946, 2045, 2144, 2241, 2340, 2436, 2535, 2634, 2732, 2829, 2927, 3026, 3124, 3221, 3320, 3418, 3516, 3615, 3711, 3810, 3909, 4006,
        4104, 4202, 4301, 4399, 4497, 4595, 4693, 4791, 4890, 4986, 5085, 5184, 5281, 5379, 5477, 5575, 5674, 5772, 5869, 5968, 6066, 6165, 6261, 6360, 6459, 6556, 6655, 6751, 6850, 6949, 7047, 7144,
        7243, 7341
    ],
    kd = {
        name: "madd_slash",
        need_level: 15,
        level_spacing: 2,
        level_master: 50,
        level_max: 60,
        type: V.ACTIVE,
        cooldown: 4,
        point: 20,
        effect({ level: r }) {
            return { frames: [{ attack: wd[r], hits: 2 }] }
        }
    },
    Ad = [
        3223, 3551, 3879, 4204, 4532, 4859, 5187, 5512, 5840, 6168, 6495, 6821, 7148, 7476, 7804, 8130, 8457, 8784, 9112, 9438, 9765, 10093, 10420, 10747, 11073, 11401, 11729, 12055, 12382, 12709,
        13037, 13363, 13690, 14018, 14345, 14672, 14998, 15326, 15654, 15980, 16307, 16634, 16962, 17289, 17616, 17943, 18270, 18597, 18925, 19251, 19579, 19905, 20233, 20560, 20887, 21214, 21541,
        21868, 22195, 22522, 22850, 23176, 23504, 23830, 24158, 24485, 24812, 25139, 25466, 25793
    ],
    Sd = {
        name: "madd_straight",
        need_level: 20,
        level_spacing: 2,
        level_master: 50,
        level_max: 60,
        cooldown: 7,
        point: 20,
        type: V.ACTIVE,
        effect({ level: r }) {
            return { frames: [{ attack: Ad[r], hits: 1 }] }
        }
    },
    xd = [
        0, 5362, 5907, 6451, 6996, 7539, 8083, 8628, 9172, 9715, 10259, 10804, 11349, 11892, 12436, 12980, 13526, 14069, 14613, 15157, 15700, 16245, 16790, 17334, 17877, 18421, 18966, 19511, 20054,
        20598, 21142, 21687, 22230, 22774, 23319, 23863, 24407, 24951, 25495, 26040, 26584, 27128, 27672, 28216, 28759, 29305, 29849, 30393, 30936, 31480, 32026, 32570, 33113, 33657, 34201, 34747,
        35289, 35834, 36378, 36921, 37466
    ],
    Td = {
        name: "madd_guard",
        need_level: 30,
        level_spacing: 2,
        level_master: 50,
        level_max: 60,
        cooldown: 10,
        point: 20,
        type: V.ACTIVE,
        effect({ level: r }) {
            return { frames: [{ attack: xd[r], hits: 5 }] }
        }
    },
    Ed = [
        0, 8399, 9250, 10103, 10955, 11807, 12658, 13511, 14363, 15214, 16068, 16919, 17773, 18624, 19475, 20329, 21180, 22032, 22884, 23736, 24588, 25440, 26293, 27144, 27998, 28849, 29700, 30553,
        31405, 32258, 33109, 33961, 34814, 35665, 36518, 37370, 38222, 39074, 39926, 40778, 41630, 42483, 43334, 44186, 45039, 45890, 46744, 47595, 48446, 49300, 50151, 51003, 51856, 52708, 53559,
        54412, 55264, 56115, 56969, 57820, 58671, 59525, 60376, 61228, 62081, 62933, 63785, 64637, 65489, 66341, 67194
    ],
    Cd = {
        name: "jumping_bear_press",
        need_level: 35,
        level_spacing: 2,
        level_master: 50,
        level_max: 60,
        cooldown: 15,
        point: 40,
        type: V.ACTIVE,
        effect({ level: r }) {
            return { frames: [{ attack: Ed[r], hits: 5 }] }
        }
    },
    Bd = [
        0, 2615, 2880, 3146, 3412, 3677, 3943, 4207, 4473, 4738, 5004, 5269, 5535, 5799, 6065, 6330, 6596, 6861, 7127, 7391, 7657, 7923, 8188, 8454, 8719, 8984, 9249, 9515, 9780, 10046, 10311, 10576,
        10841, 11107, 11373, 11638, 11904, 12168, 12434, 12699, 12965, 13230, 13496, 13760, 14026, 14291, 14557, 14823, 15088, 15354, 15618, 15884, 16149, 16415, 16680, 16946, 17210, 17476, 17741,
        18007, 18272, 18538, 18802, 19068, 19334, 19599, 19865, 20130, 20395, 20660, 20926
    ],
    Id = [
        0, 10463, 11524, 12585, 13647, 14708, 15771, 16832, 17893, 18955, 20016, 21077, 22139, 23200, 24262, 25324, 26385, 27446, 28508, 29569, 30630, 31693, 32754, 33815, 34877, 35938, 36999, 38061,
        39123, 40185, 41246, 42307, 43369, 44430, 45491, 46554, 47615, 48676, 49738, 50799, 51860, 52922, 53984, 55045, 56107, 57168, 58229, 59291, 60352, 61415, 62476, 63537, 64599, 65660, 66721,
        67783, 68844, 69906, 70968, 72029, 73090, 74152, 75213, 76274, 77337, 78398, 79459, 80521, 81582, 82643, 83705
    ],
    Rd = {
        name: "big_madd",
        need_level: 45,
        level_spacing: 2,
        level_master: 50,
        level_max: 60,
        cooldown: 45,
        point: 60,
        type: V.ACTIVE,
        effect({ level: r }) {
            return {
                frames: [
                    { attack: Bd[r], hits: 6 },
                    { attack: Id[r], hits: 1 }
                ]
            }
        }
    },
    Dd = [
        0, 3465, 3817, 4168, 4519, 4871, 5223, 5574, 5926, 6278, 6629, 6981, 7333, 7684, 8035, 8387, 8738, 9090, 9443, 9794, 10146, 10497, 10847, 11199, 11550, 11903, 12255, 12606, 12958, 13309,
        13661, 14011, 14364, 14716, 15067, 15419, 15770, 16122, 16473, 16826, 17177
    ],
    Ld = {
        name: "terrible_roar",
        need_level: 70,
        level_spacing: 2,
        level_master: 30,
        level_max: 40,
        cooldown: 40,
        point: 60,
        type: V.ACTIVE,
        effect({ level: r }) {
            return { frames: [{ attack: Dd[r], hits: 10 }] }
        }
    },
    Od = [
        0, 2618, 2883, 3149, 3414, 3680, 3945, 4211, 4476, 4743, 5008, 5274, 5539, 5805, 6070, 6336, 6601, 6868, 7133, 7399, 7664, 7930, 8195, 8461, 8726, 8991, 9258, 9523, 9789, 10054, 10320, 10585,
        10851, 11116, 11383, 11648, 11914, 12179, 12445, 12710, 12976, 13241, 13508, 13773, 14039, 14304, 14570, 14835, 15101, 15366, 15631, 15898, 16163, 16429, 16694, 16960, 17225, 17491, 17756,
        18023, 18288, 18554, 18819, 19085, 19350, 19616, 19881, 20148, 20413, 20679, 20944
    ],
    qd = {
        name: "curse_of_terror_bear",
        need_level: 80,
        level_spacing: 2,
        level_master: 30,
        level_max: 40,
        cooldown: 45,
        point: 60,
        type: V.ACTIVE,
        effect({ level: r }) {
            return { frames: [{ attack: Od[r], hits: 24 }] }
        }
    }
var kl = [kd, Sd, Td, Cd, Rd, Ld, qd]
const Md = [
        0, 131, 140, 149, 158, 167, 175, 184, 193, 202, 211, 220, 229, 238, 247, 256, 264, 273, 282, 291, 300, 309, 318, 327, 336, 345, 353, 362, 371, 380, 389, 398, 407, 416, 425, 434, 442, 451, 460,
        469, 478
    ],
    Pd = [0, 34, 35, 37, 38, 39, 41, 42, 43, 45, 46, 47, 49, 50, 51, 53, 54, 55, 57, 58, 60, 61, 62, 64, 65, 66, 68, 69, 70, 72, 73, 74, 76, 77, 78, 80, 81, 82, 84, 85, 87],
    Al = {
        name: "forbidden_curse",
        level_spacing: 3,
        level_master: 10,
        level_max: 40,
        need_level: 30,
        point: 30,
        cooldown: 10,
        type: V.BUFF,
        effect({ adapt_number: r = 0, level: s, useSkill: a }, c) {
            const { buff_entry: f } = c,
                { buff_level_add: h, buff_indatk_per: p, buff_magatk_per: m, buff_phyatk_per: y, buff_attack_add: S, buff_intstr_add: T, buff_int_per: C, buff_str_per: D } = f
            s = s.plus(h)
            const I = Pd[s].plus(S),
                L = Md[s].plus(T)
            let Q = r.divide(665).plus(1)
            a(mi.name) && (Q = Q.multiply(1.15))
            const K = I.multiply(p.plus(1)).multiply(Q).round(2),
                ne = I.multiply(y.plus(1)).multiply(Q).round(2),
                X = I.multiply(m.plus(1)).multiply(Q).round(2),
                se = L.multiply(D.plus(1)).multiply(Q).round(2),
                ae = L.multiply(C.plus(1)).multiply(Q).round(2)
            return c.changeSkill({ start: 15, end: -1, type: "attack_add", value: 0.7 }), { level: s, value: { intelligence: ae, strength: se, phyatk: ne, magatk: X, indatk: K } }
        }
    },
    Fd = kl.map(r => `mage_female.enchantress.${r.name}`),
    mi = {
        name: "favoritism",
        level_spacing: 3,
        level_master: 20,
        level_max: 40,
        need_level: 20,
        point: 10,
        cooldown: 10,
        type: V.BUFF,
        effect({ useSkill: r }, s) {
            let a = {}
            const c = 0.15
            return s.changeSkill({ condition: { names: Fd }, type: "attack_add", value: 0.1 }), { value: a, data: [c] }
        }
    },
    Wd = [
        0, 13968, 16195, 18422, 20649, 22876, 25103, 27329, 29556, 31784, 34011, 36238, 38465, 40691, 42918, 45145, 47372, 49599, 51825, 54052, 56280, 58507, 60734, 62961, 65187, 67414, 69641, 71868,
        74095, 76321, 78548, 80776, 83003, 85230, 87457, 89683, 91910, 94137, 96364, 98591, 100817, 103044, 105272, 107499, 109726, 111953, 114179, 116406, 118633, 120860, 123087, 125313, 127540,
        129768, 131995, 134222, 136449, 138675, 140902, 143129, 145356, 147583, 149809, 152036, 154264, 156491, 158718, 160945, 163171, 165398, 167625
    ],
    $d = {
        name: "destiny_puppet",
        level_spacing: 3,
        level_master: 50,
        level_max: 60,
        need_level: 35,
        cooldown: 7,
        point: 40,
        type: V.BUFF,
        effect({ useSkill: r, level: s }) {
            var h, p
            const a = r(Al.name),
                c = 0.25
            let f = {}
            if (a) {
                f = (h = a().value) != null ? h : {}
                const m = r(mi.name)
                if (m) {
                    const y = (p = m().value) != null ? p : {}
                    f = al(f, (S, T) => {
                        if (S) {
                            const C = y[T]
                            return C && (S = S.plus(C)), S
                        }
                    })
                }
            }
            return (
                (f = al(f, m => {
                    if (m) return m.multiply(c).round(3)
                })),
                { value: f, data: [c], frames: [{ attack: Wd[s], hits: 1 }] }
            )
        }
    },
    Nd = [
        0, 1193, 1383, 1573, 1763, 1953, 2145, 2335, 2525, 2715, 2905, 3095, 3285, 3476, 3666, 3856, 4046, 4236, 4426, 4618, 4808, 4998, 5188, 5378, 5568, 5758, 5948, 6139, 6329, 6519, 6709, 6900,
        7090, 7281, 7471, 7661, 7851, 8041, 8231, 8421, 8611
    ],
    Ud = {
        name: "hot_affection",
        need_level: 25,
        level_spacing: 3,
        level_master: 30,
        level_max: 40,
        cooldown: 0,
        point: 15,
        type: V.BUFF,
        effect({ level: r }, s) {
            return s.changeStatus({ speed: r == 0 ? 0 : r.multiply(0.5).plus(13) }), { frames: [{ attack: Nd[r], hits: 3 }] }
        }
    }
var Kd = [Al, Ud, mi, $d]
const zd = [
        0, 69, 73, 77, 81, 85, 90, 95, 100, 106, 112, 118, 124, 130, 137, 144, 152, 160, 168, 176, 184, 193, 202, 212, 221, 231, 241, 252, 262, 273, 284, 296, 308, 320, 332, 344, 358, 371, 384, 398,
        412, 426, 440, 456, 470, 486, 502, 518, 534, 550, 567, 581, 597, 613, 629, 645, 660, 676, 692, 708, 724
    ],
    Gd = {
        name: "puppeteer",
        level_spacing: 3,
        level_master: 50,
        level_max: 60,
        need_level: 15,
        point: 15,
        type: V.PASSIVE,
        effect({ level: r }, s) {
            const { buff_entry: a } = s,
                { passive_int_alter: c, passive_alter_level_add: f } = a
            r = r.plus(f)
            const h = zd[r].plus(c)
            return s.changeStatus({ intelligence: h }), { level: r }
        }
    },
    Hd = {
        name: "wicked_curiosity",
        need_level: 20,
        level_spacing: 3,
        level_master: 10,
        level_max: 60,
        type: V.PASSIVE,
        effect({ level: r }, s) {
            r > 1 && s.changeSkill({ start: 15, end: -1, type: "attack_add", value: (0.1).plus((0.015).multiply(r)) })
        }
    },
    Vd = [
        0, 14, 37, 59, 82, 104, 127, 149, 172, 194, 217, 239, 262, 284, 307, 329, 352, 374, 397, 419, 442, 464, 487, 509, 532, 554, 577, 599, 622, 644, 667, 689, 712, 734, 757, 779, 802, 824, 847,
        869, 892
    ],
    Yd = {
        name: "petite_diablo",
        level_spacing: 3,
        level_master: 30,
        level_max: 40,
        need_level: 48,
        type: V.PASSIVE,
        effect({ level: r }, { buff_entry: s }) {
            const { passive_int_awake: a } = s,
                c = Vd[r].plus(a)
            return { value: { intstr: c }, dungeon_status: { intelligence: c } }
        }
    },
    Zd = [
        0, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290, 300, 310, 320, 330, 340, 350, 360, 370, 380, 390, 400, 410, 420, 430, 440, 450, 460, 470, 480, 490, 500, 510, 520, 530,
        540, 550
    ],
    Xd = {
        name: "ominously_smiling",
        level_spacing: 3,
        level_master: 30,
        level_max: 40,
        need_level: 95,
        type: V.PASSIVE,
        effect({ level: r }, s) {
            const a = Zd[r]
            s.changeStatus({ intelligence: a })
        }
    },
    Jd = [
        0, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290, 300, 310, 320, 330, 340, 350, 360, 370, 380, 390, 400, 410, 420, 430, 440, 450, 460, 470, 480, 490, 500, 510, 520,
        530, 540
    ],
    Qd = {
        name: "dark_rose",
        level_spacing: 3,
        level_master: 30,
        level_max: 40,
        need_level: 75,
        type: V.PASSIVE,
        effect({ level: r }, s) {
            const a = Jd[r]
            s.changeStatus({ intelligence: a })
        }
    }
var jd = [Gd, Hd, Yd, Qd, Xd]
const e3 = [
        0, 650, 715, 781, 848, 913, 980, 1046, 1111, 1177, 1243, 1310, 1375, 1441, 1508, 1573, 1640, 1706, 1770, 1837, 1903, 1970, 2035, 2101, 2168, 2233, 2300, 2366, 2430, 2497, 2563, 2629, 2695,
        2761, 2827, 2893, 2960, 3024, 3090, 3157, 3222, 3289, 3355, 3420, 3487, 3553, 3620, 3684, 3750, 3817, 3882, 3949, 4015, 4080, 4147, 4213, 4279, 4344, 4410, 4476, 4542
    ],
    t3 = [
        0, 85, 93, 102, 110, 119, 128, 136, 145, 153, 162, 170, 179, 188, 196, 205, 213, 222, 230, 238, 247, 255, 264, 272, 281, 289, 298, 307, 315, 324, 332, 341, 349, 358, 367, 375, 384, 393, 402,
        411, 419, 428, 436, 445, 453, 462, 471, 479, 488, 496, 505, 513, 522, 531, 539, 548, 556, 565, 574, 582, 591
    ],
    n3 = {
        name: "rose_vines",
        level_spacing: 2,
        level_master: 50,
        level_max: 60,
        need_level: 10,
        cooldown: 6,
        type: V.ACTIVE,
        point: 15,
        effect({ level: r }) {
            return {
                frames: [
                    { attack: e3[r], hits: 4 },
                    { attack: t3[r], hits: 4 }
                ]
            }
        }
    },
    r3 = [
        0, 2414, 2798, 3182, 3567, 3951, 4337, 4722, 5107, 5491, 5876, 6261, 6645, 7030, 7415, 7801, 8185, 8570, 8955, 9339, 9724, 10109, 10494, 10878, 11264, 11649, 12034, 12418, 12803, 13188, 13572,
        13957, 14342, 14728, 15112, 15497, 15882, 16266, 16651, 17036, 17421, 17805, 18191, 18576, 18960, 19345, 19730, 20115, 20499, 20883, 21268, 21652, 22038, 22423, 22808, 23192, 23577, 23962,
        24346, 24731, 25116
    ],
    a3 = {
        name: "harvest",
        need_level: 20,
        level_spacing: 2,
        level_master: 50,
        level_max: 60,
        cooldown: 10,
        type: V.ACTIVE,
        effect({ level: r }) {
            return { frames: [{ attack: r3[r], hits: 2 }] }
        }
    },
    i3 = [
        0, 843, 928, 1015, 1100, 1186, 1271, 1357, 1442, 1527, 1614, 1699, 1785, 1870, 1955, 2042, 2127, 2213, 2298, 2384, 2469, 2555, 2641, 2726, 2812, 2897, 2984, 3069, 3154, 3240, 3325, 3411, 3497,
        3583, 3668, 3753, 3839, 3924, 4011, 4096, 4182, 4267, 4352, 4439, 4524, 4610, 4695, 4781, 4866, 4951, 5038, 5123, 5209, 5294, 5379, 5466, 5551, 5637, 5722, 5808, 5893
    ],
    s3 = [
        0, 6328, 6969, 7611, 8254, 8896, 9538, 10179, 10821, 11464, 12106, 12748, 13389, 14031, 14673, 15316, 15958, 16600, 17241, 17883, 18526, 19168, 19810, 20451, 21093, 21735, 22378, 23020, 23661,
        24303, 24945, 25588, 26230, 26871, 27513, 28155, 28797, 29440, 30082, 30723, 31365, 32007, 32650, 33292, 33933, 34575, 35217, 35860, 36502, 37143, 37785, 38427, 39069, 39712, 40354, 40995,
        41637, 42279, 42922, 43564, 44205
    ],
    u3 = [
        0, 114, 126, 138, 149, 161, 173, 184, 196, 208, 219, 231, 243, 254, 266, 278, 289, 301, 313, 324, 337, 348, 359, 372, 383, 394, 407, 418, 429, 442, 453, 464, 477, 488, 499, 512, 523, 534, 547,
        558, 569, 582, 593, 604, 617, 628, 639, 652, 663, 674, 687, 698, 709, 722, 733, 744, 757, 768, 779, 792, 803
    ],
    l3 = {
        name: "bramble_prison",
        need_level: 40,
        level_spacing: 2,
        level_master: 50,
        level_max: 60,
        cooldown: 20,
        type: V.ACTIVE,
        effect({ level: r }) {
            return {
                frames: [
                    { attack: i3[r], hits: 6 },
                    { attack: s3[r], hits: 1 },
                    { attack: u3[r], hits: 7 }
                ]
            }
        }
    },
    c3 = [
        0, 1493, 1645, 1796, 1948, 2100, 2251, 2403, 2554, 2706, 2857, 3009, 3160, 3312, 3464, 3615, 3767, 3918, 4070, 4221, 4373, 4524, 4676, 4828, 4979, 5131, 5282, 5434, 5585, 5737, 5888, 6040,
        6192, 6343, 6495, 6646, 6798, 6949, 7101, 7252, 7404, 7556, 7707, 7859, 8010, 8162, 8313, 8465, 8616, 8768, 8920, 9071, 9223, 9374, 9526, 9677, 9829, 9980, 10132, 10284, 10435, 10587, 10738,
        10890, 11041, 11193, 11344, 11496, 11648, 11799, 11951
    ],
    o3 = {
        name: "black_forest_brier",
        need_level: 45,
        level_spacing: 2,
        level_master: 1,
        level_max: 11,
        cooldown: 20,
        type: V.ACTIVE,
        effect({ level: r }) {
            return { frames: [{ attack: c3[r], hits: 4 }] }
        }
    },
    f3 = [
        0, 3140, 3459, 3778, 4096, 4414, 4733, 5053, 5371, 5690, 6009, 6326, 6645, 6964, 7283, 7601, 7920, 8238, 8558, 8876, 9195, 9514, 9833, 10150, 10469, 10788, 11106, 11425, 11744, 12061, 12381,
        12700, 13019, 13338, 13656
    ],
    _3 = [
        0, 698, 769, 839, 909, 980, 1051, 1123, 1194, 1265, 1334, 1405, 1476, 1548, 1618, 1689, 1760, 1830, 1901, 1971, 2043, 2114, 2185, 2255, 2325, 2396, 2468, 2539, 2610, 2680, 2750, 2821, 2893,
        2964, 3034
    ],
    p3 = [
        0, 28268, 31136, 34004, 36873, 39739, 42606, 45475, 48343, 51210, 54078, 56946, 59814, 62683, 65549, 68418, 71285, 74154, 77020, 79889, 82756, 85625, 88493, 91360, 94228, 97096, 99964, 102833,
        105699, 108566, 111435, 114303, 117170, 120038, 122906
    ],
    h3 = {
        name: "garden_of_pain",
        need_level: 75,
        level_spacing: 2,
        level_master: 30,
        level_max: 40,
        cooldown: 40,
        type: V.ACTIVE,
        effect({ level: r }) {
            return {
                frames: [
                    { attack: f3[r], hits: 9 },
                    { attack: _3[r], hits: 10 },
                    { attack: p3[r], hits: 1 }
                ]
            }
        }
    }
var g3 = [n3, a3, l3, o3, h3],
    d3 = [...jd, ...Kd, ...cd, ...kl, ...g3, ...bd]
const m3 = {
        job: "mage_female",
        alter: "enchantress",
        armor_type: "plate",
        dealer_type: "magind",
        adapt_status: "intelligence",
        care_status: { intelligence: 4, indatk: 0.5, magcri_rate: 0.5, ice_boost: 0.1, fire_boost: 0.1, light_boost: 0.1, dark_boost: 0.1, elemental_boost: 0.1 },
        is_buffer: !0,
        skills: d3
    },
    Sl = { enchantress: m3 }
function v3() {
    const r = vl()
    return { part: "pet", slots: r ? [r] : [], data: new Map() }
}
var y3 = [
    ...vi(
        "red",
        ["strength", "intelligence", "vitality", "spirit"],
        [
            { rarity: "legendary", value: 35 },
            { rarity: "unique", value: 25 },
            { rarity: "rare", value: 17 }
        ]
    ),
    ...b3(
        ["red", "green"],
        [
            ["strength", "phycri_rate"],
            ["intelligence", "magcri_rate"]
        ],
        [
            { rarity: "legendary", values: [20, 2.2] },
            { rarity: "unique", values: [15, 1.5] },
            { rarity: "rare", values: [10, 1.1] }
        ]
    ),
    ...vi(
        "blue",
        ["phyatk", "magatk", "indatk"],
        [
            { rarity: "legendary", value: 20 },
            { rarity: "unique", value: 15 },
            { rarity: "rare", value: 10 }
        ]
    ),
    ...vi(
        "yellow",
        ["strength", "intelligence"],
        [
            { rarity: "unique", value: 15 },
            { rarity: "rare", value: 10 }
        ]
    ),
    () => {
        const { character: r } = Y()
        if (r) {
            const { skills: s } = r
            return s
                .map(a => a.skill)
                .filter(a => a.need_level <= 45)
                .map(a => ({
                    name: `[{$skills.${a.name}}]{skill} Lv+1`,
                    colors: ["platinum"],
                    rarity: "legendary",
                    fame: 232,
                    effect(c) {
                        c.record(), c.changeStatus({ quadra: 8 }), c.changeSkill({ condition: { names: [a.name] }, value: 1 }), c.record()
                    }
                }))
        }
        return []
    }
]
function xl(r) {
    switch (r) {
        case "legendary":
            return 46
        case "unique":
            return 36
        case "rare":
            return 30
    }
    return 0
}
function vi(r, s, a) {
    const c = []
    for (let f of s)
        for (let { rarity: h, value: p } of a) {
            let m = xl(h)
            const y = {
                name: `emblem_${h}_${r}_${f}`,
                rarity: h,
                fame: m,
                colors: [r],
                part: "",
                effect(S) {
                    S.changeStatus({ [f]: p })
                }
            }
            c.push(y)
        }
    return c
}
function b3(r, s, a) {
    const c = []
    for (let f of s)
        for (let { rarity: h, values: p } of a) {
            let m = xl(h)
            const y = {
                name: `emblem_${h}_dual_${f.join("_")}`,
                rarity: h,
                fame: m,
                colors: r,
                part: "",
                effect(S) {
                    const T = {}
                    for (let C = 0; C < f.length; C++) T[f[C]] = p[C]
                    S.changeStatus(T)
                }
            }
            c.push(y)
        }
    return c
}
var w3 = [
    {
        name: "cpw_powatk_4070",
        parts: ["coat", "pants", "weapon"],
        tags: [""],
        fame: 130,
        effect(r) {
            r.changeStatus({ intstr: 40, triatk: 70 })
        }
    },
    {
        name: "cpw_intelligence_100",
        parts: ["coat", "pants", "weapon"],
        tags: [""],
        fame: 130,
        effect(r) {
            r.changeStatus({ intelligence: 100 })
        }
    },
    {
        name: "cpw_vitspi_50",
        parts: ["coat", "pants", "weapon"],
        tags: [""],
        fame: 130,
        effect(r) {
            Ue() ? r.changeStatus({ vitspi: 50 }) : r.changeStatus({ intstr: 40, triatk: 70 })
        }
    },
    {
        name: "cpw_intstr_75",
        fame: 108,
        parts: ["coat", "pants", "weapon"],
        effect(r) {
            Ue() ? r.changeStatus({ intelligence: 75 }) : r.changeStatus({ intstr: 20, triatk: 50 })
        }
    },
    {
        name: "accessory_quadra_70",
        fame: 130,
        parts: ["bracelet", "necklace", "ring"],
        effect(r) {
            Ue() ? r.changeStatus({ quadra: 70 }) : r.changeStatus({ elemental_boost: 30 })
        }
    },
    {
        name: "accessory_quadra_50",
        fame: 108,
        parts: ["bracelet", "necklace", "ring"],
        effect(r) {
            Ue() ? r.changeStatus({ quadra: 50 }) : r.changeStatus({ elemental_boost: 25 })
        }
    },
    {
        name: "subequip_quadra_50",
        fame: 158,
        parts: ["subequip"],
        effect(r) {
            Ue() ? r.changeStatus({ quadra: 100 }) : r.changeStatus({ triatk: 80 })
        }
    },
    {
        name: "magicstones_quadra_50",
        fame: 158,
        parts: ["magicstones"],
        effect(r) {
            Ue() ? r.changeStatus({ quadra: 100 }) : r.changeStatus({ elemental_boost: 30 })
        }
    },
    {
        name: "earrings_quadra_150",
        fame: 130,
        parts: ["earrings"],
        effect(r) {
            r.changeStatus({ quadra: 150 })
        }
    },
    {
        name: "earrings_quadra_125",
        fame: 108,
        parts: ["earrings"],
        effect(r) {
            r.changeStatus({ quadra: 125 })
        }
    },
    {
        name: "shoulder_skill_level",
        fame: 168,
        parts: ["shoulder"],
        effect(r) {
            r.changeSkill({ start: 1, end: 50, value: 1, condition: { type: V.ACTIVE } }), r.changeStatus({ quadra: 75, triatk: 20 })
        }
    },
    {
        name: "shoulder_skill_attack",
        fame: 168,
        parts: ["shoulder"],
        effect(r) {
            r.changeDamage({ skill_attack: 0.03 }), r.changeStatus({ quadra: 75, triatk: 20 })
        }
    },
    {
        name: "skill_level_add_50",
        fame: 108,
        parts: ["shoulder", "belt", "shoes"],
        effect(r) {
            r.changeSkill({ start: 1, end: 50, value: 1, condition: { type: V.ACTIVE } })
        }
    },
    {
        name: "skill_level_add_30",
        fame: 78,
        parts: ["shoulder", "belt", "shoes"],
        effect(r) {
            r.changeSkill({ start: 1, end: 30, value: 1, condition: { type: V.ACTIVE } })
        }
    },
    {
        name: "autumn_skill_add",
        fame: 168,
        parts: ["belt", "shoes"],
        effect(r) {
            r.changeSkill({ start: 1, end: 50, value: 1, condition: { type: V.ACTIVE } })
            const s = Ue() ? { quadra: 45 } : { triatk: 36 }
            r.changeStatus(s)
        }
    },
    () => {
        const { character: r } = Y()
        if (r) {
            const { skills: s } = r,
                a = s.map(f => f.skill).filter(f => f.need_level <= 85),
                c = function (f = 2) {
                    return h => ({
                        name: `title_skill_${h.name}_add_${f}`,
                        fame: h.need_level == 50 && f == 2 ? 370 : 130,
                        parts: ["title"],
                        effect(p) {
                            p.changeSkill({ condition: { names: [h.name] }, value: f })
                        }
                    })
                }
            return [...a.map(c(2)), ...a.map(c(1))]
        }
        return []
    },
    ...k3(["pet"], ["strength", "intelligence"], [{ fame: 158, value: 50 }])
]
function k3(r, s, a) {
    const c = []
    for (let f of s)
        for (let { fame: h, value: p } of a) {
            const m = {
                name: `${r.map(y => y.charAt(0)).join("")}_${f}_${p}`,
                part: "enchant",
                fame: h,
                parts: r,
                effect(y) {
                    y.changeStatus({ [f]: p })
                }
            }
            c.push(m)
        }
    return c
}
var Pn = typeof globalThis != "undefined" ? globalThis : typeof window != "undefined" ? window : typeof global != "undefined" ? global : typeof self != "undefined" ? self : {},
    yi = { exports: {} }
/**
 * @license
 * Lodash <https://lodash.com/>
 * Copyright OpenJS Foundation and other contributors <https://openjsf.org/>
 * Released under MIT license <https://lodash.com/license>
 * Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
 * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 */ ;(function (r, s) {
    ;(function () {
        var a,
            c = "4.17.21",
            f = 200,
            h = "Unsupported core-js use. Try https://npms.io/search?q=ponyfill.",
            p = "Expected a function",
            m = "Invalid `variable` option passed into `_.template`",
            y = "__lodash_hash_undefined__",
            S = 500,
            T = "__lodash_placeholder__",
            C = 1,
            D = 2,
            I = 4,
            L = 1,
            Q = 2,
            F = 1,
            K = 2,
            ne = 4,
            X = 8,
            se = 16,
            ae = 32,
            ie = 64,
            J = 128,
            ye = 256,
            ke = 512,
            j = 30,
            Ke = "...",
            Le = 800,
            nt = 16,
            Oe = 1,
            ze = 2,
            Nn = 3,
            _t = 1 / 0,
            rt = 9007199254740991,
            Un = 17976931348623157e292,
            Ft = 0 / 0,
            qe = 4294967295,
            Kn = qe - 1,
            zl = qe >>> 1,
            Gl = [
                ["ary", J],
                ["bind", F],
                ["bindKey", K],
                ["curry", X],
                ["curryRight", se],
                ["flip", ke],
                ["partial", ae],
                ["partialRight", ie],
                ["rearg", ye]
            ],
            Yt = "[object Arguments]",
            zn = "[object Array]",
            Hl = "[object AsyncFunction]",
            hn = "[object Boolean]",
            gn = "[object Date]",
            Vl = "[object DOMException]",
            Gn = "[object Error]",
            Hn = "[object Function]",
            ki = "[object GeneratorFunction]",
            at = "[object Map]",
            dn = "[object Number]",
            Yl = "[object Null]",
            mt = "[object Object]",
            Ai = "[object Promise]",
            Zl = "[object Proxy]",
            mn = "[object RegExp]",
            it = "[object Set]",
            vn = "[object String]",
            Vn = "[object Symbol]",
            Xl = "[object Undefined]",
            yn = "[object WeakMap]",
            Jl = "[object WeakSet]",
            bn = "[object ArrayBuffer]",
            Zt = "[object DataView]",
            Nr = "[object Float32Array]",
            Ur = "[object Float64Array]",
            Kr = "[object Int8Array]",
            zr = "[object Int16Array]",
            Gr = "[object Int32Array]",
            Hr = "[object Uint8Array]",
            Vr = "[object Uint8ClampedArray]",
            Yr = "[object Uint16Array]",
            Zr = "[object Uint32Array]",
            Ql = /\b__p \+= '';/g,
            jl = /\b(__p \+=) '' \+/g,
            ec = /(__e\(.*?\)|\b__t\)) \+\n'';/g,
            Si = /&(?:amp|lt|gt|quot|#39);/g,
            xi = /[&<>"']/g,
            tc = RegExp(Si.source),
            nc = RegExp(xi.source),
            rc = /<%-([\s\S]+?)%>/g,
            ac = /<%([\s\S]+?)%>/g,
            Ti = /<%=([\s\S]+?)%>/g,
            ic = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
            sc = /^\w*$/,
            uc = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g,
            Xr = /[\\^$.*+?()[\]{}|]/g,
            lc = RegExp(Xr.source),
            Jr = /^\s+/,
            cc = /\s/,
            oc = /\{(?:\n\/\* \[wrapped with .+\] \*\/)?\n?/,
            fc = /\{\n\/\* \[wrapped with (.+)\] \*/,
            _c = /,? & /,
            pc = /[^\x00-\x2f\x3a-\x40\x5b-\x60\x7b-\x7f]+/g,
            hc = /[()=,{}\[\]\/\s]/,
            gc = /\\(\\)?/g,
            dc = /\$\{([^\\}]*(?:\\.[^\\}]*)*)\}/g,
            Ei = /\w*$/,
            mc = /^[-+]0x[0-9a-f]+$/i,
            vc = /^0b[01]+$/i,
            yc = /^\[object .+?Constructor\]$/,
            bc = /^0o[0-7]+$/i,
            wc = /^(?:0|[1-9]\d*)$/,
            kc = /[\xc0-\xd6\xd8-\xf6\xf8-\xff\u0100-\u017f]/g,
            Yn = /($^)/,
            Ac = /['\n\r\u2028\u2029\\]/g,
            Zn = "\\ud800-\\udfff",
            Sc = "\\u0300-\\u036f",
            xc = "\\ufe20-\\ufe2f",
            Tc = "\\u20d0-\\u20ff",
            Ci = Sc + xc + Tc,
            Bi = "\\u2700-\\u27bf",
            Ii = "a-z\\xdf-\\xf6\\xf8-\\xff",
            Ec = "\\xac\\xb1\\xd7\\xf7",
            Cc = "\\x00-\\x2f\\x3a-\\x40\\x5b-\\x60\\x7b-\\xbf",
            Bc = "\\u2000-\\u206f",
            Ic = " \\t\\x0b\\f\\xa0\\ufeff\\n\\r\\u2028\\u2029\\u1680\\u180e\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200a\\u202f\\u205f\\u3000",
            Ri = "A-Z\\xc0-\\xd6\\xd8-\\xde",
            Di = "\\ufe0e\\ufe0f",
            Li = Ec + Cc + Bc + Ic,
            Qr = "['\u2019]",
            Rc = "[" + Zn + "]",
            Oi = "[" + Li + "]",
            Xn = "[" + Ci + "]",
            qi = "\\d+",
            Dc = "[" + Bi + "]",
            Mi = "[" + Ii + "]",
            Pi = "[^" + Zn + Li + qi + Bi + Ii + Ri + "]",
            jr = "\\ud83c[\\udffb-\\udfff]",
            Lc = "(?:" + Xn + "|" + jr + ")",
            Fi = "[^" + Zn + "]",
            ea = "(?:\\ud83c[\\udde6-\\uddff]){2}",
            ta = "[\\ud800-\\udbff][\\udc00-\\udfff]",
            Xt = "[" + Ri + "]",
            Wi = "\\u200d",
            $i = "(?:" + Mi + "|" + Pi + ")",
            Oc = "(?:" + Xt + "|" + Pi + ")",
            Ni = "(?:" + Qr + "(?:d|ll|m|re|s|t|ve))?",
            Ui = "(?:" + Qr + "(?:D|LL|M|RE|S|T|VE))?",
            Ki = Lc + "?",
            zi = "[" + Di + "]?",
            qc = "(?:" + Wi + "(?:" + [Fi, ea, ta].join("|") + ")" + zi + Ki + ")*",
            Mc = "\\d*(?:1st|2nd|3rd|(?![123])\\dth)(?=\\b|[A-Z_])",
            Pc = "\\d*(?:1ST|2ND|3RD|(?![123])\\dTH)(?=\\b|[a-z_])",
            Gi = zi + Ki + qc,
            Fc = "(?:" + [Dc, ea, ta].join("|") + ")" + Gi,
            Wc = "(?:" + [Fi + Xn + "?", Xn, ea, ta, Rc].join("|") + ")",
            $c = RegExp(Qr, "g"),
            Nc = RegExp(Xn, "g"),
            na = RegExp(jr + "(?=" + jr + ")|" + Wc + Gi, "g"),
            Uc = RegExp(
                [
                    Xt + "?" + Mi + "+" + Ni + "(?=" + [Oi, Xt, "$"].join("|") + ")",
                    Oc + "+" + Ui + "(?=" + [Oi, Xt + $i, "$"].join("|") + ")",
                    Xt + "?" + $i + "+" + Ni,
                    Xt + "+" + Ui,
                    Pc,
                    Mc,
                    qi,
                    Fc
                ].join("|"),
                "g"
            ),
            Kc = RegExp("[" + Wi + Zn + Ci + Di + "]"),
            zc = /[a-z][A-Z]|[A-Z]{2}[a-z]|[0-9][a-zA-Z]|[a-zA-Z][0-9]|[^a-zA-Z0-9 ]/,
            Gc = [
                "Array",
                "Buffer",
                "DataView",
                "Date",
                "Error",
                "Float32Array",
                "Float64Array",
                "Function",
                "Int8Array",
                "Int16Array",
                "Int32Array",
                "Map",
                "Math",
                "Object",
                "Promise",
                "RegExp",
                "Set",
                "String",
                "Symbol",
                "TypeError",
                "Uint8Array",
                "Uint8ClampedArray",
                "Uint16Array",
                "Uint32Array",
                "WeakMap",
                "_",
                "clearTimeout",
                "isFinite",
                "parseInt",
                "setTimeout"
            ],
            Hc = -1,
            ce = {}
        ;(ce[Nr] = ce[Ur] = ce[Kr] = ce[zr] = ce[Gr] = ce[Hr] = ce[Vr] = ce[Yr] = ce[Zr] = !0),
            (ce[Yt] = ce[zn] = ce[bn] = ce[hn] = ce[Zt] = ce[gn] = ce[Gn] = ce[Hn] = ce[at] = ce[dn] = ce[mt] = ce[mn] = ce[it] = ce[vn] = ce[yn] = !1)
        var le = {}
        ;(le[Yt] =
            le[zn] =
            le[bn] =
            le[Zt] =
            le[hn] =
            le[gn] =
            le[Nr] =
            le[Ur] =
            le[Kr] =
            le[zr] =
            le[Gr] =
            le[at] =
            le[dn] =
            le[mt] =
            le[mn] =
            le[it] =
            le[vn] =
            le[Vn] =
            le[Hr] =
            le[Vr] =
            le[Yr] =
            le[Zr] =
                !0),
            (le[Gn] = le[Hn] = le[yn] = !1)
        var Vc = {
                À: "A",
                Á: "A",
                Â: "A",
                Ã: "A",
                Ä: "A",
                Å: "A",
                à: "a",
                á: "a",
                â: "a",
                ã: "a",
                ä: "a",
                å: "a",
                Ç: "C",
                ç: "c",
                Ð: "D",
                ð: "d",
                È: "E",
                É: "E",
                Ê: "E",
                Ë: "E",
                è: "e",
                é: "e",
                ê: "e",
                ë: "e",
                Ì: "I",
                Í: "I",
                Î: "I",
                Ï: "I",
                ì: "i",
                í: "i",
                î: "i",
                ï: "i",
                Ñ: "N",
                ñ: "n",
                Ò: "O",
                Ó: "O",
                Ô: "O",
                Õ: "O",
                Ö: "O",
                Ø: "O",
                ò: "o",
                ó: "o",
                ô: "o",
                õ: "o",
                ö: "o",
                ø: "o",
                Ù: "U",
                Ú: "U",
                Û: "U",
                Ü: "U",
                ù: "u",
                ú: "u",
                û: "u",
                ü: "u",
                Ý: "Y",
                ý: "y",
                ÿ: "y",
                Æ: "Ae",
                æ: "ae",
                Þ: "Th",
                þ: "th",
                ß: "ss",
                Ā: "A",
                Ă: "A",
                Ą: "A",
                ā: "a",
                ă: "a",
                ą: "a",
                Ć: "C",
                Ĉ: "C",
                Ċ: "C",
                Č: "C",
                ć: "c",
                ĉ: "c",
                ċ: "c",
                č: "c",
                Ď: "D",
                Đ: "D",
                ď: "d",
                đ: "d",
                Ē: "E",
                Ĕ: "E",
                Ė: "E",
                Ę: "E",
                Ě: "E",
                ē: "e",
                ĕ: "e",
                ė: "e",
                ę: "e",
                ě: "e",
                Ĝ: "G",
                Ğ: "G",
                Ġ: "G",
                Ģ: "G",
                ĝ: "g",
                ğ: "g",
                ġ: "g",
                ģ: "g",
                Ĥ: "H",
                Ħ: "H",
                ĥ: "h",
                ħ: "h",
                Ĩ: "I",
                Ī: "I",
                Ĭ: "I",
                Į: "I",
                İ: "I",
                ĩ: "i",
                ī: "i",
                ĭ: "i",
                į: "i",
                ı: "i",
                Ĵ: "J",
                ĵ: "j",
                Ķ: "K",
                ķ: "k",
                ĸ: "k",
                Ĺ: "L",
                Ļ: "L",
                Ľ: "L",
                Ŀ: "L",
                Ł: "L",
                ĺ: "l",
                ļ: "l",
                ľ: "l",
                ŀ: "l",
                ł: "l",
                Ń: "N",
                Ņ: "N",
                Ň: "N",
                Ŋ: "N",
                ń: "n",
                ņ: "n",
                ň: "n",
                ŋ: "n",
                Ō: "O",
                Ŏ: "O",
                Ő: "O",
                ō: "o",
                ŏ: "o",
                ő: "o",
                Ŕ: "R",
                Ŗ: "R",
                Ř: "R",
                ŕ: "r",
                ŗ: "r",
                ř: "r",
                Ś: "S",
                Ŝ: "S",
                Ş: "S",
                Š: "S",
                ś: "s",
                ŝ: "s",
                ş: "s",
                š: "s",
                Ţ: "T",
                Ť: "T",
                Ŧ: "T",
                ţ: "t",
                ť: "t",
                ŧ: "t",
                Ũ: "U",
                Ū: "U",
                Ŭ: "U",
                Ů: "U",
                Ű: "U",
                Ų: "U",
                ũ: "u",
                ū: "u",
                ŭ: "u",
                ů: "u",
                ű: "u",
                ų: "u",
                Ŵ: "W",
                ŵ: "w",
                Ŷ: "Y",
                ŷ: "y",
                Ÿ: "Y",
                Ź: "Z",
                Ż: "Z",
                Ž: "Z",
                ź: "z",
                ż: "z",
                ž: "z",
                Ĳ: "IJ",
                ĳ: "ij",
                Œ: "Oe",
                œ: "oe",
                ŉ: "'n",
                ſ: "s"
            },
            Yc = { "&": "&amp;", "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#39;" },
            Zc = { "&amp;": "&", "&lt;": "<", "&gt;": ">", "&quot;": '"', "&#39;": "'" },
            Xc = { "\\": "\\", "'": "'", "\n": "n", "\r": "r", "\u2028": "u2028", "\u2029": "u2029" },
            Jc = parseFloat,
            Qc = parseInt,
            Hi = typeof Pn == "object" && Pn && Pn.Object === Object && Pn,
            jc = typeof self == "object" && self && self.Object === Object && self,
            be = Hi || jc || Function("return this")(),
            ra = s && !s.nodeType && s,
            Wt = ra && !0 && r && !r.nodeType && r,
            Vi = Wt && Wt.exports === ra,
            aa = Vi && Hi.process,
            Ge = (function () {
                try {
                    var d = Wt && Wt.require && Wt.require("util").types
                    return d || (aa && aa.binding && aa.binding("util"))
                } catch {}
            })(),
            Yi = Ge && Ge.isArrayBuffer,
            Zi = Ge && Ge.isDate,
            Xi = Ge && Ge.isMap,
            Ji = Ge && Ge.isRegExp,
            Qi = Ge && Ge.isSet,
            ji = Ge && Ge.isTypedArray
        function Me(d, w, b) {
            switch (b.length) {
                case 0:
                    return d.call(w)
                case 1:
                    return d.call(w, b[0])
                case 2:
                    return d.call(w, b[0], b[1])
                case 3:
                    return d.call(w, b[0], b[1], b[2])
            }
            return d.apply(w, b)
        }
        function eo(d, w, b, B) {
            for (var P = -1, ee = d == null ? 0 : d.length; ++P < ee; ) {
                var de = d[P]
                w(B, de, b(de), d)
            }
            return B
        }
        function He(d, w) {
            for (var b = -1, B = d == null ? 0 : d.length; ++b < B && w(d[b], b, d) !== !1; );
            return d
        }
        function to(d, w) {
            for (var b = d == null ? 0 : d.length; b-- && w(d[b], b, d) !== !1; );
            return d
        }
        function es(d, w) {
            for (var b = -1, B = d == null ? 0 : d.length; ++b < B; ) if (!w(d[b], b, d)) return !1
            return !0
        }
        function Et(d, w) {
            for (var b = -1, B = d == null ? 0 : d.length, P = 0, ee = []; ++b < B; ) {
                var de = d[b]
                w(de, b, d) && (ee[P++] = de)
            }
            return ee
        }
        function Jn(d, w) {
            var b = d == null ? 0 : d.length
            return !!b && Jt(d, w, 0) > -1
        }
        function ia(d, w, b) {
            for (var B = -1, P = d == null ? 0 : d.length; ++B < P; ) if (b(w, d[B])) return !0
            return !1
        }
        function oe(d, w) {
            for (var b = -1, B = d == null ? 0 : d.length, P = Array(B); ++b < B; ) P[b] = w(d[b], b, d)
            return P
        }
        function Ct(d, w) {
            for (var b = -1, B = w.length, P = d.length; ++b < B; ) d[P + b] = w[b]
            return d
        }
        function sa(d, w, b, B) {
            var P = -1,
                ee = d == null ? 0 : d.length
            for (B && ee && (b = d[++P]); ++P < ee; ) b = w(b, d[P], P, d)
            return b
        }
        function no(d, w, b, B) {
            var P = d == null ? 0 : d.length
            for (B && P && (b = d[--P]); P--; ) b = w(b, d[P], P, d)
            return b
        }
        function ua(d, w) {
            for (var b = -1, B = d == null ? 0 : d.length; ++b < B; ) if (w(d[b], b, d)) return !0
            return !1
        }
        var ro = la("length")
        function ao(d) {
            return d.split("")
        }
        function io(d) {
            return d.match(pc) || []
        }
        function ts(d, w, b) {
            var B
            return (
                b(d, function (P, ee, de) {
                    if (w(P, ee, de)) return (B = ee), !1
                }),
                B
            )
        }
        function Qn(d, w, b, B) {
            for (var P = d.length, ee = b + (B ? 1 : -1); B ? ee-- : ++ee < P; ) if (w(d[ee], ee, d)) return ee
            return -1
        }
        function Jt(d, w, b) {
            return w === w ? vo(d, w, b) : Qn(d, ns, b)
        }
        function so(d, w, b, B) {
            for (var P = b - 1, ee = d.length; ++P < ee; ) if (B(d[P], w)) return P
            return -1
        }
        function ns(d) {
            return d !== d
        }
        function rs(d, w) {
            var b = d == null ? 0 : d.length
            return b ? oa(d, w) / b : Ft
        }
        function la(d) {
            return function (w) {
                return w == null ? a : w[d]
            }
        }
        function ca(d) {
            return function (w) {
                return d == null ? a : d[w]
            }
        }
        function as(d, w, b, B, P) {
            return (
                P(d, function (ee, de, ue) {
                    b = B ? ((B = !1), ee) : w(b, ee, de, ue)
                }),
                b
            )
        }
        function uo(d, w) {
            var b = d.length
            for (d.sort(w); b--; ) d[b] = d[b].value
            return d
        }
        function oa(d, w) {
            for (var b, B = -1, P = d.length; ++B < P; ) {
                var ee = w(d[B])
                ee !== a && (b = b === a ? ee : b + ee)
            }
            return b
        }
        function fa(d, w) {
            for (var b = -1, B = Array(d); ++b < d; ) B[b] = w(b)
            return B
        }
        function lo(d, w) {
            return oe(w, function (b) {
                return [b, d[b]]
            })
        }
        function is(d) {
            return d && d.slice(0, cs(d) + 1).replace(Jr, "")
        }
        function Pe(d) {
            return function (w) {
                return d(w)
            }
        }
        function _a(d, w) {
            return oe(w, function (b) {
                return d[b]
            })
        }
        function wn(d, w) {
            return d.has(w)
        }
        function ss(d, w) {
            for (var b = -1, B = d.length; ++b < B && Jt(w, d[b], 0) > -1; );
            return b
        }
        function us(d, w) {
            for (var b = d.length; b-- && Jt(w, d[b], 0) > -1; );
            return b
        }
        function co(d, w) {
            for (var b = d.length, B = 0; b--; ) d[b] === w && ++B
            return B
        }
        var oo = ca(Vc),
            fo = ca(Yc)
        function _o(d) {
            return "\\" + Xc[d]
        }
        function po(d, w) {
            return d == null ? a : d[w]
        }
        function Qt(d) {
            return Kc.test(d)
        }
        function ho(d) {
            return zc.test(d)
        }
        function go(d) {
            for (var w, b = []; !(w = d.next()).done; ) b.push(w.value)
            return b
        }
        function pa(d) {
            var w = -1,
                b = Array(d.size)
            return (
                d.forEach(function (B, P) {
                    b[++w] = [P, B]
                }),
                b
            )
        }
        function ls(d, w) {
            return function (b) {
                return d(w(b))
            }
        }
        function Bt(d, w) {
            for (var b = -1, B = d.length, P = 0, ee = []; ++b < B; ) {
                var de = d[b]
                ;(de === w || de === T) && ((d[b] = T), (ee[P++] = b))
            }
            return ee
        }
        function jn(d) {
            var w = -1,
                b = Array(d.size)
            return (
                d.forEach(function (B) {
                    b[++w] = B
                }),
                b
            )
        }
        function mo(d) {
            var w = -1,
                b = Array(d.size)
            return (
                d.forEach(function (B) {
                    b[++w] = [B, B]
                }),
                b
            )
        }
        function vo(d, w, b) {
            for (var B = b - 1, P = d.length; ++B < P; ) if (d[B] === w) return B
            return -1
        }
        function yo(d, w, b) {
            for (var B = b + 1; B--; ) if (d[B] === w) return B
            return B
        }
        function jt(d) {
            return Qt(d) ? wo(d) : ro(d)
        }
        function st(d) {
            return Qt(d) ? ko(d) : ao(d)
        }
        function cs(d) {
            for (var w = d.length; w-- && cc.test(d.charAt(w)); );
            return w
        }
        var bo = ca(Zc)
        function wo(d) {
            for (var w = (na.lastIndex = 0); na.test(d); ) ++w
            return w
        }
        function ko(d) {
            return d.match(na) || []
        }
        function Ao(d) {
            return d.match(Uc) || []
        }
        var So = function d(w) {
                w = w == null ? be : en.defaults(be.Object(), w, en.pick(be, Gc))
                var b = w.Array,
                    B = w.Date,
                    P = w.Error,
                    ee = w.Function,
                    de = w.Math,
                    ue = w.Object,
                    ha = w.RegExp,
                    xo = w.String,
                    Ve = w.TypeError,
                    er = b.prototype,
                    To = ee.prototype,
                    tn = ue.prototype,
                    tr = w["__core-js_shared__"],
                    nr = To.toString,
                    re = tn.hasOwnProperty,
                    Eo = 0,
                    os = (function () {
                        var e = /[^.]+$/.exec((tr && tr.keys && tr.keys.IE_PROTO) || "")
                        return e ? "Symbol(src)_1." + e : ""
                    })(),
                    rr = tn.toString,
                    Co = nr.call(ue),
                    Bo = be._,
                    Io = ha(
                        "^" +
                            nr
                                .call(re)
                                .replace(Xr, "\\$&")
                                .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, "$1.*?") +
                            "$"
                    ),
                    ar = Vi ? w.Buffer : a,
                    It = w.Symbol,
                    ir = w.Uint8Array,
                    fs = ar ? ar.allocUnsafe : a,
                    sr = ls(ue.getPrototypeOf, ue),
                    _s = ue.create,
                    ps = tn.propertyIsEnumerable,
                    ur = er.splice,
                    hs = It ? It.isConcatSpreadable : a,
                    kn = It ? It.iterator : a,
                    $t = It ? It.toStringTag : a,
                    lr = (function () {
                        try {
                            var e = Gt(ue, "defineProperty")
                            return e({}, "", {}), e
                        } catch {}
                    })(),
                    Ro = w.clearTimeout !== be.clearTimeout && w.clearTimeout,
                    Do = B && B.now !== be.Date.now && B.now,
                    Lo = w.setTimeout !== be.setTimeout && w.setTimeout,
                    cr = de.ceil,
                    or = de.floor,
                    ga = ue.getOwnPropertySymbols,
                    Oo = ar ? ar.isBuffer : a,
                    gs = w.isFinite,
                    qo = er.join,
                    Mo = ls(ue.keys, ue),
                    me = de.max,
                    Ae = de.min,
                    Po = B.now,
                    Fo = w.parseInt,
                    ds = de.random,
                    Wo = er.reverse,
                    da = Gt(w, "DataView"),
                    An = Gt(w, "Map"),
                    ma = Gt(w, "Promise"),
                    nn = Gt(w, "Set"),
                    Sn = Gt(w, "WeakMap"),
                    xn = Gt(ue, "create"),
                    fr = Sn && new Sn(),
                    rn = {},
                    $o = Ht(da),
                    No = Ht(An),
                    Uo = Ht(ma),
                    Ko = Ht(nn),
                    zo = Ht(Sn),
                    _r = It ? It.prototype : a,
                    Tn = _r ? _r.valueOf : a,
                    ms = _r ? _r.toString : a
                function l(e) {
                    if (_e(e) && !W(e) && !(e instanceof G)) {
                        if (e instanceof Ye) return e
                        if (re.call(e, "__wrapped__")) return vu(e)
                    }
                    return new Ye(e)
                }
                var an = (function () {
                    function e() {}
                    return function (t) {
                        if (!fe(t)) return {}
                        if (_s) return _s(t)
                        e.prototype = t
                        var n = new e()
                        return (e.prototype = a), n
                    }
                })()
                function pr() {}
                function Ye(e, t) {
                    ;(this.__wrapped__ = e), (this.__actions__ = []), (this.__chain__ = !!t), (this.__index__ = 0), (this.__values__ = a)
                }
                ;(l.templateSettings = { escape: rc, evaluate: ac, interpolate: Ti, variable: "", imports: { _: l } }),
                    (l.prototype = pr.prototype),
                    (l.prototype.constructor = l),
                    (Ye.prototype = an(pr.prototype)),
                    (Ye.prototype.constructor = Ye)
                function G(e) {
                    ;(this.__wrapped__ = e), (this.__actions__ = []), (this.__dir__ = 1), (this.__filtered__ = !1), (this.__iteratees__ = []), (this.__takeCount__ = qe), (this.__views__ = [])
                }
                function Go() {
                    var e = new G(this.__wrapped__)
                    return (
                        (e.__actions__ = Ce(this.__actions__)),
                        (e.__dir__ = this.__dir__),
                        (e.__filtered__ = this.__filtered__),
                        (e.__iteratees__ = Ce(this.__iteratees__)),
                        (e.__takeCount__ = this.__takeCount__),
                        (e.__views__ = Ce(this.__views__)),
                        e
                    )
                }
                function Ho() {
                    if (this.__filtered__) {
                        var e = new G(this)
                        ;(e.__dir__ = -1), (e.__filtered__ = !0)
                    } else (e = this.clone()), (e.__dir__ *= -1)
                    return e
                }
                function Vo() {
                    var e = this.__wrapped__.value(),
                        t = this.__dir__,
                        n = W(e),
                        i = t < 0,
                        u = n ? e.length : 0,
                        o = s0(0, u, this.__views__),
                        _ = o.start,
                        g = o.end,
                        v = g - _,
                        k = i ? g : _ - 1,
                        A = this.__iteratees__,
                        x = A.length,
                        E = 0,
                        R = Ae(v, this.__takeCount__)
                    if (!n || (!i && u == v && R == v)) return Ns(e, this.__actions__)
                    var q = []
                    e: for (; v-- && E < R; ) {
                        k += t
                        for (var N = -1, M = e[k]; ++N < x; ) {
                            var z = A[N],
                                H = z.iteratee,
                                $e = z.type,
                                Ee = H(M)
                            if ($e == ze) M = Ee
                            else if (!Ee) {
                                if ($e == Oe) continue e
                                break e
                            }
                        }
                        q[E++] = M
                    }
                    return q
                }
                ;(G.prototype = an(pr.prototype)), (G.prototype.constructor = G)
                function Nt(e) {
                    var t = -1,
                        n = e == null ? 0 : e.length
                    for (this.clear(); ++t < n; ) {
                        var i = e[t]
                        this.set(i[0], i[1])
                    }
                }
                function Yo() {
                    ;(this.__data__ = xn ? xn(null) : {}), (this.size = 0)
                }
                function Zo(e) {
                    var t = this.has(e) && delete this.__data__[e]
                    return (this.size -= t ? 1 : 0), t
                }
                function Xo(e) {
                    var t = this.__data__
                    if (xn) {
                        var n = t[e]
                        return n === y ? a : n
                    }
                    return re.call(t, e) ? t[e] : a
                }
                function Jo(e) {
                    var t = this.__data__
                    return xn ? t[e] !== a : re.call(t, e)
                }
                function Qo(e, t) {
                    var n = this.__data__
                    return (this.size += this.has(e) ? 0 : 1), (n[e] = xn && t === a ? y : t), this
                }
                ;(Nt.prototype.clear = Yo), (Nt.prototype.delete = Zo), (Nt.prototype.get = Xo), (Nt.prototype.has = Jo), (Nt.prototype.set = Qo)
                function vt(e) {
                    var t = -1,
                        n = e == null ? 0 : e.length
                    for (this.clear(); ++t < n; ) {
                        var i = e[t]
                        this.set(i[0], i[1])
                    }
                }
                function jo() {
                    ;(this.__data__ = []), (this.size = 0)
                }
                function ef(e) {
                    var t = this.__data__,
                        n = hr(t, e)
                    if (n < 0) return !1
                    var i = t.length - 1
                    return n == i ? t.pop() : ur.call(t, n, 1), --this.size, !0
                }
                function tf(e) {
                    var t = this.__data__,
                        n = hr(t, e)
                    return n < 0 ? a : t[n][1]
                }
                function nf(e) {
                    return hr(this.__data__, e) > -1
                }
                function rf(e, t) {
                    var n = this.__data__,
                        i = hr(n, e)
                    return i < 0 ? (++this.size, n.push([e, t])) : (n[i][1] = t), this
                }
                ;(vt.prototype.clear = jo), (vt.prototype.delete = ef), (vt.prototype.get = tf), (vt.prototype.has = nf), (vt.prototype.set = rf)
                function yt(e) {
                    var t = -1,
                        n = e == null ? 0 : e.length
                    for (this.clear(); ++t < n; ) {
                        var i = e[t]
                        this.set(i[0], i[1])
                    }
                }
                function af() {
                    ;(this.size = 0), (this.__data__ = { hash: new Nt(), map: new (An || vt)(), string: new Nt() })
                }
                function sf(e) {
                    var t = Tr(this, e).delete(e)
                    return (this.size -= t ? 1 : 0), t
                }
                function uf(e) {
                    return Tr(this, e).get(e)
                }
                function lf(e) {
                    return Tr(this, e).has(e)
                }
                function cf(e, t) {
                    var n = Tr(this, e),
                        i = n.size
                    return n.set(e, t), (this.size += n.size == i ? 0 : 1), this
                }
                ;(yt.prototype.clear = af), (yt.prototype.delete = sf), (yt.prototype.get = uf), (yt.prototype.has = lf), (yt.prototype.set = cf)
                function Ut(e) {
                    var t = -1,
                        n = e == null ? 0 : e.length
                    for (this.__data__ = new yt(); ++t < n; ) this.add(e[t])
                }
                function of(e) {
                    return this.__data__.set(e, y), this
                }
                function ff(e) {
                    return this.__data__.has(e)
                }
                ;(Ut.prototype.add = Ut.prototype.push = of), (Ut.prototype.has = ff)
                function ut(e) {
                    var t = (this.__data__ = new vt(e))
                    this.size = t.size
                }
                function _f() {
                    ;(this.__data__ = new vt()), (this.size = 0)
                }
                function pf(e) {
                    var t = this.__data__,
                        n = t.delete(e)
                    return (this.size = t.size), n
                }
                function hf(e) {
                    return this.__data__.get(e)
                }
                function gf(e) {
                    return this.__data__.has(e)
                }
                function df(e, t) {
                    var n = this.__data__
                    if (n instanceof vt) {
                        var i = n.__data__
                        if (!An || i.length < f - 1) return i.push([e, t]), (this.size = ++n.size), this
                        n = this.__data__ = new yt(i)
                    }
                    return n.set(e, t), (this.size = n.size), this
                }
                ;(ut.prototype.clear = _f), (ut.prototype.delete = pf), (ut.prototype.get = hf), (ut.prototype.has = gf), (ut.prototype.set = df)
                function vs(e, t) {
                    var n = W(e),
                        i = !n && Vt(e),
                        u = !n && !i && qt(e),
                        o = !n && !i && !u && cn(e),
                        _ = n || i || u || o,
                        g = _ ? fa(e.length, xo) : [],
                        v = g.length
                    for (var k in e)
                        (t || re.call(e, k)) &&
                            !(_ && (k == "length" || (u && (k == "offset" || k == "parent")) || (o && (k == "buffer" || k == "byteLength" || k == "byteOffset")) || At(k, v))) &&
                            g.push(k)
                    return g
                }
                function ys(e) {
                    var t = e.length
                    return t ? e[Ca(0, t - 1)] : a
                }
                function mf(e, t) {
                    return Er(Ce(e), Kt(t, 0, e.length))
                }
                function vf(e) {
                    return Er(Ce(e))
                }
                function va(e, t, n) {
                    ;((n !== a && !lt(e[t], n)) || (n === a && !(t in e))) && bt(e, t, n)
                }
                function En(e, t, n) {
                    var i = e[t]
                    ;(!(re.call(e, t) && lt(i, n)) || (n === a && !(t in e))) && bt(e, t, n)
                }
                function hr(e, t) {
                    for (var n = e.length; n--; ) if (lt(e[n][0], t)) return n
                    return -1
                }
                function yf(e, t, n, i) {
                    return (
                        Rt(e, function (u, o, _) {
                            t(i, u, n(u), _)
                        }),
                        i
                    )
                }
                function bs(e, t) {
                    return e && ht(t, ve(t), e)
                }
                function bf(e, t) {
                    return e && ht(t, Ie(t), e)
                }
                function bt(e, t, n) {
                    t == "__proto__" && lr ? lr(e, t, { configurable: !0, enumerable: !0, value: n, writable: !0 }) : (e[t] = n)
                }
                function ya(e, t) {
                    for (var n = -1, i = t.length, u = b(i), o = e == null; ++n < i; ) u[n] = o ? a : ja(e, t[n])
                    return u
                }
                function Kt(e, t, n) {
                    return e === e && (n !== a && (e = e <= n ? e : n), t !== a && (e = e >= t ? e : t)), e
                }
                function Ze(e, t, n, i, u, o) {
                    var _,
                        g = t & C,
                        v = t & D,
                        k = t & I
                    if ((n && (_ = u ? n(e, i, u, o) : n(e)), _ !== a)) return _
                    if (!fe(e)) return e
                    var A = W(e)
                    if (A) {
                        if (((_ = l0(e)), !g)) return Ce(e, _)
                    } else {
                        var x = Se(e),
                            E = x == Hn || x == ki
                        if (qt(e)) return zs(e, g)
                        if (x == mt || x == Yt || (E && !u)) {
                            if (((_ = v || E ? {} : cu(e)), !g)) return v ? Jf(e, bf(_, e)) : Xf(e, bs(_, e))
                        } else {
                            if (!le[x]) return u ? e : {}
                            _ = c0(e, x, g)
                        }
                    }
                    o || (o = new ut())
                    var R = o.get(e)
                    if (R) return R
                    o.set(e, _),
                        Fu(e)
                            ? e.forEach(function (M) {
                                  _.add(Ze(M, t, n, M, e, o))
                              })
                            : Mu(e) &&
                              e.forEach(function (M, z) {
                                  _.set(z, Ze(M, t, n, z, e, o))
                              })
                    var q = k ? (v ? Wa : Fa) : v ? Ie : ve,
                        N = A ? a : q(e)
                    return (
                        He(N || e, function (M, z) {
                            N && ((z = M), (M = e[z])), En(_, z, Ze(M, t, n, z, e, o))
                        }),
                        _
                    )
                }
                function wf(e) {
                    var t = ve(e)
                    return function (n) {
                        return ws(n, e, t)
                    }
                }
                function ws(e, t, n) {
                    var i = n.length
                    if (e == null) return !i
                    for (e = ue(e); i--; ) {
                        var u = n[i],
                            o = t[u],
                            _ = e[u]
                        if ((_ === a && !(u in e)) || !o(_)) return !1
                    }
                    return !0
                }
                function ks(e, t, n) {
                    if (typeof e != "function") throw new Ve(p)
                    return On(function () {
                        e.apply(a, n)
                    }, t)
                }
                function Cn(e, t, n, i) {
                    var u = -1,
                        o = Jn,
                        _ = !0,
                        g = e.length,
                        v = [],
                        k = t.length
                    if (!g) return v
                    n && (t = oe(t, Pe(n))), i ? ((o = ia), (_ = !1)) : t.length >= f && ((o = wn), (_ = !1), (t = new Ut(t)))
                    e: for (; ++u < g; ) {
                        var A = e[u],
                            x = n == null ? A : n(A)
                        if (((A = i || A !== 0 ? A : 0), _ && x === x)) {
                            for (var E = k; E--; ) if (t[E] === x) continue e
                            v.push(A)
                        } else o(t, x, i) || v.push(A)
                    }
                    return v
                }
                var Rt = Zs(pt),
                    As = Zs(wa, !0)
                function kf(e, t) {
                    var n = !0
                    return (
                        Rt(e, function (i, u, o) {
                            return (n = !!t(i, u, o)), n
                        }),
                        n
                    )
                }
                function gr(e, t, n) {
                    for (var i = -1, u = e.length; ++i < u; ) {
                        var o = e[i],
                            _ = t(o)
                        if (_ != null && (g === a ? _ === _ && !We(_) : n(_, g)))
                            var g = _,
                                v = o
                    }
                    return v
                }
                function Af(e, t, n, i) {
                    var u = e.length
                    for (n = $(n), n < 0 && (n = -n > u ? 0 : u + n), i = i === a || i > u ? u : $(i), i < 0 && (i += u), i = n > i ? 0 : $u(i); n < i; ) e[n++] = t
                    return e
                }
                function Ss(e, t) {
                    var n = []
                    return (
                        Rt(e, function (i, u, o) {
                            t(i, u, o) && n.push(i)
                        }),
                        n
                    )
                }
                function we(e, t, n, i, u) {
                    var o = -1,
                        _ = e.length
                    for (n || (n = f0), u || (u = []); ++o < _; ) {
                        var g = e[o]
                        t > 0 && n(g) ? (t > 1 ? we(g, t - 1, n, i, u) : Ct(u, g)) : i || (u[u.length] = g)
                    }
                    return u
                }
                var ba = Xs(),
                    xs = Xs(!0)
                function pt(e, t) {
                    return e && ba(e, t, ve)
                }
                function wa(e, t) {
                    return e && xs(e, t, ve)
                }
                function dr(e, t) {
                    return Et(t, function (n) {
                        return St(e[n])
                    })
                }
                function zt(e, t) {
                    t = Lt(t, e)
                    for (var n = 0, i = t.length; e != null && n < i; ) e = e[gt(t[n++])]
                    return n && n == i ? e : a
                }
                function Ts(e, t, n) {
                    var i = t(e)
                    return W(e) ? i : Ct(i, n(e))
                }
                function xe(e) {
                    return e == null ? (e === a ? Xl : Yl) : $t && $t in ue(e) ? i0(e) : v0(e)
                }
                function ka(e, t) {
                    return e > t
                }
                function Sf(e, t) {
                    return e != null && re.call(e, t)
                }
                function xf(e, t) {
                    return e != null && t in ue(e)
                }
                function Tf(e, t, n) {
                    return e >= Ae(t, n) && e < me(t, n)
                }
                function Aa(e, t, n) {
                    for (var i = n ? ia : Jn, u = e[0].length, o = e.length, _ = o, g = b(o), v = 1 / 0, k = []; _--; ) {
                        var A = e[_]
                        _ && t && (A = oe(A, Pe(t))), (v = Ae(A.length, v)), (g[_] = !n && (t || (u >= 120 && A.length >= 120)) ? new Ut(_ && A) : a)
                    }
                    A = e[0]
                    var x = -1,
                        E = g[0]
                    e: for (; ++x < u && k.length < v; ) {
                        var R = A[x],
                            q = t ? t(R) : R
                        if (((R = n || R !== 0 ? R : 0), !(E ? wn(E, q) : i(k, q, n)))) {
                            for (_ = o; --_; ) {
                                var N = g[_]
                                if (!(N ? wn(N, q) : i(e[_], q, n))) continue e
                            }
                            E && E.push(q), k.push(R)
                        }
                    }
                    return k
                }
                function Ef(e, t, n, i) {
                    return (
                        pt(e, function (u, o, _) {
                            t(i, n(u), o, _)
                        }),
                        i
                    )
                }
                function Bn(e, t, n) {
                    ;(t = Lt(t, e)), (e = pu(e, t))
                    var i = e == null ? e : e[gt(Je(t))]
                    return i == null ? a : Me(i, e, n)
                }
                function Es(e) {
                    return _e(e) && xe(e) == Yt
                }
                function Cf(e) {
                    return _e(e) && xe(e) == bn
                }
                function Bf(e) {
                    return _e(e) && xe(e) == gn
                }
                function In(e, t, n, i, u) {
                    return e === t ? !0 : e == null || t == null || (!_e(e) && !_e(t)) ? e !== e && t !== t : If(e, t, n, i, In, u)
                }
                function If(e, t, n, i, u, o) {
                    var _ = W(e),
                        g = W(t),
                        v = _ ? zn : Se(e),
                        k = g ? zn : Se(t)
                    ;(v = v == Yt ? mt : v), (k = k == Yt ? mt : k)
                    var A = v == mt,
                        x = k == mt,
                        E = v == k
                    if (E && qt(e)) {
                        if (!qt(t)) return !1
                        ;(_ = !0), (A = !1)
                    }
                    if (E && !A) return o || (o = new ut()), _ || cn(e) ? su(e, t, n, i, u, o) : r0(e, t, v, n, i, u, o)
                    if (!(n & L)) {
                        var R = A && re.call(e, "__wrapped__"),
                            q = x && re.call(t, "__wrapped__")
                        if (R || q) {
                            var N = R ? e.value() : e,
                                M = q ? t.value() : t
                            return o || (o = new ut()), u(N, M, n, i, o)
                        }
                    }
                    return E ? (o || (o = new ut()), a0(e, t, n, i, u, o)) : !1
                }
                function Rf(e) {
                    return _e(e) && Se(e) == at
                }
                function Sa(e, t, n, i) {
                    var u = n.length,
                        o = u,
                        _ = !i
                    if (e == null) return !o
                    for (e = ue(e); u--; ) {
                        var g = n[u]
                        if (_ && g[2] ? g[1] !== e[g[0]] : !(g[0] in e)) return !1
                    }
                    for (; ++u < o; ) {
                        g = n[u]
                        var v = g[0],
                            k = e[v],
                            A = g[1]
                        if (_ && g[2]) {
                            if (k === a && !(v in e)) return !1
                        } else {
                            var x = new ut()
                            if (i) var E = i(k, A, v, e, t, x)
                            if (!(E === a ? In(A, k, L | Q, i, x) : E)) return !1
                        }
                    }
                    return !0
                }
                function Cs(e) {
                    if (!fe(e) || p0(e)) return !1
                    var t = St(e) ? Io : yc
                    return t.test(Ht(e))
                }
                function Df(e) {
                    return _e(e) && xe(e) == mn
                }
                function Lf(e) {
                    return _e(e) && Se(e) == it
                }
                function Of(e) {
                    return _e(e) && Lr(e.length) && !!ce[xe(e)]
                }
                function Bs(e) {
                    return typeof e == "function" ? e : e == null ? Re : typeof e == "object" ? (W(e) ? Ds(e[0], e[1]) : Rs(e)) : Ju(e)
                }
                function xa(e) {
                    if (!Ln(e)) return Mo(e)
                    var t = []
                    for (var n in ue(e)) re.call(e, n) && n != "constructor" && t.push(n)
                    return t
                }
                function qf(e) {
                    if (!fe(e)) return m0(e)
                    var t = Ln(e),
                        n = []
                    for (var i in e) (i == "constructor" && (t || !re.call(e, i))) || n.push(i)
                    return n
                }
                function Ta(e, t) {
                    return e < t
                }
                function Is(e, t) {
                    var n = -1,
                        i = Be(e) ? b(e.length) : []
                    return (
                        Rt(e, function (u, o, _) {
                            i[++n] = t(u, o, _)
                        }),
                        i
                    )
                }
                function Rs(e) {
                    var t = Na(e)
                    return t.length == 1 && t[0][2]
                        ? fu(t[0][0], t[0][1])
                        : function (n) {
                              return n === e || Sa(n, e, t)
                          }
                }
                function Ds(e, t) {
                    return Ka(e) && ou(t)
                        ? fu(gt(e), t)
                        : function (n) {
                              var i = ja(n, e)
                              return i === a && i === t ? ei(n, e) : In(t, i, L | Q)
                          }
                }
                function mr(e, t, n, i, u) {
                    e !== t &&
                        ba(
                            t,
                            function (o, _) {
                                if ((u || (u = new ut()), fe(o))) Mf(e, t, _, n, mr, i, u)
                                else {
                                    var g = i ? i(Ga(e, _), o, _ + "", e, t, u) : a
                                    g === a && (g = o), va(e, _, g)
                                }
                            },
                            Ie
                        )
                }
                function Mf(e, t, n, i, u, o, _) {
                    var g = Ga(e, n),
                        v = Ga(t, n),
                        k = _.get(v)
                    if (k) {
                        va(e, n, k)
                        return
                    }
                    var A = o ? o(g, v, n + "", e, t, _) : a,
                        x = A === a
                    if (x) {
                        var E = W(v),
                            R = !E && qt(v),
                            q = !E && !R && cn(v)
                        ;(A = v),
                            E || R || q
                                ? W(g)
                                    ? (A = g)
                                    : he(g)
                                    ? (A = Ce(g))
                                    : R
                                    ? ((x = !1), (A = zs(v, !0)))
                                    : q
                                    ? ((x = !1), (A = Gs(v, !0)))
                                    : (A = [])
                                : qn(v) || Vt(v)
                                ? ((A = g), Vt(g) ? (A = Nu(g)) : (!fe(g) || St(g)) && (A = cu(v)))
                                : (x = !1)
                    }
                    x && (_.set(v, A), u(A, v, i, o, _), _.delete(v)), va(e, n, A)
                }
                function Ls(e, t) {
                    var n = e.length
                    if (!!n) return (t += t < 0 ? n : 0), At(t, n) ? e[t] : a
                }
                function Os(e, t, n) {
                    t.length
                        ? (t = oe(t, function (o) {
                              return W(o)
                                  ? function (_) {
                                        return zt(_, o.length === 1 ? o[0] : o)
                                    }
                                  : o
                          }))
                        : (t = [Re])
                    var i = -1
                    t = oe(t, Pe(O()))
                    var u = Is(e, function (o, _, g) {
                        var v = oe(t, function (k) {
                            return k(o)
                        })
                        return { criteria: v, index: ++i, value: o }
                    })
                    return uo(u, function (o, _) {
                        return Zf(o, _, n)
                    })
                }
                function Pf(e, t) {
                    return qs(e, t, function (n, i) {
                        return ei(e, i)
                    })
                }
                function qs(e, t, n) {
                    for (var i = -1, u = t.length, o = {}; ++i < u; ) {
                        var _ = t[i],
                            g = zt(e, _)
                        n(g, _) && Rn(o, Lt(_, e), g)
                    }
                    return o
                }
                function Ff(e) {
                    return function (t) {
                        return zt(t, e)
                    }
                }
                function Ea(e, t, n, i) {
                    var u = i ? so : Jt,
                        o = -1,
                        _ = t.length,
                        g = e
                    for (e === t && (t = Ce(t)), n && (g = oe(e, Pe(n))); ++o < _; )
                        for (var v = 0, k = t[o], A = n ? n(k) : k; (v = u(g, A, v, i)) > -1; ) g !== e && ur.call(g, v, 1), ur.call(e, v, 1)
                    return e
                }
                function Ms(e, t) {
                    for (var n = e ? t.length : 0, i = n - 1; n--; ) {
                        var u = t[n]
                        if (n == i || u !== o) {
                            var o = u
                            At(u) ? ur.call(e, u, 1) : Ra(e, u)
                        }
                    }
                    return e
                }
                function Ca(e, t) {
                    return e + or(ds() * (t - e + 1))
                }
                function Wf(e, t, n, i) {
                    for (var u = -1, o = me(cr((t - e) / (n || 1)), 0), _ = b(o); o--; ) (_[i ? o : ++u] = e), (e += n)
                    return _
                }
                function Ba(e, t) {
                    var n = ""
                    if (!e || t < 1 || t > rt) return n
                    do t % 2 && (n += e), (t = or(t / 2)), t && (e += e)
                    while (t)
                    return n
                }
                function U(e, t) {
                    return Ha(_u(e, t, Re), e + "")
                }
                function $f(e) {
                    return ys(on(e))
                }
                function Nf(e, t) {
                    var n = on(e)
                    return Er(n, Kt(t, 0, n.length))
                }
                function Rn(e, t, n, i) {
                    if (!fe(e)) return e
                    t = Lt(t, e)
                    for (var u = -1, o = t.length, _ = o - 1, g = e; g != null && ++u < o; ) {
                        var v = gt(t[u]),
                            k = n
                        if (v === "__proto__" || v === "constructor" || v === "prototype") return e
                        if (u != _) {
                            var A = g[v]
                            ;(k = i ? i(A, v, g) : a), k === a && (k = fe(A) ? A : At(t[u + 1]) ? [] : {})
                        }
                        En(g, v, k), (g = g[v])
                    }
                    return e
                }
                var Ps = fr
                        ? function (e, t) {
                              return fr.set(e, t), e
                          }
                        : Re,
                    Uf = lr
                        ? function (e, t) {
                              return lr(e, "toString", { configurable: !0, enumerable: !1, value: ni(t), writable: !0 })
                          }
                        : Re
                function Kf(e) {
                    return Er(on(e))
                }
                function Xe(e, t, n) {
                    var i = -1,
                        u = e.length
                    t < 0 && (t = -t > u ? 0 : u + t), (n = n > u ? u : n), n < 0 && (n += u), (u = t > n ? 0 : (n - t) >>> 0), (t >>>= 0)
                    for (var o = b(u); ++i < u; ) o[i] = e[i + t]
                    return o
                }
                function zf(e, t) {
                    var n
                    return (
                        Rt(e, function (i, u, o) {
                            return (n = t(i, u, o)), !n
                        }),
                        !!n
                    )
                }
                function vr(e, t, n) {
                    var i = 0,
                        u = e == null ? i : e.length
                    if (typeof t == "number" && t === t && u <= zl) {
                        for (; i < u; ) {
                            var o = (i + u) >>> 1,
                                _ = e[o]
                            _ !== null && !We(_) && (n ? _ <= t : _ < t) ? (i = o + 1) : (u = o)
                        }
                        return u
                    }
                    return Ia(e, t, Re, n)
                }
                function Ia(e, t, n, i) {
                    var u = 0,
                        o = e == null ? 0 : e.length
                    if (o === 0) return 0
                    t = n(t)
                    for (var _ = t !== t, g = t === null, v = We(t), k = t === a; u < o; ) {
                        var A = or((u + o) / 2),
                            x = n(e[A]),
                            E = x !== a,
                            R = x === null,
                            q = x === x,
                            N = We(x)
                        if (_) var M = i || q
                        else k ? (M = q && (i || E)) : g ? (M = q && E && (i || !R)) : v ? (M = q && E && !R && (i || !N)) : R || N ? (M = !1) : (M = i ? x <= t : x < t)
                        M ? (u = A + 1) : (o = A)
                    }
                    return Ae(o, Kn)
                }
                function Fs(e, t) {
                    for (var n = -1, i = e.length, u = 0, o = []; ++n < i; ) {
                        var _ = e[n],
                            g = t ? t(_) : _
                        if (!n || !lt(g, v)) {
                            var v = g
                            o[u++] = _ === 0 ? 0 : _
                        }
                    }
                    return o
                }
                function Ws(e) {
                    return typeof e == "number" ? e : We(e) ? Ft : +e
                }
                function Fe(e) {
                    if (typeof e == "string") return e
                    if (W(e)) return oe(e, Fe) + ""
                    if (We(e)) return ms ? ms.call(e) : ""
                    var t = e + ""
                    return t == "0" && 1 / e == -_t ? "-0" : t
                }
                function Dt(e, t, n) {
                    var i = -1,
                        u = Jn,
                        o = e.length,
                        _ = !0,
                        g = [],
                        v = g
                    if (n) (_ = !1), (u = ia)
                    else if (o >= f) {
                        var k = t ? null : t0(e)
                        if (k) return jn(k)
                        ;(_ = !1), (u = wn), (v = new Ut())
                    } else v = t ? [] : g
                    e: for (; ++i < o; ) {
                        var A = e[i],
                            x = t ? t(A) : A
                        if (((A = n || A !== 0 ? A : 0), _ && x === x)) {
                            for (var E = v.length; E--; ) if (v[E] === x) continue e
                            t && v.push(x), g.push(A)
                        } else u(v, x, n) || (v !== g && v.push(x), g.push(A))
                    }
                    return g
                }
                function Ra(e, t) {
                    return (t = Lt(t, e)), (e = pu(e, t)), e == null || delete e[gt(Je(t))]
                }
                function $s(e, t, n, i) {
                    return Rn(e, t, n(zt(e, t)), i)
                }
                function yr(e, t, n, i) {
                    for (var u = e.length, o = i ? u : -1; (i ? o-- : ++o < u) && t(e[o], o, e); );
                    return n ? Xe(e, i ? 0 : o, i ? o + 1 : u) : Xe(e, i ? o + 1 : 0, i ? u : o)
                }
                function Ns(e, t) {
                    var n = e
                    return (
                        n instanceof G && (n = n.value()),
                        sa(
                            t,
                            function (i, u) {
                                return u.func.apply(u.thisArg, Ct([i], u.args))
                            },
                            n
                        )
                    )
                }
                function Da(e, t, n) {
                    var i = e.length
                    if (i < 2) return i ? Dt(e[0]) : []
                    for (var u = -1, o = b(i); ++u < i; ) for (var _ = e[u], g = -1; ++g < i; ) g != u && (o[u] = Cn(o[u] || _, e[g], t, n))
                    return Dt(we(o, 1), t, n)
                }
                function Us(e, t, n) {
                    for (var i = -1, u = e.length, o = t.length, _ = {}; ++i < u; ) {
                        var g = i < o ? t[i] : a
                        n(_, e[i], g)
                    }
                    return _
                }
                function La(e) {
                    return he(e) ? e : []
                }
                function Oa(e) {
                    return typeof e == "function" ? e : Re
                }
                function Lt(e, t) {
                    return W(e) ? e : Ka(e, t) ? [e] : mu(te(e))
                }
                var Gf = U
                function Ot(e, t, n) {
                    var i = e.length
                    return (n = n === a ? i : n), !t && n >= i ? e : Xe(e, t, n)
                }
                var Ks =
                    Ro ||
                    function (e) {
                        return be.clearTimeout(e)
                    }
                function zs(e, t) {
                    if (t) return e.slice()
                    var n = e.length,
                        i = fs ? fs(n) : new e.constructor(n)
                    return e.copy(i), i
                }
                function qa(e) {
                    var t = new e.constructor(e.byteLength)
                    return new ir(t).set(new ir(e)), t
                }
                function Hf(e, t) {
                    var n = t ? qa(e.buffer) : e.buffer
                    return new e.constructor(n, e.byteOffset, e.byteLength)
                }
                function Vf(e) {
                    var t = new e.constructor(e.source, Ei.exec(e))
                    return (t.lastIndex = e.lastIndex), t
                }
                function Yf(e) {
                    return Tn ? ue(Tn.call(e)) : {}
                }
                function Gs(e, t) {
                    var n = t ? qa(e.buffer) : e.buffer
                    return new e.constructor(n, e.byteOffset, e.length)
                }
                function Hs(e, t) {
                    if (e !== t) {
                        var n = e !== a,
                            i = e === null,
                            u = e === e,
                            o = We(e),
                            _ = t !== a,
                            g = t === null,
                            v = t === t,
                            k = We(t)
                        if ((!g && !k && !o && e > t) || (o && _ && v && !g && !k) || (i && _ && v) || (!n && v) || !u) return 1
                        if ((!i && !o && !k && e < t) || (k && n && u && !i && !o) || (g && n && u) || (!_ && u) || !v) return -1
                    }
                    return 0
                }
                function Zf(e, t, n) {
                    for (var i = -1, u = e.criteria, o = t.criteria, _ = u.length, g = n.length; ++i < _; ) {
                        var v = Hs(u[i], o[i])
                        if (v) {
                            if (i >= g) return v
                            var k = n[i]
                            return v * (k == "desc" ? -1 : 1)
                        }
                    }
                    return e.index - t.index
                }
                function Vs(e, t, n, i) {
                    for (var u = -1, o = e.length, _ = n.length, g = -1, v = t.length, k = me(o - _, 0), A = b(v + k), x = !i; ++g < v; ) A[g] = t[g]
                    for (; ++u < _; ) (x || u < o) && (A[n[u]] = e[u])
                    for (; k--; ) A[g++] = e[u++]
                    return A
                }
                function Ys(e, t, n, i) {
                    for (var u = -1, o = e.length, _ = -1, g = n.length, v = -1, k = t.length, A = me(o - g, 0), x = b(A + k), E = !i; ++u < A; ) x[u] = e[u]
                    for (var R = u; ++v < k; ) x[R + v] = t[v]
                    for (; ++_ < g; ) (E || u < o) && (x[R + n[_]] = e[u++])
                    return x
                }
                function Ce(e, t) {
                    var n = -1,
                        i = e.length
                    for (t || (t = b(i)); ++n < i; ) t[n] = e[n]
                    return t
                }
                function ht(e, t, n, i) {
                    var u = !n
                    n || (n = {})
                    for (var o = -1, _ = t.length; ++o < _; ) {
                        var g = t[o],
                            v = i ? i(n[g], e[g], g, n, e) : a
                        v === a && (v = e[g]), u ? bt(n, g, v) : En(n, g, v)
                    }
                    return n
                }
                function Xf(e, t) {
                    return ht(e, Ua(e), t)
                }
                function Jf(e, t) {
                    return ht(e, uu(e), t)
                }
                function br(e, t) {
                    return function (n, i) {
                        var u = W(n) ? eo : yf,
                            o = t ? t() : {}
                        return u(n, e, O(i, 2), o)
                    }
                }
                function sn(e) {
                    return U(function (t, n) {
                        var i = -1,
                            u = n.length,
                            o = u > 1 ? n[u - 1] : a,
                            _ = u > 2 ? n[2] : a
                        for (o = e.length > 3 && typeof o == "function" ? (u--, o) : a, _ && Te(n[0], n[1], _) && ((o = u < 3 ? a : o), (u = 1)), t = ue(t); ++i < u; ) {
                            var g = n[i]
                            g && e(t, g, i, o)
                        }
                        return t
                    })
                }
                function Zs(e, t) {
                    return function (n, i) {
                        if (n == null) return n
                        if (!Be(n)) return e(n, i)
                        for (var u = n.length, o = t ? u : -1, _ = ue(n); (t ? o-- : ++o < u) && i(_[o], o, _) !== !1; );
                        return n
                    }
                }
                function Xs(e) {
                    return function (t, n, i) {
                        for (var u = -1, o = ue(t), _ = i(t), g = _.length; g--; ) {
                            var v = _[e ? g : ++u]
                            if (n(o[v], v, o) === !1) break
                        }
                        return t
                    }
                }
                function Qf(e, t, n) {
                    var i = t & F,
                        u = Dn(e)
                    function o() {
                        var _ = this && this !== be && this instanceof o ? u : e
                        return _.apply(i ? n : this, arguments)
                    }
                    return o
                }
                function Js(e) {
                    return function (t) {
                        t = te(t)
                        var n = Qt(t) ? st(t) : a,
                            i = n ? n[0] : t.charAt(0),
                            u = n ? Ot(n, 1).join("") : t.slice(1)
                        return i[e]() + u
                    }
                }
                function un(e) {
                    return function (t) {
                        return sa(Zu(Yu(t).replace($c, "")), e, "")
                    }
                }
                function Dn(e) {
                    return function () {
                        var t = arguments
                        switch (t.length) {
                            case 0:
                                return new e()
                            case 1:
                                return new e(t[0])
                            case 2:
                                return new e(t[0], t[1])
                            case 3:
                                return new e(t[0], t[1], t[2])
                            case 4:
                                return new e(t[0], t[1], t[2], t[3])
                            case 5:
                                return new e(t[0], t[1], t[2], t[3], t[4])
                            case 6:
                                return new e(t[0], t[1], t[2], t[3], t[4], t[5])
                            case 7:
                                return new e(t[0], t[1], t[2], t[3], t[4], t[5], t[6])
                        }
                        var n = an(e.prototype),
                            i = e.apply(n, t)
                        return fe(i) ? i : n
                    }
                }
                function jf(e, t, n) {
                    var i = Dn(e)
                    function u() {
                        for (var o = arguments.length, _ = b(o), g = o, v = ln(u); g--; ) _[g] = arguments[g]
                        var k = o < 3 && _[0] !== v && _[o - 1] !== v ? [] : Bt(_, v)
                        if (((o -= k.length), o < n)) return nu(e, t, wr, u.placeholder, a, _, k, a, a, n - o)
                        var A = this && this !== be && this instanceof u ? i : e
                        return Me(A, this, _)
                    }
                    return u
                }
                function Qs(e) {
                    return function (t, n, i) {
                        var u = ue(t)
                        if (!Be(t)) {
                            var o = O(n, 3)
                            ;(t = ve(t)),
                                (n = function (g) {
                                    return o(u[g], g, u)
                                })
                        }
                        var _ = e(t, n, i)
                        return _ > -1 ? u[o ? t[_] : _] : a
                    }
                }
                function js(e) {
                    return kt(function (t) {
                        var n = t.length,
                            i = n,
                            u = Ye.prototype.thru
                        for (e && t.reverse(); i--; ) {
                            var o = t[i]
                            if (typeof o != "function") throw new Ve(p)
                            if (u && !_ && xr(o) == "wrapper") var _ = new Ye([], !0)
                        }
                        for (i = _ ? i : n; ++i < n; ) {
                            o = t[i]
                            var g = xr(o),
                                v = g == "wrapper" ? $a(o) : a
                            v && za(v[0]) && v[1] == (J | X | ae | ye) && !v[4].length && v[9] == 1 ? (_ = _[xr(v[0])].apply(_, v[3])) : (_ = o.length == 1 && za(o) ? _[g]() : _.thru(o))
                        }
                        return function () {
                            var k = arguments,
                                A = k[0]
                            if (_ && k.length == 1 && W(A)) return _.plant(A).value()
                            for (var x = 0, E = n ? t[x].apply(this, k) : A; ++x < n; ) E = t[x].call(this, E)
                            return E
                        }
                    })
                }
                function wr(e, t, n, i, u, o, _, g, v, k) {
                    var A = t & J,
                        x = t & F,
                        E = t & K,
                        R = t & (X | se),
                        q = t & ke,
                        N = E ? a : Dn(e)
                    function M() {
                        for (var z = arguments.length, H = b(z), $e = z; $e--; ) H[$e] = arguments[$e]
                        if (R)
                            var Ee = ln(M),
                                Ne = co(H, Ee)
                        if ((i && (H = Vs(H, i, u, R)), o && (H = Ys(H, o, _, R)), (z -= Ne), R && z < k)) {
                            var ge = Bt(H, Ee)
                            return nu(e, t, wr, M.placeholder, n, H, ge, g, v, k - z)
                        }
                        var ct = x ? n : this,
                            Tt = E ? ct[e] : e
                        return (
                            (z = H.length),
                            g ? (H = y0(H, g)) : q && z > 1 && H.reverse(),
                            A && v < z && (H.length = v),
                            this && this !== be && this instanceof M && (Tt = N || Dn(Tt)),
                            Tt.apply(ct, H)
                        )
                    }
                    return M
                }
                function eu(e, t) {
                    return function (n, i) {
                        return Ef(n, e, t(i), {})
                    }
                }
                function kr(e, t) {
                    return function (n, i) {
                        var u
                        if (n === a && i === a) return t
                        if ((n !== a && (u = n), i !== a)) {
                            if (u === a) return i
                            typeof n == "string" || typeof i == "string" ? ((n = Fe(n)), (i = Fe(i))) : ((n = Ws(n)), (i = Ws(i))), (u = e(n, i))
                        }
                        return u
                    }
                }
                function Ma(e) {
                    return kt(function (t) {
                        return (
                            (t = oe(t, Pe(O()))),
                            U(function (n) {
                                var i = this
                                return e(t, function (u) {
                                    return Me(u, i, n)
                                })
                            })
                        )
                    })
                }
                function Ar(e, t) {
                    t = t === a ? " " : Fe(t)
                    var n = t.length
                    if (n < 2) return n ? Ba(t, e) : t
                    var i = Ba(t, cr(e / jt(t)))
                    return Qt(t) ? Ot(st(i), 0, e).join("") : i.slice(0, e)
                }
                function e0(e, t, n, i) {
                    var u = t & F,
                        o = Dn(e)
                    function _() {
                        for (var g = -1, v = arguments.length, k = -1, A = i.length, x = b(A + v), E = this && this !== be && this instanceof _ ? o : e; ++k < A; ) x[k] = i[k]
                        for (; v--; ) x[k++] = arguments[++g]
                        return Me(E, u ? n : this, x)
                    }
                    return _
                }
                function tu(e) {
                    return function (t, n, i) {
                        return (
                            i && typeof i != "number" && Te(t, n, i) && (n = i = a), (t = xt(t)), n === a ? ((n = t), (t = 0)) : (n = xt(n)), (i = i === a ? (t < n ? 1 : -1) : xt(i)), Wf(t, n, i, e)
                        )
                    }
                }
                function Sr(e) {
                    return function (t, n) {
                        return (typeof t == "string" && typeof n == "string") || ((t = Qe(t)), (n = Qe(n))), e(t, n)
                    }
                }
                function nu(e, t, n, i, u, o, _, g, v, k) {
                    var A = t & X,
                        x = A ? _ : a,
                        E = A ? a : _,
                        R = A ? o : a,
                        q = A ? a : o
                    ;(t |= A ? ae : ie), (t &= ~(A ? ie : ae)), t & ne || (t &= ~(F | K))
                    var N = [e, t, u, R, x, q, E, g, v, k],
                        M = n.apply(a, N)
                    return za(e) && hu(M, N), (M.placeholder = i), gu(M, e, t)
                }
                function Pa(e) {
                    var t = de[e]
                    return function (n, i) {
                        if (((n = Qe(n)), (i = i == null ? 0 : Ae($(i), 292)), i && gs(n))) {
                            var u = (te(n) + "e").split("e"),
                                o = t(u[0] + "e" + (+u[1] + i))
                            return (u = (te(o) + "e").split("e")), +(u[0] + "e" + (+u[1] - i))
                        }
                        return t(n)
                    }
                }
                var t0 =
                    nn && 1 / jn(new nn([, -0]))[1] == _t
                        ? function (e) {
                              return new nn(e)
                          }
                        : ii
                function ru(e) {
                    return function (t) {
                        var n = Se(t)
                        return n == at ? pa(t) : n == it ? mo(t) : lo(t, e(t))
                    }
                }
                function wt(e, t, n, i, u, o, _, g) {
                    var v = t & K
                    if (!v && typeof e != "function") throw new Ve(p)
                    var k = i ? i.length : 0
                    if ((k || ((t &= ~(ae | ie)), (i = u = a)), (_ = _ === a ? _ : me($(_), 0)), (g = g === a ? g : $(g)), (k -= u ? u.length : 0), t & ie)) {
                        var A = i,
                            x = u
                        i = u = a
                    }
                    var E = v ? a : $a(e),
                        R = [e, t, n, i, u, A, x, o, _, g]
                    if (
                        (E && d0(R, E),
                        (e = R[0]),
                        (t = R[1]),
                        (n = R[2]),
                        (i = R[3]),
                        (u = R[4]),
                        (g = R[9] = R[9] === a ? (v ? 0 : e.length) : me(R[9] - k, 0)),
                        !g && t & (X | se) && (t &= ~(X | se)),
                        !t || t == F)
                    )
                        var q = Qf(e, t, n)
                    else t == X || t == se ? (q = jf(e, t, g)) : (t == ae || t == (F | ae)) && !u.length ? (q = e0(e, t, n, i)) : (q = wr.apply(a, R))
                    var N = E ? Ps : hu
                    return gu(N(q, R), e, t)
                }
                function au(e, t, n, i) {
                    return e === a || (lt(e, tn[n]) && !re.call(i, n)) ? t : e
                }
                function iu(e, t, n, i, u, o) {
                    return fe(e) && fe(t) && (o.set(t, e), mr(e, t, a, iu, o), o.delete(t)), e
                }
                function n0(e) {
                    return qn(e) ? a : e
                }
                function su(e, t, n, i, u, o) {
                    var _ = n & L,
                        g = e.length,
                        v = t.length
                    if (g != v && !(_ && v > g)) return !1
                    var k = o.get(e),
                        A = o.get(t)
                    if (k && A) return k == t && A == e
                    var x = -1,
                        E = !0,
                        R = n & Q ? new Ut() : a
                    for (o.set(e, t), o.set(t, e); ++x < g; ) {
                        var q = e[x],
                            N = t[x]
                        if (i) var M = _ ? i(N, q, x, t, e, o) : i(q, N, x, e, t, o)
                        if (M !== a) {
                            if (M) continue
                            E = !1
                            break
                        }
                        if (R) {
                            if (
                                !ua(t, function (z, H) {
                                    if (!wn(R, H) && (q === z || u(q, z, n, i, o))) return R.push(H)
                                })
                            ) {
                                E = !1
                                break
                            }
                        } else if (!(q === N || u(q, N, n, i, o))) {
                            E = !1
                            break
                        }
                    }
                    return o.delete(e), o.delete(t), E
                }
                function r0(e, t, n, i, u, o, _) {
                    switch (n) {
                        case Zt:
                            if (e.byteLength != t.byteLength || e.byteOffset != t.byteOffset) return !1
                            ;(e = e.buffer), (t = t.buffer)
                        case bn:
                            return !(e.byteLength != t.byteLength || !o(new ir(e), new ir(t)))
                        case hn:
                        case gn:
                        case dn:
                            return lt(+e, +t)
                        case Gn:
                            return e.name == t.name && e.message == t.message
                        case mn:
                        case vn:
                            return e == t + ""
                        case at:
                            var g = pa
                        case it:
                            var v = i & L
                            if ((g || (g = jn), e.size != t.size && !v)) return !1
                            var k = _.get(e)
                            if (k) return k == t
                            ;(i |= Q), _.set(e, t)
                            var A = su(g(e), g(t), i, u, o, _)
                            return _.delete(e), A
                        case Vn:
                            if (Tn) return Tn.call(e) == Tn.call(t)
                    }
                    return !1
                }
                function a0(e, t, n, i, u, o) {
                    var _ = n & L,
                        g = Fa(e),
                        v = g.length,
                        k = Fa(t),
                        A = k.length
                    if (v != A && !_) return !1
                    for (var x = v; x--; ) {
                        var E = g[x]
                        if (!(_ ? E in t : re.call(t, E))) return !1
                    }
                    var R = o.get(e),
                        q = o.get(t)
                    if (R && q) return R == t && q == e
                    var N = !0
                    o.set(e, t), o.set(t, e)
                    for (var M = _; ++x < v; ) {
                        E = g[x]
                        var z = e[E],
                            H = t[E]
                        if (i) var $e = _ ? i(H, z, E, t, e, o) : i(z, H, E, e, t, o)
                        if (!($e === a ? z === H || u(z, H, n, i, o) : $e)) {
                            N = !1
                            break
                        }
                        M || (M = E == "constructor")
                    }
                    if (N && !M) {
                        var Ee = e.constructor,
                            Ne = t.constructor
                        Ee != Ne && "constructor" in e && "constructor" in t && !(typeof Ee == "function" && Ee instanceof Ee && typeof Ne == "function" && Ne instanceof Ne) && (N = !1)
                    }
                    return o.delete(e), o.delete(t), N
                }
                function kt(e) {
                    return Ha(_u(e, a, wu), e + "")
                }
                function Fa(e) {
                    return Ts(e, ve, Ua)
                }
                function Wa(e) {
                    return Ts(e, Ie, uu)
                }
                var $a = fr
                    ? function (e) {
                          return fr.get(e)
                      }
                    : ii
                function xr(e) {
                    for (var t = e.name + "", n = rn[t], i = re.call(rn, t) ? n.length : 0; i--; ) {
                        var u = n[i],
                            o = u.func
                        if (o == null || o == e) return u.name
                    }
                    return t
                }
                function ln(e) {
                    var t = re.call(l, "placeholder") ? l : e
                    return t.placeholder
                }
                function O() {
                    var e = l.iteratee || ri
                    return (e = e === ri ? Bs : e), arguments.length ? e(arguments[0], arguments[1]) : e
                }
                function Tr(e, t) {
                    var n = e.__data__
                    return _0(t) ? n[typeof t == "string" ? "string" : "hash"] : n.map
                }
                function Na(e) {
                    for (var t = ve(e), n = t.length; n--; ) {
                        var i = t[n],
                            u = e[i]
                        t[n] = [i, u, ou(u)]
                    }
                    return t
                }
                function Gt(e, t) {
                    var n = po(e, t)
                    return Cs(n) ? n : a
                }
                function i0(e) {
                    var t = re.call(e, $t),
                        n = e[$t]
                    try {
                        e[$t] = a
                        var i = !0
                    } catch {}
                    var u = rr.call(e)
                    return i && (t ? (e[$t] = n) : delete e[$t]), u
                }
                var Ua = ga
                        ? function (e) {
                              return e == null
                                  ? []
                                  : ((e = ue(e)),
                                    Et(ga(e), function (t) {
                                        return ps.call(e, t)
                                    }))
                          }
                        : si,
                    uu = ga
                        ? function (e) {
                              for (var t = []; e; ) Ct(t, Ua(e)), (e = sr(e))
                              return t
                          }
                        : si,
                    Se = xe
                ;((da && Se(new da(new ArrayBuffer(1))) != Zt) || (An && Se(new An()) != at) || (ma && Se(ma.resolve()) != Ai) || (nn && Se(new nn()) != it) || (Sn && Se(new Sn()) != yn)) &&
                    (Se = function (e) {
                        var t = xe(e),
                            n = t == mt ? e.constructor : a,
                            i = n ? Ht(n) : ""
                        if (i)
                            switch (i) {
                                case $o:
                                    return Zt
                                case No:
                                    return at
                                case Uo:
                                    return Ai
                                case Ko:
                                    return it
                                case zo:
                                    return yn
                            }
                        return t
                    })
                function s0(e, t, n) {
                    for (var i = -1, u = n.length; ++i < u; ) {
                        var o = n[i],
                            _ = o.size
                        switch (o.type) {
                            case "drop":
                                e += _
                                break
                            case "dropRight":
                                t -= _
                                break
                            case "take":
                                t = Ae(t, e + _)
                                break
                            case "takeRight":
                                e = me(e, t - _)
                                break
                        }
                    }
                    return { start: e, end: t }
                }
                function u0(e) {
                    var t = e.match(fc)
                    return t ? t[1].split(_c) : []
                }
                function lu(e, t, n) {
                    t = Lt(t, e)
                    for (var i = -1, u = t.length, o = !1; ++i < u; ) {
                        var _ = gt(t[i])
                        if (!(o = e != null && n(e, _))) break
                        e = e[_]
                    }
                    return o || ++i != u ? o : ((u = e == null ? 0 : e.length), !!u && Lr(u) && At(_, u) && (W(e) || Vt(e)))
                }
                function l0(e) {
                    var t = e.length,
                        n = new e.constructor(t)
                    return t && typeof e[0] == "string" && re.call(e, "index") && ((n.index = e.index), (n.input = e.input)), n
                }
                function cu(e) {
                    return typeof e.constructor == "function" && !Ln(e) ? an(sr(e)) : {}
                }
                function c0(e, t, n) {
                    var i = e.constructor
                    switch (t) {
                        case bn:
                            return qa(e)
                        case hn:
                        case gn:
                            return new i(+e)
                        case Zt:
                            return Hf(e, n)
                        case Nr:
                        case Ur:
                        case Kr:
                        case zr:
                        case Gr:
                        case Hr:
                        case Vr:
                        case Yr:
                        case Zr:
                            return Gs(e, n)
                        case at:
                            return new i()
                        case dn:
                        case vn:
                            return new i(e)
                        case mn:
                            return Vf(e)
                        case it:
                            return new i()
                        case Vn:
                            return Yf(e)
                    }
                }
                function o0(e, t) {
                    var n = t.length
                    if (!n) return e
                    var i = n - 1
                    return (
                        (t[i] = (n > 1 ? "& " : "") + t[i]),
                        (t = t.join(n > 2 ? ", " : " ")),
                        e.replace(
                            oc,
                            `{
/* [wrapped with ` +
                                t +
                                `] */
`
                        )
                    )
                }
                function f0(e) {
                    return W(e) || Vt(e) || !!(hs && e && e[hs])
                }
                function At(e, t) {
                    var n = typeof e
                    return (t = t == null ? rt : t), !!t && (n == "number" || (n != "symbol" && wc.test(e))) && e > -1 && e % 1 == 0 && e < t
                }
                function Te(e, t, n) {
                    if (!fe(n)) return !1
                    var i = typeof t
                    return (i == "number" ? Be(n) && At(t, n.length) : i == "string" && t in n) ? lt(n[t], e) : !1
                }
                function Ka(e, t) {
                    if (W(e)) return !1
                    var n = typeof e
                    return n == "number" || n == "symbol" || n == "boolean" || e == null || We(e) ? !0 : sc.test(e) || !ic.test(e) || (t != null && e in ue(t))
                }
                function _0(e) {
                    var t = typeof e
                    return t == "string" || t == "number" || t == "symbol" || t == "boolean" ? e !== "__proto__" : e === null
                }
                function za(e) {
                    var t = xr(e),
                        n = l[t]
                    if (typeof n != "function" || !(t in G.prototype)) return !1
                    if (e === n) return !0
                    var i = $a(n)
                    return !!i && e === i[0]
                }
                function p0(e) {
                    return !!os && os in e
                }
                var h0 = tr ? St : ui
                function Ln(e) {
                    var t = e && e.constructor,
                        n = (typeof t == "function" && t.prototype) || tn
                    return e === n
                }
                function ou(e) {
                    return e === e && !fe(e)
                }
                function fu(e, t) {
                    return function (n) {
                        return n == null ? !1 : n[e] === t && (t !== a || e in ue(n))
                    }
                }
                function g0(e) {
                    var t = Rr(e, function (i) {
                            return n.size === S && n.clear(), i
                        }),
                        n = t.cache
                    return t
                }
                function d0(e, t) {
                    var n = e[1],
                        i = t[1],
                        u = n | i,
                        o = u < (F | K | J),
                        _ = (i == J && n == X) || (i == J && n == ye && e[7].length <= t[8]) || (i == (J | ye) && t[7].length <= t[8] && n == X)
                    if (!(o || _)) return e
                    i & F && ((e[2] = t[2]), (u |= n & F ? 0 : ne))
                    var g = t[3]
                    if (g) {
                        var v = e[3]
                        ;(e[3] = v ? Vs(v, g, t[4]) : g), (e[4] = v ? Bt(e[3], T) : t[4])
                    }
                    return (
                        (g = t[5]),
                        g && ((v = e[5]), (e[5] = v ? Ys(v, g, t[6]) : g), (e[6] = v ? Bt(e[5], T) : t[6])),
                        (g = t[7]),
                        g && (e[7] = g),
                        i & J && (e[8] = e[8] == null ? t[8] : Ae(e[8], t[8])),
                        e[9] == null && (e[9] = t[9]),
                        (e[0] = t[0]),
                        (e[1] = u),
                        e
                    )
                }
                function m0(e) {
                    var t = []
                    if (e != null) for (var n in ue(e)) t.push(n)
                    return t
                }
                function v0(e) {
                    return rr.call(e)
                }
                function _u(e, t, n) {
                    return (
                        (t = me(t === a ? e.length - 1 : t, 0)),
                        function () {
                            for (var i = arguments, u = -1, o = me(i.length - t, 0), _ = b(o); ++u < o; ) _[u] = i[t + u]
                            u = -1
                            for (var g = b(t + 1); ++u < t; ) g[u] = i[u]
                            return (g[t] = n(_)), Me(e, this, g)
                        }
                    )
                }
                function pu(e, t) {
                    return t.length < 2 ? e : zt(e, Xe(t, 0, -1))
                }
                function y0(e, t) {
                    for (var n = e.length, i = Ae(t.length, n), u = Ce(e); i--; ) {
                        var o = t[i]
                        e[i] = At(o, n) ? u[o] : a
                    }
                    return e
                }
                function Ga(e, t) {
                    if (!(t === "constructor" && typeof e[t] == "function") && t != "__proto__") return e[t]
                }
                var hu = du(Ps),
                    On =
                        Lo ||
                        function (e, t) {
                            return be.setTimeout(e, t)
                        },
                    Ha = du(Uf)
                function gu(e, t, n) {
                    var i = t + ""
                    return Ha(e, o0(i, b0(u0(i), n)))
                }
                function du(e) {
                    var t = 0,
                        n = 0
                    return function () {
                        var i = Po(),
                            u = nt - (i - n)
                        if (((n = i), u > 0)) {
                            if (++t >= Le) return arguments[0]
                        } else t = 0
                        return e.apply(a, arguments)
                    }
                }
                function Er(e, t) {
                    var n = -1,
                        i = e.length,
                        u = i - 1
                    for (t = t === a ? i : t; ++n < t; ) {
                        var o = Ca(n, u),
                            _ = e[o]
                        ;(e[o] = e[n]), (e[n] = _)
                    }
                    return (e.length = t), e
                }
                var mu = g0(function (e) {
                    var t = []
                    return (
                        e.charCodeAt(0) === 46 && t.push(""),
                        e.replace(uc, function (n, i, u, o) {
                            t.push(u ? o.replace(gc, "$1") : i || n)
                        }),
                        t
                    )
                })
                function gt(e) {
                    if (typeof e == "string" || We(e)) return e
                    var t = e + ""
                    return t == "0" && 1 / e == -_t ? "-0" : t
                }
                function Ht(e) {
                    if (e != null) {
                        try {
                            return nr.call(e)
                        } catch {}
                        try {
                            return e + ""
                        } catch {}
                    }
                    return ""
                }
                function b0(e, t) {
                    return (
                        He(Gl, function (n) {
                            var i = "_." + n[0]
                            t & n[1] && !Jn(e, i) && e.push(i)
                        }),
                        e.sort()
                    )
                }
                function vu(e) {
                    if (e instanceof G) return e.clone()
                    var t = new Ye(e.__wrapped__, e.__chain__)
                    return (t.__actions__ = Ce(e.__actions__)), (t.__index__ = e.__index__), (t.__values__ = e.__values__), t
                }
                function w0(e, t, n) {
                    ;(n ? Te(e, t, n) : t === a) ? (t = 1) : (t = me($(t), 0))
                    var i = e == null ? 0 : e.length
                    if (!i || t < 1) return []
                    for (var u = 0, o = 0, _ = b(cr(i / t)); u < i; ) _[o++] = Xe(e, u, (u += t))
                    return _
                }
                function k0(e) {
                    for (var t = -1, n = e == null ? 0 : e.length, i = 0, u = []; ++t < n; ) {
                        var o = e[t]
                        o && (u[i++] = o)
                    }
                    return u
                }
                function A0() {
                    var e = arguments.length
                    if (!e) return []
                    for (var t = b(e - 1), n = arguments[0], i = e; i--; ) t[i - 1] = arguments[i]
                    return Ct(W(n) ? Ce(n) : [n], we(t, 1))
                }
                var S0 = U(function (e, t) {
                        return he(e) ? Cn(e, we(t, 1, he, !0)) : []
                    }),
                    x0 = U(function (e, t) {
                        var n = Je(t)
                        return he(n) && (n = a), he(e) ? Cn(e, we(t, 1, he, !0), O(n, 2)) : []
                    }),
                    T0 = U(function (e, t) {
                        var n = Je(t)
                        return he(n) && (n = a), he(e) ? Cn(e, we(t, 1, he, !0), a, n) : []
                    })
                function E0(e, t, n) {
                    var i = e == null ? 0 : e.length
                    return i ? ((t = n || t === a ? 1 : $(t)), Xe(e, t < 0 ? 0 : t, i)) : []
                }
                function C0(e, t, n) {
                    var i = e == null ? 0 : e.length
                    return i ? ((t = n || t === a ? 1 : $(t)), (t = i - t), Xe(e, 0, t < 0 ? 0 : t)) : []
                }
                function B0(e, t) {
                    return e && e.length ? yr(e, O(t, 3), !0, !0) : []
                }
                function I0(e, t) {
                    return e && e.length ? yr(e, O(t, 3), !0) : []
                }
                function R0(e, t, n, i) {
                    var u = e == null ? 0 : e.length
                    return u ? (n && typeof n != "number" && Te(e, t, n) && ((n = 0), (i = u)), Af(e, t, n, i)) : []
                }
                function yu(e, t, n) {
                    var i = e == null ? 0 : e.length
                    if (!i) return -1
                    var u = n == null ? 0 : $(n)
                    return u < 0 && (u = me(i + u, 0)), Qn(e, O(t, 3), u)
                }
                function bu(e, t, n) {
                    var i = e == null ? 0 : e.length
                    if (!i) return -1
                    var u = i - 1
                    return n !== a && ((u = $(n)), (u = n < 0 ? me(i + u, 0) : Ae(u, i - 1))), Qn(e, O(t, 3), u, !0)
                }
                function wu(e) {
                    var t = e == null ? 0 : e.length
                    return t ? we(e, 1) : []
                }
                function D0(e) {
                    var t = e == null ? 0 : e.length
                    return t ? we(e, _t) : []
                }
                function L0(e, t) {
                    var n = e == null ? 0 : e.length
                    return n ? ((t = t === a ? 1 : $(t)), we(e, t)) : []
                }
                function O0(e) {
                    for (var t = -1, n = e == null ? 0 : e.length, i = {}; ++t < n; ) {
                        var u = e[t]
                        i[u[0]] = u[1]
                    }
                    return i
                }
                function ku(e) {
                    return e && e.length ? e[0] : a
                }
                function q0(e, t, n) {
                    var i = e == null ? 0 : e.length
                    if (!i) return -1
                    var u = n == null ? 0 : $(n)
                    return u < 0 && (u = me(i + u, 0)), Jt(e, t, u)
                }
                function M0(e) {
                    var t = e == null ? 0 : e.length
                    return t ? Xe(e, 0, -1) : []
                }
                var P0 = U(function (e) {
                        var t = oe(e, La)
                        return t.length && t[0] === e[0] ? Aa(t) : []
                    }),
                    F0 = U(function (e) {
                        var t = Je(e),
                            n = oe(e, La)
                        return t === Je(n) ? (t = a) : n.pop(), n.length && n[0] === e[0] ? Aa(n, O(t, 2)) : []
                    }),
                    W0 = U(function (e) {
                        var t = Je(e),
                            n = oe(e, La)
                        return (t = typeof t == "function" ? t : a), t && n.pop(), n.length && n[0] === e[0] ? Aa(n, a, t) : []
                    })
                function $0(e, t) {
                    return e == null ? "" : qo.call(e, t)
                }
                function Je(e) {
                    var t = e == null ? 0 : e.length
                    return t ? e[t - 1] : a
                }
                function N0(e, t, n) {
                    var i = e == null ? 0 : e.length
                    if (!i) return -1
                    var u = i
                    return n !== a && ((u = $(n)), (u = u < 0 ? me(i + u, 0) : Ae(u, i - 1))), t === t ? yo(e, t, u) : Qn(e, ns, u, !0)
                }
                function U0(e, t) {
                    return e && e.length ? Ls(e, $(t)) : a
                }
                var K0 = U(Au)
                function Au(e, t) {
                    return e && e.length && t && t.length ? Ea(e, t) : e
                }
                function z0(e, t, n) {
                    return e && e.length && t && t.length ? Ea(e, t, O(n, 2)) : e
                }
                function G0(e, t, n) {
                    return e && e.length && t && t.length ? Ea(e, t, a, n) : e
                }
                var H0 = kt(function (e, t) {
                    var n = e == null ? 0 : e.length,
                        i = ya(e, t)
                    return (
                        Ms(
                            e,
                            oe(t, function (u) {
                                return At(u, n) ? +u : u
                            }).sort(Hs)
                        ),
                        i
                    )
                })
                function V0(e, t) {
                    var n = []
                    if (!(e && e.length)) return n
                    var i = -1,
                        u = [],
                        o = e.length
                    for (t = O(t, 3); ++i < o; ) {
                        var _ = e[i]
                        t(_, i, e) && (n.push(_), u.push(i))
                    }
                    return Ms(e, u), n
                }
                function Va(e) {
                    return e == null ? e : Wo.call(e)
                }
                function Y0(e, t, n) {
                    var i = e == null ? 0 : e.length
                    return i ? (n && typeof n != "number" && Te(e, t, n) ? ((t = 0), (n = i)) : ((t = t == null ? 0 : $(t)), (n = n === a ? i : $(n))), Xe(e, t, n)) : []
                }
                function Z0(e, t) {
                    return vr(e, t)
                }
                function X0(e, t, n) {
                    return Ia(e, t, O(n, 2))
                }
                function J0(e, t) {
                    var n = e == null ? 0 : e.length
                    if (n) {
                        var i = vr(e, t)
                        if (i < n && lt(e[i], t)) return i
                    }
                    return -1
                }
                function Q0(e, t) {
                    return vr(e, t, !0)
                }
                function j0(e, t, n) {
                    return Ia(e, t, O(n, 2), !0)
                }
                function e1(e, t) {
                    var n = e == null ? 0 : e.length
                    if (n) {
                        var i = vr(e, t, !0) - 1
                        if (lt(e[i], t)) return i
                    }
                    return -1
                }
                function t1(e) {
                    return e && e.length ? Fs(e) : []
                }
                function n1(e, t) {
                    return e && e.length ? Fs(e, O(t, 2)) : []
                }
                function r1(e) {
                    var t = e == null ? 0 : e.length
                    return t ? Xe(e, 1, t) : []
                }
                function a1(e, t, n) {
                    return e && e.length ? ((t = n || t === a ? 1 : $(t)), Xe(e, 0, t < 0 ? 0 : t)) : []
                }
                function i1(e, t, n) {
                    var i = e == null ? 0 : e.length
                    return i ? ((t = n || t === a ? 1 : $(t)), (t = i - t), Xe(e, t < 0 ? 0 : t, i)) : []
                }
                function s1(e, t) {
                    return e && e.length ? yr(e, O(t, 3), !1, !0) : []
                }
                function u1(e, t) {
                    return e && e.length ? yr(e, O(t, 3)) : []
                }
                var l1 = U(function (e) {
                        return Dt(we(e, 1, he, !0))
                    }),
                    c1 = U(function (e) {
                        var t = Je(e)
                        return he(t) && (t = a), Dt(we(e, 1, he, !0), O(t, 2))
                    }),
                    o1 = U(function (e) {
                        var t = Je(e)
                        return (t = typeof t == "function" ? t : a), Dt(we(e, 1, he, !0), a, t)
                    })
                function f1(e) {
                    return e && e.length ? Dt(e) : []
                }
                function _1(e, t) {
                    return e && e.length ? Dt(e, O(t, 2)) : []
                }
                function p1(e, t) {
                    return (t = typeof t == "function" ? t : a), e && e.length ? Dt(e, a, t) : []
                }
                function Ya(e) {
                    if (!(e && e.length)) return []
                    var t = 0
                    return (
                        (e = Et(e, function (n) {
                            if (he(n)) return (t = me(n.length, t)), !0
                        })),
                        fa(t, function (n) {
                            return oe(e, la(n))
                        })
                    )
                }
                function Su(e, t) {
                    if (!(e && e.length)) return []
                    var n = Ya(e)
                    return t == null
                        ? n
                        : oe(n, function (i) {
                              return Me(t, a, i)
                          })
                }
                var h1 = U(function (e, t) {
                        return he(e) ? Cn(e, t) : []
                    }),
                    g1 = U(function (e) {
                        return Da(Et(e, he))
                    }),
                    d1 = U(function (e) {
                        var t = Je(e)
                        return he(t) && (t = a), Da(Et(e, he), O(t, 2))
                    }),
                    m1 = U(function (e) {
                        var t = Je(e)
                        return (t = typeof t == "function" ? t : a), Da(Et(e, he), a, t)
                    }),
                    v1 = U(Ya)
                function y1(e, t) {
                    return Us(e || [], t || [], En)
                }
                function b1(e, t) {
                    return Us(e || [], t || [], Rn)
                }
                var w1 = U(function (e) {
                    var t = e.length,
                        n = t > 1 ? e[t - 1] : a
                    return (n = typeof n == "function" ? (e.pop(), n) : a), Su(e, n)
                })
                function xu(e) {
                    var t = l(e)
                    return (t.__chain__ = !0), t
                }
                function k1(e, t) {
                    return t(e), e
                }
                function Cr(e, t) {
                    return t(e)
                }
                var A1 = kt(function (e) {
                    var t = e.length,
                        n = t ? e[0] : 0,
                        i = this.__wrapped__,
                        u = function (o) {
                            return ya(o, e)
                        }
                    return t > 1 || this.__actions__.length || !(i instanceof G) || !At(n)
                        ? this.thru(u)
                        : ((i = i.slice(n, +n + (t ? 1 : 0))),
                          i.__actions__.push({ func: Cr, args: [u], thisArg: a }),
                          new Ye(i, this.__chain__).thru(function (o) {
                              return t && !o.length && o.push(a), o
                          }))
                })
                function S1() {
                    return xu(this)
                }
                function x1() {
                    return new Ye(this.value(), this.__chain__)
                }
                function T1() {
                    this.__values__ === a && (this.__values__ = Wu(this.value()))
                    var e = this.__index__ >= this.__values__.length,
                        t = e ? a : this.__values__[this.__index__++]
                    return { done: e, value: t }
                }
                function E1() {
                    return this
                }
                function C1(e) {
                    for (var t, n = this; n instanceof pr; ) {
                        var i = vu(n)
                        ;(i.__index__ = 0), (i.__values__ = a), t ? (u.__wrapped__ = i) : (t = i)
                        var u = i
                        n = n.__wrapped__
                    }
                    return (u.__wrapped__ = e), t
                }
                function B1() {
                    var e = this.__wrapped__
                    if (e instanceof G) {
                        var t = e
                        return this.__actions__.length && (t = new G(this)), (t = t.reverse()), t.__actions__.push({ func: Cr, args: [Va], thisArg: a }), new Ye(t, this.__chain__)
                    }
                    return this.thru(Va)
                }
                function I1() {
                    return Ns(this.__wrapped__, this.__actions__)
                }
                var R1 = br(function (e, t, n) {
                    re.call(e, n) ? ++e[n] : bt(e, n, 1)
                })
                function D1(e, t, n) {
                    var i = W(e) ? es : kf
                    return n && Te(e, t, n) && (t = a), i(e, O(t, 3))
                }
                function L1(e, t) {
                    var n = W(e) ? Et : Ss
                    return n(e, O(t, 3))
                }
                var O1 = Qs(yu),
                    q1 = Qs(bu)
                function M1(e, t) {
                    return we(Br(e, t), 1)
                }
                function P1(e, t) {
                    return we(Br(e, t), _t)
                }
                function F1(e, t, n) {
                    return (n = n === a ? 1 : $(n)), we(Br(e, t), n)
                }
                function Tu(e, t) {
                    var n = W(e) ? He : Rt
                    return n(e, O(t, 3))
                }
                function Eu(e, t) {
                    var n = W(e) ? to : As
                    return n(e, O(t, 3))
                }
                var W1 = br(function (e, t, n) {
                    re.call(e, n) ? e[n].push(t) : bt(e, n, [t])
                })
                function $1(e, t, n, i) {
                    ;(e = Be(e) ? e : on(e)), (n = n && !i ? $(n) : 0)
                    var u = e.length
                    return n < 0 && (n = me(u + n, 0)), Or(e) ? n <= u && e.indexOf(t, n) > -1 : !!u && Jt(e, t, n) > -1
                }
                var N1 = U(function (e, t, n) {
                        var i = -1,
                            u = typeof t == "function",
                            o = Be(e) ? b(e.length) : []
                        return (
                            Rt(e, function (_) {
                                o[++i] = u ? Me(t, _, n) : Bn(_, t, n)
                            }),
                            o
                        )
                    }),
                    U1 = br(function (e, t, n) {
                        bt(e, n, t)
                    })
                function Br(e, t) {
                    var n = W(e) ? oe : Is
                    return n(e, O(t, 3))
                }
                function K1(e, t, n, i) {
                    return e == null ? [] : (W(t) || (t = t == null ? [] : [t]), (n = i ? a : n), W(n) || (n = n == null ? [] : [n]), Os(e, t, n))
                }
                var z1 = br(
                    function (e, t, n) {
                        e[n ? 0 : 1].push(t)
                    },
                    function () {
                        return [[], []]
                    }
                )
                function G1(e, t, n) {
                    var i = W(e) ? sa : as,
                        u = arguments.length < 3
                    return i(e, O(t, 4), n, u, Rt)
                }
                function H1(e, t, n) {
                    var i = W(e) ? no : as,
                        u = arguments.length < 3
                    return i(e, O(t, 4), n, u, As)
                }
                function V1(e, t) {
                    var n = W(e) ? Et : Ss
                    return n(e, Dr(O(t, 3)))
                }
                function Y1(e) {
                    var t = W(e) ? ys : $f
                    return t(e)
                }
                function Z1(e, t, n) {
                    ;(n ? Te(e, t, n) : t === a) ? (t = 1) : (t = $(t))
                    var i = W(e) ? mf : Nf
                    return i(e, t)
                }
                function X1(e) {
                    var t = W(e) ? vf : Kf
                    return t(e)
                }
                function J1(e) {
                    if (e == null) return 0
                    if (Be(e)) return Or(e) ? jt(e) : e.length
                    var t = Se(e)
                    return t == at || t == it ? e.size : xa(e).length
                }
                function Q1(e, t, n) {
                    var i = W(e) ? ua : zf
                    return n && Te(e, t, n) && (t = a), i(e, O(t, 3))
                }
                var j1 = U(function (e, t) {
                        if (e == null) return []
                        var n = t.length
                        return n > 1 && Te(e, t[0], t[1]) ? (t = []) : n > 2 && Te(t[0], t[1], t[2]) && (t = [t[0]]), Os(e, we(t, 1), [])
                    }),
                    Ir =
                        Do ||
                        function () {
                            return be.Date.now()
                        }
                function e_(e, t) {
                    if (typeof t != "function") throw new Ve(p)
                    return (
                        (e = $(e)),
                        function () {
                            if (--e < 1) return t.apply(this, arguments)
                        }
                    )
                }
                function Cu(e, t, n) {
                    return (t = n ? a : t), (t = e && t == null ? e.length : t), wt(e, J, a, a, a, a, t)
                }
                function Bu(e, t) {
                    var n
                    if (typeof t != "function") throw new Ve(p)
                    return (
                        (e = $(e)),
                        function () {
                            return --e > 0 && (n = t.apply(this, arguments)), e <= 1 && (t = a), n
                        }
                    )
                }
                var Za = U(function (e, t, n) {
                        var i = F
                        if (n.length) {
                            var u = Bt(n, ln(Za))
                            i |= ae
                        }
                        return wt(e, i, t, n, u)
                    }),
                    Iu = U(function (e, t, n) {
                        var i = F | K
                        if (n.length) {
                            var u = Bt(n, ln(Iu))
                            i |= ae
                        }
                        return wt(t, i, e, n, u)
                    })
                function Ru(e, t, n) {
                    t = n ? a : t
                    var i = wt(e, X, a, a, a, a, a, t)
                    return (i.placeholder = Ru.placeholder), i
                }
                function Du(e, t, n) {
                    t = n ? a : t
                    var i = wt(e, se, a, a, a, a, a, t)
                    return (i.placeholder = Du.placeholder), i
                }
                function Lu(e, t, n) {
                    var i,
                        u,
                        o,
                        _,
                        g,
                        v,
                        k = 0,
                        A = !1,
                        x = !1,
                        E = !0
                    if (typeof e != "function") throw new Ve(p)
                    ;(t = Qe(t) || 0), fe(n) && ((A = !!n.leading), (x = "maxWait" in n), (o = x ? me(Qe(n.maxWait) || 0, t) : o), (E = "trailing" in n ? !!n.trailing : E))
                    function R(ge) {
                        var ct = i,
                            Tt = u
                        return (i = u = a), (k = ge), (_ = e.apply(Tt, ct)), _
                    }
                    function q(ge) {
                        return (k = ge), (g = On(z, t)), A ? R(ge) : _
                    }
                    function N(ge) {
                        var ct = ge - v,
                            Tt = ge - k,
                            Qu = t - ct
                        return x ? Ae(Qu, o - Tt) : Qu
                    }
                    function M(ge) {
                        var ct = ge - v,
                            Tt = ge - k
                        return v === a || ct >= t || ct < 0 || (x && Tt >= o)
                    }
                    function z() {
                        var ge = Ir()
                        if (M(ge)) return H(ge)
                        g = On(z, N(ge))
                    }
                    function H(ge) {
                        return (g = a), E && i ? R(ge) : ((i = u = a), _)
                    }
                    function $e() {
                        g !== a && Ks(g), (k = 0), (i = v = u = g = a)
                    }
                    function Ee() {
                        return g === a ? _ : H(Ir())
                    }
                    function Ne() {
                        var ge = Ir(),
                            ct = M(ge)
                        if (((i = arguments), (u = this), (v = ge), ct)) {
                            if (g === a) return q(v)
                            if (x) return Ks(g), (g = On(z, t)), R(v)
                        }
                        return g === a && (g = On(z, t)), _
                    }
                    return (Ne.cancel = $e), (Ne.flush = Ee), Ne
                }
                var t_ = U(function (e, t) {
                        return ks(e, 1, t)
                    }),
                    n_ = U(function (e, t, n) {
                        return ks(e, Qe(t) || 0, n)
                    })
                function r_(e) {
                    return wt(e, ke)
                }
                function Rr(e, t) {
                    if (typeof e != "function" || (t != null && typeof t != "function")) throw new Ve(p)
                    var n = function () {
                        var i = arguments,
                            u = t ? t.apply(this, i) : i[0],
                            o = n.cache
                        if (o.has(u)) return o.get(u)
                        var _ = e.apply(this, i)
                        return (n.cache = o.set(u, _) || o), _
                    }
                    return (n.cache = new (Rr.Cache || yt)()), n
                }
                Rr.Cache = yt
                function Dr(e) {
                    if (typeof e != "function") throw new Ve(p)
                    return function () {
                        var t = arguments
                        switch (t.length) {
                            case 0:
                                return !e.call(this)
                            case 1:
                                return !e.call(this, t[0])
                            case 2:
                                return !e.call(this, t[0], t[1])
                            case 3:
                                return !e.call(this, t[0], t[1], t[2])
                        }
                        return !e.apply(this, t)
                    }
                }
                function a_(e) {
                    return Bu(2, e)
                }
                var i_ = Gf(function (e, t) {
                        t = t.length == 1 && W(t[0]) ? oe(t[0], Pe(O())) : oe(we(t, 1), Pe(O()))
                        var n = t.length
                        return U(function (i) {
                            for (var u = -1, o = Ae(i.length, n); ++u < o; ) i[u] = t[u].call(this, i[u])
                            return Me(e, this, i)
                        })
                    }),
                    Xa = U(function (e, t) {
                        var n = Bt(t, ln(Xa))
                        return wt(e, ae, a, t, n)
                    }),
                    Ou = U(function (e, t) {
                        var n = Bt(t, ln(Ou))
                        return wt(e, ie, a, t, n)
                    }),
                    s_ = kt(function (e, t) {
                        return wt(e, ye, a, a, a, t)
                    })
                function u_(e, t) {
                    if (typeof e != "function") throw new Ve(p)
                    return (t = t === a ? t : $(t)), U(e, t)
                }
                function l_(e, t) {
                    if (typeof e != "function") throw new Ve(p)
                    return (
                        (t = t == null ? 0 : me($(t), 0)),
                        U(function (n) {
                            var i = n[t],
                                u = Ot(n, 0, t)
                            return i && Ct(u, i), Me(e, this, u)
                        })
                    )
                }
                function c_(e, t, n) {
                    var i = !0,
                        u = !0
                    if (typeof e != "function") throw new Ve(p)
                    return fe(n) && ((i = "leading" in n ? !!n.leading : i), (u = "trailing" in n ? !!n.trailing : u)), Lu(e, t, { leading: i, maxWait: t, trailing: u })
                }
                function o_(e) {
                    return Cu(e, 1)
                }
                function f_(e, t) {
                    return Xa(Oa(t), e)
                }
                function __() {
                    if (!arguments.length) return []
                    var e = arguments[0]
                    return W(e) ? e : [e]
                }
                function p_(e) {
                    return Ze(e, I)
                }
                function h_(e, t) {
                    return (t = typeof t == "function" ? t : a), Ze(e, I, t)
                }
                function g_(e) {
                    return Ze(e, C | I)
                }
                function d_(e, t) {
                    return (t = typeof t == "function" ? t : a), Ze(e, C | I, t)
                }
                function m_(e, t) {
                    return t == null || ws(e, t, ve(t))
                }
                function lt(e, t) {
                    return e === t || (e !== e && t !== t)
                }
                var v_ = Sr(ka),
                    y_ = Sr(function (e, t) {
                        return e >= t
                    }),
                    Vt = Es(
                        (function () {
                            return arguments
                        })()
                    )
                        ? Es
                        : function (e) {
                              return _e(e) && re.call(e, "callee") && !ps.call(e, "callee")
                          },
                    W = b.isArray,
                    b_ = Yi ? Pe(Yi) : Cf
                function Be(e) {
                    return e != null && Lr(e.length) && !St(e)
                }
                function he(e) {
                    return _e(e) && Be(e)
                }
                function w_(e) {
                    return e === !0 || e === !1 || (_e(e) && xe(e) == hn)
                }
                var qt = Oo || ui,
                    k_ = Zi ? Pe(Zi) : Bf
                function A_(e) {
                    return _e(e) && e.nodeType === 1 && !qn(e)
                }
                function S_(e) {
                    if (e == null) return !0
                    if (Be(e) && (W(e) || typeof e == "string" || typeof e.splice == "function" || qt(e) || cn(e) || Vt(e))) return !e.length
                    var t = Se(e)
                    if (t == at || t == it) return !e.size
                    if (Ln(e)) return !xa(e).length
                    for (var n in e) if (re.call(e, n)) return !1
                    return !0
                }
                function x_(e, t) {
                    return In(e, t)
                }
                function T_(e, t, n) {
                    n = typeof n == "function" ? n : a
                    var i = n ? n(e, t) : a
                    return i === a ? In(e, t, a, n) : !!i
                }
                function Ja(e) {
                    if (!_e(e)) return !1
                    var t = xe(e)
                    return t == Gn || t == Vl || (typeof e.message == "string" && typeof e.name == "string" && !qn(e))
                }
                function E_(e) {
                    return typeof e == "number" && gs(e)
                }
                function St(e) {
                    if (!fe(e)) return !1
                    var t = xe(e)
                    return t == Hn || t == ki || t == Hl || t == Zl
                }
                function qu(e) {
                    return typeof e == "number" && e == $(e)
                }
                function Lr(e) {
                    return typeof e == "number" && e > -1 && e % 1 == 0 && e <= rt
                }
                function fe(e) {
                    var t = typeof e
                    return e != null && (t == "object" || t == "function")
                }
                function _e(e) {
                    return e != null && typeof e == "object"
                }
                var Mu = Xi ? Pe(Xi) : Rf
                function C_(e, t) {
                    return e === t || Sa(e, t, Na(t))
                }
                function B_(e, t, n) {
                    return (n = typeof n == "function" ? n : a), Sa(e, t, Na(t), n)
                }
                function I_(e) {
                    return Pu(e) && e != +e
                }
                function R_(e) {
                    if (h0(e)) throw new P(h)
                    return Cs(e)
                }
                function D_(e) {
                    return e === null
                }
                function L_(e) {
                    return e == null
                }
                function Pu(e) {
                    return typeof e == "number" || (_e(e) && xe(e) == dn)
                }
                function qn(e) {
                    if (!_e(e) || xe(e) != mt) return !1
                    var t = sr(e)
                    if (t === null) return !0
                    var n = re.call(t, "constructor") && t.constructor
                    return typeof n == "function" && n instanceof n && nr.call(n) == Co
                }
                var Qa = Ji ? Pe(Ji) : Df
                function O_(e) {
                    return qu(e) && e >= -rt && e <= rt
                }
                var Fu = Qi ? Pe(Qi) : Lf
                function Or(e) {
                    return typeof e == "string" || (!W(e) && _e(e) && xe(e) == vn)
                }
                function We(e) {
                    return typeof e == "symbol" || (_e(e) && xe(e) == Vn)
                }
                var cn = ji ? Pe(ji) : Of
                function q_(e) {
                    return e === a
                }
                function M_(e) {
                    return _e(e) && Se(e) == yn
                }
                function P_(e) {
                    return _e(e) && xe(e) == Jl
                }
                var F_ = Sr(Ta),
                    W_ = Sr(function (e, t) {
                        return e <= t
                    })
                function Wu(e) {
                    if (!e) return []
                    if (Be(e)) return Or(e) ? st(e) : Ce(e)
                    if (kn && e[kn]) return go(e[kn]())
                    var t = Se(e),
                        n = t == at ? pa : t == it ? jn : on
                    return n(e)
                }
                function xt(e) {
                    if (!e) return e === 0 ? e : 0
                    if (((e = Qe(e)), e === _t || e === -_t)) {
                        var t = e < 0 ? -1 : 1
                        return t * Un
                    }
                    return e === e ? e : 0
                }
                function $(e) {
                    var t = xt(e),
                        n = t % 1
                    return t === t ? (n ? t - n : t) : 0
                }
                function $u(e) {
                    return e ? Kt($(e), 0, qe) : 0
                }
                function Qe(e) {
                    if (typeof e == "number") return e
                    if (We(e)) return Ft
                    if (fe(e)) {
                        var t = typeof e.valueOf == "function" ? e.valueOf() : e
                        e = fe(t) ? t + "" : t
                    }
                    if (typeof e != "string") return e === 0 ? e : +e
                    e = is(e)
                    var n = vc.test(e)
                    return n || bc.test(e) ? Qc(e.slice(2), n ? 2 : 8) : mc.test(e) ? Ft : +e
                }
                function Nu(e) {
                    return ht(e, Ie(e))
                }
                function $_(e) {
                    return e ? Kt($(e), -rt, rt) : e === 0 ? e : 0
                }
                function te(e) {
                    return e == null ? "" : Fe(e)
                }
                var N_ = sn(function (e, t) {
                        if (Ln(t) || Be(t)) {
                            ht(t, ve(t), e)
                            return
                        }
                        for (var n in t) re.call(t, n) && En(e, n, t[n])
                    }),
                    Uu = sn(function (e, t) {
                        ht(t, Ie(t), e)
                    }),
                    qr = sn(function (e, t, n, i) {
                        ht(t, Ie(t), e, i)
                    }),
                    U_ = sn(function (e, t, n, i) {
                        ht(t, ve(t), e, i)
                    }),
                    K_ = kt(ya)
                function z_(e, t) {
                    var n = an(e)
                    return t == null ? n : bs(n, t)
                }
                var G_ = U(function (e, t) {
                        e = ue(e)
                        var n = -1,
                            i = t.length,
                            u = i > 2 ? t[2] : a
                        for (u && Te(t[0], t[1], u) && (i = 1); ++n < i; )
                            for (var o = t[n], _ = Ie(o), g = -1, v = _.length; ++g < v; ) {
                                var k = _[g],
                                    A = e[k]
                                ;(A === a || (lt(A, tn[k]) && !re.call(e, k))) && (e[k] = o[k])
                            }
                        return e
                    }),
                    H_ = U(function (e) {
                        return e.push(a, iu), Me(Ku, a, e)
                    })
                function V_(e, t) {
                    return ts(e, O(t, 3), pt)
                }
                function Y_(e, t) {
                    return ts(e, O(t, 3), wa)
                }
                function Z_(e, t) {
                    return e == null ? e : ba(e, O(t, 3), Ie)
                }
                function X_(e, t) {
                    return e == null ? e : xs(e, O(t, 3), Ie)
                }
                function J_(e, t) {
                    return e && pt(e, O(t, 3))
                }
                function Q_(e, t) {
                    return e && wa(e, O(t, 3))
                }
                function j_(e) {
                    return e == null ? [] : dr(e, ve(e))
                }
                function ep(e) {
                    return e == null ? [] : dr(e, Ie(e))
                }
                function ja(e, t, n) {
                    var i = e == null ? a : zt(e, t)
                    return i === a ? n : i
                }
                function tp(e, t) {
                    return e != null && lu(e, t, Sf)
                }
                function ei(e, t) {
                    return e != null && lu(e, t, xf)
                }
                var np = eu(function (e, t, n) {
                        t != null && typeof t.toString != "function" && (t = rr.call(t)), (e[t] = n)
                    }, ni(Re)),
                    rp = eu(function (e, t, n) {
                        t != null && typeof t.toString != "function" && (t = rr.call(t)), re.call(e, t) ? e[t].push(n) : (e[t] = [n])
                    }, O),
                    ap = U(Bn)
                function ve(e) {
                    return Be(e) ? vs(e) : xa(e)
                }
                function Ie(e) {
                    return Be(e) ? vs(e, !0) : qf(e)
                }
                function ip(e, t) {
                    var n = {}
                    return (
                        (t = O(t, 3)),
                        pt(e, function (i, u, o) {
                            bt(n, t(i, u, o), i)
                        }),
                        n
                    )
                }
                function sp(e, t) {
                    var n = {}
                    return (
                        (t = O(t, 3)),
                        pt(e, function (i, u, o) {
                            bt(n, u, t(i, u, o))
                        }),
                        n
                    )
                }
                var up = sn(function (e, t, n) {
                        mr(e, t, n)
                    }),
                    Ku = sn(function (e, t, n, i) {
                        mr(e, t, n, i)
                    }),
                    lp = kt(function (e, t) {
                        var n = {}
                        if (e == null) return n
                        var i = !1
                        ;(t = oe(t, function (o) {
                            return (o = Lt(o, e)), i || (i = o.length > 1), o
                        })),
                            ht(e, Wa(e), n),
                            i && (n = Ze(n, C | D | I, n0))
                        for (var u = t.length; u--; ) Ra(n, t[u])
                        return n
                    })
                function cp(e, t) {
                    return zu(e, Dr(O(t)))
                }
                var op = kt(function (e, t) {
                    return e == null ? {} : Pf(e, t)
                })
                function zu(e, t) {
                    if (e == null) return {}
                    var n = oe(Wa(e), function (i) {
                        return [i]
                    })
                    return (
                        (t = O(t)),
                        qs(e, n, function (i, u) {
                            return t(i, u[0])
                        })
                    )
                }
                function fp(e, t, n) {
                    t = Lt(t, e)
                    var i = -1,
                        u = t.length
                    for (u || ((u = 1), (e = a)); ++i < u; ) {
                        var o = e == null ? a : e[gt(t[i])]
                        o === a && ((i = u), (o = n)), (e = St(o) ? o.call(e) : o)
                    }
                    return e
                }
                function _p(e, t, n) {
                    return e == null ? e : Rn(e, t, n)
                }
                function pp(e, t, n, i) {
                    return (i = typeof i == "function" ? i : a), e == null ? e : Rn(e, t, n, i)
                }
                var Gu = ru(ve),
                    Hu = ru(Ie)
                function hp(e, t, n) {
                    var i = W(e),
                        u = i || qt(e) || cn(e)
                    if (((t = O(t, 4)), n == null)) {
                        var o = e && e.constructor
                        u ? (n = i ? new o() : []) : fe(e) ? (n = St(o) ? an(sr(e)) : {}) : (n = {})
                    }
                    return (
                        (u ? He : pt)(e, function (_, g, v) {
                            return t(n, _, g, v)
                        }),
                        n
                    )
                }
                function gp(e, t) {
                    return e == null ? !0 : Ra(e, t)
                }
                function dp(e, t, n) {
                    return e == null ? e : $s(e, t, Oa(n))
                }
                function mp(e, t, n, i) {
                    return (i = typeof i == "function" ? i : a), e == null ? e : $s(e, t, Oa(n), i)
                }
                function on(e) {
                    return e == null ? [] : _a(e, ve(e))
                }
                function vp(e) {
                    return e == null ? [] : _a(e, Ie(e))
                }
                function yp(e, t, n) {
                    return n === a && ((n = t), (t = a)), n !== a && ((n = Qe(n)), (n = n === n ? n : 0)), t !== a && ((t = Qe(t)), (t = t === t ? t : 0)), Kt(Qe(e), t, n)
                }
                function bp(e, t, n) {
                    return (t = xt(t)), n === a ? ((n = t), (t = 0)) : (n = xt(n)), (e = Qe(e)), Tf(e, t, n)
                }
                function wp(e, t, n) {
                    if (
                        (n && typeof n != "boolean" && Te(e, t, n) && (t = n = a),
                        n === a && (typeof t == "boolean" ? ((n = t), (t = a)) : typeof e == "boolean" && ((n = e), (e = a))),
                        e === a && t === a ? ((e = 0), (t = 1)) : ((e = xt(e)), t === a ? ((t = e), (e = 0)) : (t = xt(t))),
                        e > t)
                    ) {
                        var i = e
                        ;(e = t), (t = i)
                    }
                    if (n || e % 1 || t % 1) {
                        var u = ds()
                        return Ae(e + u * (t - e + Jc("1e-" + ((u + "").length - 1))), t)
                    }
                    return Ca(e, t)
                }
                var kp = un(function (e, t, n) {
                    return (t = t.toLowerCase()), e + (n ? Vu(t) : t)
                })
                function Vu(e) {
                    return ti(te(e).toLowerCase())
                }
                function Yu(e) {
                    return (e = te(e)), e && e.replace(kc, oo).replace(Nc, "")
                }
                function Ap(e, t, n) {
                    ;(e = te(e)), (t = Fe(t))
                    var i = e.length
                    n = n === a ? i : Kt($(n), 0, i)
                    var u = n
                    return (n -= t.length), n >= 0 && e.slice(n, u) == t
                }
                function Sp(e) {
                    return (e = te(e)), e && nc.test(e) ? e.replace(xi, fo) : e
                }
                function xp(e) {
                    return (e = te(e)), e && lc.test(e) ? e.replace(Xr, "\\$&") : e
                }
                var Tp = un(function (e, t, n) {
                        return e + (n ? "-" : "") + t.toLowerCase()
                    }),
                    Ep = un(function (e, t, n) {
                        return e + (n ? " " : "") + t.toLowerCase()
                    }),
                    Cp = Js("toLowerCase")
                function Bp(e, t, n) {
                    ;(e = te(e)), (t = $(t))
                    var i = t ? jt(e) : 0
                    if (!t || i >= t) return e
                    var u = (t - i) / 2
                    return Ar(or(u), n) + e + Ar(cr(u), n)
                }
                function Ip(e, t, n) {
                    ;(e = te(e)), (t = $(t))
                    var i = t ? jt(e) : 0
                    return t && i < t ? e + Ar(t - i, n) : e
                }
                function Rp(e, t, n) {
                    ;(e = te(e)), (t = $(t))
                    var i = t ? jt(e) : 0
                    return t && i < t ? Ar(t - i, n) + e : e
                }
                function Dp(e, t, n) {
                    return n || t == null ? (t = 0) : t && (t = +t), Fo(te(e).replace(Jr, ""), t || 0)
                }
                function Lp(e, t, n) {
                    return (n ? Te(e, t, n) : t === a) ? (t = 1) : (t = $(t)), Ba(te(e), t)
                }
                function Op() {
                    var e = arguments,
                        t = te(e[0])
                    return e.length < 3 ? t : t.replace(e[1], e[2])
                }
                var qp = un(function (e, t, n) {
                    return e + (n ? "_" : "") + t.toLowerCase()
                })
                function Mp(e, t, n) {
                    return (
                        n && typeof n != "number" && Te(e, t, n) && (t = n = a),
                        (n = n === a ? qe : n >>> 0),
                        n ? ((e = te(e)), e && (typeof t == "string" || (t != null && !Qa(t))) && ((t = Fe(t)), !t && Qt(e)) ? Ot(st(e), 0, n) : e.split(t, n)) : []
                    )
                }
                var Pp = un(function (e, t, n) {
                    return e + (n ? " " : "") + ti(t)
                })
                function Fp(e, t, n) {
                    return (e = te(e)), (n = n == null ? 0 : Kt($(n), 0, e.length)), (t = Fe(t)), e.slice(n, n + t.length) == t
                }
                function Wp(e, t, n) {
                    var i = l.templateSettings
                    n && Te(e, t, n) && (t = a), (e = te(e)), (t = qr({}, t, i, au))
                    var u = qr({}, t.imports, i.imports, au),
                        o = ve(u),
                        _ = _a(u, o),
                        g,
                        v,
                        k = 0,
                        A = t.interpolate || Yn,
                        x = "__p += '",
                        E = ha((t.escape || Yn).source + "|" + A.source + "|" + (A === Ti ? dc : Yn).source + "|" + (t.evaluate || Yn).source + "|$", "g"),
                        R =
                            "//# sourceURL=" +
                            (re.call(t, "sourceURL") ? (t.sourceURL + "").replace(/\s/g, " ") : "lodash.templateSources[" + ++Hc + "]") +
                            `
`
                    e.replace(E, function (M, z, H, $e, Ee, Ne) {
                        return (
                            H || (H = $e),
                            (x += e.slice(k, Ne).replace(Ac, _o)),
                            z &&
                                ((g = !0),
                                (x +=
                                    `' +
__e(` +
                                    z +
                                    `) +
'`)),
                            Ee &&
                                ((v = !0),
                                (x +=
                                    `';
` +
                                    Ee +
                                    `;
__p += '`)),
                            H &&
                                (x +=
                                    `' +
((__t = (` +
                                    H +
                                    `)) == null ? '' : __t) +
'`),
                            (k = Ne + M.length),
                            M
                        )
                    }),
                        (x += `';
`)
                    var q = re.call(t, "variable") && t.variable
                    if (!q)
                        x =
                            `with (obj) {
` +
                            x +
                            `
}
`
                    else if (hc.test(q)) throw new P(m)
                    ;(x = (v ? x.replace(Ql, "") : x).replace(jl, "$1").replace(ec, "$1;")),
                        (x =
                            "function(" +
                            (q || "obj") +
                            `) {
` +
                            (q
                                ? ""
                                : `obj || (obj = {});
`) +
                            "var __t, __p = ''" +
                            (g ? ", __e = _.escape" : "") +
                            (v
                                ? `, __j = Array.prototype.join;
function print() { __p += __j.call(arguments, '') }
`
                                : `;
`) +
                            x +
                            `return __p
}`)
                    var N = Xu(function () {
                        return ee(o, R + "return " + x).apply(a, _)
                    })
                    if (((N.source = x), Ja(N))) throw N
                    return N
                }
                function $p(e) {
                    return te(e).toLowerCase()
                }
                function Np(e) {
                    return te(e).toUpperCase()
                }
                function Up(e, t, n) {
                    if (((e = te(e)), e && (n || t === a))) return is(e)
                    if (!e || !(t = Fe(t))) return e
                    var i = st(e),
                        u = st(t),
                        o = ss(i, u),
                        _ = us(i, u) + 1
                    return Ot(i, o, _).join("")
                }
                function Kp(e, t, n) {
                    if (((e = te(e)), e && (n || t === a))) return e.slice(0, cs(e) + 1)
                    if (!e || !(t = Fe(t))) return e
                    var i = st(e),
                        u = us(i, st(t)) + 1
                    return Ot(i, 0, u).join("")
                }
                function zp(e, t, n) {
                    if (((e = te(e)), e && (n || t === a))) return e.replace(Jr, "")
                    if (!e || !(t = Fe(t))) return e
                    var i = st(e),
                        u = ss(i, st(t))
                    return Ot(i, u).join("")
                }
                function Gp(e, t) {
                    var n = j,
                        i = Ke
                    if (fe(t)) {
                        var u = "separator" in t ? t.separator : u
                        ;(n = "length" in t ? $(t.length) : n), (i = "omission" in t ? Fe(t.omission) : i)
                    }
                    e = te(e)
                    var o = e.length
                    if (Qt(e)) {
                        var _ = st(e)
                        o = _.length
                    }
                    if (n >= o) return e
                    var g = n - jt(i)
                    if (g < 1) return i
                    var v = _ ? Ot(_, 0, g).join("") : e.slice(0, g)
                    if (u === a) return v + i
                    if ((_ && (g += v.length - g), Qa(u))) {
                        if (e.slice(g).search(u)) {
                            var k,
                                A = v
                            for (u.global || (u = ha(u.source, te(Ei.exec(u)) + "g")), u.lastIndex = 0; (k = u.exec(A)); ) var x = k.index
                            v = v.slice(0, x === a ? g : x)
                        }
                    } else if (e.indexOf(Fe(u), g) != g) {
                        var E = v.lastIndexOf(u)
                        E > -1 && (v = v.slice(0, E))
                    }
                    return v + i
                }
                function Hp(e) {
                    return (e = te(e)), e && tc.test(e) ? e.replace(Si, bo) : e
                }
                var Vp = un(function (e, t, n) {
                        return e + (n ? " " : "") + t.toUpperCase()
                    }),
                    ti = Js("toUpperCase")
                function Zu(e, t, n) {
                    return (e = te(e)), (t = n ? a : t), t === a ? (ho(e) ? Ao(e) : io(e)) : e.match(t) || []
                }
                var Xu = U(function (e, t) {
                        try {
                            return Me(e, a, t)
                        } catch (n) {
                            return Ja(n) ? n : new P(n)
                        }
                    }),
                    Yp = kt(function (e, t) {
                        return (
                            He(t, function (n) {
                                ;(n = gt(n)), bt(e, n, Za(e[n], e))
                            }),
                            e
                        )
                    })
                function Zp(e) {
                    var t = e == null ? 0 : e.length,
                        n = O()
                    return (
                        (e = t
                            ? oe(e, function (i) {
                                  if (typeof i[1] != "function") throw new Ve(p)
                                  return [n(i[0]), i[1]]
                              })
                            : []),
                        U(function (i) {
                            for (var u = -1; ++u < t; ) {
                                var o = e[u]
                                if (Me(o[0], this, i)) return Me(o[1], this, i)
                            }
                        })
                    )
                }
                function Xp(e) {
                    return wf(Ze(e, C))
                }
                function ni(e) {
                    return function () {
                        return e
                    }
                }
                function Jp(e, t) {
                    return e == null || e !== e ? t : e
                }
                var Qp = js(),
                    jp = js(!0)
                function Re(e) {
                    return e
                }
                function ri(e) {
                    return Bs(typeof e == "function" ? e : Ze(e, C))
                }
                function eh(e) {
                    return Rs(Ze(e, C))
                }
                function th(e, t) {
                    return Ds(e, Ze(t, C))
                }
                var nh = U(function (e, t) {
                        return function (n) {
                            return Bn(n, e, t)
                        }
                    }),
                    rh = U(function (e, t) {
                        return function (n) {
                            return Bn(e, n, t)
                        }
                    })
                function ai(e, t, n) {
                    var i = ve(t),
                        u = dr(t, i)
                    n == null && !(fe(t) && (u.length || !i.length)) && ((n = t), (t = e), (e = this), (u = dr(t, ve(t))))
                    var o = !(fe(n) && "chain" in n) || !!n.chain,
                        _ = St(e)
                    return (
                        He(u, function (g) {
                            var v = t[g]
                            ;(e[g] = v),
                                _ &&
                                    (e.prototype[g] = function () {
                                        var k = this.__chain__
                                        if (o || k) {
                                            var A = e(this.__wrapped__),
                                                x = (A.__actions__ = Ce(this.__actions__))
                                            return x.push({ func: v, args: arguments, thisArg: e }), (A.__chain__ = k), A
                                        }
                                        return v.apply(e, Ct([this.value()], arguments))
                                    })
                        }),
                        e
                    )
                }
                function ah() {
                    return be._ === this && (be._ = Bo), this
                }
                function ii() {}
                function ih(e) {
                    return (
                        (e = $(e)),
                        U(function (t) {
                            return Ls(t, e)
                        })
                    )
                }
                var sh = Ma(oe),
                    uh = Ma(es),
                    lh = Ma(ua)
                function Ju(e) {
                    return Ka(e) ? la(gt(e)) : Ff(e)
                }
                function ch(e) {
                    return function (t) {
                        return e == null ? a : zt(e, t)
                    }
                }
                var oh = tu(),
                    fh = tu(!0)
                function si() {
                    return []
                }
                function ui() {
                    return !1
                }
                function _h() {
                    return {}
                }
                function ph() {
                    return ""
                }
                function hh() {
                    return !0
                }
                function gh(e, t) {
                    if (((e = $(e)), e < 1 || e > rt)) return []
                    var n = qe,
                        i = Ae(e, qe)
                    ;(t = O(t)), (e -= qe)
                    for (var u = fa(i, t); ++n < e; ) t(n)
                    return u
                }
                function dh(e) {
                    return W(e) ? oe(e, gt) : We(e) ? [e] : Ce(mu(te(e)))
                }
                function mh(e) {
                    var t = ++Eo
                    return te(e) + t
                }
                var vh = kr(function (e, t) {
                        return e + t
                    }, 0),
                    yh = Pa("ceil"),
                    bh = kr(function (e, t) {
                        return e / t
                    }, 1),
                    wh = Pa("floor")
                function kh(e) {
                    return e && e.length ? gr(e, Re, ka) : a
                }
                function Ah(e, t) {
                    return e && e.length ? gr(e, O(t, 2), ka) : a
                }
                function Sh(e) {
                    return rs(e, Re)
                }
                function xh(e, t) {
                    return rs(e, O(t, 2))
                }
                function Th(e) {
                    return e && e.length ? gr(e, Re, Ta) : a
                }
                function Eh(e, t) {
                    return e && e.length ? gr(e, O(t, 2), Ta) : a
                }
                var Ch = kr(function (e, t) {
                        return e * t
                    }, 1),
                    Bh = Pa("round"),
                    Ih = kr(function (e, t) {
                        return e - t
                    }, 0)
                function Rh(e) {
                    return e && e.length ? oa(e, Re) : 0
                }
                function Dh(e, t) {
                    return e && e.length ? oa(e, O(t, 2)) : 0
                }
                return (
                    (l.after = e_),
                    (l.ary = Cu),
                    (l.assign = N_),
                    (l.assignIn = Uu),
                    (l.assignInWith = qr),
                    (l.assignWith = U_),
                    (l.at = K_),
                    (l.before = Bu),
                    (l.bind = Za),
                    (l.bindAll = Yp),
                    (l.bindKey = Iu),
                    (l.castArray = __),
                    (l.chain = xu),
                    (l.chunk = w0),
                    (l.compact = k0),
                    (l.concat = A0),
                    (l.cond = Zp),
                    (l.conforms = Xp),
                    (l.constant = ni),
                    (l.countBy = R1),
                    (l.create = z_),
                    (l.curry = Ru),
                    (l.curryRight = Du),
                    (l.debounce = Lu),
                    (l.defaults = G_),
                    (l.defaultsDeep = H_),
                    (l.defer = t_),
                    (l.delay = n_),
                    (l.difference = S0),
                    (l.differenceBy = x0),
                    (l.differenceWith = T0),
                    (l.drop = E0),
                    (l.dropRight = C0),
                    (l.dropRightWhile = B0),
                    (l.dropWhile = I0),
                    (l.fill = R0),
                    (l.filter = L1),
                    (l.flatMap = M1),
                    (l.flatMapDeep = P1),
                    (l.flatMapDepth = F1),
                    (l.flatten = wu),
                    (l.flattenDeep = D0),
                    (l.flattenDepth = L0),
                    (l.flip = r_),
                    (l.flow = Qp),
                    (l.flowRight = jp),
                    (l.fromPairs = O0),
                    (l.functions = j_),
                    (l.functionsIn = ep),
                    (l.groupBy = W1),
                    (l.initial = M0),
                    (l.intersection = P0),
                    (l.intersectionBy = F0),
                    (l.intersectionWith = W0),
                    (l.invert = np),
                    (l.invertBy = rp),
                    (l.invokeMap = N1),
                    (l.iteratee = ri),
                    (l.keyBy = U1),
                    (l.keys = ve),
                    (l.keysIn = Ie),
                    (l.map = Br),
                    (l.mapKeys = ip),
                    (l.mapValues = sp),
                    (l.matches = eh),
                    (l.matchesProperty = th),
                    (l.memoize = Rr),
                    (l.merge = up),
                    (l.mergeWith = Ku),
                    (l.method = nh),
                    (l.methodOf = rh),
                    (l.mixin = ai),
                    (l.negate = Dr),
                    (l.nthArg = ih),
                    (l.omit = lp),
                    (l.omitBy = cp),
                    (l.once = a_),
                    (l.orderBy = K1),
                    (l.over = sh),
                    (l.overArgs = i_),
                    (l.overEvery = uh),
                    (l.overSome = lh),
                    (l.partial = Xa),
                    (l.partialRight = Ou),
                    (l.partition = z1),
                    (l.pick = op),
                    (l.pickBy = zu),
                    (l.property = Ju),
                    (l.propertyOf = ch),
                    (l.pull = K0),
                    (l.pullAll = Au),
                    (l.pullAllBy = z0),
                    (l.pullAllWith = G0),
                    (l.pullAt = H0),
                    (l.range = oh),
                    (l.rangeRight = fh),
                    (l.rearg = s_),
                    (l.reject = V1),
                    (l.remove = V0),
                    (l.rest = u_),
                    (l.reverse = Va),
                    (l.sampleSize = Z1),
                    (l.set = _p),
                    (l.setWith = pp),
                    (l.shuffle = X1),
                    (l.slice = Y0),
                    (l.sortBy = j1),
                    (l.sortedUniq = t1),
                    (l.sortedUniqBy = n1),
                    (l.split = Mp),
                    (l.spread = l_),
                    (l.tail = r1),
                    (l.take = a1),
                    (l.takeRight = i1),
                    (l.takeRightWhile = s1),
                    (l.takeWhile = u1),
                    (l.tap = k1),
                    (l.throttle = c_),
                    (l.thru = Cr),
                    (l.toArray = Wu),
                    (l.toPairs = Gu),
                    (l.toPairsIn = Hu),
                    (l.toPath = dh),
                    (l.toPlainObject = Nu),
                    (l.transform = hp),
                    (l.unary = o_),
                    (l.union = l1),
                    (l.unionBy = c1),
                    (l.unionWith = o1),
                    (l.uniq = f1),
                    (l.uniqBy = _1),
                    (l.uniqWith = p1),
                    (l.unset = gp),
                    (l.unzip = Ya),
                    (l.unzipWith = Su),
                    (l.update = dp),
                    (l.updateWith = mp),
                    (l.values = on),
                    (l.valuesIn = vp),
                    (l.without = h1),
                    (l.words = Zu),
                    (l.wrap = f_),
                    (l.xor = g1),
                    (l.xorBy = d1),
                    (l.xorWith = m1),
                    (l.zip = v1),
                    (l.zipObject = y1),
                    (l.zipObjectDeep = b1),
                    (l.zipWith = w1),
                    (l.entries = Gu),
                    (l.entriesIn = Hu),
                    (l.extend = Uu),
                    (l.extendWith = qr),
                    ai(l, l),
                    (l.add = vh),
                    (l.attempt = Xu),
                    (l.camelCase = kp),
                    (l.capitalize = Vu),
                    (l.ceil = yh),
                    (l.clamp = yp),
                    (l.clone = p_),
                    (l.cloneDeep = g_),
                    (l.cloneDeepWith = d_),
                    (l.cloneWith = h_),
                    (l.conformsTo = m_),
                    (l.deburr = Yu),
                    (l.defaultTo = Jp),
                    (l.divide = bh),
                    (l.endsWith = Ap),
                    (l.eq = lt),
                    (l.escape = Sp),
                    (l.escapeRegExp = xp),
                    (l.every = D1),
                    (l.find = O1),
                    (l.findIndex = yu),
                    (l.findKey = V_),
                    (l.findLast = q1),
                    (l.findLastIndex = bu),
                    (l.findLastKey = Y_),
                    (l.floor = wh),
                    (l.forEach = Tu),
                    (l.forEachRight = Eu),
                    (l.forIn = Z_),
                    (l.forInRight = X_),
                    (l.forOwn = J_),
                    (l.forOwnRight = Q_),
                    (l.get = ja),
                    (l.gt = v_),
                    (l.gte = y_),
                    (l.has = tp),
                    (l.hasIn = ei),
                    (l.head = ku),
                    (l.identity = Re),
                    (l.includes = $1),
                    (l.indexOf = q0),
                    (l.inRange = bp),
                    (l.invoke = ap),
                    (l.isArguments = Vt),
                    (l.isArray = W),
                    (l.isArrayBuffer = b_),
                    (l.isArrayLike = Be),
                    (l.isArrayLikeObject = he),
                    (l.isBoolean = w_),
                    (l.isBuffer = qt),
                    (l.isDate = k_),
                    (l.isElement = A_),
                    (l.isEmpty = S_),
                    (l.isEqual = x_),
                    (l.isEqualWith = T_),
                    (l.isError = Ja),
                    (l.isFinite = E_),
                    (l.isFunction = St),
                    (l.isInteger = qu),
                    (l.isLength = Lr),
                    (l.isMap = Mu),
                    (l.isMatch = C_),
                    (l.isMatchWith = B_),
                    (l.isNaN = I_),
                    (l.isNative = R_),
                    (l.isNil = L_),
                    (l.isNull = D_),
                    (l.isNumber = Pu),
                    (l.isObject = fe),
                    (l.isObjectLike = _e),
                    (l.isPlainObject = qn),
                    (l.isRegExp = Qa),
                    (l.isSafeInteger = O_),
                    (l.isSet = Fu),
                    (l.isString = Or),
                    (l.isSymbol = We),
                    (l.isTypedArray = cn),
                    (l.isUndefined = q_),
                    (l.isWeakMap = M_),
                    (l.isWeakSet = P_),
                    (l.join = $0),
                    (l.kebabCase = Tp),
                    (l.last = Je),
                    (l.lastIndexOf = N0),
                    (l.lowerCase = Ep),
                    (l.lowerFirst = Cp),
                    (l.lt = F_),
                    (l.lte = W_),
                    (l.max = kh),
                    (l.maxBy = Ah),
                    (l.mean = Sh),
                    (l.meanBy = xh),
                    (l.min = Th),
                    (l.minBy = Eh),
                    (l.stubArray = si),
                    (l.stubFalse = ui),
                    (l.stubObject = _h),
                    (l.stubString = ph),
                    (l.stubTrue = hh),
                    (l.multiply = Ch),
                    (l.nth = U0),
                    (l.noConflict = ah),
                    (l.noop = ii),
                    (l.now = Ir),
                    (l.pad = Bp),
                    (l.padEnd = Ip),
                    (l.padStart = Rp),
                    (l.parseInt = Dp),
                    (l.random = wp),
                    (l.reduce = G1),
                    (l.reduceRight = H1),
                    (l.repeat = Lp),
                    (l.replace = Op),
                    (l.result = fp),
                    (l.round = Bh),
                    (l.runInContext = d),
                    (l.sample = Y1),
                    (l.size = J1),
                    (l.snakeCase = qp),
                    (l.some = Q1),
                    (l.sortedIndex = Z0),
                    (l.sortedIndexBy = X0),
                    (l.sortedIndexOf = J0),
                    (l.sortedLastIndex = Q0),
                    (l.sortedLastIndexBy = j0),
                    (l.sortedLastIndexOf = e1),
                    (l.startCase = Pp),
                    (l.startsWith = Fp),
                    (l.subtract = Ih),
                    (l.sum = Rh),
                    (l.sumBy = Dh),
                    (l.template = Wp),
                    (l.times = gh),
                    (l.toFinite = xt),
                    (l.toInteger = $),
                    (l.toLength = $u),
                    (l.toLower = $p),
                    (l.toNumber = Qe),
                    (l.toSafeInteger = $_),
                    (l.toString = te),
                    (l.toUpper = Np),
                    (l.trim = Up),
                    (l.trimEnd = Kp),
                    (l.trimStart = zp),
                    (l.truncate = Gp),
                    (l.unescape = Hp),
                    (l.uniqueId = mh),
                    (l.upperCase = Vp),
                    (l.upperFirst = ti),
                    (l.each = Tu),
                    (l.eachRight = Eu),
                    (l.first = ku),
                    ai(
                        l,
                        (function () {
                            var e = {}
                            return (
                                pt(l, function (t, n) {
                                    re.call(l.prototype, n) || (e[n] = t)
                                }),
                                e
                            )
                        })(),
                        { chain: !1 }
                    ),
                    (l.VERSION = c),
                    He(["bind", "bindKey", "curry", "curryRight", "partial", "partialRight"], function (e) {
                        l[e].placeholder = l
                    }),
                    He(["drop", "take"], function (e, t) {
                        ;(G.prototype[e] = function (n) {
                            n = n === a ? 1 : me($(n), 0)
                            var i = this.__filtered__ && !t ? new G(this) : this.clone()
                            return i.__filtered__ ? (i.__takeCount__ = Ae(n, i.__takeCount__)) : i.__views__.push({ size: Ae(n, qe), type: e + (i.__dir__ < 0 ? "Right" : "") }), i
                        }),
                            (G.prototype[e + "Right"] = function (n) {
                                return this.reverse()[e](n).reverse()
                            })
                    }),
                    He(["filter", "map", "takeWhile"], function (e, t) {
                        var n = t + 1,
                            i = n == Oe || n == Nn
                        G.prototype[e] = function (u) {
                            var o = this.clone()
                            return o.__iteratees__.push({ iteratee: O(u, 3), type: n }), (o.__filtered__ = o.__filtered__ || i), o
                        }
                    }),
                    He(["head", "last"], function (e, t) {
                        var n = "take" + (t ? "Right" : "")
                        G.prototype[e] = function () {
                            return this[n](1).value()[0]
                        }
                    }),
                    He(["initial", "tail"], function (e, t) {
                        var n = "drop" + (t ? "" : "Right")
                        G.prototype[e] = function () {
                            return this.__filtered__ ? new G(this) : this[n](1)
                        }
                    }),
                    (G.prototype.compact = function () {
                        return this.filter(Re)
                    }),
                    (G.prototype.find = function (e) {
                        return this.filter(e).head()
                    }),
                    (G.prototype.findLast = function (e) {
                        return this.reverse().find(e)
                    }),
                    (G.prototype.invokeMap = U(function (e, t) {
                        return typeof e == "function"
                            ? new G(this)
                            : this.map(function (n) {
                                  return Bn(n, e, t)
                              })
                    })),
                    (G.prototype.reject = function (e) {
                        return this.filter(Dr(O(e)))
                    }),
                    (G.prototype.slice = function (e, t) {
                        e = $(e)
                        var n = this
                        return n.__filtered__ && (e > 0 || t < 0)
                            ? new G(n)
                            : (e < 0 ? (n = n.takeRight(-e)) : e && (n = n.drop(e)), t !== a && ((t = $(t)), (n = t < 0 ? n.dropRight(-t) : n.take(t - e))), n)
                    }),
                    (G.prototype.takeRightWhile = function (e) {
                        return this.reverse().takeWhile(e).reverse()
                    }),
                    (G.prototype.toArray = function () {
                        return this.take(qe)
                    }),
                    pt(G.prototype, function (e, t) {
                        var n = /^(?:filter|find|map|reject)|While$/.test(t),
                            i = /^(?:head|last)$/.test(t),
                            u = l[i ? "take" + (t == "last" ? "Right" : "") : t],
                            o = i || /^find/.test(t)
                        !u ||
                            (l.prototype[t] = function () {
                                var _ = this.__wrapped__,
                                    g = i ? [1] : arguments,
                                    v = _ instanceof G,
                                    k = g[0],
                                    A = v || W(_),
                                    x = function (z) {
                                        var H = u.apply(l, Ct([z], g))
                                        return i && E ? H[0] : H
                                    }
                                A && n && typeof k == "function" && k.length != 1 && (v = A = !1)
                                var E = this.__chain__,
                                    R = !!this.__actions__.length,
                                    q = o && !E,
                                    N = v && !R
                                if (!o && A) {
                                    _ = N ? _ : new G(this)
                                    var M = e.apply(_, g)
                                    return M.__actions__.push({ func: Cr, args: [x], thisArg: a }), new Ye(M, E)
                                }
                                return q && N ? e.apply(this, g) : ((M = this.thru(x)), q ? (i ? M.value()[0] : M.value()) : M)
                            })
                    }),
                    He(["pop", "push", "shift", "sort", "splice", "unshift"], function (e) {
                        var t = er[e],
                            n = /^(?:push|sort|unshift)$/.test(e) ? "tap" : "thru",
                            i = /^(?:pop|shift)$/.test(e)
                        l.prototype[e] = function () {
                            var u = arguments
                            if (i && !this.__chain__) {
                                var o = this.value()
                                return t.apply(W(o) ? o : [], u)
                            }
                            return this[n](function (_) {
                                return t.apply(W(_) ? _ : [], u)
                            })
                        }
                    }),
                    pt(G.prototype, function (e, t) {
                        var n = l[t]
                        if (n) {
                            var i = n.name + ""
                            re.call(rn, i) || (rn[i] = []), rn[i].push({ name: t, func: n })
                        }
                    }),
                    (rn[wr(a, K).name] = [{ name: "wrapper", func: a }]),
                    (G.prototype.clone = Go),
                    (G.prototype.reverse = Ho),
                    (G.prototype.value = Vo),
                    (l.prototype.at = A1),
                    (l.prototype.chain = S1),
                    (l.prototype.commit = x1),
                    (l.prototype.next = T1),
                    (l.prototype.plant = C1),
                    (l.prototype.reverse = B1),
                    (l.prototype.toJSON = l.prototype.valueOf = l.prototype.value = I1),
                    (l.prototype.first = l.prototype.head),
                    kn && (l.prototype[kn] = E1),
                    l
                )
            },
            en = So()
        Wt ? (((Wt.exports = en)._ = en), (ra._ = en)) : (be._ = en)
    }.call(Pn))
})(yi, yi.exports)
var A3 = [
    {
        name: "pet_2021_spring_normal",
        icon: "/icon/pet/2021_spring_normal.png",
        effect(r) {
            r.changeStatus({ quadra: 150, elemental_boost: 25, crirate: 10 }),
                r.changeDamage({ bonus_damage: 0.1, triatk_per: 0.12 }),
                r.changeSkill({ start: 1, end: 80, value: 1 }),
                r.changeSkill({ start: 1, end: -1, type: "cd_raduction", value: 0.05 })
        }
    }
]
const Y = ci("character", {
    state() {
        return {
            character_list: Sl,
            character_level: 100,
            equip_columns: {},
            avatar_columns: {},
            pet_column: v3(),
            siroco_columns: [void 0, void 0, void 0],
            ozma_columns: [void 0, void 0, void 0, void 0, void 0],
            talismans_columns: [1, 2, 3],
            base_setting: { adventurer_level: 40, guild_buff: !0, apc: { type: "phyatk", intstr: 5e3, attack: 3e3 }, dungeon_buff: "ozma" }
        }
    },
    getters: {
        using_equips() {
            const r = []
            for (let s in this.equip_columns) {
                const a = this.equip_columns[s]
                a.item && r.push(a.item)
            }
            return r
        },
        enchant_list() {
            return hl(w3).filter(Mr)
        },
        emblem_list() {
            return hl(y3).filter(Mr)
        },
        avatar_list() {
            return p5.map(r => {
                let { options: s } = r
                return (s = pl(s)), ft(ot({}, r), { options: s })
            })
        }
    },
    actions: {
        setOzma(r, s) {
            this.ozma_columns[r] = s
        },
        clarOzma() {
            this.ozma_columns = this.ozma_columns.map(r => {})
        },
        setSiroco(r, s) {
            this.siroco_columns[r] = s
        },
        setEquipSuit(r) {
            if (this.character) {
                const s = xg(r)
                for (let a of s) this.setEquip(a.part, a)
            }
        },
        setEquip(r, s) {
            const a = this.equip_columns[r]
            a && (a.item = s)
        },
        setAvatar(r, s) {
            const a = this.avatar_columns[r]
            a && (a.item = s)
        },
        setAvatarSuit(r) {
            if (!this.character) return
            const s = this.avatar_list.filter(a => a.suit_name == r)
            for (let a of s) this.setAvatar(a.part, a)
        },
        newCharacter(r) {
            var f
            if (!this.character_list[r]) throw new Error(`not found character by ${r}.`)
            const s = new z2(Sl[r])
            ;(this.equip_columns = S3(s)), (this.avatar_columns = x3()), (this.character = s)
            const a = vm(),
                c = J3()
            return (
                c.load((f = this.character) == null ? void 0 : f.alter),
                this.$subscribe(
                    yi.exports.debounce(async () => {
                        var h
                        c.save((h = this.character) == null ? void 0 : h.alter), await a.calculate_self()
                    }, 20)
                ),
                this.setPet(A3[0]),
                s
            )
        },
        setColumnData(r, s, a) {
            const c = this.equip_columns[r]
            c && c.data.set(s, a)
        },
        getColumnData(r, s) {
            const a = this.equip_columns[r]
            if (a) return a.data.get(s)
        },
        setPet(r) {
            this.pet_column.item = r
        }
    }
})
function S3(r) {
    let s = {}
    for (let a of gl) {
        const c = []
        for (let h of rd) {
            const p = h(a)
            p && c.push(p)
        }
        const f = new Map()
        a == "weapon" && f.set("refine_level", 8),
            f.set("has_cursed", !0),
            f.set("upgrade_level", a == "earrings" ? 12 : 10),
            f.set("adapt_status", r == null ? void 0 : r.adapt_status),
            f.set("armor_type", r == null ? void 0 : r.armor_type),
            (s[a] = { part: a, slots: c, data: f })
    }
    return s
}
function x3() {
    let r = {}
    for (let s of ul) {
        const a = [],
            c = new Map()
        r[s] = { part: s, slots: a, data: c }
    }
    return r
}
function e4(r) {
    const { character: s } = Y()
    if (s) {
        const { job: a, alter: c } = s
        return (r = r.replace(`${a}.${c}.`, `${c}/skill/`)), `/resources/${r}.png`
    }
    return ""
}
function t4(r) {
    const { character_list: s } = Y()
    return !!s[r]
}
function T3(r) {
    var a
    const { character: s } = Y()
    return s && (a = s.care_status[r]) != null ? a : 0
}
function Ue() {
    const { character: r } = Y()
    return !!(r == null ? void 0 : r.is_buffer)
}
function n4(r) {
    var a
    const { equip_columns: s } = Y()
    return (a = s[r]) == null ? void 0 : a.item
}
function r4(...r) {
    const { using_equips: s } = Y()
    for (let a of r) if (!s.some(c => c.name == a)) return !1
    return r.length > 0
}
function tt(r) {
    const { siroco_columns: s } = Y()
    return s.some(a => (a == null ? void 0 : a.name) == r)
}
function a4(r, s) {
    var c
    const { ozma_columns: a } = Y()
    return ((c = a[s]) == null ? void 0 : c.name) == r
}
function Fn(r) {
    r.changeBuff({ buff_intstr_per: 0.03 })
}
function Wn(r) {
    r.changeBuff({ awake_intstr_per: 0.03 })
}
function $n(r) {
    r.changeBuff({ passive_alter: 80 })
}
const E3 = [
    {
        part: "pants",
        suit_name: "assassin",
        effect(r) {
            Fn(r), tt("asassin_ring") && r.changeBuff({ awake_intstr_add: 28 })
        }
    },
    {
        part: "ring",
        suit_name: "assassin",
        effect(r) {
            Wn(r), tt("asassin_subequip") && r.changeBuff({ passive_alter: 55 })
        }
    },
    {
        part: "subequip",
        suit_name: "assassin",
        effect(r) {
            $n(r), tt("asassin_pants") && r.changeBuff({ buff_intstr_per: 0.01, buff_triatk_per: 0.01 })
        }
    }
]
var C3 = E3.map(r => ((r.name = `${r.suit_name}_${r.part}`), (r.icon = `/icon/siroco/${r.name}.png`), r))
const B3 = [
    {
        part: "pants",
        suit_name: "lodos",
        effect(r) {
            Fn(r), tt("lodos_ring") && r.changeBuff({ awake_intstr_per: 0.02 })
        }
    },
    {
        part: "ring",
        suit_name: "lodos",
        effect(r) {
            Wn(r), tt("lodos_subequip") && r.changeBuff({ buff_intstr_per: 0.02 })
        }
    },
    {
        part: "subequip",
        suit_name: "lodos",
        effect(r) {
            $n(r), tt("lodos_pants") && r.changeBuff({ buff_triatk_per: 0.01, passive_alter: 30 })
        }
    }
]
var I3 = B3.map(r => ((r.name = `${r.suit_name}_${r.part}`), (r.icon = `/icon/siroco/${r.name}.png`), r))
const R3 = [
    {
        part: "pants",
        suit_name: "nameless",
        effect(r) {
            Fn(r), tt("nameless_ring") && r.changeBuff({ awake_intstr_add: 20, awake_intstr_per: 0.01 })
        }
    },
    {
        part: "ring",
        suit_name: "nameless",
        effect(r) {
            Wn(r), tt("nameless_subequip") && r.changeBuff({ passive_alter: 55 })
        }
    },
    {
        part: "subequip",
        suit_name: "nameless",
        effect(r) {
            $n(r), tt("nameless_pants") && r.changeBuff({ buff_triatk_per: 0.01, passive_alter: 30 })
        }
    }
]
var D3 = R3.map(r => ((r.name = `${r.suit_name}_${r.part}`), (r.icon = `/icon/siroco/${r.name}.png`), r))
const L3 = [
    {
        part: "pants",
        suit_name: "nex",
        effect(r) {
            Fn(r), tt("nex_ring") && r.changeBuff({ awake_intstr_add: 40 })
        }
    },
    {
        part: "ring",
        suit_name: "nex",
        effect(r) {
            Wn(r), tt("nex_subequip") && r.changeBuff({ passive_alter: 80 })
        }
    },
    {
        suit_name: "nex",
        part: "subequip",
        effect(r) {
            $n(r), tt("nex_pants") && r.changeBuff({ buff_triatk_per: 0.02 })
        }
    }
]
var O3 = L3.map(r => ((r.name = `${r.suit_name}_${r.part}`), (r.icon = `/icon/siroco/${r.name}.png`), r))
const q3 = [
    { part: "pants", suit_name: "roxy", effect: Fn },
    { part: "ring", suit_name: "roxy", effect: Wn },
    { part: "subequip", suit_name: "roxy", effect: $n }
]
var M3 = q3.map(r => ((r.name = `${r.suit_name}_${r.part}`), (r.icon = `/icon/siroco/${r.name}.png`), r))
const i4 = ["nex", "assassin", "roxy", "nameless", "lodos"],
    P3 = [O3, C3, M3, D3, I3].reduce((r, s) => r.concat(s)).map(r => ((r.fame = 116), r)),
    F3 = [
        {
            name: "astaroth",
            fame: 116,
            effect(r) {
                r.changeBuff({ buff_triatk_per: 0.01, awake_intstr_add: 22, passive_alter: 55 })
            }
        },
        {
            name: "berias",
            fame: 116,
            effect(r) {
                r.changeBuff({ passive_alter: 138 })
            }
        },
        {
            name: "redmayne",
            fame: 116,
            effect(r) {
                r.changeBuff({ buff_intstr_per: 0.02, awake_intstr_add: 25, passive_alter: 46 })
            }
        },
        {
            name: "rosenbach",
            fame: 116,
            effect(r) {
                r.changeBuff({ buff_intstr_per: 0.01, buff_triatk_per: 0.01, awake_intstr_add: 37 })
            }
        },
        {
            name: "timat",
            fame: 116,
            effect(r) {
                r.changeBuff({ buff_intstr_per: 0.03, awake_level_per: 0.01, awake_intstr_add: 21 })
            }
        }
    ],
    bi = ["pants", "ring", "subequip", "weapon"],
    W3 = ["intstr_per", "triatk_per", "critical_damage", "additional_damage", "final_damage", "bonus_damage"]
function $3(r) {
    const s = []
    for (let a of r)
        s.push({
            name: `awake_modifier_${a}`,
            key: [a, "awake"],
            effect(c) {
                c.changeSkill({ condition: { levels: [50, 75, 100], type: V.AWAKE }, value: 2 })
            }
        })
    return s
}
var Tl = $3(W3),
    N3 = {
        name: "awake",
        key: ["awake", "awake"],
        effect(r) {
            r.changeSkill({ condition: { levels: [50, 75, 100], type: V.AWAKE }, value: 2 })
        }
    }
const U3 = [
        { name: "intstr_per", key: ["intstr_per", "passive_alter"], buff_data: [20, 100, 160], effect(r) {} },
        {
            name: "triatk_per",
            key: ["triatk_per", "awake_intstr_add"],
            buff_data: [10, 10, 30],
            effect(r, s) {
                s && r.changeBuff({ buff_level_add: 1 })
            }
        },
        {
            name: "critical_damage",
            key: ["critical_damage", "buff_intstr_per"],
            buff_data: [0.01, 0.02, 0.05],
            effect(r, s) {
                s && r.changeBuff({ awake_intstr_per: 0.02 })
            }
        },
        {
            name: "additional_damage",
            key: ["additional_damage", "awake_intstr_add"],
            buff_data: [10, 10, 40],
            effect(r, s) {
                s && r.changeBuff({ buff_intstr_per: 0.04 })
            }
        },
        {
            name: "final_damage",
            key: ["final_damage", "awake_intstr_per"],
            buff_data: [0.01, 0.01, 0.04],
            effect(r, s) {
                s && r.changeBuff({ buff_triatk_per: 0.02 })
            }
        },
        {
            name: "bonus_damage",
            key: ["bonus_damage", "buff_triatk_per"],
            buff_data: [0.01, 0.01, 0.04],
            effect(r, s) {
                s && r.changeBuff({ awake_intstr_add: 25 })
            }
        }
    ],
    K3 = [
        { name: "intstr_per", buff_data: [20, 120, 180], key: ["intstr_per", "passive_alter"], effect(r) {} },
        {
            name: "triatk_per",
            key: ["triatk_per", "buff_intstr_per"],
            buff_data: [0.01, 0.01, 0.04],
            effect(r) {
                r.changeBuff({ awake_level_add: 1 })
            }
        },
        {
            name: "critical_damage",
            key: ["critical_damage", "buff_intstr_per"],
            buff_data: [0.01, 0.02, 0.05],
            effect(r) {
                r.changeBuff({ awake_intstr_per: 0.03 })
            }
        },
        {
            name: "additional_damage",
            key: ["additional_damage", "awake_intstr_add"],
            buff_data: [10, 30, 60],
            effect(r) {
                r.changeBuff({ buff_intstr_per: 0.03 })
            }
        },
        {
            name: "final_damage",
            key: ["final_damage", "awake_intstr_per"],
            buff_data: [0.01, 0.02, 0.05],
            effect(r) {
                r.changeBuff({ buff_triatk_per: 0.02 })
            }
        },
        {
            name: "bonus_damage",
            key: ["bonus_damage", "buff_triatk_per"],
            buff_data: [0.01, 0.01, 0.04],
            effect(r) {
                r.changeBuff({ awake_intstr_add: 30 })
            }
        }
    ]
function z3(r, s, a, c = 5, f = !1) {
    let { buff_data: h, key: p, name: m } = r
    const [y, S] = p
    if (y != "awake") {
        const T = f ? 0.04 : 0.02
        a = m.startsWith("awake_") ? 0 : a
        const C = [T, a, a.plus(T.multiply(4))]
        let D = c > 0 ? Fr(C, c) : a
        if ((s.changeDamage({ [y]: D }), h)) {
            const I = c > 0 ? Fr(h, c - 1) : 0
            s.changeBuff({ [S]: I })
        }
    }
    r.effect(s, c)
}
function G3(r, s) {
    let a = []
    return r == "weapon" ? (a.push(...K3), s == "auto" || s == "awake" ? (Ue() ? a.push(Tl[0]) : a.push(...Tl)) : a.push(N3)) : a.push(...U3), a
}
const H3 = [],
    V3 = [],
    Y3 = [],
    Z3 = {}
var X3 = { equips: H3, sirocos: V3, ozmas: Y3, modifiers: Z3 }
const J3 = ci("config", {
    getters: {
        avatars() {
            var a
            const { avatar_columns: r } = Y(),
                s = []
            for (let c in r) {
                const f = r[c]
                s.push({ part: c, name: (a = f.item) == null ? void 0 : a.name, data: Object.fromEntries(f.data.entries()) })
            }
            return s
        },
        equips() {
            var a
            const { equip_columns: r } = Y(),
                s = []
            for (let c in r) {
                const f = r[c]
                s.push({ part: c, name: (a = f.item) == null ? void 0 : a.name, data: Object.fromEntries(f.data.entries()) })
            }
            return s
        },
        sirocos() {
            const { siroco_columns: r } = Y()
            return r.map(s => {
                var a
                return (a = s == null ? void 0 : s.name) != null ? a : ""
            })
        },
        ozmas() {
            const { ozma_columns: r } = Y()
            return r.map(s => {
                var a
                return (a = s == null ? void 0 : s.name) != null ? a : ""
            })
        },
        modifers() {
            var a
            const { equip_columns: r } = Y(),
                s = {}
            for (let c = 0; c < bi.length; c++) {
                const f = bi[c],
                    { data: h } = r[f]
                s[f] = (a = h.get("modifier")) != null ? a : ["none", "none", 0]
            }
            return s
        },
        userConfig() {
            return { avatars: this.avatars, equips: this.equips, sirocos: this.sirocos, ozmas: this.ozmas, modifiers: this.modifers }
        }
    },
    actions: {
        load(r) {
            if (r) {
                let s = X3
                const a = localStorage.getItem(`user-config-${r}`)
                a && (s = Object.assign({}, s, JSON.parse(a)))
                const { avatars: c = [], equips: f = [], sirocos: h = [], ozmas: p = [], modifiers: m = {} } = s
                j3(c), Q3(f), em(h), tm(p), nm(m)
            }
        },
        save(r) {
            r && localStorage.setItem(`user-config-${r}`, JSON.stringify(this.userConfig))
        }
    }
})
function Q3(r) {
    var a, c
    const s = Y()
    for (let f of gl) {
        const h = Tg(f),
            p = r.find(y => y.part == f)
        let m = h[0]
        if (h.length > 0 && p) {
            m = (a = h.find(S => p.name == S.name)) != null ? a : h[0]
            let y = (c = p.data) != null ? c : {}
            for (let S in y) {
                const T = y[S]
                s.setColumnData(f, S, T)
            }
        }
        s.setEquip(f, m)
    }
}
function j3(r) {
    var a, c, f
    const s = Y()
    for (let h of ul) {
        const p = s.avatar_list.filter(S => S.part == h),
            m = r.find(S => S.part == h)
        let y = p[0]
        if (p.length > 0 && m) {
            y = (a = p.find(T => m.name == T.name)) != null ? a : p[0]
            let S = (c = m.data) != null ? c : {}
            for (let T in S) {
                const C = S[T]
                ;(f = s.avatar_columns[h]) == null || f.data.set(T, C)
            }
        }
        s.setAvatar(h, y)
    }
}
function em(r) {
    const s = Y()
    P3.filter(c => r.includes(c.name)).forEach((c, f) => s.setSiroco(f, c))
}
function tm(r) {
    const s = Y()
    r.map(c => F3.find(f => f.name == c)).forEach((c, f) => s.setOzma(f, c))
}
function nm(r) {
    const s = Y()
    for (let a in r) s.setColumnData(a, "modifier", r[a])
}
const El = [],
    Cl = "zero-timeout-message"
window.addEventListener(
    "message",
    r => {
        if (r.source == window && r.data == Cl) {
            r.stopPropagation()
            const s = El.shift()
            s == null || s()
        }
    },
    !0
)
function rm(r) {
    El.push(r), window.postMessage(Cl, "*")
}
function am(r) {
    return new Promise(s => {
        rm(() => {
            const a = r()
            s(a)
        })
    })
}
async function im(r, s, a) {
    r = Array.from(r)
    const c = [],
        f = []
    async function h() {
        const p = r.pop()
        if (!p) return Promise.resolve()
        const m = am(() => a(p))
        c.push(m)
        const y = m.then(() => {
            f.splice(f.indexOf(y), 1)
        })
        return f.push(y), await (f.length >= s ? Promise.race(f) : Promise.resolve()), await h()
    }
    return await h(), await Promise.all(c)
}
const sm = function () {
        var h
        const r = new De(),
            s = [],
            { character: a, avatar_columns: c } = Y()
        for (let p of Object.keys(c)) {
            const m = c[p],
                { item: y, slots: S = [], data: T } = m
            if (!y) continue
            s.push(y)
            let D = y.options
            if (a && (D == null ? void 0 : D.length)) {
                const I = (h = T.get("option_index")) != null ? h : 0
                let L = D[I]
                typeof L == "function" ? L(r) : r.changeStatus(L)
            }
            r.force(y)
            for (let I of S) I.effect(m, r)
        }
        let f = dl(s, _5)
        for (let p of f) r.force(p)
        return [r]
    },
    Bl = [
        {
            name: "accessory_suit_01",
            needCount: 2,
            effect(r) {
                r.changeBuff({ buff_intstr_per: 0.02, passive_alter: 60, awake_intstr_add: 45 }), r.changeDamage({ intstr_per: 0.1, triatk_per: 0.14 })
            }
        },
        {
            name: "accessory_suit_01",
            needCount: 3,
            effect(r) {
                r.changeBuff({ buff_level_add: 1, buff_intstr_per: 0.02, awake_level_add: 2, awake_intstr_per: 0.08 }), r.changeDamage({ critical_damage: 0.2 })
            }
        },
        {
            name: "accessory_suit_02",
            needCount: 2,
            effect(r) {
                r.changeStatus({ elemental_boost: 77, triatk: 77, crirate: 0.07 }), r.changeBuff({ passive_alter: 140, buff_intstr_per: 0.04, awake_intstr_add: 45 })
            }
        },
        {
            name: "accessory_suit_02",
            needCount: 3,
            effect(r) {
                r.changeStatus({ elemental_boost: 77, triatk: 77, crirate: 0.07 }), r.changeBuff({ buff_level_add: 1, awake_level_add: 2, awake_intstr_add: 8 }), r.changeDamage({ skill_attack: 0.31 })
            }
        }
    ]
Bl.forEach(r => (r.parts = ["bracelet", "necklace", "ring"]))
const Il = [
    {
        name: "cloth_suit_01",
        needCount: 2,
        effect(r) {
            r.changeBuff({ buff_level_add: 2, awake_level_add: 1, awake_intstr_add: 135, passive_alter: 100 }), r.changeDamage({ final_damage: 0.14, skill_attack: 0.14 })
        }
    },
    {
        name: "cloth_suit_01",
        needCount: 3,
        effect(r) {
            r.changeBuff({ buff_intstr_per: 0.24, awake_intstr_add: 153, passive_alter: 50 }), r.changeDamage({ additional_damage: 0.22, critical_damage: 0.1 })
        }
    },
    {
        name: "cloth_suit_01",
        needCount: 5,
        effect(r) {
            r.changeBuff({ buff_intstr_per: 0.2, awake_intstr_per: 0.08, passive_alter: 85 }),
                r.changeDamage({ skill_attack: 0.19 }),
                r.changeStatus({ crirate: 0.05 }),
                r.changeSkill({ start: 1, end: 100, value: 2, condition: { ignores: [95] } })
        }
    },
    {
        name: "cloth_suit_02",
        needCount: 2,
        effect(r) {
            r.changeDamage({ critical_damage: 0.17, final_damage: 0.1 }), r.changeBuff({ passive_alter: 105, buff_level_add: 1, buff_intstr_per: 0.12, awake_intstr_add: 135 })
        }
    },
    {
        name: "cloth_suit_02",
        needCount: 3,
        effect(r) {
            r.changeDamage({ triatk_per: 0.11, skill_attack: 0.15 }),
                r.changeStatus({ attack_speed: 0.1, movement_speed: 0.1, casting_speed: 0.15 }),
                r.changeBuff({ passive_alter: 145, buff_level_add: 1, buff_intstr_per: 0.08, awake_intstr_add: 192, awake_intstr_per: 0.05 })
        }
    },
    {
        name: "cloth_suit_02",
        needCount: 5,
        effect(r) {
            r.changeDamage({ skill_attack: 0.4 }),
                r.changeStatus({ crirate: 0.1 }),
                r.changeBuff({ passive_alter: 100, buff_level_add: 1, buff_intstr_per: 0.35, awake_intstr_per: 0.05 }),
                r.is_buffer && r.changeSkill({ start: 30, end: 50, value: 2 })
        }
    }
]
Il.forEach(r => (r.parts = ["coat", "shoulder", "pants", "belt", "shoes"]))
const Rl = [
    {
        name: "special_suit_01",
        needCount: 2,
        effect(r) {
            r.changeBuff({ buff_level_add: 1, buff_intstr_per: 0.04, awake_level_add: 1, awake_intstr_add: 35, passive_alter: 205 }), r.changeDamage({ triatk_per: 0.1, final_damage: 0.08 })
            const s = (a, c) => {
                a.changeDamage({ critical_damage: 0.05 })
            }
            r.once("use_equip", s, ["special_epic_subequip_01", "special_epic_earrings_01"]), r.once("use_equip", s, ["special_epic_subequip_01", "special_mythic_earrings_01"])
        }
    },
    {
        name: "special_suit_01",
        needCount: 3,
        effect(r) {
            r.changeBuff({ buff_level_add: 1, awake_level_add: 2 }), r.changeDamage({ skill_attack: 0.1, intstr_per: 0.1 })
        }
    }
]
Rl.forEach(r => (r.parts = ["subequip", "magicstones", "earrings"]))
const um = [...Il, ...Rl, ...Bl].map(r => {
        var s
        return (r.fame = (s = r.fame) != null ? s : 265), r
    }),
    lm = function (r) {
        var a
        const s = []
        for (let c of r) {
            const f = new De(),
                h = []
            for (let m of Object.keys(c)) {
                const y = c[m],
                    { item: S, slots: T = [] } = y
                if (!S) continue
                if ((h.push(S), f.force(S), S.rarity == "mythic")) {
                    let { mythic_properties: I } = S
                    for (let { deal_data: L, buff_data: Q, effect: F } of I) {
                        let K = Fr(L, 4),
                            ne = Fr(Q, 4)
                        F(f, [K, ne])
                    }
                }
                let C = S.type
                gi.includes(C) && (C = (a = y.data.get("armor_type")) != null ? a : C)
                const D = d5(ft(ot({}, S), { type: C }))
                f.changeStatus(D), f.changeStatus(S.status)
                for (let I of T) I.effect(y, f)
                f.listen("use_equip", S.name)
            }
            let p = dl(h, um)
            for (let m of p) f.force(m)
            s.push(f)
        }
        return s
    },
    cm = function () {
        const r = new De(),
            { siroco_columns: s } = Y()
        for (let a of s) r.force(a)
        return [r]
    },
    om = function () {
        const r = new De(),
            { ozma_columns: s } = Y()
        for (let a of s) r.force(a)
        return [r]
    },
    fm = function () {
        var m
        const r = new De(),
            { pet_column: s } = Y(),
            { item: a, red: c, green: f, blue: h, slots: p } = s
        if (a) {
            r.force(a), c && r.force(c), f && r.force(f), h && r.force(h)
            for (let y of p) (m = y.effect) == null || m.call(y, s, r)
        }
        return [r]
    },
    _m = [
        0, 0, 10, 30, 50, 70, 90, 110, 125, 140, 155, 170, 185, 185, 185, 185, 200, 200, 215, 215, 230, 230, 230, 230, 230, 245, 245, 245, 245, 245, 260, 260, 260, 260, 260, 275, 275, 275, 275, 275,
        290
    ],
    pm = function () {
        const r = new De()
        let { adventurer_level: s, guild_buff: a } = Y().base_setting
        return (s = Math.max(1, Math.min(40, s))), r.changeStatus({ quadra: _m[s] }), a && r.changeStatus({ quadra: 120 }), [r]
    },
    hm = [0.08, 0.08, 0.06, 0.06, 0.06, 0.04, 0.04],
    gm = function () {
        const r = new De()
        let { talismans_columns: s } = Y()
        s = s.slice(0, 3)
        for (let a of s) r.changeBuff({ buff_intstr_per: hm[a] }), r.changeFame(46 * 3 + 185)
        return [r]
    },
    dm = function () {
        var f, h
        const { equip_columns: r } = Y(),
            s = Ue()
        let a = []
        for (let p of bi) {
            const { item: m, data: y } = r[p],
                S = []
            if (!(m == null ? void 0 : m.modifier)) continue
            let T = 0,
                [C, D, I] = (f = y.get("modifier")) != null ? f : ["none", "none", 0]
            if (((T = (h = m.modifier[1]) != null ? h : 0), C == "auto")) I = 4
            else if (C == "none" || D == "none") {
                if (s) continue
                ;(D = m.modifier[0]), (I = 0)
            }
            const L = p == "weapon",
                Q = G3(p, C)
            for (let F of Q) {
                const K = new De()
                F && (z3(F, K, T, I, L), m.name == "weapon_epic_broom_01" && K.changeBuff({ awake_level_add: 2 })), C == "auto" && K.modifiers.push([p, F.name, I]), S.push(K)
            }
            a.push(S)
        }
        return (
            (a = _i(...a)),
            a.map(p => {
                const m = new De()
                for (let y of p) m.apply(y)
                return m
            })
        )
    }
var mm = [pm, sm, lm, gm, dm, cm, om, fm]
const vm = ci("calc", {
    state() {
        return { result: void 0, calculating: !1, funcs: mm }
    },
    getters: {
        fame() {
            var r, s
            return (s = (r = this.result) == null ? void 0 : r.context.fame) != null ? s : 0
        }
    },
    actions: {
        async calculate_self() {
            var a
            let { character: r, equip_columns: s } = Y()
            if (r) {
                s = Wh(s)
                const c = (a = await this.calculate([s])) != null ? a : []
                this.result = c.max(f => f.rate)[1]
            }
        },
        async calculate(...r) {
            const { character: s } = Y()
            if (!s) throw new Error("not found character.")
            if (this.calculating) return []
            console.time("calculating"), (this.calculating = !0), console.time("calculating-funcs")
            let a = this.funcs.map(f => f(...r))
            console.timeEnd("calculating-funcs"), (a = ym(a)), (a = _i(...a))
            const c = await im(a, 20, f => s.compute_buffer(f))
            return (this.calculating = !1), console.timeEnd("calculating"), c
        }
    }
})
function ym(r) {
    const s = []
    let a
    function c() {
        a && (s.push([a]), (a = void 0))
    }
    for (let f of r) f.length == 1 ? (a || (a = new De()), a.apply(f[0])) : f.length > 1 && (c(), s.push(f))
    return c(), s
}
const bm = $h(),
    wm = "modulepreload",
    Dl = {},
    km = "/",
    Wr = function (s, a) {
        return !a || a.length === 0
            ? s()
            : Promise.all(
                  a.map(c => {
                      if (((c = `${km}${c}`), c in Dl)) return
                      Dl[c] = !0
                      const f = c.endsWith(".css"),
                          h = f ? '[rel="stylesheet"]' : ""
                      if (document.querySelector(`link[href="${c}"]${h}`)) return
                      const p = document.createElement("link")
                      if (((p.rel = f ? "stylesheet" : wm), f || ((p.as = "script"), (p.crossOrigin = "")), (p.href = c), document.head.appendChild(p), f))
                          return new Promise((m, y) => {
                              p.addEventListener("load", m), p.addEventListener("error", y)
                          })
                  })
              ).then(() => s())
    }
function Am() {
    var r = navigator.userAgent.toLowerCase()
    return ["ipad", "iphone os", "midp", "rv:1.2.3.4", "ucweb", "android", "windows ce", "windows mobile"].some(a => r.includes(a))
}
const Ll = Nh({
    history: Uh("/"),
    routes: [
        { name: "index", path: "/", redirect: "/pc" },
        { name: "pc", path: "/pc", component: () => Wr(() => import("./home.dc90e187.js"), ["assets/home.dc90e187.js", "assets/home.88cb1ea8.css", "assets/vendor.5859bc74.js"]) },
        {
            name: "pc-character",
            path: "/pc/character/:alter",
            component: () => Wr(() => import("./index.c9f5a509.js"), ["assets/index.c9f5a509.js", "assets/index.e8c7886e.css", "assets/vendor.5859bc74.js"])
        },
        { name: "mobile", path: "/mobile", component: () => Wr(() => import("./home.dc90e187.js"), ["assets/home.dc90e187.js", "assets/home.88cb1ea8.css", "assets/vendor.5859bc74.js"]) },
        {
            name: "mobile-character",
            path: "/mobile/character/:alter",
            component: () => Wr(() => import("./index.c9f5a509.js"), ["assets/index.c9f5a509.js", "assets/index.e8c7886e.css", "assets/vendor.5859bc74.js"])
        }
    ]
})
Ll.beforeEach((r, s, a) => {
    const c = !Am()
    return !c && r.path.startsWith("/pc") ? a({ path: r.path.replace("/pc", "/mobile") }) : c && r.path.startsWith("mobile") ? a({ path: r.path.replace("/mobile", "/pc") }) : a()
})
const Ol = /\[\w+(=\w+)?(,\w+(=\w+)?)*\]/g,
    $r = /{\$?\w+(\.\w+)*(\[\w+(=\w+)?(,\w+(=\w+)?)*\])?}/g
class Sm {
    constructor({ locale: s, data: a }) {
        Z(this, "locale")
        Z(this, "languages")
        Z(this, "current_dictionary")
        ;(this.locale = dt(s != null ? s : "zh-cn")), (this.languages = dt([])), (this.current_dictionary = je(() => this.getDictionary(this.locale.value))), a.forEach(c => this.pushData(c))
    }
    async pushData({ locale: s, dictionaries: a }) {
        ;(a = ql(a)), this.languages.value.push({ locale: s, dictionaries: a })
    }
    getDictionary(s) {
        let a = this.languages.value.find(c => c.locale == s)
        return a || ((a = { locale: s, dictionaries: {} }), this.languages.value.push(a)), a.dictionaries
    }
    setLocale(s) {
        this.locale.value = s
    }
    getKey(s) {
        var f, h
        const a = this.current_dictionary.value
        if (!a || !!a[s]) return s
        const c = Object.keys(a)
        for (let p of c) {
            if (!/{[\w]+}/g.test(p)) continue
            const m = new RegExp(`^${p.replace(/({\w+})/g, "([a-zA-Z0-9-\\s_]+)")}$`)
            if (m.test(s)) {
                const y = (f = p.match(/{(\w+)}/g)) != null ? f : [],
                    S = (h = s.match(m)) != null ? h : []
                let T = {},
                    C = ""
                const D = s.lastIndexOf(".")
                D > 0 && (C = s.slice(0, D))
                for (let L = 0; L < y.length; L++) {
                    let Q = y[L].slice(1, -1)
                    T[Q] = this.getTrans(C.concat(".").concat(S[L + 1]))
                }
                let I = a[p]
                return (I = Pl(I, T)), (a[s] = I), s
            }
        }
    }
    getTrans(s) {
        var c
        if (!s) return ""
        let a = this.current_dictionary.value
        if (a)
            if ($r.test(s))
                s = s.replace($r, f => {
                    f = f.slice(1, -1)
                    const h = {},
                        p = []
                    let m = f.match(Ol),
                        y = f
                    if (m == null ? void 0 : m.length) {
                        y = y.replace(Ol, () => "")
                        for (let S of m) {
                            if (!S) continue
                            const C = S.slice(1, -1).split(",")
                            for (let D of C)
                                if (D.includes("=")) {
                                    const [I, L] = D.split("=")
                                    h[I] = L
                                } else p.push(D)
                        }
                    }
                    return Pl(this.getTrans(y), h, ...p)
                })
            else {
                const f = s.split(".")
                if (f.length > 1) {
                    let h = f.length - 1
                    const p = f[h]
                    for (; h > -1; ) {
                        let m = f.slice(0, h).concat(p).join("."),
                            y = this.getKey(m)
                        if (y) {
                            s = y
                            break
                        }
                        h--
                    }
                }
                s = (c = a[s]) != null ? c : s
            }
        return s
    }
    compile(s, ...a) {
        var f
        let c = Ml(s, ...a)
        return (s = (f = this.getTrans(c)) != null ? f : c), s
    }
    install(s) {
        s.provide(wi, this),
            s.directive("trans", (a, { value: c, arg: f }) => {
                let h = c != null ? c : a.textContent
                a.textContent = this.compile(h, f)
            })
    }
}
const wi = Symbol("[Translator]instance")
function ql(r, s = "") {
    const a = {}
    for (let c in r) {
        let f = r[c]
        const h = s ? s.concat(".").concat(c) : c
        typeof f == "object" ? Object.assign(a, ql(f, h)) : (a[h] = f)
    }
    return a
}
function xm(r) {
    return new Sm(r)
}
function Ml(r, ...s) {
    return (
        (s = s.filter(a => !!a)),
        !!r && $r.test(r)
            ? (r = r.replace($r, a => {
                  var f
                  let c = a.slice(1, -1)
                  return (c = (f = Ml(c, ...s)) != null ? f : ""), `{${c}}`
              }))
            : (r == null ? void 0 : r.startsWith("$"))
            ? (r = r.slice(1))
            : (r = [...s, r].join(".")),
        r
    )
}
function s4(...r) {
    const s = Mt(wi)
    return function (a) {
        var c
        return a ? ((c = s == null ? void 0 : s.compile(a, ...r)) != null ? c : a) : ""
    }
}
function u4() {
    return Mt(wi)
}
function Pl(r, s = {}, ...a) {
    let c = 0
    return (
        (r = r.replace(/{}/g, f => {
            var h
            return (h = a[c++]) != null ? h : f
        })),
        (r = r.replace(/{\d+}/g, f => {
            var p
            const h = parseInt(f.slice(1, -1))
            return (p = a[h]) != null ? p : f
        })),
        (r = r.replace(/{\w+}/g, f => {
            var p
            const h = f.slice(1, -1)
            return (p = s[h]) != null ? p : f
        })),
        r
    )
}
const Tm = "zh-cn",
    Em = {
        accessory: "\u9996\u9970",
        active_skill: "\u4E3B\u52A8\u6280\u80FD",
        all_jobs: "\u6240\u6709\u804C\u4E1A",
        amplify: "\u589E\u5E45",
        reinforce: "\u5F3A\u5316",
        quadra: "\u529B\u91CF\u3001\u667A\u529B\u3001\u4F53\u529B\u3001\u7CBE\u795E",
        vitspi: "\u4F53\u529B\u3001\u7CBE\u795E",
        back: "\u8FD4\u56DE",
        belt: "\u8170\u5E26",
        global_change: "\u5168\u5C40\u4FEE\u6539",
        equips: "\u88C5\u5907",
        bracelet: "\u624B\u956F",
        broom: "\u626B\u628A",
        buff_skill: "buff\u6280\u80FD",
        cloth_armor: "\u5E03\u7532",
        coat: "\u4E0A\u8863",
        earrings: "\u8033\u73AF",
        enchanting: "\u9644\u9B54",
        epic: "\u53F2\u8BD7",
        equip: "\u88C5\u5907",
        "first-page": "\u88C5\u5907/\u9009\u62E9/\u6253\u9020",
        "fourth-page": "\u81EA\u9009\u88C5\u5907\u8BA1\u7B97",
        heavy: "\u91CD\u7532",
        indatk: "\u72EC\u7ACB\u653B\u51FB\u529B",
        intelligence: "\u667A\u529B",
        leather_armor: "\u76AE\u7532",
        level: "\u7B49\u7EA7",
        light_armor: "\u8F7B\u7532",
        magatk: "\u9B54\u6CD5\u653B\u51FB\u529B",
        magicstones: "\u9B54\u6CD5\u77F3",
        mythic: "\u795E\u8BDD",
        legendary: "\u4F20\u8BF4",
        unique: "\u795E\u5668",
        rare: "\u7A00\u6709",
        title: "\u79F0\u53F7",
        necklace: "\u9879\u94FE",
        overview: "\u6982\u89C8",
        pants: "\u4E0B\u88C5",
        part: "\u90E8\u4F4D",
        phyatk: "\u7269\u7406\u653B\u51FB\u529B",
        plate_armor: "\u677F\u7532",
        ring: "\u6212\u6307",
        magcri_rate: "\u9B54\u6CD5\u66B4\u51FB\u7387",
        phycri_rate: "\u7269\u7406\u66B4\u51FB\u7387",
        "second-page": "\u6280\u80FD/\u7B26\u6587/\u836F\u5242",
        shoes: "\u978B",
        shoulder: "\u62A4\u80A9",
        skill: "\u6280\u80FD",
        special: "\u7279\u6B8A\u88C5\u5907",
        spirit: "\u7CBE\u795E",
        strength: "\u529B\u91CF",
        strint: "\u529B\u91CF\u3001\u667A\u529B",
        subequip: "\u8F85\u52A9\u88C5\u5907",
        adventurer_fame: "\u5192\u9669\u5BB6\u540D\u671B",
        suit: "\u5957\u88C5",
        suits: "\u5957\u88C5",
        total: "\u5168\u90E8",
        triatk: "\u7269\u7406\u3001\u9B54\u6CD5\u3001\u72EC\u7ACB\u653B\u51FB\u529B",
        vitality: "\u4F53\u529B",
        weapon: "\u6B66\u5668",
        use: "\u4F7F\u7528",
        avatar: "\u65F6\u88C5",
        cd: "\u51B7\u5374\u65F6\u95F4",
        all_skill: "\u6240\u6709\u6280\u80FD",
        adapt_number: "\u9002\u7528\u6570\u503C",
        avatar_column: "\u65F6\u88C5\u680F",
        profile: "\u4E2A\u4EBA\u4FE1\u606F",
        neck: "\u80F8\u90E8",
        face: "\u8138\u90E8",
        hair: "\u5934\u53D1",
        cap: "\u5934\u90E8",
        suit_effect: "\u5957\u88C5\u6548\u679C",
        look_suit_effect: "\u67E5\u770B\u5957\u88C5\u6548\u679C",
        nex: "\u5948\u514B\u65AF",
        lodos: "\u6D1B\u591A\u65AF",
        assassin: "\u6697\u6740\u8005",
        roxy: "\u5362\u514B\u897F",
        nameless: "\u5B88\u95E8\u4EBA",
        astaroth: "\u963F\u65AF\u7279\u7F57\u65AF",
        berias: "\u8D1D\u5229\u4E9A\u65AF",
        redmayne: "\u96F7\u5FB7\u6885\u6069",
        rosenbach: "\u7F57\u65AF\u5DF4\u8D6B",
        timat: "\u6CF0\u739B\u7279",
        elemental_boost: "\u6240\u6709\u5C5E\u6027\u5F3A\u5316",
        fire_boost: "\u706B\u5C5E\u6027\u5F3A\u5316",
        ice_boost: "\u51B0\u5C5E\u6027\u5F3A\u5316",
        light_boost: "\u5149\u5C5E\u6027\u5F3A\u5316",
        dark_boost: "\u6697\u5C5E\u6027\u5F3A\u5316",
        intstr: "\u529B\u91CF\u3001\u667A\u529B",
        fuse: "\u878D\u5408",
        modifier_property: "\u9074\u9009\u5C5E\u6027",
        mythic_property: "\u795E\u8BDD\u5C5E\u6027",
        final_damage: "\u6700\u7EC8\u4F24\u5BB3",
        skill_attack: "\u6280\u80FD\u653B\u51FB\u529B",
        intstr_per: "\u529B\u91CF/\u667A\u529B",
        triatk_per: "\u7269\u7406/\u9B54\u6CD5/\u72EC\u7ACB\u653B\u51FB\u529B",
        critical_damage: "\u66B4\u51FB\u4F24\u5BB3",
        bonus_damage: "\u9644\u52A0\u4F24\u5BB3",
        bonus_elemental_damage: "\u9644\u52A0\u5C5E\u6027\u4F24\u5BB3",
        additional_damage: "\u589E\u52A0\u4F24\u5BB3",
        awake_level_add: "\u89C9\u9192 Lv+{}",
        awake_intstr_add: "\u89C9\u9192\u529B\u667A+{}",
        awake_intstr_per: "\u89C9\u9192\u529B\u667A+{}%",
        buff_indatk_add: "Buff\u72EC\u7ACB\u653B\u51FB\u529B+{}",
        buff_indatk_per: " Buff\u72EC\u7ACB\u653B\u51FB\u529B+{}%",
        buff_int_add: "Buff\u529B\u91CF+{}",
        buff_int_per: "Buff\u529B\u91CF+{}%",
        buff_level_add: "Buff Lv+{}",
        buff_magatk_add: "Buff\u9B54\u6CD5\u653B\u51FB\u529B+{}",
        buff_magatk_per: "Buff\u9B54\u6CD5\u653B\u51FB\u529B+{}%",
        buff_phyatk_add: "Buff\u7269\u7406\u653B\u51FB\u529B+{}",
        buff_phyatk_per: "Buff\u7269\u7406\u653B\u51FB\u529B+{}%",
        buff_intstr_add: "Buff\u529B\u667A+{}",
        buff_intstr_per: "Buff\u529B\u667A+{}%",
        buff_str_add: "Buff\u667A\u529B+{}",
        buff_str_per: "Buff\u667A\u529B+{}%",
        buff_triatk_add: "Buff\u653B\u51FB\u529B+{}",
        buff_triatk_per: "Buff\u653B\u51FB\u529B+{}%",
        passive_int_alter: "\u667A\u529B+{}",
        passive_vitspi_alter: "\u4F53\u529B/\u7CBE\u795E+{}",
        awake: "\u89C9\u9192",
        auto: "\u81EA\u52A8",
        none: "\u65E0",
        armor: "\u9632\u5177",
        siroco: "\u5E0C\u6D1B\u514B",
        ozma: "\u5965\u5179\u739B",
        fuse_property: "\u878D\u5408\u5C5E\u6027",
        modifier: "\u9074\u9009",
        custom: "\u81EA\u9009",
        property: "\u5C5E\u6027",
        casting_speed: "\u65BD\u653E\u901F\u5EA6",
        attack_speed: "\u653B\u51FB\u901F\u5EA6",
        movement_speed: "\u79FB\u52A8\u901F\u5EA6",
        aura: "\u5149\u73AF",
        skin: "\u76AE\u80A4",
        pet: "\u5BA0\u7269",
        max_value: "\u6700\u5927\u503C",
        min_value: "\u6700\u5C0F\u503C",
        damage: "\u4F24\u5BB3",
        comments: {
            abbr: {
                triatk: "\u4E09\u653B",
                quadra: "\u56DB\u7EF4",
                intstr: "\u529B\u667A",
                vitspi: "\u4F53\u7CBE",
                awake_intstr_add: "\u89C9\u9192\u6280\u80FD\u529B\u667A\u589E\u52A0\u91CF +{}",
                awake_intstr_per: "\u89C9\u9192\u6280\u80FD\u529B\u667A\u589E\u52A0\u91CF +{}%",
                buff_indatk_add: "Buff\u6280\u80FD\u72EC\u7ACB\u589E\u52A0\u91CF +{}",
                buff_indatk_per: "Buff\u6280\u80FD\u72EC\u7ACB\u589E\u52A0\u91CF +{}%",
                buff_int_add: "Buff\u6280\u80FD\u529B\u91CF\u589E\u52A0\u91CF +{}",
                buff_int_per: "Buff\u6280\u80FD\u529B\u91CF\u589E\u52A0\u91CF +{}%",
                buff_level_add: "Buff\u6280\u80FDLv +{}",
                buff_magatk_add: "Buff\u6280\u80FD\u9B54\u653B\u589E\u52A0\u91CF +{}",
                buff_magatk_per: "Buff\u6280\u80FD\u9B54\u653B\u589E\u52A0\u91CF +{}%",
                buff_phyatk_add: "Buff\u6280\u80FD\u7269\u653B\u589E\u52A0\u91CF +{}",
                buff_phyatk_per: "Buff\u6280\u80FD\u7269\u653B\u589E\u52A0\u91CF +{}%",
                buff_intstr_add: "Buff\u6280\u80FD\u529B\u667A\u589E\u52A0\u91CF +{}",
                buff_intstr_per: "Buff\u6280\u80FD\u529B\u667A\u589E\u52A0\u91CF +{}%",
                buff_str_add: "Buff\u6280\u80FD\u667A\u529B\u589E\u52A0\u91CF +{}",
                buff_str_per: "Buff\u6280\u80FD\u667A\u529B\u589E\u52A0\u91CF +{}%",
                buff_triatk_add: "Buff\u6280\u80FD\u4E09\u653B\u589E\u52A0\u91CF +{}",
                buff_triatk_per: "Buff\u6280\u80FD\u4E09\u653B\u589E\u52A0\u91CF +{}%"
            },
            option_value: "\u9009\u62E9\u5C5E\u6027\u503C",
            none_option_value: "\u65E0\u9644\u52A0\u5C5E\u6027",
            blue_socket_column: "\u84DD\u8272\u5FBD\u7AE0\u9576\u5D4C\u680F",
            yellow_socket_column: "\u9EC4\u8272\u5FBD\u7AE0\u9576\u5D4C\u680F",
            green_socket_column: "\u7EFF\u8272\u5FBD\u7AE0\u9576\u5D4C\u680F",
            red_socket_column: "\u7EA2\u8272\u5FBD\u7AE0\u9576\u5D4C\u680F",
            platinum_socket_column: "\u767D\u91D1\u5FBD\u7AE0\u9576\u5D4C\u680F",
            awake_level_add: "Lv50 \u6280\u80FDLv +{}",
            awake_intstr_add: "Lv50 \u6280\u80FD\u529B\u91CF\u3001\u667A\u529B\u589E\u52A0\u91CF +{}",
            awake_intstr_per: "Lv50 \u6280\u80FD\u529B\u91CF\u3001\u667A\u529B\u589E\u52A0\u91CF +{}%",
            buff_indatk_add: "Lv30 Buff\u6280\u80FD\u72EC\u7ACB\u653B\u51FB\u529B\u589E\u52A0\u91CF +{}",
            buff_indatk_per: "Lv30 Buff\u6280\u80FD\u72EC\u7ACB\u653B\u51FB\u529B\u589E\u52A0\u91CF +{}%",
            buff_int_add: "Lv30 Buff\u6280\u80FD\u529B\u91CF\u589E\u52A0\u91CF +{}",
            buff_int_per: "Lv30 Buff\u6280\u80FD\u529B\u91CF\u589E\u52A0\u91CF +{}%",
            buff_level_add: "Lv30 Buff\u6280\u80FDLv +{}",
            buff_magatk_add: "Lv30 Buff\u6280\u80FD\u9B54\u6CD5\u653B\u51FB\u529B\u589E\u52A0\u91CF +{}",
            buff_magatk_per: "Lv30 Buff\u6280\u80FD\u9B54\u6CD5\u653B\u51FB\u529B\u589E\u52A0\u91CF +{}%",
            buff_phyatk_add: "Lv30 Buff\u6280\u80FD\u7269\u7406\u653B\u51FB\u529B\u589E\u52A0\u91CF +{}",
            buff_phyatk_per: "Lv30 Buff\u6280\u80FD\u7269\u7406\u653B\u51FB\u529B\u589E\u52A0\u91CF +{}%",
            buff_intstr_add: "Lv30 Buff\u6280\u80FD\u529B\u91CF\u3001\u667A\u529B\u589E\u52A0\u91CF +{}",
            buff_intstr_per: "Lv30 Buff\u6280\u80FD\u529B\u91CF\u3001\u667A\u529B\u589E\u52A0\u91CF +{}%",
            buff_str_add: "Lv30 Buff\u6280\u80FD\u667A\u529B\u589E\u52A0\u91CF +{}",
            buff_str_per: "Lv30 Buff\u6280\u80FD\u667A\u529B\u589E\u52A0\u91CF +{}%",
            buff_triatk_add: "Lv30 Buff\u6280\u80FD\u7269\u7406\u3001\u9B54\u6CD5\u3001\u72EC\u7ACB\u653B\u51FB\u529B\u589E\u52A0\u91CF +{}",
            buff_triatk_per: "Lv30 Buff\u6280\u80FD\u7269\u7406\u3001\u9B54\u6CD5\u3001\u72EC\u7ACB\u653B\u51FB\u529B\u589E\u52A0\u91CF +{}%",
            passive_int_alter: "[\u542F\u793A\xB7\u5723\u6B4C][\u4EBA\u5076\u64CD\u7EB5\u8005]\u667A\u529B +{}",
            passive_int_awake: "[\u8654\u8BDA\u4FE1\u5FF5][\u5C11\u5973\u7684\u7231]\u667A\u529B +{}",
            passive_vitspi_alter: "[\u5B88\u62A4\u6069\u8D50]\u4F53\u529B\u3001\u7CBE\u795E +{}",
            passive_vitspi_awake: "[\u4FE1\u5FF5\u5149\u73AF]\u4F53\u529B\u3001\u7CBE\u795E +{}",
            final_damage: "\u6700\u7EC8\u4F24\u5BB3 +{}%",
            skill_attack: "\u6280\u80FD\u653B\u51FB\u529B +{}%",
            intstr_per: "\u529B\u91CF\u3001\u667A\u529B +{}%",
            triatk_per: "\u7269\u7406\u3001\u9B54\u6CD5\u3001\u72EC\u7ACB\u653B\u51FB\u529B +{}%",
            critical_damage: "\u66B4\u51FB\u65F6\uFF0C\u989D\u5916\u589E\u52A0{}%\u7684\u4F24\u5BB3\u589E\u52A0\u91CF\u3002",
            bonus_damage: "\u653B\u51FB\u65F6\uFF0C\u9644\u52A0{}%\u7684\u4F24\u5BB3\u3002",
            bonus_elemental_damage: "\u653B\u51FB\u65F6\uFF0C\u9644\u52A0{}%\u7684\u5C5E\u6027\u4F24\u5BB3\u3002",
            additional_damage: "\u653B\u51FB\u65F6\uFF0C\u989D\u5916\u589E\u52A0{}%\u7684\u4F24\u5BB3\u589E\u52A0\u91CF\u3002"
        },
        items: {
            red: "\u7EA2\u8272",
            yellow: "\u9EC4\u8272",
            blue: "\u84DD\u8272",
            green: "\u7EFF\u8272",
            dual: "\u53CC\u91CD",
            redgreen: "\u53CC\u91CD",
            unique: "\u707F\u70C2\u7684",
            rare: "\u534E\u4E3D\u7684",
            legendary: "\u73B2\u73D1",
            intelligence_magcri_rate: "\u667A\u529B+\u9B54\u6CD5\u66B4\u51FB",
            strength_phycri_rate: "\u529B\u91CF+\u7269\u7406\u66B4\u51FB",
            "emblem_{rarity}_dual_{type}": "{rarity}\u53CC\u91CD\u5FBD\u7AE0[{type}]",
            "emblem_{rarity}_{color}_{type}": "{rarity}{color}\u5FBD\u7AE0[{type}]",
            accessory_epic_bracelet_01: "\u83B1\u591A\uFF1A\u53D8\u5E7B\u7684\u89C4\u5F8B",
            accessory_epic_bracelet_02: "\u767D\u8C61\u4E4B\u5E87\u62A4",
            accessory_epic_necklace_01: "\u80AF\u90A3\u5179\uFF1A\u7CBE\u795E\u71CE\u539F\u4E4B\u706B",
            accessory_epic_necklace_02: "\u56DB\u53F6\u8349\u4E4B\u521D\u5FC3",
            accessory_epic_ring_01: "\u76D6\u67CF\uFF1A\u5B8C\u7F8E\u7684\u5747\u8861",
            accessory_epic_ring_02: "\u7EA2\u5154\u4E4B\u795D\u798F",
            accessory_mythic_bracelet_01: "\u83B1\u591A\uFF1A\u79E9\u5E8F\u521B\u9020\u8005",
            accessory_mythic_bracelet_02: "\u4F3D\u5185\u4EC0\u7684\u6C38\u6052\u5E87\u62A4",
            accessory_suit_01: "\u4E0A\u53E4\u5C18\u5C01\u672F\u5F0F",
            accessory_suit_02: "\u5E78\u8FD0\u4E09\u89D2",
            cloth_epic_belt_01: "\u9B54\u6CD5\u5E08[???]\u7684\u8170\u5E26",
            cloth_epic_belt_02: "\u70ED\u60C5\u821E\u52A8\u6851\u5DF4",
            cloth_epic_coat_01: "\u9B54\u6CD5\u5E08[???]\u7684\u957F\u888D",
            cloth_epic_coat_02: "\u4F18\u96C5\u65CB\u5F8B\u534E\u5C14\u5179",
            cloth_epic_pants_01: "\u9B54\u6CD5\u5E08[???]\u7684\u62A4\u817F",
            cloth_epic_pants_02: "\u9B45\u60D1\u5F8B\u52A8\u4F26\u5DF4",
            cloth_epic_shoes_01: "\u9B54\u6CD5\u5E08[???]\u7684\u957F\u9774",
            cloth_epic_shoes_02: "\u6FC0\u70C8\u6B22\u52A8\u8E22\u8E0F",
            cloth_epic_shoulder_01: "\u9B54\u6CD5\u5E08[???]\u7684\u62AB\u98CE",
            cloth_epic_shoulder_02: "\u6027\u611F\u6D12\u8131\u63A2\u6208",
            cloth_mythic_coat_01: "\u5927\u9B54\u6CD5\u5E08[???]\u7684\u957F\u888D",
            cloth_mythic_coat_02: "\u6D6A\u6F2B\u65CB\u5F8B\u534E\u5C14\u5179",
            cloth_suit_01: "\u9057\u5FD8\u9B54\u6CD5\u5E08\u7684\u9988\u8D60",
            cloth_suit_02: "\u5929\u5802\u821E\u59EC",
            ditto: "\u767E\u53D8\u602A",
            special_epic_earrings_01: "\u519B\u795E\u7684\u53E4\u602A\u8033\u73AF",
            special_epic_magicstones_01: "\u519B\u795E\u7684\u5E87\u62A4\u5B9D\u77F3",
            special_epic_subequip_01: "\u519B\u795E\u7684\u9057\u4E66",
            special_mythic_earrings_01: "\u519B\u795E\u7684\u5FC3\u4E4B\u6240\u5FF5",
            special_suit_01: "\u519B\u795E\u7684\u9690\u79D8\u9057\u4EA7",
            weapon_epic_broom_01: "\u4E16\u754C\u6811\u4E4B\u7CBE\u7075",
            title_2021_spring_supremacy: "\u7A7F\u8D8A\u661F\u7A7A\u7684\u7948\u613F",
            title_2021_labour_supremacy: "\u4E5D\u9704\u9A91\u58EB\u4E4B\u8A93",
            unique_avatar: "\u795E\u5668\u88C5\u626E",
            holiday_avatar: "\u8282\u65E5\u88C5\u626E",
            rare_avatar: "\u7A00\u6709\u88C5\u626E",
            rare_avatar_clone_weapon: "\u7A00\u6709\u514B\u9686\u6B66\u5668\u88C5\u626E"
        },
        jobs: {
            agent: "\u82CD\u66AE\xB7\u7279\u5DE5",
            asura: "\u6781\u8BE3\xB7\u963F\u4FEE\u7F57",
            avenger: "\u795E\u542F\xB7\u590D\u4EC7\u8005",
            battle_mage: "\u77E5\u6E90\xB7\u6218\u6597\u6CD5\u5E08",
            berserker: "\u6781\u8BE3\xB7\u72C2\u6218\u58EB",
            blade: "\u6781\u8BE3\xB7\u5203\u5F71",
            blood_mage: "\u77E5\u6E90\xB7\u8840\u6CD5\u5E08",
            chaos: "\u7693\u66E6\xB7\u6DF7\u6C8C\u9B54\u7075",
            creator: "\u77E5\u6E90\xB7\u7F14\u9020\u8005",
            crusader_female: "\u795E\u542F\xB7\u5723\u9A91\u58EB",
            crusader_male: "\u795E\u542F\xB7\u5723\u9A91\u58EB",
            dark_knight: "\u6781\u8BE3\xB7\u9ED1\u6697\u6B66\u58EB",
            dark_lancer: "\u5343\u9B42\xB7\u6697\u67AA\u58EB",
            dark_templar: "\u6781\u8BE3\xB7\u6697\u6BBF\u9A91\u58EB",
            demon_slayer: "\u6781\u8BE3\xB7\u5951\u9B54\u8005",
            demonic_lancer: "\u9B54\u67AA\u58EB",
            dimension_walker: "\u77E5\u6E90\xB7\u6B21\u5143\u884C\u8005",
            dragon_knight: "\u7693\u66E6\xB7\u9F99\u9A91\u58EB",
            dragonian_lacner: "\u5343\u9B42\xB7\u72E9\u730E\u8005",
            duelist: "\u5343\u9B42\xB7\u51B3\u6218\u8005",
            elemental_bomber: "\u77E5\u6E90\xB7\u5143\u7D20\u7206\u7834\u5E08",
            elven_knight: "\u7693\u66E6\xB7\u7CBE\u7075\u9A91\u58EB",
            enchantress: "\u77E5\u6E90\xB7\u5C0F\u9B54\u5973",
            exorcist: "\u795E\u542F\xB7\u9A71\u9B54\u5E08",
            extra: "\u5916\u4F20",
            fighter_female: "\u683C\u6597\u5BB6(\u5973)",
            fighter_male: "\u683C\u6597\u5BB6(\u7537)",
            glacial_master: "\u77E5\u6E90\xB7\u51B0\u7ED3\u5E08",
            grappler_female: "\u5F52\u5143\xB7\u67D4\u9053\u5BB6",
            grappler_male: "\u5F52\u5143\xB7\u67D4\u9053\u5BB6",
            gunblader: "\u67AA\u5251\u58EB",
            gunner_female: "\u795E\u67AA\u624B(\u5973)",
            gunner_male: "\u795E\u67AA\u624B(\u7537)",
            hitman: "\u82CD\u66AE\xB7\u6697\u5203",
            infighter: "\u795E\u542F\xB7\u84DD\u62F3\u5723\u4F7F",
            inquistor: "\u795E\u542F\xB7\u5F02\u7AEF\u5BA1\u5224\u8005",
            knight: "\u5B88\u62A4\u8005",
            kunoichi: "\u9690\u591C\xB7\u5FCD\u8005",
            launcher_female: "\u91CD\u9704\xB7\u67AA\u70AE\u5E08",
            launcher_male: "\u91CD\u9704\xB7\u67AA\u70AE\u5E08",
            mage_female: "\u9B54\u6CD5\u5E08(\u5973)",
            mage_male: "\u9B54\u6CD5\u5E08(\u7537)",
            magiciant: "\u77E5\u6E90\xB7\u5143\u7D20\u5E08",
            mechanic_female: "\u91CD\u9704\xB7\u673A\u68B0\u5E08",
            mechanic_male: "\u91CD\u9704\xB7\u673A\u68B0\u5E08",
            mistress: "\u795E\u542F\xB7\u8BF1\u9B54\u8005",
            necro: "\u9690\u591C\xB7\u6B7B\u7075\u672F\u58EB",
            nenmaster_female: "\u5F52\u5143\xB7\u6C14\u529F\u5E08",
            nenmaster_male: "\u5F52\u5143\xB7\u6C14\u529F\u5E08",
            paladin: "\u7693\u66E6\xB7\u5E15\u62C9\u4E01",
            priest_female: "\u5723\u804C\u8005(\u7537)",
            priest_male: "\u5723\u804C\u8005(\u7537)",
            ranger_female: "\u91CD\u9704\xB7\u6F2B\u6E38\u67AA\u624B",
            ranger_male: "\u91CD\u9704\xB7\u6F2B\u6E38\u67AA\u624B",
            rogue: "\u9690\u591C\xB7\u523A\u5BA2",
            shadow_dancer: "\u9690\u591C\xB7\u5F71\u821E\u8005",
            sorceress: "\u795E\u542F\xB7\u5DEB\u5973",
            soul_bringer: "\u6781\u8BE3\xB7\u9B3C\u6CE3",
            specialist: "\u82CD\u66AE\xB7\u6E90\u80FD\u4E13\u5BB6",
            spitfire_female: "\u91CD\u9704\xB7\u5F39\u836F\u4E13\u5BB6",
            spitfire_male: "\u91CD\u9704\xB7\u5F39\u836F\u4E13\u5BB6",
            street_fighter_female: "\u5F52\u5143\xB7\u8857\u9738",
            street_fighter_male: "\u5F52\u5143\xB7\u8857\u9738",
            striker_female: "\u5F52\u5143\xB7\u6563\u6253",
            striker_male: "\u5F52\u5143\xB7\u6563\u6253",
            summoner: "\u77E5\u6E90\xB7\u53EC\u5524\u5E08",
            swift_master: "\u77E5\u6E90\xB7\u9010\u98CE\u8005",
            sword_ghost: "\u6781\u8BE3\xB7\u5251\u5F71",
            sword_master: "\u6781\u8BE3\xB7\u9A6D\u5251\u58EB",
            swordman_female: "\u9B3C\u5251\u58EB(\u5973)",
            swordman_male: "\u9B3C\u5251\u58EB(\u7537)",
            thief: "\u6697\u591C\u4F7F\u8005",
            trouble_shooter: "\u82CD\u66AE\xB7\u6218\u7EBF\u4F63\u5175",
            vanguard: "\u5343\u9B42\xB7\u5F81\u6218\u8005",
            vegabond: "\u6781\u8BE3\xB7\u6D41\u6D6A\u6B66\u58EB",
            weapon_master: "\u6781\u8BE3\xB7\u5251\u9B42",
            witch: "\u77E5\u6E90\xB7\u9B54\u9053\u5B66\u8005"
        },
        skills: {
            mage_female: {
                keep_away: "\u522B\u8FC7\u6765!",
                enchantress: {
                    puppeteer: "\u4EBA\u5076\u64CD\u7EB5\u8005",
                    wicked_curiosity: "\u90AA\u6076\u7684\u597D\u5947\u5FC3",
                    marionette: "\u5F00\u5E55!\u4EBA\u5076\u5267\u573A",
                    curtain_call: "\u7EC8\u5E55!\u4EBA\u5076\u5267\u573A",
                    forbidden_curse: "\u7981\u5FCC\u8BC5\u5492",
                    destiny_puppet: "\u6B7B\u547D\u53EC\u5524",
                    favoritism: "\u5C0F\u9B54\u5973\u7684\u504F\u7231",
                    petite_diablo: "\u5C11\u5973\u7684\u7231",
                    dark_rose: "\u51A5\u6708\u7EFD\u653E",
                    ominously_smiling: "\u4E0D\u8BE6\u7684\u5FAE\u7B11",
                    forest_of_dolls: "\u6B22\u8FCE\u6765\u5230\u4EBA\u5076\u4E4B\u68EE",
                    big_madd: "\u53D8\u5927\u5427!\u75AF\u75AF\u718A",
                    hex: "\u4EBA\u5076\u620F\u6CD5",
                    madd_slash: "\u75AF\u72C2\u4E71\u6293",
                    madd_straight: "\u75AF\u718A\u706B\u7BAD\u62F3",
                    madd_guard: "\u75AF\u718A\u5B88\u62A4",
                    jumping_bear_press: "\u75AF\u75AF\u718A\u5760\u51FB",
                    terrible_roar: "\u54C7\u5494\u5494!",
                    curse_of_terror_bear: "\u5486\u54EE\u5427!\u75AF\u75AF\u718A",
                    hot_affection: "\u706B\u70ED\u7684\u7231\u610F",
                    rose_vines: "\u73AB\u7470\u85E4\u8513",
                    harvest: "\u8537\u8587\u85E4\u97AD",
                    bramble_prison: "\u8537\u8587\u56DA\u72F1",
                    black_forest_brier: "\u6797\u4E2D\u5C0F\u5C4B",
                    garden_of_pain: "\u82E6\u75DB\u5EAD\u9662"
                }
            }
        }
    }
var Cm = { locale: Tm, dictionaries: Em },
    Bm = xm({ locale: "zh-cn", data: [Cm] })
var Im = Pt({
    name: "calc-button",
    props: { disabled: { type: Boolean, default: !1 }, small: { type: Boolean, default: !1 }, to: { type: String, default: null } },
    setup(r, { slots: s }) {
        return () => oi(r.to ? el : "button", { to: r.to, class: { "i-button": !0, disabled: r.disabled, small: r.small } }, Mn(s, "default"))
    }
})
const Fl = Symbol("[i-tabs]active"),
    Wl = Symbol("[i-tabs]active-class")
var Rm = Pt({
        name: "calc-tabs",
        props: { modelValue: { type: [Object, Number, String, Array] }, vertical: { type: Boolean }, activeClass: { type: String } },
        setup(r, { emit: s, slots: a }) {
            const c = je({ set: f => s("update:modelValue", f), get: () => r.modelValue })
            return (
                fn(Fl, c),
                fn(
                    Wl,
                    je(() => r.activeClass)
                ),
                () => pe("ul", { class: { "i-tabs": !0, vertical: !!r.vertical } }, [Mn(a, "default")])
            )
        }
    }),
    Dm = Pt({
        name: "calc-tab",
        props: ["value", "width", "to"],
        setup(r, s) {
            const a = Mt(Fl),
                c = je(() => r.value == (a == null ? void 0 : a.value)),
                f = Kh(),
                h = Mt(Wl)
            return () => {
                var y, S, T, C
                const m = { style: { width: `${(y = r.width) != null ? y : 120}px` }, class: { "i-tab": !0, [(S = h == null ? void 0 : h.value) != null ? S : "active"]: c.value } }
                return r.to
                    ? oi(el, ft(ot({}, m), { to: r.to }), (T = f.default) == null ? void 0 : T.call(f))
                    : oi(
                          "li",
                          ft(ot({}, m), {
                              onClick() {
                                  a && (a.value = r.value)
                              }
                          }),
                          (C = f.default) == null ? void 0 : C.call(f)
                      )
            }
        }
    })
const $l = Symbol("[i-selection]active"),
    Nl = Symbol("[i-selection]init"),
    Ul = Symbol("[i-selection]model-value"),
    Kl = Symbol("[i-selection]active-class"),
    Lm = {
        modelValue: {
            type: [String, Number, Object, Boolean],
            default() {
                return null
            }
        },
        defaultValue: {
            type: [String, Number, Object, Boolean],
            default() {
                return null
            }
        },
        activeClass: { type: String }
    }
function Om(r, s) {
    const a = dt(),
        c = je(() => r.modelValue),
        f = je({
            set(p) {
                s.emit("update:modelValue", p == null ? void 0 : p.value), (a.value = p)
            },
            get() {
                var p, m
                return (m = (p = h.find(y => y.value.value == r.modelValue)) == null ? void 0 : p.value) != null ? m : a.value
            }
        })
    fn(Ul, c),
        fn($l, f),
        fn(
            Kl,
            je(() => r.activeClass)
        )
    const h = []
    return (
        fn(
            Nl,
            p => (
                h.push(p),
                (p.value.value == r.modelValue || f.value == null) && (a.value = p.value),
                () => {
                    h.splice(h.indexOf(p), 1)
                }
            )
        ),
        { active: f, modelValue: c }
    )
}
var qm = Pt({
    name: "i-select",
    props: ft(ot({}, Lm), { disabled: { type: Boolean, default: !1 }, width: { type: Number, default: 120 }, emptyLabel: { type: String } }),
    setup(r, s) {
        const { active: a } = Om(r, s),
            c = dt(!1),
            f = dt()
        fi(c, y)
        function h() {
            c.value = !c.value && !S.value
        }
        const p = dt({ x: 0, y: 0, w: 0 }),
            m = je(() => ({ left: `${p.value.x}px`, top: `${p.value.y}px`, width: `${p.value.w}px` }))
        function y() {
            if (f.value) {
                const { width: T, height: C, left: D, top: I } = f.value.getBoundingClientRect()
                p.value = { w: T, x: D, y: I + C + 4 }
            }
        }
        const S = je(() => !!r.disabled)
        return (
            zh(f, () => (c.value = !1)),
            window.addEventListener("resize", y),
            window.addEventListener("scroll", y),
            tl(() => {
                window.removeEventListener("resize", y), window.removeEventListener("scroll", y)
            }),
            () => {
                function T() {
                    var D, I
                    return pe("div", { class: { "i-select-trigger": !0, disabled: r.disabled }, ref: f }, [
                        pe("span", { class: "i-select-label", textContent: (I = (D = a.value) == null ? void 0 : D.key) != null ? I : r.emptyLabel }, null),
                        pe("span", { class: "i-select-down-icon" }, null)
                    ])
                }
                const C = () => {
                    const { slots: D } = s
                    return pe(Gh, { to: "body" }, { default: () => [nl(pe("div", { class: "i-select-dropdown", style: m.value }, [Mn(D, "default")]), [[Hh, c.value]])] })
                }
                return pe("div", { class: "i-select", onClick: h }, [T(), C()])
            }
        )
    }
})
const Mm = { value: { type: [Object, String, Number, Boolean] }, label: { type: [Function, String] } }
function Pm(r) {
    var m
    const s = Mt($l),
        a = Mt(Nl),
        c = Mt(Ul),
        f = je(() => {
            var y
            return r.value == ((y = s == null ? void 0 : s.value) == null ? void 0 : y.value)
        }),
        h = je(() => {
            var S
            let y = r.label
            return typeof y == "function" && (y = (S = y(r.value)) != null ? S : r.value), { key: y, value: r.value }
        })
    if (a) {
        const y = a(h)
        tl(y)
    }
    fi(h, (y, S) => {
        s && S.value == c.value && (s.value = y)
    })
    const p = (m = Mt(Kl)) != null ? m : dt("active")
    return { active: s, isActive: f, current: h, activeClass: p }
}
var Fm = Pt({
    name: "i-option",
    props: Mm,
    setup(r) {
        const { active: s, activeClass: a, isActive: c, current: f } = Pm(r)
        function h() {
            s.value = f.value
        }
        return () => {
            const p = ["i-option"]
            return c.value && p.push(a.value), pe("span", { class: p, onClick: h, textContent: f.value.key }, null)
        }
    }
})
var Wm = Pt({
    name: "calc-menu",
    props: ["label"],
    setup(r, { slots: s }) {
        const a = dt(!1)
        function c() {
            ;(a.value = !a.value), console.log("open")
        }
        return () =>
            pe("div", { class: "i-menu" }, [
                pe("div", { class: "i-menu-content" }, [
                    pe("div", { onClick: c, class: "i-menu-expand-icon" }, [
                        pe("svg", { viewBox: "0 0 1024 1024", width: "100%", height: "100%" }, [
                            pe(
                                "path",
                                {
                                    d: "M85.312 85.312v853.376h853.376V85.312H85.312zM0 0h1024v1024H0V0z m554.624 213.312v256h256v85.376h-256v256H469.312v-256h-256V469.376h256v-256h85.312z",
                                    fill: "currentColor"
                                },
                                null
                            )
                        ])
                    ]),
                    nl(pe("div", { class: "i-menu-label" }, null), [[Vh("trans"), r.label]])
                ]),
                pe("div", { class: { "i-menu-items": !0, expand: a.value } }, [Mn(s, "default")])
            ])
    }
})
var $m = Pt({
        name: "i-checkbox",
        props: { checked: { type: Boolean }, label: { type: String } },
        setup(r, { emit: s, slots: a }) {
            const c = dt(!!r.checked)
            function f() {
                ;(c.value = !c.value), s("update:checked", c.value)
            }
            return () =>
                pe("div", { class: "i-checkbox", onClick: f }, [
                    pe("span", { class: { "i-checkbox-icon": !0, checked: c.value } }, null),
                    pe("span", { class: "i-checkbox-label", textContent: r.label }, null)
                ])
        }
    }),
    Nm = Pt({
        name: "i-collapse",
        props: { title: { type: String }, modelValue: { type: Boolean } },
        setup(r, { slots: s, emit: a }) {
            const c = dt(r.modelValue)
            fi(
                () => r.modelValue,
                p => (c.value = p)
            )
            const f = je({
                set(p) {
                    a("update:modelValue", p), (c.value = p)
                },
                get() {
                    return !!c.value
                }
            })
            function h() {
                f.value = !f.value
            }
            return () =>
                pe("div", { class: "my-1 overflow-hidden h-auto rounded bg-hex-#00000078 border-1 border-hex-#ffffff28" }, [
                    pe("div", { onClick: h, textContent: r.title, class: "bg-gradient-to-b to-hex-#122438 from-hex-#244281 w-full h-6 text-xs text-center items-center justify-center flex" }, null),
                    pe("div", { class: ["transition-all", "ease-in-out", "h-auto"].concat(f.value ? ["max-h-200"] : ["max-h-0"]) }, [pe("div", { class: "p-2" }, [Mn(s, "default")])])
                ])
        }
    })
const Um = [Im, Rm, Dm, qm, Fm, Wm, $m, Nm]
var Km = {
    install(r) {
        Um.forEach(s => {
            r.component(s.name.replace("i-", "calc-"), s)
        })
    }
}
Number.prototype.plus = function (r = 0) {
    return this.valueOf() + r
}
Number.prototype.minus = function (r = 0) {
    return this.valueOf() - r
}
Number.prototype.multiply = function (r = 1) {
    return this.valueOf() * r
}
Number.prototype.divide = function (r = 1) {
    return this.valueOf() / r
}
Number.prototype.perMulti = function (r = 0) {
    return this.multiply(r).plus(this.valueOf()).plus(r)
}
Number.prototype.exactlyDivide = function (r = 1) {
    return Math.floor(this.valueOf() / r)
}
Number.prototype.round = function (r = 0) {
    return Number(this.valueOf().toFixed(r))
}
Number.prototype.floor = function (r = 0) {
    return Math.floor(this.valueOf())
}
Yh(Zh).use(bm).use(Ll).use(Km).use(Bm).mount("#app")
export {
    i4 as A,
    P3 as B,
    De as C,
    tt as D,
    F3 as E,
    a4 as F,
    bi as G,
    Ue as H,
    W3 as I,
    J3 as J,
    z3 as K,
    rl as L,
    e4 as M,
    Im as N,
    Xm as O,
    xg as P,
    Ym as Q,
    Jm as R,
    V as S,
    Qm as T,
    Rm as U,
    Dm as V,
    u4 as W,
    Qh as a,
    vm as b,
    G3 as c,
    Y as d,
    Pr as e,
    n4 as f,
    Fr as g,
    t4 as h,
    jm as i,
    Zm as j,
    Eg as k,
    gl as l,
    Vm as m,
    _l as n,
    ul as o,
    dl as p,
    pl as q,
    Mr as r,
    um as s,
    Hm as t,
    s4 as u,
    _5 as v,
    r4 as w,
    pn as x,
    gi as y,
    d5 as z
}
